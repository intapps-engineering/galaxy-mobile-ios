//
//  DBManager.swift
//  Tbstar
//
//  Created by Apple on 14/04/21.
//

import Foundation
import SQLite3

class DatabaseManager: NSObject {
    
    func copyDatabaseIfNeeded()
    {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = NSURL(fileURLWithPath: path)
        
        let filePath = url.appendingPathComponent("TbStarr.sqlite")?.path
        
        let fileManager = FileManager.default
        let databaseURl = URL(fileURLWithPath: filePath!)
        let fileExist = fileManager.fileExists(atPath: filePath!)
        
        if (fileExist)
        {
            print("FILE AVAILABLE")
            print("File Path :: \(String(describing: filePath))")
            return
        }
        
        print("FILE NOT AVAILABLE")
        let documentsURL = Bundle.main.resourceURL?.appendingPathComponent("TbStarr.sqlite")
        do {
            try fileManager.copyItem(at: documentsURL!, to: databaseURl as URL)
            print("Copied File Path :: \(databaseURl)")
            
        } catch let error as NSError {
            print("Couldn't copy file to final location! Error:\(error.description)")
        }
    }
    
    func GetDatabasePath() -> String
    {
        //return databasePath
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = NSURL(fileURLWithPath: path)
        
        let filePath = url.appendingPathComponent("TbStarr.sqlite")?.path
        
        return filePath!
    }
    
    func PPMV_Patient_Details_Table_Create() -> Bool {
        
        var database : OpaquePointer? = nil
        let databasePAth = GetDatabasePath()
        
        let createTableString = "CREATE TABLE IF NOT EXISTS PPMV_Patient_Details(PatientID INTEGER PRIMARY KEY,latitude TEXT,longitude TEXT ,middleName TEXT,lga TEXT, siteOfDisease TEXT,visitDate TEXT,dateOfBirth TEXT,phoneNumber TEXT,age TEXT,hivStatus TEXT,typeOfPresumptiveTbCase TEXT,secondPhoneNumber TEXT,patientWorker TEXT,sex TEXT,state TEXT,address TEXT,lastName TEXT,hadTBbefore TEXT,firstName TEXT,CoughingUpBlood TEXT,Swelling TEXT,FailureToThrive TEXT,FeverFor3weeksOrMore TEXT,CoughFor2WeeksOrMore TEXT,NightSweats TEXT,UnexplainedWeightLoss TEXT,covidDiarrhea TEXT,covidSoreThroat TEXT,covidShaking TEXT,covidFever TEXT,covidRunnyNose TEXT,covidCough TEXT,covidLossOfTaste TEXT,createdDate TEXT,clientHasNoTB INTEGER,referToCLinic INTEGER,typesOfSpecimen TEXT,dateOfCollection TEXT,reasonOfExamination TEXT,previousTreatmentForTb TEXT,typeOfTest TEXT,numberOfSample TEXT,temperature TEXT,nameOfCollectOffcer TEXT,nameOfCollectOffcerPhNo TEXT,isSendingSampleToLabAfterInitialCollection INTEGER,laboratoryName TEXT,labId TEXT,dateOfDispatch TEXT);"
        
        if sqlite3_open(databasePAth, &database) == SQLITE_OK {
            
            if sqlite3_prepare_v2(database, createTableString, -1, &database, nil) == SQLITE_OK
            {
                if sqlite3_step(database) == SQLITE_DONE
                {
                    print("PPMV_Patient_Details table created.")
                    return true
                } else {
                    print("PPMV_Patient_Details table could not be created.")
                }
            } else {
                print("CREATE TABLE statement could not be prepared.")
            }
            sqlite3_finalize(database)
        }
        return false
    }

    
    func ExecuteQuery(query : String) -> Bool
    {
        var database : OpaquePointer? = nil
        let databasePAth = GetDatabasePath()
        var ans = false
        
        if sqlite3_open(databasePAth, &database) == SQLITE_OK {
            var insertStatement : OpaquePointer? = nil
            
            if sqlite3_prepare_v2(database, query, -1, &insertStatement, nil) == SQLITE_OK {
                if sqlite3_step(insertStatement) == SQLITE_DONE {
                    ans = true
                }
                else{
                    ans = false
                    NSLog("Error while binding variable. '%s'", sqlite3_errmsg(database));
                }
            }
            else {
                ans = false
                NSLog("Error while creating update statement. '%s'", sqlite3_errmsg(database));
            }
            sqlite3_finalize(insertStatement)
        }
        else {
            ans = false
        }
        sqlite3_close(database)
        return ans
    }
    
    func fetchPatientInfoData(query:String) -> [[String:Any]]
    {
        var database : OpaquePointer? = nil
        // let databasePAth = DatabaseManager().GetDatabasePath()
        let databasePAth = GetDatabasePath()
        
        var arrData = [[String : Any]]()
        if sqlite3_open(databasePAth, &database) == SQLITE_OK
        {
            var selectStatement : OpaquePointer? = nil
            if sqlite3_prepare_v2(database, query, -1, &selectStatement, nil) == SQLITE_OK
            {
                while sqlite3_step(selectStatement) == SQLITE_ROW
                {
                    let patientOfflineID = sqlite3_column_int(selectStatement, 0)
                    let latitude = String(cString: sqlite3_column_text(selectStatement, 1))
                    let longitude = String(cString: sqlite3_column_text(selectStatement, 2))
                    
                    var tbDict = [String : String]()
                    tbDict["CoughingUpBlood"] = String(cString: sqlite3_column_text(selectStatement, 20))
                    tbDict["Swelling"] = String(cString: sqlite3_column_text(selectStatement, 21))
                    tbDict["FailureToThrive"] = String(cString: sqlite3_column_text(selectStatement, 22))
                    tbDict["FeverFor3weeksOrMore"] = String(cString: sqlite3_column_text(selectStatement, 23))
                    tbDict["CoughFor2WeeksOrMore"] = String(cString: sqlite3_column_text(selectStatement, 24))
                    tbDict["NightSweats"] = String(cString: sqlite3_column_text(selectStatement, 25))
                    tbDict["UnexplainedWeightLoss"] = String(cString: sqlite3_column_text(selectStatement, 26))
                    
                    var covidDict = [String : String]()
                    covidDict["covidDiarrhea"] = String(cString: sqlite3_column_text(selectStatement, 27))
                    covidDict["covidSoreThroat"] = String(cString: sqlite3_column_text(selectStatement, 28))
                    covidDict["covidShaking"] = String(cString: sqlite3_column_text(selectStatement, 29))
                    covidDict["covidFever"] = String(cString: sqlite3_column_text(selectStatement, 30))
                    covidDict["covidRunnyNose"] = String(cString: sqlite3_column_text(selectStatement, 31))
                    covidDict["covidCough"] = String(cString: sqlite3_column_text(selectStatement, 32))
                    covidDict["covidLossOfTaste"] = String(cString: sqlite3_column_text(selectStatement, 33))
                    
                    var patientdatadict = [String  : Any]()
                    patientdatadict["middleName"] = String(cString: sqlite3_column_text(selectStatement, 3))
                    patientdatadict["lga"] = String(cString: sqlite3_column_text(selectStatement, 4))
                    patientdatadict["siteOfDisease"] = String(cString: sqlite3_column_text(selectStatement, 5))
                    patientdatadict["visitDate"] = String(cString: sqlite3_column_text(selectStatement, 6))
                    patientdatadict["dateOfBirth"] = String(cString: sqlite3_column_text(selectStatement, 7))
                    patientdatadict["phoneNumber"] = String(cString: sqlite3_column_text(selectStatement, 8))
                    patientdatadict["age"] = String(cString: sqlite3_column_text(selectStatement, 9))
                    
                    let age = String(cString: sqlite3_column_text(selectStatement, 9))
                    patientdatadict["hivStatus"] = String(cString: sqlite3_column_text(selectStatement, 10))
                    patientdatadict["typeOfPresumptiveTbCase"] = String(cString: sqlite3_column_text(selectStatement, 11))
                    patientdatadict["secondPhoneNumber"] = String(cString: sqlite3_column_text(selectStatement, 12))
                    patientdatadict["patientWorker"] = String(cString: sqlite3_column_text(selectStatement, 13))
                    patientdatadict["sex"] = String(cString: sqlite3_column_text(selectStatement, 14))
                    
                    let sex = String(cString: sqlite3_column_text(selectStatement, 14))
                    patientdatadict["state"] = String(cString: sqlite3_column_text(selectStatement, 15))
                    patientdatadict["address"] = String(cString: sqlite3_column_text(selectStatement, 16))
                    patientdatadict["lastName"] = String(cString: sqlite3_column_text(selectStatement, 17))
                    patientdatadict["hadTBbefore"] = String(cString: sqlite3_column_text(selectStatement, 18))
                    patientdatadict["firstName"] = String(cString: sqlite3_column_text(selectStatement, 19))
                    
                    let createDate = String(cString: sqlite3_column_text(selectStatement, 34))
                    //answerDict.values.contains("yes")
                    if tbDict.values.contains("yes") {
                        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : tbDict,
                                                                                 "covidSymptoms": covidDict.values.contains("") ? [:] : covidDict,
                                                                                 "clientHasNoTB" : "",
                                                                                 "clientHasTB" : ["patientInformation" : patientdatadict],
                                                                                 "latitude" : latitude,
                                                                                 "longitude" : longitude],
                                                             "patientOfflineID": patientOfflineID,
                                                             "createDate": createDate]
                        
                        
                        arrData.append(parametrDict)
                    }else {
                        
                        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : tbDict,
                                                                                 "covidSymptoms": covidDict.values.contains("") ? [:] : covidDict,
                                                                                 "clientHasNoTB" : ["patientInformation" : ["age" : age,"sex" : sex]],
                                                                                 "latitude" : latitude,
                                                                                 "longitude" : longitude],
                                                             "patientOfflineID": patientOfflineID,
                                                             "createDate": createDate]
                        arrData.append(parametrDict)
                    }
                    
                }
            }
            sqlite3_finalize(selectStatement)
        }
         sqlite3_close(database)
        return arrData
    }
}
