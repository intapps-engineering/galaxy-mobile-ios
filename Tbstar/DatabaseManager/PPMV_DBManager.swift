//
//  PPMV_DBManager.swift
//  Tbstar
//
//  Created by user1 on 18/06/21.
//

import Foundation
import SQLite3

struct  SaveDataInPPMV_Sqlite_Data {
    
    var latitude : String?
    var longitude : String?
    var middleName : String?
    var lga : String?
    var siteOfDisease : String?
    var visitDate : String?
    var dateOfBirth : String?
    var phoneNumber : String?
    var age : String?
    var hivStatus : String?
    var typeOfPresumptiveTbCase : String?
    var secondPhoneNumber : String?
    var patientWorker : String?
    var sex : String?
    var state : String?
    var address : String?
    var lastName : String?
    var hadTBbefore : String?
    var firstName : String?
    var CoughingUpBlood : String?
    var Swelling : String?
    var FailureToThrive : String?
    var FeverFor3weeksOrMore : String?
    var CoughFor2WeeksOrMore : String?
    var NightSweats : String?
    var UnexplainedWeightLoss : String?
    var covidDiarrhea : String?
    var covidSoreThroat : String?
    var covidShaking : String?
    var covidFever : String?
    var covidRunnyNose : String?
    var covidCough : String?
    var covidLossOfTaste : String?
    var createdDate : String?
    var clientHasNoTB : String?
    var referToCLinic = 0
    var typesOfSpecimen : String?
    var dateOfCollection : String?
    var reasonOfExamination : String?
    var previousTreatmentForTb : String?
    var typeOfTest : String?
    var numberOfSample : String?
    var temperature : String?
    var nameOfCollectOffcer : String?
    var nameOfCollectOffcerPhNo : String?
    var isSendingSampleToLabAfterInitialCollection = 0
    var laboratoryName : String?
    var labId : String?
    var dateOfDispatch : String?
}

class PPMV_DBManager: NSObject {
    
    func SaveDataInPPMV_Sqlite(data : SaveDataInPPMV_Sqlite_Data) -> Bool {
     
        var strQuerry = ""
        var ans = false
            
            strQuerry = "insert or replace into PPMV_Patient_Details(latitude,longitude,middleName,lga,siteOfDisease,visitDate,dateOfBirth,phoneNumber,age,hivStatus,typeOfPresumptiveTbCase,secondPhoneNumber,patientWorker,sex,state,address,lastName,hadTBbefore,firstName,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea,covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste,typesOfSpecimen,dateOfCollection,reasonOfExamination,previousTreatmentForTb,typeOfTest,numberOfSample,temperature,nameOfCollectOffcer,nameOfCollectOffcerPhNo,laboratoryName,labId,dateOfDispatch,createdDate,clientHasNoTB,referToCLinic,isSendingSampleToLabAfterInitialCollection) values (\(data.latitude ?? ""),\(data.longitude  ?? ""),\(data.middleName ?? ""),\(data.lga ?? ""),\(data.siteOfDisease ?? ""),\(data.visitDate ?? ""),\(data.dateOfBirth ?? ""),\(data.phoneNumber ?? ""),\(data.age ?? ""),\(data.hivStatus ?? ""),\(data.typeOfPresumptiveTbCase ?? ""),\(data.secondPhoneNumber ?? ""),\(data.patientWorker ?? ""),\(data.sex ?? ""),\(data.state ?? ""),\(data.address ?? ""),\(data.lastName ?? ""),\(data.hadTBbefore ?? ""),\(data.firstName ?? ""),\(data.CoughingUpBlood ?? ""),\(data.Swelling ?? ""),\(data.FailureToThrive ?? ""),\(data.FeverFor3weeksOrMore ?? ""),\(data.CoughFor2WeeksOrMore ?? ""),\(data.NightSweats ?? ""),\(data.UnexplainedWeightLoss ?? ""),\(data.covidDiarrhea ?? ""),\(data.covidSoreThroat ?? ""),\(data.covidShaking ?? ""),\(data.covidFever ?? ""),\(data.covidRunnyNose ?? ""),\(data.covidCough ?? ""),\(data.covidLossOfTaste ?? ""),\(data.typesOfSpecimen ?? ""),\(data.dateOfCollection ?? ""),\(data.reasonOfExamination ?? ""),\(data.previousTreatmentForTb ?? ""),\(data.typeOfTest ?? ""),\(data.numberOfSample ?? ""),\(data.temperature ?? ""),\(data.nameOfCollectOffcer ?? ""),\(data.nameOfCollectOffcerPhNo ?? ""),\(data.laboratoryName ?? ""),\(data.labId ?? ""),\(data.dateOfDispatch ?? ""),\(data.createdDate ?? ""),\(data.clientHasNoTB ?? ""),\(data.referToCLinic),\(data.isSendingSampleToLabAfterInitialCollection))"

            print(strQuerry)
            ans = DatabaseManager().ExecuteQuery(query: strQuerry)
            return ans
    }
    
    
    func fetchPPMV_PatientInfoData(query:String) -> [[String:Any]]
    {
        var database : OpaquePointer? = nil
        // let databasePAth = DatabaseManager().GetDatabasePath()
        let databasePAth = DatabaseManager().GetDatabasePath()
        
        var arrData = [[String : Any]]()
        if sqlite3_open(databasePAth, &database) == SQLITE_OK
        {
            var selectStatement : OpaquePointer? = nil
            if sqlite3_prepare_v2(database, query, -1, &selectStatement, nil) == SQLITE_OK
            {
                while sqlite3_step(selectStatement) == SQLITE_ROW
                {
                    let patientOfflineID = sqlite3_column_int(selectStatement, 0)
                    let latitude = String(cString: sqlite3_column_text(selectStatement, 1))
                    let longitude = String(cString: sqlite3_column_text(selectStatement, 2))
                    let patientHasNoTB = String(cString: sqlite3_column_text(selectStatement, 35))
                    
                    var tbDict = [String : String]()
                    tbDict["CoughingUpBlood"] = String(cString: sqlite3_column_text(selectStatement, 20))
                    tbDict["Swelling"] = String(cString: sqlite3_column_text(selectStatement, 21))
                    tbDict["FailureToThrive"] = String(cString: sqlite3_column_text(selectStatement, 22))
                    tbDict["FeverFor3weeksOrMore"] = String(cString: sqlite3_column_text(selectStatement, 23))
                    tbDict["CoughFor2WeeksOrMore"] = String(cString: sqlite3_column_text(selectStatement, 24))
                    tbDict["NightSweats"] = String(cString: sqlite3_column_text(selectStatement, 25))
                    tbDict["UnexplainedWeightLoss"] = String(cString: sqlite3_column_text(selectStatement, 26))
                    
                    var covidDict = [String : String]()
                    covidDict["covidDiarrhea"] = String(cString: sqlite3_column_text(selectStatement, 27))
                    covidDict["covidSoreThroat"] = String(cString: sqlite3_column_text(selectStatement, 28))
                    covidDict["covidShaking"] = String(cString: sqlite3_column_text(selectStatement, 29))
                    covidDict["covidFever"] = String(cString: sqlite3_column_text(selectStatement, 30))
                    covidDict["covidRunnyNose"] = String(cString: sqlite3_column_text(selectStatement, 31))
                    covidDict["covidCough"] = String(cString: sqlite3_column_text(selectStatement, 32))
                    covidDict["covidLossOfTaste"] = String(cString: sqlite3_column_text(selectStatement, 33))
                    
                    var patientdatadict = [String  : Any]()
                    patientdatadict["middleName"] = String(cString: sqlite3_column_text(selectStatement, 3))
                    patientdatadict["lga"] = String(cString: sqlite3_column_text(selectStatement, 4))
                    patientdatadict["siteOfDisease"] = String(cString: sqlite3_column_text(selectStatement, 5))
                    patientdatadict["visitDate"] = String(cString: sqlite3_column_text(selectStatement, 6))
                    patientdatadict["dateOfBirth"] = String(cString: sqlite3_column_text(selectStatement, 7))
                    patientdatadict["phoneNumber"] = String(cString: sqlite3_column_text(selectStatement, 8))
                    patientdatadict["age"] = String(cString: sqlite3_column_text(selectStatement, 9))
                    let age = String(cString: sqlite3_column_text(selectStatement, 9))
                    patientdatadict["typeOfPresumptiveTbCase"] = String(cString: sqlite3_column_text(selectStatement, 11))
                    patientdatadict["secondPhoneNumber"] = String(cString: sqlite3_column_text(selectStatement, 12))
                    patientdatadict["patientWorker"] = String(cString: sqlite3_column_text(selectStatement, 13))
                    patientdatadict["sex"] = String(cString: sqlite3_column_text(selectStatement, 14))
                    
                    let sex = String(cString: sqlite3_column_text(selectStatement, 14))
                    patientdatadict["state"] = String(cString: sqlite3_column_text(selectStatement, 15))
                    patientdatadict["address"] = String(cString: sqlite3_column_text(selectStatement, 16))
                    patientdatadict["lastName"] = String(cString: sqlite3_column_text(selectStatement, 17))
                    patientdatadict["hadTBbefore"] = String(cString: sqlite3_column_text(selectStatement, 18))
                    patientdatadict["firstName"] = String(cString: sqlite3_column_text(selectStatement, 19))
                    
                    let createDate = String(cString: sqlite3_column_text(selectStatement, 34))
                                        
                    let dictSpecimen : [String: Any] = [
                        "hivStatus": String(cString: sqlite3_column_text(selectStatement, 10)) ,
                        "referToCLinic" : sqlite3_column_int(selectStatement, 36) ,
                        "typesOfSpecimen" : String(cString: sqlite3_column_text(selectStatement, 37)),
                        "dateOfCollection" : String(cString: sqlite3_column_text(selectStatement, 38)),
                        "reasonOfExamination" : String(cString: sqlite3_column_text(selectStatement, 39)),
                        "previousTreatmentForTb" : String(cString: sqlite3_column_text(selectStatement, 40)),
                        "typeOfTest" : String(cString: sqlite3_column_text(selectStatement, 41)),
                        "numberOfSample" : String(cString: sqlite3_column_text(selectStatement, 42)),
                        "temperature" : String(cString: sqlite3_column_text(selectStatement, 43)),
                        "nameOfCollectOffcer" : String(cString: sqlite3_column_text(selectStatement, 44)),
                        "nameOfCollectOffcerPhNo" : String(cString: sqlite3_column_text(selectStatement, 45)),
                        "laboratoryName" : String(cString: sqlite3_column_text(selectStatement, 47)),
                        "labId" : String(cString: sqlite3_column_text(selectStatement, 48)),
                        "dateOfDispatch" : String(cString: sqlite3_column_text(selectStatement, 49)),
                        "isSendingSampleToLabAfterInitialCollection" : sqlite3_column_int(selectStatement, 46),
                    ]
                    
                    var arrSpecime = [[String: Any]]()
                    arrSpecime.append(dictSpecimen)

                    if tbDict.values.contains("yes") {
                        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : tbDict,
                                                                                 "covidSymptoms": covidDict.values.contains("") ? [:] : covidDict,
                                                                                 "clientHasNoTB" : "",
                                                                                 "clientHasTB" : ["specimenDetails" : arrSpecime ,"patientInformation" : patientdatadict],
                                                                                 "latitude" : latitude,
                                                                                 "longitude" : longitude],
                                                             "patientOfflineID": patientOfflineID,
                                                             "createDate": createDate]
                        
                        
                        arrData.append(parametrDict)
                        
                    } else {
                        
                        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : tbDict,
                                                                                 "covidSymptoms": covidDict.values.contains("") ? [:] : covidDict,
                                                                                 "clientHasNoTB" : ["patientInformation" : ["age" : age,"sex" : sex]],
                                                                                 "latitude" : latitude,
                                                                                 "longitude" : longitude],
                                                             "patientOfflineID": patientOfflineID,
                                                             "createDate": createDate]
                        arrData.append(parametrDict)
                    }
                }
            }
            sqlite3_finalize(selectStatement)
        }
         sqlite3_close(database)
        return arrData
    }
}
