//
//  Utility.swift
//  Tbstar
//
//  Created by Viprak-Heena on 16/02/21.
//

import Foundation
import UIKit

class Utility: NSObject {
    
    //SideMenu
    class func openSideMenu() {
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        SideMenuManager.default.menuLeftNavigationController = storyboard.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 ? 320 : 280;
    }
    class func generatRandomEncKey() -> String {
        
        let min = 4;
        let max = 12;
        var temcpChar: Character?
        var encKeyStr = ""
        let randomNumber = Int.random(in: 0 ..< (max - min + 1)) + min
        
        for _ in 0..<randomNumber{
            //temcpChar = (Character) (Int.random(in: 0 ..< 96) + 32)
            if let myUnicodeScalar = UnicodeScalar((Int.random(in: 0 ..< 96) + 32)){
                 temcpChar = Character(myUnicodeScalar)
                encKeyStr.append(temcpChar ?? "x")
            }
        }
        print("Random enc Key : \(encKeyStr)")
        return encKeyStr
    }
    class func convertDateformat(date:String, currentFormate:String,requiredFormate:String) -> (String,Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = currentFormate
        var strDate = ""
        var tdate = Date()
        if let tempDate = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = requiredFormate
            tdate = tempDate
            strDate = dateFormatter.string(from: tempDate)
        }
        return (strDate,tdate)
    }


    
}
