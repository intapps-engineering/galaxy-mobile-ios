//
//  Network.swift
//  Tbstar
//
//  Created by Viprak-Heena on 01/02/21.
//

import Foundation
import Alamofire
import CryptoSwift

///live Url

//let BaseUrl = "https://tbstarr.org/api/" //staging -- > "http://188.166.115.143:8001/"

let BaseUrl = "http://188.166.115.143:8001/"
let BaseUrlForSharePatientDetails = "http://188.166.115.143:8282/"
let NO_ENDPOINT_BaseURL = "http://188.166.115.143:8282/no-api-0.0.1/api/v1/"

let independentUserDetailUrl = BaseUrl + "patient/deatilsForindependentUser"
let facilityListLGAUrl = BaseUrl + "facilityListByLGA"
let accountVerificationUrl = BaseUrl + "accountVerifaction"
let signUpUrl = BaseUrl + "signUp1"
let loginUrl = BaseUrl + "logIn1"
let resetPswdUrl = BaseUrl + "resetPassword"
let logoutUrl = BaseUrl + "logOut"
let forgetPswdTokenUrl = BaseUrl + "forgetPassword"
let updateAPKVersionUrl = BaseUrl + "clinic/updateApkVersion"
let screenIndependentUserPatientUrl = BaseUrl + "patient/screeningIndependentUserPatient"
let fetchLGAListUrl = BaseUrl + "admin/lgas"
let fetchDotListUrl = BaseUrl + "dotsCentersByLGA"
let reportTBAfterCollectDetailsUrl = BaseUrl + "patient/reportTbAfterCollectDetails"
let findRecentCaseUrl = BaseUrl + "patient/findCase"
let updateCovidStatusOfPatientUrl = BaseUrl + "patient/independent/covid"
let feedbackSubjectUrl = BaseUrl + "admin/feedbackSubjects"
let dashboardUrl = BaseUrl + "patient/dashBoardApi"
let editProfileUrl = BaseUrl + "editProfile"
let changePswdUrl = BaseUrl + "authResetPassword"
let sentFeedbackUrl = BaseUrl + "admin/userFeedback"

let patientOnTreatmentUrl = BaseUrl + "patient/patientsOnTreatment"
let treatmentCompletedUrl = BaseUrl + "patient/treatmentCompleted"
let patientScreenedUrl = BaseUrl + "patient/patientsScreened"
let presumptivePatientUrl = BaseUrl + "patient/presumptivepatients"
let confirmedPatientUrl = BaseUrl + "patient/confirmedTBcases"

let screenPatientOfflineUrl = BaseUrl + "patient/screeningIndependentUserPatientOffline"
let reportTBAfterCollectingDetailsOfflineUrl = BaseUrl + "patient/reportTbAfterCollectDetailsOffline"

let treatmentOutcomesUrl = BaseUrl + "admin/treatmentOutcomes"
let notificationListUrl = BaseUrl + "AllPushNotifications"

let findIndependentUser = BaseUrlForSharePatientDetails + "no-api-0.0.1/api/v1/ip"
let findSharePatientList = BaseUrlForSharePatientDetails + "no-api-0.0.1/api/v1/ip/share-patient-record"


let APIKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0YkFwcEBpbnRAMTIzIiwibmFtZSI6InBhc3MiLCJpYXQiOjE1MTYyMzkwMjJ9.CwnApT_ogvtZRF9Mwkzox2ieAh61rw08Xdv7LgTKLHw"

func getDetailsForIndependentUser(completionHandler : @escaping(Bool,String) -> Void)  {
    
    let headers = ["loggedInOrNotType" : "0",
                   "apikey" : APIKey,
                   "int" : encKeyForGetAnd,
                   "accept" : "application/json"]
    
    guard let apiurl = URL(string: independentUserDetailUrl) else {
        completionHandler(false,"")
        return
    }
    var request =  URLRequest(url: apiurl)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    AF.request(request).responseJSON { (response) in
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
}
func getListOfFacilitiesFromLGAAPI(paradict : [String:Any], completionHandler : @escaping(Bool,String) -> Void){
    
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    var strdata = ""
    var isSuccess = false
    guard let apiurl = URL(string: facilityListLGAUrl) else {
        completionHandler(isSuccess,strdata)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any] {
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(false,strdata)
            break
        }
    }
}

func verifyAccountAPI(paradict : [String:Any], completionHandler : @escaping(Bool,String,String) -> Void){
    
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: accountVerificationUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func signUpAPI(paradict :[String:Any],completionHandler : @escaping(Bool,[String:Any],String) -> Void)
{
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = [String:Any]()
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: signUpUrl) else {
        completionHandler(false,[:],"")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                strdata = json
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                strdata = json
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func resetPasswordAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void)
{
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: resetPswdUrl) else {
        completionHandler(false,"","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func loginAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String,String) -> Void)
{
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    var accessToken = ""
    var encKey = encKeyForGetAnd
    
    guard let apiurl = URL(string: loginUrl) else {
        completionHandler(false,"","",accessToken)
        return
    }
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
            }
            completionHandler(isSuccess,strdata,message,accessToken)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message,accessToken)
            break
        }
    }
}
func logOutAPI(paradict :[String:Any],accessToken : String,completionHandler : @escaping(Bool,String,String) -> Void)
{
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = ["loggedInOrNotType" : "1",
                                 "apikey" : APIKey,
                                 "int" : encKey,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: logoutUrl) else {
        completionHandler(false,"","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func forgotPswdTokenAPI(paradict : [String:Any], completionHandler : @escaping(Bool,String,String) -> Void){
    
    
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: forgetPswdTokenUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func updateAppVersionAPI(paradict :[String:Any],accessToken : String,completionHandler : @escaping(Bool,Int,String) -> Void)
{
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var resCode = 0
    var msg = ""
    
    guard let apiurl = URL(string: updateAPKVersionUrl) else {
        completionHandler(false,resCode,msg)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                resCode = json["resCode"] as? Int ?? 0
                msg = json["message"] as? String ?? ""
            }
            completionHandler(isSuccess,resCode,msg)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any] {
                resCode = json["resCode"] as? Int ?? 0
                msg = json["message"] as? String ?? ""
            }
            completionHandler(isSuccess,resCode,msg)
            break
        }
    }
}
func screenIndependentUserPatientAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: screenIndependentUserPatientUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func fetchLGAListAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: fetchLGAListUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func fetchDotCenterListAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: fetchDotListUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func reportTBAfterCollectDetailsAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: reportTBAfterCollectDetailsUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func reportTBAfterCollectDetailsOfflineAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: reportTBAfterCollectingDetailsOfflineUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func findRecentCasesAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: findRecentCaseUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}
func updateCovidSymptomsOfPatientAPI(apiURL : String, paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: apiURL) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
    
}
func getFeedbackSubject(completionHandler : @escaping(Bool,String) -> Void) {
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    
    let headers = ["token" : accessToken,
                   "apikey" : APIKey,
                   "int" : encKey,
                   "accept" : "application/json"]
    
    guard let apiurl = URL(string: feedbackSubjectUrl) else {
        completionHandler(false,"")
        return
    }
    var request =  URLRequest(url: apiurl)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    AF.request(request).responseJSON { (response) in
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let token = JsonData["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
}
func getAllNotificationListAPI(completionHandler : @escaping(Bool,String) -> Void) {
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    
    let headers = ["token" : accessToken,
                   "apikey" : APIKey,
                   "int" : encKey,
                   "accept" : "application/json"]
    
    guard let apiurl = URL(string: notificationListUrl) else {
        completionHandler(false,"")
        return
    }
    var request =  URLRequest(url: apiurl)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    AF.request(request).responseJSON { (response) in
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let token = JsonData["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
}

func getDashboardListScreenAPI(completionHandler : @escaping(Bool,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    
    let headers = ["token" : accessToken,
                   "apikey" : APIKey,
                   "int" : encKey,
                   "accept" : "application/json"]
    
    guard let apiurl = URL(string: dashboardUrl) else {
        completionHandler(false,"")
        return
    }
    var request =  URLRequest(url: apiurl)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    
    AF.request(request).responseJSON { (response) in
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
    
}
func editProfileAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: editProfileUrl) else {
        completionHandler(isSuccess,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,message)
            break
        }
    }
    
}
func changePasswordAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: changePswdUrl) else {
        completionHandler(isSuccess,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,message)
            break
        }
    }
    
}
func sendFeedbackAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: sentFeedbackUrl) else {
        completionHandler(isSuccess,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,message)
            break
        }
    }
    
}
func getPatientTreatedList(paradict :[String:Any], apiUrl : String,  completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var message = ""
    var strdata = ""
    
    guard let apiurl = URL(string: apiUrl) else {
        completionHandler(isSuccess,message,strdata)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,message,strdata)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,message,strdata)
            break
        }
    }
    
}
func screenIndependentUserPatientOfflineAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: screenPatientOfflineUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func getTreatmentOutcomesAPI(completionHandler : @escaping(Bool,String) -> Void)  {
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    guard let apiurl = URL(string: treatmentOutcomesUrl) else {
        completionHandler(false,"")
        return
    }
    var request =  URLRequest(url: apiurl)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    AF.request(request).responseJSON { (response) in
        print(response.result)
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
}

func GetUserFromPhoneNumber(paradict :[String:String],completionHandler : @escaping(SharePatientDetails_To_User) -> Void, failure:@escaping ((_ error: Error?) -> Void)) {
    
    let headers: HTTPHeaders = [:]
    
    AF.request(findIndependentUser,method: .get , parameters: paradict, headers: headers).responseJSON { response in
        do
        {
            print(response)
            let jsonDictionary:NSDictionary;
            jsonDictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            DispatchQueue.main.async {
                let keyExists = jsonDictionary["independentUsers"] != nil
                if keyExists {
                    var arrIndependentUser : NSArray = NSArray()
                    arrIndependentUser = jsonDictionary.value(forKey: "independentUsers") as! NSArray
                    
                    if arrIndependentUser.count > 0 {
                        
                        var dictIndependentUser : NSDictionary = NSDictionary()
                        dictIndependentUser = arrIndependentUser.object(at: 0) as! NSDictionary
                        
                        let dict = SharePatientDetails_To_User(facilityName: dictIndependentUser.value(forKey: "facilityName") as? String, facilityType: dictIndependentUser.value(forKey: "facilityType") as? String, firstName: dictIndependentUser.value(forKey: "firstName") as? String, id: dictIndependentUser.value(forKey: "id") as? String, lastName: dictIndependentUser.value(forKey: "lastName") as? String, lgaId: dictIndependentUser.value(forKey: "lgaId") as? String, lgaName: dictIndependentUser.value(forKey: "lgaName") as? String, phoneNumber: dictIndependentUser.value(forKey: "phoneNumber") as? String, stateId: dictIndependentUser.value(forKey: "stateId") as? String, stateName: dictIndependentUser.value(forKey: "stateName") as? String, userType: dictIndependentUser.value(forKey: "userType") as? String)
                        completionHandler(dict)
                    }
                }
            }
        }
        catch
        {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                print(error)
                failure(error)
            }
        }
        
    }
}

func GetSharePatintList(paradict :[String:Any],completionHandler : @escaping([PatientInformation]) -> Void, failure:@escaping ((_ error: Error?) -> Void)) {
    
    var arrSharePatientDetails = [PatientInformation]()
    let headers: HTTPHeaders = [:]
    
    AF.request(findSharePatientList,method: .get, parameters: paradict, headers: headers).responseJSON { response in
        do
        {
            print(response)
            let jsonDictionary:NSDictionary;
            jsonDictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            DispatchQueue.main.async {
                
                let keyExists = jsonDictionary["sharedIpPatients"] != nil
                if keyExists {
                    var arrShareUser : NSArray = NSArray()
                    arrShareUser = jsonDictionary.value(forKey: "sharedIpPatients") as! NSArray
                    
                    for i in 0..<arrShareUser.count {
                        var dictIndependentUser : NSDictionary = NSDictionary()
                        dictIndependentUser = arrShareUser.object(at: i) as! NSDictionary
                        
                        let dict = PatientInformation(dict: dictIndependentUser as! [String : Any])
                        arrSharePatientDetails.append(dict)
                    }
                    completionHandler(arrSharePatientDetails)
                }
            }
        }
        catch
        {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                print(error)
                failure(error)
            }
        }
        
    }
}

func SharepatientDetails(paradict :[String:String],completionHandler : @escaping(Bool,NSDictionary) -> Void, failure:@escaping ((_ error: Error?) -> Void)) {
    
    AF.request(findSharePatientList,method: .post ,parameters: paradict, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
        do
        {
            var jsonDictionary : NSDictionary = NSDictionary()
            jsonDictionary = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            DispatchQueue.main.async {
                completionHandler(true,jsonDictionary)
            }
        }
        catch
        {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                print(error)
                failure(error)
            }
        }
        
    }
}

func encryptParadict(parametrDic:[String:Any],encKEy : String) -> [String:Any] {
    
    do {
        let decodedkey = Array(encKEy.utf8).sha256()
        let aes = try AES(key: decodedkey, blockMode: CBC(iv: ivEnc), padding: .pkcs5)
        if let jsonString = convertDictionaryToString(dict: parametrDic){
            let encData = try aes.encrypt(Array(jsonString.utf8))
            return ["EncryptionData" : encData.toHexString()]
        }
    }
    catch {
        print(error)
    }
    return [:]
}

func convertDictionaryToString(dict : [String:Any]) -> String?{
    
    var Json : String?
    if let theJSONData = try? JSONSerialization.data(
        withJSONObject: dict,
        options: []) {
        
        let theJSONText = String(data: theJSONData,encoding: .utf8)
        Json = theJSONText
    }
    return Json
    
}
