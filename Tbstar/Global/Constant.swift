//
//  Constant.swift
//  Tbstar
//
//  Created by Apple on 27/01/21.
//

import Foundation
import UIKit
import DeviceCheck

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height

var Defaultss = UserDefaults.standard
let appName = "TB STAR"
let encKeyForGet = "1234564y56yw45tw45tyw4twqtgxsdfr"
let encKeyForGetAnd = "1234564y56yw45tw45tyw4twqtg"

let IVForEnc = "0000000000000000"
let platform = "ios"

let ivEnc:[UInt8] = Array(IVForEnc.utf8)
let decodedStatickey = Array(encKeyForGetAnd.utf8).sha256()
var currentMenu = MenuOption.Home
var currentLang = AppLanguage.English

let quicksandRegularFont = "Quicksand-Regular"

let APIDateFormate = "yyyy-MM-dd"
let displayDateFormat = "MM/dd/yyyy"
let apiDateFormatFromAPI = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let NewapiDateFormatFromAPI = "yyyy-MM-dd'T'HH:mm:ssZ"

let SharePatient = "SharePatient"
let RecentPatient = "RecentPatient"

let IndependentSelected = "IndependentSelected"
let NetworkProviderSelected = "NetworkProviderSelected"
let NetworkOfficerSelected = "NetworkOfficerSelected"

let USER_TYPE_INDEPENDENT = "independent"
let USER_TYPE_PPMV = "ppmv"
let SPECIFY_USERS = "SPECIFY_USERS"

let independentSupportUrl = "https://tbstarr.org/API/uploads/TB%20STARR_Independent%20&%20Network%20Providers%20Manual_V1.pdf"
//let deviceId = UIDevice.current.identifierForVendor!.uuidString
//let deviceToken = generateToken()

struct color {
    static let underlineActiveColor = "#00306f"
    static let underlineInactiveColor = "#979797"
    static let appMainBlueColor = "#00306f"
    static let appLightBlueColor = "#859CBA"
    static let appRedColor = "AB133F"
}
struct appMessages {
    
    static let validMobileNo = "validNo".localized
    static let noConnection = "chckConnection".localized
    static let enterFname = "enterFname".localized
    static let enterFacilityCode = "enterFacilityCode".localized
    static let enterSurName = "enterSname".localized
    static let selectDesignation = "selectDesignation".localized
    static let selectGender = "selectSex".localized
    static let selectFacility = "selectFtype".localized
    static let selectState = "selectStatee".localized
    static let selectLGA = "selectLGA".localized
    static let enterFacilityName = "enterFacName".localized
    static let enetrAddress = "enterAdd".localized
    static let enterPswd = "pswdBlank".localized
    static let validPswd = "validPswd".localized
    static let enterCnfmPswd = "cnfmPswdBlank".localized
    static let pswdShouldBeSame = "pswdSame".localized
    static let validAlternateNo = "vailAltNo".localized
    
    static let enterOtp = "varCode".localized
    static let incorrectOtp = "invalidCode".localized
    static let somethingWrong = "tryAgain".localized
    static let resetPswdSuccessfully = "pswdReset".localized
    
    static let questionMustAns = "allQue".localized
    static let enterAge = "enterAge".localized
    static let locationAccess = "Please allow \(appName) to Location access while using the application."
    static let enterDOB = "enterDOB".localized
    
    static let selectForTreatedTB = "treatedTB".localized
    static let selectTypeOfTBCase = "selectTBCae".localized
    static let selectTypeOfDisease = "selectDisease".localized
    static let selectIfHealthWorker = "selectHealthWorker".localized
    static let selectHIVStatus = "ansForHIV".localized
    
    static let allQuestionsMustBeAnswered = "allQue".localized
    
    static let enterAFBSampleCollectionDate = "afbCollDate".localized
    static let enterValidAFBTestSendDate = "afbSendtoLABDate".localized
    static let enterValidAFBResultDate = "afbResultDate".localized
    static let enterAfbtestResult = "enterAfbResult".localized
    
    static let enterMTBSampleCollectionDate = "mtbCollDate".localized
    static let enterValidMTBSendDate = "mtbSendtoLABDate".localized
    static let enterValidMTBResultDate = "mtbResultDate".localized
    static let enterMTBtestResult = "entermtbResult".localized
    
    static let enterXrayTextResult = "enterXrayResult".localized
    
    static let selectOtherTypeOfTBTest = "selectTBCase".localized
    static let enterOtherTestResult = "enterOtherTB".localized
    static let selectDrugResistance = "selectDrug".localized
    
    static let selectSupportSystem = "selectSupport".localized
    static let selectLength = "selectLength".localized
    
    static let selectTreatmentOutcome = "selectOutcome".localized
    
    static let selectDrugtype = "selectDrug".localized
    static let enterFixdrug = "enterFixedDrug".localized
    static let enterLoosedrug = "enterLooseDrug".localized
    
    static let selectFacilityy = "selectFac*".localized
    static let enterSearchText = "enterSearch".localized
    static let selectSearchType = "enterSType".localized
    
    static let eneterOLDPswd = "enterOldPswd".localized
    static let enterNewPswd = "enternewPswd".localized
    static let enterCnfirmPswd = "entercnfmPswd".localized
    
    static let provideEmail = "provideEmail".localized
    static let selectSubject = "selectSubj".localized
    
    static let selectSpecimanType = "selectSpecimanType".localized
    static let validCollectionDate = "validCollectionDate".localized
    static let CompareCollectionDate = "CompareCollectionDate".localized
    static let ValidDispatchDate = "ValidDispatchDate".localized
    static let SelectHIV = "SelectHIV".localized
    static let SelectReasonForExamination = "SelectReasonForExamination".localized
    static let SelectTReatedForTB = "SelectTReatedForTB".localized
    static let SelectTypeRequest = "SelectTypeRequest".localized
    static let SelectSample = "SelectSample".localized
}

struct UDKey {
    static let kIsAgreeToTerms          =  "isAgreeToTerms"
    static let kIsLoggedIn              =  "isLoggedIn"
    static let kIsUser_Details_PhoneNo  =  "isUser_Details_PhoneNo"
    static let kIsUser_Details_Password =  "isUser_Details_Password"
    static let kLoginData               =  "LoginData"
    static let kNotificationToken       =  "NotificyaionToken"
    static let kFCMNotificationToken    =  "FCMNotificationToken"
    static let kDeviceToken             =  "deviceToken"
    static let kDeviceId                =  "deviceID"
    static let kEncKey                  =  "encryptionKey"
    static let kAccessToken             =  "accessToken"
    static let kAppLang                 =  "currentLang"
    static let kDesiDetails             =  "designationDetail"
    static let kPPMVdeatilsForDispatch  =  "PPMVdeatilsForDispatch"
    static let kTbCollecionDetail       =  "tbCollectionRecord"
}
enum MenuOption {
    case Home
    case Dashboard
    case Profile
    case OfflineSync
    case SupportNFeedback
    case Logout
    case EditProfile
    case ChangePswd
    case ChangeLang
    
    case ScreenNewClient
    case MyClient
}

enum CurrentPage {
    case resgisterStep1
    case resgisterStep2
    case resgisterStep3
}
enum AppLanguage : String {
    case English = "en"
    case Hausa = "ha"
}
enum Gender : String {
    case Male = "Male"
    case Female = "Female"
}
enum HIVStatus : String {
    case Positive = "positive"
    case Negative = "negative"
    case Unknown  = "unknown"
}
enum TestedForHIV : String,CaseIterable {
    case Positive = "yes - positive"
    case Negative = "yes - negative"
    case Nottested = "Was not tested"
}
enum HIVStatusOnArrival : String,CaseIterable {
    case Positive = "Yes - Positive"
    case Negative = "Yes - Negative"
    case Unknown  = "Did not know status"
}
enum XpertResults : String,CaseIterable {
    case MTBNotDetected = "mtb not detected"
    case MTBDetectedRIFNOTDetected = "mtb detected - rif resistance detected"
    case MTBDetectedRIFDetected = "mtb detected - rif resistance not detected"
    case MTBDetectedRIFInterminate = "mtb detected - rif resistance indeterminate"
    case InvalidResult = "error or incompleted/ invalid result"
}
enum ChestXRayResult : String,CaseIterable {
    case SuggestiveOFTB = "x-ray suggestive of tb"
    case NotSuggestiveOFTB = "x-ray not suggestive of tb"
}
enum TypeOfTBCase : String,CaseIterable {
    case Culture = "culture"
    case LPA = "lpa"
    case FirstLinedst = "1st line dst"
    case SecondLinedst = "2nd line dst"
    case Other = "Other"
}
enum ResultOFOtherTBTest : String,CaseIterable {
    case TBDetected = "tb detected"
    case TBNotDetected = "tb not detected"
}
enum TBCase : String {
    case DSTB = "DS-TB"
    case DRTB = "DR-TB"
}
enum Disease : String {
    case PTB = "Pulmonary TB(PTB)"
    case EPTB = "Extra-Pulmonary TB(EPTB)"
    case PulmonaryLungs = "pulmonary(lungs)"
    case ExPulmonary = "ExPulmonary(EPTB)"
}
enum hadTBbefore  : String{
    case Yes = "yes"
    case No = "no"
    case Unknown = "unknown"
}
enum isHealthCareWorker : String{
    case Yes = "yes"
    case No = "no"
}
enum YesNO: String {
    case Yes = "yes"
    case No = "no"
}

enum TreatmentOutcome : String,CaseIterable {
    case LostFollowUP = "Lost to follow-up"
    case Died = "Died"
    case Completed = "Treatment completed"
    case Failure = "Treatment failure"
    case NotEvaluated = "Not evaluated"
    case Success = "Treatment success"
}

enum DrugType : String, CaseIterable{
    case FixedDrug = "fixed drug"
    case LooseDrug = "loose drug"
}
enum FixDrugCombination : String,CaseIterable {
    case RHZE = "Rifampicin + Isoniazid + Pyrazinamide + Ethambutol (RHZE)"
    case RHZ = "Rifampicin + Isoniazid + Pyrazinamide (RHZ)"
    case RH = "Rifampicin + Isoniazid (RH)"
}
enum LooseDrugCombination : String,CaseIterable {
    case Rifampicin = "Rifampicin"
    case Isoniazid = "Isoniazid"
    case Ethambutol = "Ethambutol"
    case Pyrazinamide = "Pyrazinamide"
    case Others = "Others(specify)"
}
enum DateType {
    case BirthDate
    case VisitDate
    case SpecimenCollectionDateAFB
    case SpecimenSentLabDateAFB
    case ResultDateAFB
    case SpecimenCollectionDateXpert
    case SpecimenSentLabDateXpert
    case ResultDateXpert
    case TreatmentIntiateDate
}
func generateToken() -> String{
    
    var token = ""
    DCDevice.current.generateToken {
        (data, error) in
        guard let data = data else {
            return
        }
        token = data.base64EncodedString()
        print("Device Token : \(token)")
    }
    return token
}
