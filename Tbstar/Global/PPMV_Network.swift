//
//  UserFlowNetwork.swift
//  Tbstar
//
//  Created by Mavani on 29/05/21.
//

import Foundation
import Alamofire
import CryptoSwift

let USER_signUpUrl = BaseUrl + "facilityValidation1"
let USER_LabList = BaseUrl + "lab/labList1"
let USER_Add_Recent_Serch_Lab =  BaseUrl + "lab/addRecentSearch"
let USER_Recent_Serch_Lab_List =  BaseUrl + "lab/recentSearchList1"
let USER_DropDown_List =  BaseUrl + "patient/deatilsForDispatchForm"
let USER_SelectLab_Form =  BaseUrl + "patient/screeningPatientPpmv1"
let USER_Screnning_With_No_TB_URL = BaseUrl + "patient/screeningClinic1"
let URL_USER_PATIENT_LIST = BaseUrl + "patient/ppmvClientList"
let URL_UPDATE_PATIENT_DETAILS = BaseUrl + "patient/updatePatientDetails"
let PPMV_updateCovidStatusOfPatientUrl = BaseUrl + "patient/covid"
let PPMV_updateSpecimenDetailsUrl = BaseUrl + "patient/updateSpecimenDetails"
let PPMV_DashboardUrl = BaseUrl + "patient/dashBoardApippmv"
let PPMV_DashboardScreeningUrl = BaseUrl + "patient/dashBoardApilist"
let PPMV_Notification_Delete_URL = BaseUrl + "alert"

//MARK:- New Client Screen
func USER_FacilityCode_Access(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void)
{
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json"]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: USER_signUpUrl) else {
        completionHandler(false,"","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_Get_Lab_List(paradict :[String:Any],completionHandler : @escaping(String,String,String) -> Void)
{
    var accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = ""
    var message = ""
    
    guard let apiurl = URL(string: USER_LabList) else {
        completionHandler("","","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? String{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_ADD_RECENT_SERCH_LAB(paradict :[String:Any],completionHandler : @escaping(String,String,String) -> Void)
{
    var accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = ""
    var message = ""
    
    guard let apiurl = URL(string: USER_Add_Recent_Serch_Lab) else {
        completionHandler("","","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? String{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_Get_Recent_Search_Lab_List(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void)
{
    var accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: USER_Recent_Serch_Lab_List) else {
        completionHandler(false,"","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? Bool {
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_GET_CLINIC_AND_LAB_LIST_BASE_ON_SERCH(paradict :[String:Any],completionHandler : @escaping(String,String,String) -> Void) {
    
    var accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = ""
    var message = ""
    
    guard let apiurl = URL(string: USER_LabList) else {
        completionHandler("","","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? String {
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_GET_ForDispatchForm(completionHandler : @escaping(String,String,String) -> Void) {
    
    var accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = ""
    var message = ""
    
    guard let apiurl = URL(string: USER_DropDown_List) else {
        completionHandler("","","")
        return
    }
    AF.request(apiurl, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? String {
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    accessToken = token
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_PPMV_LAB_SELECT_FORM_DETAILS_PASS(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void) {
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["loggedInOrNotType" : "0",
                                 "apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: USER_SelectLab_Form) else {
        completionHandler(false,"","")
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKeyForGetAnd)
    AF.request(apiurl, method: .post, parameters: encParadict, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                if let success = json["success"] as? Bool {
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func screeningPPMVUserAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: USER_Screnning_With_No_TB_URL) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}


//MARK:- My Client
func PPMV_GET_PATIENT_LIST(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: URL_USER_PATIENT_LIST) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_UPDATE_PATIENT_DETAILS(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: URL_UPDATE_PATIENT_DETAILS) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["result"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func USER_UPDATE_SPECIMEN_DETAILS(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: PPMV_updateSpecimenDetailsUrl) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["result"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

//Dashboard..
func getPPMV_DashboardListScreenAPI(paradict :[String:Any],completionHandler : @escaping(Bool,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    
    let headers : HTTPHeaders = ["token" : accessToken,
                   "apikey" : APIKey,
                   "int" : encKey,
                   "accept" : "application/json"]
    
    guard let apiurl = URL(string: PPMV_DashboardUrl) else {
        completionHandler(false,"")
        return
    }
    
    AF.request(apiurl,method: .get , parameters: paradict, encoding: URLEncoding(destination: .queryString), headers: headers).responseJSON { response in
        if let JsonData = response.value as? [String:Any] {
            if let success = JsonData["success"] as? Bool, success == true{
                if let data = JsonData["data"] as? String {
                    completionHandler(success,data)
                }else{
                    completionHandler(success,"")
                }
            }else{
                completionHandler(false,"")
            }
        }else{
            completionHandler(false,"")
        }
    }
    
}

func getPPMV_PatientTreatedList(paradict :[String:Any],  completionHandler : @escaping(Bool,String,String) -> Void){
    
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    var isSuccess = false
    var message = ""
    var strdata = ""
    
    guard let apiurl = URL(string: PPMV_DashboardScreeningUrl) else {
        completionHandler(isSuccess,message,strdata)
        return
    }
    
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .post, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,message,strdata)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let data = json["data"] as? String {
                    strdata = data
                }
            }
            completionHandler(isSuccess,message,strdata)
            break
        }
    }
    
}

func DeleteNotification(paradict :[String:Any],completionHandler : @escaping(Bool,String,String) -> Void){
    
    var encKey = encKeyForGetAnd
    if let key = Defaultss.value(forKey: UDKey.kEncKey) as? String{
        encKey = key
    }
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = [
        "apikey" : APIKey,
        "int" : encKey,
        "accept" : "application/json",
        "token" : accessToken]
    
    
    var strdata = ""
    var isSuccess = false
    var message = ""
    
    guard let apiurl = URL(string: PPMV_Notification_Delete_URL) else {
        completionHandler(isSuccess,strdata,message)
        return
    }
    let encParadict = encryptParadict(parametrDic: paradict,encKEy: encKey)
    
    AF.request(apiurl, method: .delete, parameters: encParadict , encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
        print(response.result)
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                if let success = json["success"] as? Bool{
                    isSuccess = success
                }
                if let data = json["result"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
                if let token = json["accessToken"] as? String{
                    Defaultss.setValue(token, forKey: UDKey.kAccessToken)
                    Defaultss.synchronize()
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        case .failure(let error):
            print(error)
            if let json = response.value as? [String:Any]{
                if let data = json["data"] as? String {
                    strdata = data
                }
                if let msg = json["message"] as? String{
                    message = msg
                }
            }
            completionHandler(isSuccess,strdata,message)
            break
        }
    }
}

func PPMV_GET_ALETS_CASES_LIST(apiUrl : String , paradict :[String:Any],completionHandler : @escaping([String :Any], Bool) -> Void){
        
    let accessToken = Defaultss.value(forKey: UDKey.kAccessToken) as? String ?? ""
    let headers : HTTPHeaders = ["apikey" : APIKey,
                                 "int" : encKeyForGetAnd,
                                 "accept" : "application/json",
                                 "token" : accessToken]
    
    var isSuccess = false

    guard let APIURL = URL(string: NO_ENDPOINT_BaseURL + apiUrl) else {
        completionHandler([:] , isSuccess)
        return
    }
    
    AF.request(APIURL, method: .get, parameters: paradict ,encoding: URLEncoding(destination: .queryString), headers: headers ).responseJSON { (response) in
        switch response.result {
        case .success(_):
            if let json = response.value as? [String:Any] {
                
                isSuccess = true
                completionHandler(json, isSuccess)
            }
            break
        case .failure(let error):
            print(error)
            completionHandler([:] , isSuccess)
            break
        }
    }
}
