//
//  AppDelegate.swift
//  Tbstar
//
//  Created by Apple on 27/01/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import DeviceCheck
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        setupFirstView()
        FirebaseApp.configure()
        generateDeviceIdNToken()
        Messaging.messaging().delegate = self
        DatabaseManager().copyDatabaseIfNeeded()
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) { [self] granted, error in
                print("Permission granted: \(granted)")
                if let error = error {
                    print("D'oh: \(error.localizedDescription)")
                } else {
                    self.registerForPushNotifications()
                }
            }
        return true
    }
    func registerForPushNotifications() {
   
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func setupFirstView()  {
        
        if UserDefaults.standard.bool(forKey: UDKey.kIsAgreeToTerms){
            // Open loginVC
            if UserDefaults.standard.bool(forKey: UDKey.kIsLoggedIn){
                
                if  let userType =  Defaultss.value(forKey: SPECIFY_USERS) as? String,
                    userType == USER_TYPE_INDEPENDENT {
                    
                    if let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController,
                       let navVC = storyboard.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController{
                        navVC.setViewControllers([homeVC], animated: true)
                        window?.rootViewController = navVC
                        window?.makeKeyAndVisible()
                        Utility.openSideMenu()
                    }

                } else {
                    
                    if let homeVC = storyboard.instantiateViewController(withIdentifier: "UserHomeVC") as? UserHomeVC,
                       let navVC = storyboard.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController{
                        navVC.setViewControllers([homeVC], animated: true)
                        window?.rootViewController = navVC
                        window?.makeKeyAndVisible()
                        Utility.openSideMenu()
                    }

                }

            }else{
                
                if let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController,
                   let navVC = storyboard.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController{
                    navVC.setViewControllers([loginVC], animated: true)
                    window?.rootViewController = navVC
                    window?.makeKeyAndVisible()
                }
            }
        }else{
            
            if let aggrementVC = storyboard.instantiateViewController(withIdentifier: "AgreementViewController") as? AgreementViewController,
               let navVC = storyboard.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController{
                navVC.setViewControllers([aggrementVC], animated: true)
                window?.rootViewController = navVC
                window?.makeKeyAndVisible()
            }
        }
    }
    func generateDeviceIdNToken()  {
        
        if (Defaultss.value(forKey: UDKey.kDeviceToken) as? String) == nil
        {
            DCDevice.current.generateToken {
                (data, error) in
                if let data = data {
                    let token = data.base64EncodedString()
                    Defaultss.setValue(token, forKey: UDKey.kDeviceToken)
                }else{
                    let token = UUID().uuidString
                    Defaultss.setValue(token, forKey: UDKey.kDeviceToken)
                }
                
            }
        }
        if (Defaultss.value(forKey: UDKey.kDeviceId) as? String) == nil{
           let deviceID =  UIDevice.current.identifierForVendor!.uuidString
            Defaultss.setValue(deviceID, forKey: UDKey.kDeviceId)
        }
        Defaultss.synchronize()
        //if let device
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print(token)
        UserDefaults.standard.setValue(token, forKey: UDKey.kNotificationToken)
        UserDefaults.standard.synchronize()
        print("Device Token: \(token)")
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Tbstar")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
extension AppDelegate : MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("FCM Token \(fcmToken)")
        UserDefaults.standard.setValue(fcmToken, forKey: UDKey.kFCMNotificationToken)
        UserDefaults.standard.synchronize()
    }
    
}
