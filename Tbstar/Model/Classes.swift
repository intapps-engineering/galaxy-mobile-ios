//
//  Classes.swift
//  Tbstar
//
//  Created by Viprak-Heena on 05/02/21.
//

import Foundation

class Designation: NSObject {
    
    var name = ""
    
    init(dict :[String:Any]) {
        name = dict["designationName"] as? String ?? ""
    }
}
class FacilityName: NSObject {
    
    var name = ""
    
    init(dict :[String:Any]) {
        name = dict["facilityName"] as? String ?? ""
    }
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "facilityName")
    }
    required init?(coder: NSCoder) {
        name = coder.decodeObject(forKey: "facilityName") as? String ?? ""
    }
    
}
class Address: NSObject {
    
    var id = ""
    var zone = ""
    var stateName = ""
    var scanCode = ""
    var stateId = ""
    var LGA : [String]?
    
    
    init(dict : [String:Any]) {
        id = dict["_id"] as? String ?? ""
        zone = dict["Zone"] as? String ?? ""
        stateName = dict["State"] as? String ?? ""
        scanCode = dict["sonCode"] as? String ?? ""
        stateId = dict["stateId"] as? String ?? ""
        LGA = dict["LGA"] as? [String]
    }
}

class DesignationDetail: NSObject {
    
    var publicFaciltyList = [FacilityName]()
    var privateFaciltyList = [FacilityName]()
    var designationList = [Designation]()
    var addressList = [Address]()
    
    init(dict : [String:Any]) {
        if let desDetailList = dict["designationDeatils"] as? [[String:Any]]{
            if desDetailList.count > 0,
               let facilityDict = desDetailList[0]["facilityList"] as? [String:Any],
               let publicFaciltyDictList = facilityDict["publicFacilty"] as? [[String:Any]]{
                publicFaciltyList = publicFaciltyDictList.map(FacilityName.init)
            }
            if desDetailList.count > 0,
               let facilityDict = desDetailList[0]["facilityList"] as? [String:Any],
               let privateFaciltyDictList = facilityDict["privateFacilty"] as? [[String:Any]]{
                privateFaciltyList = privateFaciltyDictList.map(FacilityName.init)
            }
            if desDetailList.count > 0,
               let designDictList = desDetailList[0]["designationList"] as? [[String:Any]]{
                designationList = designDictList.map(Designation.init)
            }
        }
        if let addressDictList = dict["addressDetails"] as? [[String:Any]]{
            addressList = addressDictList.map(Address.init)
        }
    }
    func encode(with coder: NSCoder) {
        coder.encode(publicFaciltyList, forKey: "publicFacilty")
        coder.encode(privateFaciltyList, forKey: "privateFacilty")
        coder.encode(designationList, forKey: "designationList")
        coder.encode(addressList, forKey: "addressDetails")
    }
    required init?(coder: NSCoder) {
        publicFaciltyList = coder.decodeObject(forKey: "publicFacilty") as? [FacilityName] ?? [FacilityName]()
        privateFaciltyList = coder.decodeObject(forKey: "privateFacilty") as? [FacilityName] ?? [FacilityName]()
        designationList = coder.decodeObject(forKey: "designationList") as? [Designation] ?? [Designation]()
        addressList = coder.decodeObject(forKey: "addressDetails") as? [Address] ?? [Address]()
    }
}

class LGA: NSObject {
    
    var id = ""
    var code = 0
    var name = ""
    var state_code_for_mapping = 0
    
    
    init(dict:[String:Any]) {
        id = dict["_id"] as? String ?? ""
        code = dict["code"] as? Int ?? 0
        name = dict["name"] as? String ?? ""
        state_code_for_mapping = dict["state_code_for_mapping"] as? Int ?? 0
    }
}

class DotsCenter: NSObject {
    
    var id = ""
    var etbId = ""
    var name = ""
    var address = ""
    var lga = ""
    var state = ""
    var dhisId = ""
    var lgaId = ""
    var stateId = ""
    
    init(dict : [String:Any]) {
        id = dict["_id"] as? String ?? ""
        etbId = dict["etbId"] as? String ?? ""
        name = dict["name"] as? String ?? ""
        address = dict["address"] as? String ?? ""
        lga = dict["lga"] as? String ?? ""
        state = dict["state"] as? String ?? ""
        dhisId = dict["dhisId"] as? String ?? ""
        lgaId = dict["lgaId"] as? String ?? ""
        stateId = dict["stateId"] as? String ?? ""
    }
    
}

class PatientInformation : NSObject {
    
    var patientTBSymptoms : PatientTBSymptoms?
    var patientCovidSymptoms : PatientCovidSymptoms?
    var ptbCaseDetail : PTBCaseDetail?
    var afbTestDetail : AFBTestDetail?
    var mtbTestDetail : MTBTestDetail?
    var xrayPerformedDetail : XrayPerformedDetail?
    var otherTbTestDetail : OtherTBTestDetail?
    var initiateTreatmentDetail : InitiateTreatmentDetail?
    var specifyTreatmentDetail : SpecifyTreatmentDetail?
    var refferedToOtherFacilityDetail : PatientReferredToOtherFacilty?
    var treatmentOutcomeDetail : TreatmentOutcomeDetail?
    
    var firstName = ""
    var middleName = ""
    var surname = ""
    var phoneNumber = ""
    var secondPhoneNumber = ""
    var address = ""
    var HasTBTag = ""
    var visitDate = ""
    var dateOfBirth = ""
    var age = ""
    var sex = ""
    var lga = ""
    var state = ""
    var hadTBbefore = ""
    var hivStatus = ""
    var typeOfPresumptiveTbCase = ""
    var siteOfDisease = ""
    var patientWorker = ""
    var patientRecordNumber = ""
    var _id = ""
    var userId = ""
    var patientStatusId = ""
    var patientStatusLabel = ""
    var createDate = ""
    var __v = 0
    var lattitude = 0.0
    var longitude = 0.0
    var offlineId =  0
    
    
    init(dict : [String : Any]) {
        
        if let patientDetail  = dict["patientDetails"] as? [String : Any] {
            if let patientSymptoms = patientDetail["patientSymptoms"] as? [String:Any],
               patientSymptoms.count > 0{
                patientTBSymptoms = PatientTBSymptoms(dict: patientSymptoms)
            }
            lattitude = patientDetail["latitude"] as? Double ?? 0.0
            longitude = patientDetail["longitude"] as? Double ?? 0.0
            
            if let covidSymptoms = patientDetail["covidSymptoms"] as? [String:Any], covidSymptoms.count > 0{
                patientCovidSymptoms = PatientCovidSymptoms(dict: covidSymptoms)
            }
            if let clientHasTB = patientDetail["clientHasTB"] as? [String : Any]{
                
                if let infoDict = clientHasTB["patientInformation"] as? [String :Any]{
                    
                    firstName = infoDict["firstName"] as? String ?? ""
                    middleName = infoDict["middleName"] as? String ?? ""
                    surname = infoDict["lastName"] as? String ?? ""
                    phoneNumber = infoDict["phoneNumber"] as? String ?? ""
                    secondPhoneNumber = infoDict["secondPhoneNumber"] as? String ?? ""
                    address = infoDict["address"] as? String ?? ""
                    HasTBTag = infoDict["HasTBTag"] as? String ?? ""
                    visitDate =  infoDict["visitDate"] as? String ?? ""
                    dateOfBirth = infoDict["dateOfBirth"] as? String ?? ""
                    age = infoDict["age"] as? String ?? ""
                    sex = infoDict["sex"] as? String ?? ""
                    lga = infoDict["lga"] as? String ?? ""
                    state = infoDict["state"] as? String ?? ""
                    hadTBbefore = infoDict["hadTBbefore"] as? String ?? ""
                    hivStatus = infoDict["hivStatus"] as? String ?? ""
                    typeOfPresumptiveTbCase = infoDict["typeOfPresumptiveTbCase"] as? String ?? ""
                    siteOfDisease = infoDict["siteOfDisease"] as? String ?? ""
                    patientWorker = infoDict["patientWorker"] as? String ?? ""
                    
                    if let caseDetail = infoDict["ptbCase"] as? [String:Any]{
                        ptbCaseDetail = PTBCaseDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["afbTest"] as? [String:Any]{
                        afbTestDetail = AFBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["mtbTest"] as? [String:Any]{
                        mtbTestDetail = MTBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["xRayPerformed"] as? [String:Any]{
                        xrayPerformedDetail = XrayPerformedDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["otherTbTest"] as? [String:Any]{
                        otherTbTestDetail = OtherTBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["initiateTreatment"] as? [String:Any]{
                        initiateTreatmentDetail = InitiateTreatmentDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["specifyTreatment"] as? [String:Any]{
                        specifyTreatmentDetail = SpecifyTreatmentDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["patientReferredToOtherFacilty"] as? [String:Any]{
                        refferedToOtherFacilityDetail = PatientReferredToOtherFacilty(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["treatmentOutcome"] as? [String : Any] {
                        treatmentOutcomeDetail = TreatmentOutcomeDetail(dict: caseDetail)
                    }
                }
                patientRecordNumber = clientHasTB["patientRecordNumber"] as? String ?? ""
            }
        }
        offlineId = dict["patientOfflineID"] as? Int ?? 0
        _id = dict["_id"] as? String ?? ""
        userId = dict["userId"] as? String ?? ""
        patientStatusId = dict["patientStatusId"] as? String ?? ""
        patientStatusLabel = dict["patientStatusLabel"] as? String ?? ""
        createDate = dict["createDate"] as? String ?? ""
        __v = dict["__v"] as? Int ?? 0
    }
    
}

class PatientTBSymptoms: NSObject {
    
    var CoughFor2WeeksOrMore = ""
    var FeverFor3weeksOrMore = ""
    var NightSweats = ""
    var UnexplainedWeightLoss = ""
    var Swelling = ""
    var FailureToThrive = ""
    var CoughingUpBlood = ""
    
    init(dict : [String : Any]) {
        CoughFor2WeeksOrMore = dict["CoughFor2WeeksOrMore"] as? String ?? ""
        FeverFor3weeksOrMore = dict["FeverFor3weeksOrMore"] as? String ?? ""
        NightSweats = dict["NightSweats"] as? String ?? ""
        UnexplainedWeightLoss = dict["UnexplainedWeightLoss"] as? String ?? ""
        Swelling = dict["Swelling"] as? String ?? ""
        FailureToThrive = dict["FailureToThrive"] as? String ?? ""
        CoughingUpBlood = dict["CoughingUpBlood"] as? String ?? ""
    }
}
class PatientCovidSymptoms: NSObject {
    
    var covidCough = ""
    var covidFever = ""
    var covidLossOfTaste = ""
    var covidRunnyNose = ""
    var covidDiarrhea = ""
    var covidSoreThroat = ""
    var covidShaking = ""
    
    init(dict : [String : Any]) {
        covidCough = dict["covidCough"] as? String ?? ""
        covidFever = dict["covidFever"] as? String ?? ""
        covidLossOfTaste = dict["covidLossOfTaste"] as? String ?? ""
        covidRunnyNose = dict["covidRunnyNose"] as? String ?? ""
        covidDiarrhea = dict["covidDiarrhea"] as? String ?? ""
        covidSoreThroat = dict["covidSoreThroat"] as? String ?? ""
        covidShaking = dict["covidShaking"] as? String ?? ""
    }
}

class PTBCaseDetail: NSObject {
    
    var typeOfPresumtivePtbCase = ""
    var siteOfDisease = ""
    var patientIsAhealthWorker = ""
    var whoReferredThePatient = ""
    var didPatientKnowHivStatusUponArrival = ""
    var wasPatientCounselledForHivTesting = ""
    var wasPatientTestedForHiv = ""
    var sourceofreferral = ""
    
    init(dict : [String : Any]) {
        typeOfPresumtivePtbCase = dict["typeOfPresumtivePtbCase"] as? String ?? ""
        siteOfDisease = dict["siteOfDisease"] as? String ?? ""
        patientIsAhealthWorker = dict["patientIsAhealthWorker"] as? String ?? ""
        whoReferredThePatient = dict["whoReferredThePatient"] as? String ?? ""
        didPatientKnowHivStatusUponArrival = dict["didPatientKnowHivStatusUponArrival"] as? String ?? ""
        wasPatientCounselledForHivTesting = dict["wasPatientCounselledForHivTesting"] as? String ?? ""
        wasPatientTestedForHiv = dict["wasPatientTestedForHiv"] as? String ?? ""
        sourceofreferral = dict["sourceofreferral"] as? String ?? ""
    }
}

class AFBTestDetail: NSObject {
    
    var afbTestConducted = ""
    var afbResults = ""
    var dateResultsReleased = ""
    var dateOfSpecimenCollection = ""
    var dateSpecimenWasSentToLaboratory = ""
    
    init(dict : [String : Any]) {
        afbTestConducted = dict["afbTestConducted"] as? String ?? ""
        afbResults = dict["afbResults"] as? String ?? ""
        dateResultsReleased = dict["dateResultsReleased"] as? String ?? ""
        dateOfSpecimenCollection = dict["dateOfSpecimenCollection"] as? String ?? ""
        dateSpecimenWasSentToLaboratory = dict["dateSpecimenWasSentToLaboratory"] as? String ?? ""
    }
    
}

class MTBTestDetail: NSObject {
    
    var rifTestConducted = ""
    var xpertResults = ""
    var dateOfSpecimenCollection = ""
    var dateResultsReleased = ""
    var dateSpecimenWasSentToLaboratory = ""
    
    init(dict : [String : Any]) {
        rifTestConducted = dict["rifTestConducted"] as? String ?? ""
        xpertResults = dict["xpertResults"] as? String ?? ""
        dateOfSpecimenCollection = dict["dateOfSpecimenCollection"] as? String ?? ""
        dateResultsReleased = dict["dateResultsReleased"] as? String ?? ""
        dateSpecimenWasSentToLaboratory = dict["dateSpecimenWasSentToLaboratory"] as? String ?? ""
    }
}

class XrayPerformedDetail : NSObject {
    var chestXrayPerformed = ""
    var xRaySuggestiveOfTb = ""
    var otherXrayResultComments = ""
    
    init(dict : [String : Any]) {
        chestXrayPerformed = dict["chestXrayPerformed"] as? String ?? ""
        xRaySuggestiveOfTb = dict["xRaySuggestiveOfTb"] as? String ?? ""
        otherXrayResultComments = dict["otherXrayResultComments"] as? String ?? ""
    }
}

class OtherTBTestDetail: NSObject {
    var otherTbTestConducted = ""
    var typeOfTest = ""
    var resultOfTest = ""
    var drugResistanceDetected = ""
    var otherTestResultComments = ""
    
    init(dict : [String : Any]) {
        otherTbTestConducted = dict["otherTbTestConducted"] as? String ?? ""
        typeOfTest = dict["typeOfTest"] as? String ?? ""
        resultOfTest = dict["resultOfTest"] as? String ?? ""
        drugResistanceDetected = dict["drugResistanceDetected"] as? String ?? ""
        otherTestResultComments = dict["otherTestResultComments"] as? String ?? ""
    }
    
}
class InitiateTreatmentDetail: NSObject {
    
    var didYouInitiateTreatment = ""
    var isDotProvidedByTreatment = ""
    var lengthOfProposedRegimen = ""
    var otherTreatmentDetails = ""
    var dateTreatmentInitiated = ""
    var dateOfTreatmentEnd = ""
    
    init(dict : [String : Any]) {
        didYouInitiateTreatment = dict["didYouInitiateTreatment"] as? String ?? ""
        isDotProvidedByTreatment = dict["isDotProvidedByTreatment"] as? String ?? ""
        lengthOfProposedRegimen = dict["lengthOfProposedRegimen"] as? String ?? ""
        otherTreatmentDetails = dict["otherTreatmentDetails"] as? String ?? ""
        dateTreatmentInitiated = dict["dateTreatmentInitiated"] as? String ?? ""
        dateOfTreatmentEnd = dict["dateOfTreatmentEnd"] as? String ?? ""
    }
}
class SpecifyTreatmentDetail: NSObject {
    
    var specifyTreatment = ""
    var fixedDrugCombination = ""
    var specifyCombination = ""
    var otherTreatmentDetails = ""
    var drugType = ""
    
    init(dict : [String : Any]) {
        specifyTreatment = dict["specifyTreatment"] as? String ?? ""
        fixedDrugCombination = dict["fixedDrugCombination"] as? String ?? ""
        specifyCombination = dict["specifyCombination"] as? String ?? ""
        drugType = dict["drugType"] as? String ?? ""
        otherTreatmentDetails = dict["otherTreatmentDetails"] as? String ?? ""
    }
}
class TreatmentOutcomeDetail: NSObject {
    
    var treatmentOutcomeId = ""
    var treatmentOutcomeLabel = ""
    
    init(dict : [String : Any]) {
        treatmentOutcomeId = dict["treatmentOutcomeId"] as? String ?? ""
        treatmentOutcomeLabel = dict["treatmentOutcomeLabel"] as? String ?? ""
    }
    
}
class FeedbackSubject: NSObject {
    
    var id = ""
    var _id = ""
    var name = ""
    
    init(dict : [String : Any]) {
        id = dict["id"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        name = dict["name"] as? String ?? ""
    }
}
class PatientReferredToOtherFacilty: NSObject {
    
    var patientReferredToOtherFacilty = ""
    var otherFacilityName = ""
    var otherFaciltyAddres = ""
    var stateId = ""
    var lgaId = ""
    
    init(dict : [String : Any]) {
        patientReferredToOtherFacilty = dict["patientReferredToOtherFacilty"] as? String ?? ""
        otherFacilityName = dict["otherFacilityName"] as? String ?? ""
        otherFaciltyAddres = dict["otherFaciltyAddres"] as? String ?? ""
        stateId = dict["stateId"] as? String ?? ""
        lgaId = dict["lgaId"] as? String ?? ""
    }
}

class TreatmentOutcomes: NSObject {
    
    var id = ""
    var _id = ""
    var name = ""
    
    init(dict : [String : Any]) {
        id = dict["id"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        name = dict["name"] as? String ?? ""
    }
    
}


struct SharePatientDetails_To_User {
    
    let facilityName : String?
    let facilityType : String?
    let firstName : String?
    let id : String?
    let lastName : String?
    let lgaId : String?
    let lgaName : String?
    let phoneNumber : String?
    let stateId : String?
    let stateName : String?
    let userType : String?
}


struct SharePatientDetails {
    
    var patientTBSymptoms : PatientTBSymptoms?
    var patientCovidSymptoms : PatientCovidSymptoms?
    var ptbCaseDetail : PTBCaseDetail?
    var afbTestDetail : AFBTestDetail?
    var mtbTestDetail : MTBTestDetail?
    var xrayPerformedDetail : XrayPerformedDetail?
    var otherTbTestDetail : OtherTBTestDetail?
    var initiateTreatmentDetail : InitiateTreatmentDetail?
    var specifyTreatmentDetail : SpecifyTreatmentDetail?
    var refferedToOtherFacilityDetail : PatientReferredToOtherFacilty?
    var treatmentOutcomeDetail : TreatmentOutcomeDetail?
    
    var firstName = ""
    var middleName = ""
    var surname = ""
    var phoneNumber = ""
    var secondPhoneNumber = ""
    var address = ""
    var HasTBTag = ""
    var visitDate = ""
    var dateOfBirth = ""
    var age = ""
    var sex = ""
    var lga = ""
    var state = ""
    var hadTBbefore = ""
    var hivStatus = ""
    var typeOfPresumptiveTbCase = ""
    var siteOfDisease = ""
    var patientWorker = ""
    var patientRecordNumber = ""
    var _id = ""
    var userId = ""
    var patientStatusId = ""
    var patientStatusLabel = ""
    var createDate = ""
    var __v = 0
    var lattitude = 0.0
    var longitude = 0.0
    var offlineId =  0
    
    init(dict : [String : Any]){
        
        if let patientDetail  = dict["patientDetails"] as? [String : Any] {
            if let patientSymptoms = patientDetail["patientSymptoms"] as? [String:Any],
               patientSymptoms.count > 0{
                patientTBSymptoms = PatientTBSymptoms(dict: patientSymptoms)
            }
            lattitude = patientDetail["latitude"] as? Double ?? 0.0
            longitude = patientDetail["longitude"] as? Double ?? 0.0
            
            if let covidSymptoms = patientDetail["covidSymptoms"] as? [String:Any], covidSymptoms.count > 0{
                patientCovidSymptoms = PatientCovidSymptoms(dict: covidSymptoms)
            }
            if let clientHasTB = patientDetail["clientHasTB"] as? [String : Any]{
                
                if let infoDict = clientHasTB["patientInformation"] as? [String :Any]{
                    
                    firstName = infoDict["firstName"] as? String ?? ""
                    middleName = infoDict["middleName"] as? String ?? ""
                    surname = infoDict["lastName"] as? String ?? ""
                    phoneNumber = infoDict["phoneNumber"] as? String ?? ""
                    secondPhoneNumber = infoDict["secondPhoneNumber"] as? String ?? ""
                    address = infoDict["address"] as? String ?? ""
                    HasTBTag = infoDict["HasTBTag"] as? String ?? ""
                    visitDate =  infoDict["visitDate"] as? String ?? ""
                    dateOfBirth = infoDict["dateOfBirth"] as? String ?? ""
                    age = infoDict["age"] as? String ?? ""
                    sex = infoDict["sex"] as? String ?? ""
                    lga = infoDict["lga"] as? String ?? ""
                    state = infoDict["state"] as? String ?? ""
                    hadTBbefore = infoDict["hadTBbefore"] as? String ?? ""
                    hivStatus = infoDict["hivStatus"] as? String ?? ""
                    typeOfPresumptiveTbCase = infoDict["typeOfPresumptiveTbCase"] as? String ?? ""
                    siteOfDisease = infoDict["siteOfDisease"] as? String ?? ""
                    patientWorker = infoDict["patientWorker"] as? String ?? ""
                    
                    if let caseDetail = infoDict["ptbCase"] as? [String:Any]{
                        ptbCaseDetail = PTBCaseDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["afbTest"] as? [String:Any]{
                        afbTestDetail = AFBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["mtbTest"] as? [String:Any]{
                        mtbTestDetail = MTBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["xRayPerformed"] as? [String:Any]{
                        xrayPerformedDetail = XrayPerformedDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["otherTbTest"] as? [String:Any]{
                        otherTbTestDetail = OtherTBTestDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["initiateTreatment"] as? [String:Any]{
                        initiateTreatmentDetail = InitiateTreatmentDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["specifyTreatment"] as? [String:Any]{
                        specifyTreatmentDetail = SpecifyTreatmentDetail(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["patientReferredToOtherFacilty"] as? [String:Any]{
                        refferedToOtherFacilityDetail = PatientReferredToOtherFacilty(dict: caseDetail)
                    }
                    if let caseDetail = infoDict["treatmentOutcome"] as? [String : Any] {
                        treatmentOutcomeDetail = TreatmentOutcomeDetail(dict: caseDetail)
                    }
                }
                patientRecordNumber = clientHasTB["patientRecordNumber"] as? String ?? ""
            }
        }
        offlineId = dict["patientOfflineID"] as? Int ?? 0
        _id = dict["_id"] as? String ?? ""
        userId = dict["userId"] as? String ?? ""
        patientStatusId = dict["patientStatusId"] as? String ?? ""
        patientStatusLabel = dict["patientStatusLabel"] as? String ?? ""
        createDate = dict["createDate"] as? String ?? ""
        __v = dict["__v"] as? Int ?? 0
    }
}


class PPMV_Lab_List : NSObject {
    
    var LGA = ""
    var _id = ""
    var __v = ""
    var activeStatus = ""
    var address = ""
    var dotsAvailability = ""
    var facilityName = ""
    var facilityState = ""
    
    var facilityType = ""
    var phoneNumber = ""
    var refPatientId = "refPatientId" //Array
    var specimenId = "specimenId" //Array
    var userId = "userId" //Array
    var userType = ""
    
    var arrRefPatientId : NSArray = NSArray()
    var arrSpecimenId : NSArray = NSArray()
    var arrUserId : NSArray = NSArray()
    
    init(dict : [String : Any]) {
        
        let dictNSMutable = NSMutableDictionary(dictionary: dict)

        arrRefPatientId = dictNSMutable.value(forKey: refPatientId) as? NSArray ?? []

        arrSpecimenId = dictNSMutable.value(forKey: specimenId) as? NSArray ?? []

        arrUserId = dictNSMutable.value(forKey: userId) as? NSArray ?? []
        
        LGA = dictNSMutable["LGA"] as? String ?? ""
        _id = dictNSMutable["_id"] as? String ?? ""
        activeStatus = dictNSMutable["activeStatus"] as? String ?? ""
        address = dictNSMutable["address"] as? String ?? ""
        dotsAvailability = dictNSMutable["dotsAvailability"] as? String ?? ""
        facilityName = dictNSMutable["facilityName"] as? String ?? ""
        facilityState = dictNSMutable["facilityState"] as? String ?? ""
        phoneNumber = dictNSMutable["phoneNumber"] as? String ?? ""
        userType = dictNSMutable["facilityName"] as? String ?? ""
    }
}


class PPMV_Lab_Drop_Down: NSObject {
    
    var hivstatus = [DictHIVStatus]()
    var TbStatus = [DictPreviousTBCheck]()
    var SpecimanType = [DictSpeciMenTypes]()
    var TestReason = [DictTestReasons]()
    var TestType = [DictTestType]()
    
    init(dict : [String:Any]) {
        
        if let desDetailList = dict["deatilsForDispatch"] as? NSDictionary {
            
            if desDetailList.count > 0, let designDictList = desDetailList["hivStatus"] as? [[String:Any]] {
                hivstatus = designDictList.map(DictHIVStatus.init)
            }

            if desDetailList.count > 0, let designDictList = desDetailList["previousTbCheck"] as? [[String:Any]] {
                TbStatus = designDictList.map(DictPreviousTBCheck.init)
            }
            
            if desDetailList.count > 0, let designDictList = desDetailList["specimenTypes"] as? [[String:Any]] {
                SpecimanType = designDictList.map(DictSpeciMenTypes.init)
            }

            if desDetailList.count > 0, let designDictList = desDetailList["testReasons"] as? [[String:Any]] {
                TestReason = designDictList.map(DictTestReasons.init)
            }

            if desDetailList.count > 0, let designDictList = desDetailList["testTypes"] as? [[String:Any]] {
                TestType = designDictList.map(DictTestType.init)
            }

        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(hivstatus, forKey: "hivStatus")
        coder.encode(TbStatus, forKey: "previousTbCheck")
        coder.encode(SpecimanType, forKey: "specimenTypes")
        coder.encode(TestReason, forKey: "testReasons")
        coder.encode(TestType, forKey: "testTypes")
    }
    
    required init?(coder: NSCoder) {
        hivstatus = coder.decodeObject(forKey: "hivStatus") as? [DictHIVStatus] ?? [DictHIVStatus]()
        TbStatus = coder.decodeObject(forKey: "previousTbCheck") as? [DictPreviousTBCheck] ?? [DictPreviousTBCheck]()
        SpecimanType = coder.decodeObject(forKey: "specimenTypes") as? [DictSpeciMenTypes] ?? [DictSpeciMenTypes]()
        TestReason = coder.decodeObject(forKey: "testReasons") as? [DictTestReasons] ?? [DictTestReasons]()
        TestType = coder.decodeObject(forKey: "testTypes") as? [DictTestType] ?? [DictTestType]()
    }
}

class DictHIVStatus : NSObject {
    
    var hivStatus = ""
    
    init(dict : [String:Any]) {
        hivStatus = dict["hivStatus"] as? String ?? ""
    }
}

class DictPreviousTBCheck : NSObject {
    
    var TBCheck = ""
    
    init(dict : [String:Any]) {
        TBCheck = dict["status"] as? String ?? ""
    }
}

class DictSpeciMenTypes : NSObject {
    
    var __v = ""
    var _id = ""
    var created = ""
    var specimenTypes = ""
    
    init(dict : [String:Any]) {
        __v = dict["__v"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        created = dict["created"] as? String ?? ""
        specimenTypes = dict["specimenTypes"] as? String ?? ""
    }
}

class DictTestReasons : NSObject {
    
    var _id = ""
    var created = ""
    var reasonOfTest = ""
    
    init(dict : [String:Any]) {
        
        reasonOfTest = dict["reasonOfTest"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        created = dict["created"] as? String ?? ""
    }
}

class DictTestType : NSObject {
    
    var __v = ""
    var _id = ""
    var created = ""
    var typeOfTest = ""
    
    init(dict : [String:Any]) {
        
        typeOfTest = dict["typeOfTest"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        __v = dict["__v"] as? String ?? ""
        created = dict["created"] as? String ?? ""
    }
}
