//
//  OTP.swift
//  Tbstar
//
//  Created by Viprak-Heena on 09/02/21.
//

import Foundation

class OTP: NSObject {
    
    var verificationOtp = ""
    var phoneNumber = ""
    var expire = ""
    var deviceId = ""
    var deviceToken = ""
    var deviceType = ""
    var validationStatus = ""
    var _id = ""
    var userType = ""
    var created = ""
    var updated = ""
    var __v = ""
    
    init(dict : [String : Any]) {
        
        verificationOtp = dict["verificationOtp"] as? String ?? ""
        phoneNumber = dict ["phoneNumber"] as? String ?? ""
        expire = dict["expire"] as? String ?? ""
        deviceId = dict["deviceId"] as? String ?? ""
        deviceToken = dict["deviceToken"] as? String ?? ""
        deviceType = dict["deviceType"] as? String ?? ""
        validationStatus = dict["validationStatus"] as? String ?? ""
        _id = dict["_id"] as? String ?? ""
        userType = dict["userType"] as? String ?? ""
        created = dict["created"] as? String ?? ""
        updated = dict["updated"] as? String ?? ""
        __v = dict["__v"] as? String ?? ""
        
    }
}
