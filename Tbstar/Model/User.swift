//
//  User.swift
//  Tbstar
//
//  Created by Apple on 15/02/21.
//

import Foundation

class User: NSObject,NSCoding{
    
   // static var supportsSecureCoding: Bool
    
    //static var supportsSecureCoding: Bool
    
    var accessToken = ""
    var userId = ""
    var userType = ""
    
    var phoneNumber = ""
    var specimenId = [String]()
    var activeStatus = false
    var termsConditionStatus = false
    var verificationStatus = false
    var __v = 0
    var firstName = ""
    var pasword = ""
    var deviceTokenForNotification = ""
    var email = ""
    var surName = ""
    var patientId = [String]()
    var roleId = ""
    var refPatientId = [String]()
    var networkOfficerAssignedFacilityIds = [String]()
    var deviceToken = ""
    var createDate = ""
    var deviceId = ""
    
    var designation = ""
    var middleName = ""
    var sex = ""
    var secondPhoneNumber = ""
    var facilityType = ""
    var facilityName = ""
    var address = ""
    var facilityCode = ""
    var state = ""
    var lga = ""
    var apkVersion = ""
    
    /*func supportsSecureCoding()->Bool{
        return true
    }*/
    
    init(dict : [String:Any]) {
        
        accessToken = dict["accessToken"] as? String ?? ""
        userId = dict["_id"] as? String ??  dict["userId"] as? String ?? ""
        userType = dict["userType"] as? String ?? ""
        phoneNumber = dict["phoneNumber"] as? String ?? ""
        specimenId = dict["specimenId"] as? [String] ?? []
        activeStatus = dict["activeStatus"] as? Bool ?? false
        termsConditionStatus = dict["termsConditionStatus"] as? Bool ?? false
        verificationStatus = dict["verificationStatus"] as? Bool ?? false
        __v = dict["__v"] as? Int ?? 0
        firstName = dict["firstName"] as? String ?? ""
        pasword = dict["password"] as? String ?? ""
        deviceTokenForNotification = dict["deviceTokenForNotification"] as? String ?? ""
        email = dict["email"] as? String ?? ""
        surName = dict["surName"] as? String ?? ""
        patientId = dict["patientId"] as? [String] ?? []
        roleId = dict["roleId"] as? String ?? ""
        refPatientId = dict["refPatientId"] as? [String] ?? []
        networkOfficerAssignedFacilityIds = dict["networkOfficerAssignedFacilityIds"] as? [String] ?? []
        deviceToken = dict["deviceToken"] as? String ?? ""
        createDate = dict["createDate"] as? String ?? ""
        deviceId = dict["deviceId"] as? String ?? ""
        
        designation = dict["designation"] as? String ?? ""
        middleName = dict["middleName"] as? String ?? ""
        sex = dict["sex"] as? String ?? ""
        secondPhoneNumber = dict["secondPhoneNumber"] as? String ?? ""
        facilityType = dict["facilityType"] as? String ?? ""
        facilityName = dict["facilityName"] as? String ?? ""
        address = dict["address"] as? String ?? ""
        facilityCode = dict["facilityCode"] as? String ?? dict["faccode"] as? String ?? ""
        state = dict["state"] as? String ?? ""
        lga = dict["lga"] as? String ?? ""
        apkVersion = dict["apkVersion"] as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(accessToken, forKey: "accessToken")
        coder.encode(userId, forKey: "userId")
        coder.encode(userType, forKey: "userType")
        
        coder.encode(phoneNumber, forKey: "phoneNumber")
        coder.encode(specimenId, forKey: "specimenId")
        coder.encode(activeStatus, forKey: "activeStatus")
        coder.encode(termsConditionStatus, forKey: "termsConditionStatus")
        coder.encode(verificationStatus, forKey: "verificationStatus")
        coder.encode(__v, forKey: "__v")
        coder.encode(firstName, forKey: "firstName")
        coder.encode(pasword, forKey: "password")
        coder.encode(deviceTokenForNotification, forKey: "deviceTokenForNotification")
        coder.encode(email, forKey: "email")
        coder.encode(surName, forKey: "surName")
        coder.encode(patientId, forKey: "patientId")
        coder.encode(roleId, forKey: "roleId")
        coder.encode(refPatientId, forKey: "refPatientId")
        coder.encode(networkOfficerAssignedFacilityIds, forKey: "networkOfficerAssignedFacilityIds")
        coder.encode(deviceToken, forKey: "deviceToken")
        coder.encode(createDate, forKey: "createDate")
        coder.encode(deviceId, forKey: "deviceId")
        
        coder.encode(designation, forKey: "designation")
        coder.encode(middleName, forKey: "middleName")
        coder.encode(sex, forKey: "sex")
        coder.encode(secondPhoneNumber, forKey: "secondPhoneNumber")
        coder.encode(facilityType, forKey: "facilityType")
        coder.encode(facilityName, forKey: "facilityName")
        coder.encode(address, forKey: "address")
        coder.encode(facilityCode, forKey: "facilityCode")
        coder.encode(state, forKey: "state")
        coder.encode(lga, forKey: "lga")
        coder.encode(apkVersion, forKey: "apkVersion")

    }
    
    required init?(coder: NSCoder) {
        userId = coder.decodeObject(forKey: "userId") as? String ?? ""
        accessToken = coder.decodeObject(forKey: "accessToken") as? String ?? ""
        userType = coder.decodeObject(forKey: "userType") as? String ?? ""
        
        phoneNumber = coder.decodeObject(forKey: "phoneNumber") as? String ?? ""
        specimenId = coder.decodeObject(forKey: "specimenId") as? [String] ?? []
        activeStatus = coder.decodeObject(forKey: "activeStatus") as? Bool ?? false
        termsConditionStatus = coder.decodeObject(forKey: "termsConditionStatus") as? Bool ?? false
        verificationStatus = coder.decodeObject(forKey: "verificationStatus") as? Bool ?? false
        __v = coder.decodeObject(forKey: "__v") as? Int ?? 0
        firstName = coder.decodeObject(forKey: "firstName") as? String ?? ""
        pasword = coder.decodeObject(forKey: "password") as? String ?? ""
        deviceTokenForNotification = coder.decodeObject(forKey: "deviceTokenForNotification") as? String ?? ""
        email = coder.decodeObject(forKey: "email") as? String ?? ""
        surName = coder.decodeObject(forKey: "surName") as? String ?? ""
        roleId = coder.decodeObject(forKey: "roleId") as? String ?? ""
        refPatientId = coder.decodeObject(forKey: "refPatientId") as? [String] ?? []
        networkOfficerAssignedFacilityIds = coder.decodeObject(forKey: "networkOfficerAssignedFacilityIds") as? [String] ?? []
        deviceToken = coder.decodeObject(forKey: "deviceToken") as? String ?? ""
        createDate = coder.decodeObject(forKey: "createDate") as? String ?? ""
        deviceId = coder.decodeObject(forKey: "deviceId") as? String ?? ""
        
        designation = coder.decodeObject(forKey: "designation") as? String ?? ""
        middleName = coder.decodeObject(forKey: "middleName") as? String ?? ""
        sex = coder.decodeObject(forKey: "sex") as? String ?? ""
        secondPhoneNumber = coder.decodeObject(forKey: "secondPhoneNumber") as? String ?? ""
        facilityType = coder.decodeObject(forKey: "facilityType") as? String ?? ""
        facilityName = coder.decodeObject(forKey: "facilityName") as? String ?? ""
        address = coder.decodeObject(forKey: "address") as? String ?? ""
        facilityCode = coder.decodeObject(forKey: "facilityCode") as? String ?? ""
        state = coder.decodeObject(forKey: "state") as? String ?? ""
        lga = coder.decodeObject(forKey: "lga") as? String ?? ""
        apkVersion = coder.decodeObject(forKey: "apkVersion") as? String ?? ""
        
    }
}
