//
//  PPMV_Classes.swift
//  Tbstar
//
//  Created by Mavani on 11/06/21.
//

import Foundation

class PPMV_PatientInformation : NSObject {
    
    var patientTBSymptoms : PatientTBSymptoms?
    var patientCovidSymptoms : PatientCovidSymptoms?
    var CollectSampleID = [CollectSampleDetailsId]()
    var contact = [Contacts]()
    var referPatitentsId = [ReferPatitentsId]()
    var specimenDetails = [SpecimenDetails]()
    
    var firstName = ""
    var middleName = ""
    var surname = ""
    var phoneNumber = ""
    var secondPhoneNumber = ""
    var address = ""
    var HasTBTag = ""
    var visitDate = ""
    var dateOfBirth = ""
    var age = ""
    var sex = ""
    var lga = ""
    var state = ""
    var hadTBbefore = ""
    var hivStatus = ""
    var typeOfPresumptiveTbCase = ""
    var siteOfDisease = ""
    var patientWorker = ""
    var patientRecordNumber = ""
    var _id = ""
    var userId = ""
    var patientStatusId = ""
    var patientStatusLabel = ""
    var createDate = ""
    var __v = 0
    var lattitude = 0.0
    var longitude = 0.0
    var offlineId =  0
    var currentStatus = 0;
    var patientReferredInPersonToFacilityId = ""
    var labID = ""
    var patientReferredInPersonToFacilityName = ""
    var referForDots = 0
    var referForTreatment = 0
    var screenedAtFacilityName = ""
    var screenedAtFacilityId = ""
    var hasPhysicalResult = false
    var note = ""
    
    init(dict : [String : Any]) {
                
        __v = dict["__v"] as? Int ?? 0
        _id = dict["_id"] as? String ?? ""
        createDate = dict["createDate"] as? String ?? ""
        currentStatus = dict["currentStatus"] as? Int ?? 0
        userId = dict["userId"] as? String ?? ""
        patientStatusId = dict["patientStatusId"] as? String ?? ""
        patientStatusLabel = dict["patientStatusLabel"] as? String ?? ""
        patientReferredInPersonToFacilityId = dict["patientReferredInPersonToFacilityId"] as? String ?? ""
        patientReferredInPersonToFacilityName = dict["patientReferredInPersonToFacilityName"] as? String ?? ""
        patientStatusId = dict["patientStatusId"] as? String ?? ""
        patientStatusLabel = dict["patientStatusLabel"] as? String ?? ""
        referForDots = dict["referForDots"] as? Int ?? 0
        referForTreatment = dict["referForTreatment"] as? Int ?? 0
        screenedAtFacilityId = dict["screenedAtFacilityId"] as? String ?? ""
        screenedAtFacilityName = dict["screenedAtFacilityName"] as? String ?? ""

        if dict.count > 0, let collectsampleID =  dict["collectSampleDetailsId"] as? [[String:Any]] {
            CollectSampleID = collectsampleID.map(CollectSampleDetailsId.init)
        }

        if let contacts = dict["contacts"] as? [[String:Any]],
           dict.count > 0{
            contact = contacts.map(Contacts.init)
        }
        
        if let referId = dict["referPatitentsId"] as? [[String:Any]],
           dict.count > 0{
            referPatitentsId = referId.map(ReferPatitentsId.init)
        }

        if let patientDetail  = dict["patientDetails"] as? [String : Any] {
            
            if let patientSymptoms = patientDetail["patientSymptoms"] as? [String:Any],
               patientSymptoms.count > 0 {
                patientTBSymptoms = PatientTBSymptoms(dict: patientSymptoms)
            }
            
            lattitude = patientDetail["latitude"] as? Double ?? 0.0
            longitude = patientDetail["longitude"] as? Double ?? 0.0
            
            if let covidSymptoms = patientDetail["covidSymptoms"] as? [String:Any], covidSymptoms.count > 0 {
                patientCovidSymptoms = PatientCovidSymptoms(dict: covidSymptoms)
            }
            
            if let clientHasTB = patientDetail["clientHasTB"] as? [String : Any]{
                
                patientRecordNumber = clientHasTB["patientRecordNumber"] as? String ?? ""
                
                if let specimendetails = clientHasTB["specimenDetails"] as? [[String:Any]],
                   clientHasTB.count > 0{
                    specimenDetails = specimendetails.map(SpecimenDetails.init)
                }

                if let infoDict = clientHasTB["patientInformation"] as? [String :Any] {
                    
                    address = infoDict["address"] as? String ?? ""
                    age = infoDict["age"] as? String ?? ""
                    secondPhoneNumber = infoDict["alternatePhoneNumber"] as? String ?? ""
                    dateOfBirth = infoDict["dateOfBirth"] as? String ?? ""
                    firstName = infoDict["firstName"] as? String ?? ""
                    hasPhysicalResult = infoDict["hasPhysicalResult"] as? Bool ?? false
                    labID = infoDict["labId"] as? String ?? ""
                    surname = infoDict["lastName"] as? String ?? ""
                    lga = infoDict["lga"] as? String ?? ""
                    middleName = infoDict["middleName"] as? String ?? ""
                    note = infoDict["note"] as? String ?? ""
                    patientWorker = infoDict["patientWorker"] as? String ?? ""
                    phoneNumber = infoDict["phoneNumber"] as? String ?? ""
                    sex = infoDict["sex"] as? String ?? ""
                    siteOfDisease = infoDict["siteOfDisease"] as? String ?? ""
                    state = infoDict["state"] as? String ?? ""
                    hivStatus = infoDict["hivStatus"] as? String ?? ""
                    typeOfPresumptiveTbCase = infoDict["typeOfPresumptiveTbCase"] as? String ?? ""
                    visitDate =  infoDict["visitDate"] as? String ?? ""
                }
            }
        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(CollectSampleID, forKey: "collectSampleDetailsId")
        coder.encode(contact, forKey: "contacts")
        coder.encode(referPatitentsId, forKey: "referPatitentsId")
        coder.encode(specimenDetails, forKey: "specimenDetails")
    }
    
    required init?(coder: NSCoder) {
        CollectSampleID = coder.decodeObject(forKey: "collectSampleDetailsId") as? [CollectSampleDetailsId] ?? [CollectSampleDetailsId]()
        contact = coder.decodeObject(forKey: "contacts") as? [Contacts] ?? [Contacts]()
        referPatitentsId = coder.decodeObject(forKey: "referPatitentsId") as? [ReferPatitentsId] ?? [ReferPatitentsId]()
        specimenDetails = coder.decodeObject(forKey: "specimenDetails") as? [SpecimenDetails] ?? [SpecimenDetails]()
    }
}

class CollectSampleDetailsId : NSObject {
        
    init(dict : [String:Any]) {
        
    }
}

class Contacts : NSObject {
        
    init(dict : [String:Any]) {
        
    }
}

class ReferPatitentsId : NSObject {
        
    init(dict : [String:Any]) {
        
    }
}

class SpecimenDetails : NSObject {
    
    var _id = ""
    var acceptUpdateStatus = 0
    var dateOfCollection = ""
    var dateOfDispatch = ""
    var hivStatus = ""
    var labId = ""
    var laboratoryName = ""
    var nameOfCollectOffcer = ""
    var numberOfSample = 0
    var previousTreatmentForTb = ""
    var reasonOfExamination = ""
    var sampleActionDate = ""
    var sampleExternalId = ""
    var specimenId = ""
    var status = 0
    var temperature = ""
    var typeOfTest = ""
    var typesOfSpecimen = ""
    var nameOfCollectOffcerPhNo = ""

    init(dict : [String:Any]) {
        
        _id = dict["_id"] as? String ?? ""
        acceptUpdateStatus = dict["acceptUpdateStatus"] as? Int ?? 0
        dateOfCollection = dict["dateOfCollection"] as? String ?? ""
        dateOfDispatch = dict["dateOfDispatch"] as? String ?? ""
        hivStatus = dict["hivStatus"] as? String ?? ""
        labId = dict["labId"] as? String ?? ""
        laboratoryName = dict["laboratoryName"] as? String ?? ""
        nameOfCollectOffcer = dict["nameOfCollectOffcer"] as? String ?? ""
        numberOfSample = dict["numberOfSample"] as? Int ?? 0
        previousTreatmentForTb = dict["previousTreatmentForTb"] as? String ?? ""
        reasonOfExamination = dict["reasonOfExamination"] as? String ?? ""
        sampleActionDate = dict["sampleActionDate"] as? String ?? ""
        sampleExternalId = dict["sampleExternalId"] as? String ?? ""
        specimenId = dict["specimenId"] as? String ?? ""
        status = dict["status"] as? Int ?? 0
        temperature = dict["temperature"] as? String ?? ""
        typeOfTest = dict["typeOfTest"] as? String ?? ""
        typesOfSpecimen = dict["typesOfSpecimen"] as? String ?? ""
        nameOfCollectOffcerPhNo = dict["nameOfCollectOffcerPhNo"]  as? String ?? ""
    }
}


class PUSHNotification : NSObject {
    
    var __v = 0
    var _id = ""
    var createDate = ""
    var pushNotificationBody = ""
    var pushNotificationTitle = ""
    var pushType = ""
    var pushed = 0
    var referredby = ""
    var referredtowhom : NSArray = NSArray()
    var whoisreferred = ""

    init(dict : [String:Any]) {
        
        __v = dict["__v"] as? Int ?? 0
        _id = dict["_id"] as? String ?? ""
        createDate = dict["createDate"] as? String ?? ""
        pushNotificationBody = dict["pushNotificationBody"] as? String ?? ""
        pushNotificationTitle = dict["pushNotificationTitle"] as? String ?? ""
        pushType = dict["pushType"] as? String ?? ""
        referredby = dict["referredby"] as? String ?? ""
        whoisreferred = dict["whoisreferred"] as? String ?? ""
        referredtowhom = dict["referredtowhom"] as? NSArray ?? []
        pushed = dict["pushed"] as? Int ?? 0
    }

}


class PPMV_HighPriority_Alerts_Data {
    
    var contact = [Contacts]()
    var sampledetails = [PPMV_SampleDetails]()
    
    var age = 0
    var belongsToNetwork = 0
    var covidPresumptive = 0
    var referredForSampleCollection = 0
    var screenedForCovid = 0
    var stateId = 0
    var tested = 0
    var processStatusCode = 0
    var hasStartedTreatment = 0

    var currentStatus = ""
    var dateConfirmedPositive = ""
    var dateOfBirth = ""
    var dateScreened = ""
    var facilityPhoneNumber = ""
    var facilityReferredToId = ""
    var facilityScreenedAtId = ""
    var facilityScreenedAtName = ""
    var facilityType = ""
    var firstName = ""
    var gender = ""
    var id = ""
    var lastName = ""
    var lgaId = ""
    var lgaName = ""
    var otherName = ""
    var phoneNumber = ""
    var recordNumber = ""
    var stateName = ""
    var status = ""
    var treatmentReferralDate = ""
    var xpertDateTested = ""
    var xpertTestTypeResult = ""
    
    
    init(dict : [String:Any]) {

        if let contacts = dict["contacts"] as? [[String:Any]],
           dict.count > 0{
            contact = contacts.map(Contacts.init)
        }
        
        if let referId = dict["sampleDetails"] as? [[String:Any]],
           dict.count > 0{
            sampledetails = referId.map(PPMV_SampleDetails.init)
        }

        age = dict["age"] as? Int ?? 0
        belongsToNetwork = dict["belongsToNetwork"] as? Int ?? 0
        covidPresumptive = dict["covidPresumptive"] as? Int ?? 0
        referredForSampleCollection = dict["referredForSampleCollection"] as? Int ?? 0
        screenedForCovid = dict["screenedForCovid"] as? Int ?? 0
        stateId = dict["stateId"] as? Int ?? 0
        tested = dict["tested"] as? Int ?? 0
        processStatusCode = dict["processStatusCode"] as? Int ?? 0
        hasStartedTreatment = dict["hasStartedTreatment"] as? Int ?? 0
        currentStatus = dict["currentStatus"] as? String ?? ""
        dateConfirmedPositive = dict["dateConfirmedPositive"] as? String ?? ""
        dateOfBirth = dict["dateOfBirth"] as? String ?? ""
        dateScreened = dict["dateScreened"] as? String ?? ""
        facilityPhoneNumber = dict["facilityPhoneNumber"] as? String ?? ""
        facilityReferredToId = dict["facilityReferredToId"] as? String ?? ""
        facilityScreenedAtId = dict["facilityScreenedAtId"] as? String ?? ""
        facilityScreenedAtName = dict["facilityScreenedAtName"] as? String ?? ""
        facilityType = dict["facilityType"] as? String ?? ""
        firstName = dict["firstName"] as? String ?? ""
        gender = dict["gender"] as? String ?? ""
        id = dict["id"] as? String ?? ""
        lastName = dict["lastName"] as? String ?? ""
        lgaId = dict["lgaId"] as? String ?? ""
        lgaName = dict["lgaName"] as? String ?? ""
        otherName = dict["otherName"] as? String ?? ""
        phoneNumber = dict["phoneNumber"] as? String ?? ""
        recordNumber = dict["recordNumber"] as? String ?? ""
        stateName = dict["stateName"] as? String ?? ""
        status = dict["status"] as? String ?? ""
        treatmentReferralDate = dict["treatmentReferralDate"] as? String ?? ""
        xpertDateTested = dict["xpertDateTested"] as? String ?? ""
        xpertTestTypeResult = dict["xpertTestTypeResult"] as? String ?? ""

    }

    func encode(with coder: NSCoder) {
        coder.encode(sampledetails, forKey: "sampleDetails")
        coder.encode(contact, forKey: "contacts")
    }
    
    required init?(coder: NSCoder) {
        sampledetails = coder.decodeObject(forKey: "sampleDetails") as? [PPMV_SampleDetails] ?? [PPMV_SampleDetails]()
        contact = coder.decodeObject(forKey: "contacts") as? [Contacts] ?? [Contacts]()
    }
}


class PPMV_SampleDetails : NSObject {
    
    var dateOfCollection = ""
    var status = 0
    var dateOfDispatch = ""
    var dispatchPerson = ""
    var dispatchPersonPhoneNumber = ""
    var labId = ""
    var labLgaName = ""
    var labName = ""
    var labLgaId = 0
    var labPhoneNumber = ""
    var labStateName = ""
    var patientId = ""
    var sampleId = ""
    var sampleType = ""
    var labStateId = 0
    var testType = ""

    init(dict : [String:Any]) {
        
        status = dict["status"] as? Int ?? 0
        labStateId = dict["labStateId"] as? Int ?? 0
        labLgaId = dict["labLgaId"] as? Int ?? 0
        labStateId = dict["labStateId"] as? Int ?? 0
        dateOfCollection = dict["dateOfCollection"] as? String ?? ""
        dateOfDispatch = dict["dateOfDispatch"] as? String ?? ""
        dispatchPerson = dict["dispatchPerson"] as? String ?? ""
        labId = dict["labId"] as? String ?? ""
        dispatchPersonPhoneNumber = dict["dispatchPersonPhoneNumber"] as? String ?? ""
        labLgaName = dict["labLgaName"] as? String ?? ""
        labName = dict["labName"] as? String ?? ""
        labPhoneNumber = dict["labPhoneNumber"] as? String ?? ""
        labStateName = dict["labStateName"] as? String ?? ""
        patientId = dict["patientId"] as? String ?? ""
        sampleId = dict["sampleId"] as? String ?? ""
        sampleType = dict["sampleType"] as? String ?? ""
        testType = dict["testType"] as? String ?? ""
    }
    
}
