//
//  Facility.swift
//  Tbstar
//
//  Created by Viprak-Heena on 08/02/21.
//

import Foundation

class Facility: NSObject {
    
    var LGA = ""
    var id = ""
    var activeStatus = false
    var address = ""
    var dotsAvailability = 0
    var facilityName = ""
    var facilityState = ""
    var facilityType = ""
    var phoneNumber = 00
    var refPatientId : [String]?
    var specimenId : [String]?
    var userId : [String]?
    var userTyep = ""
    
    init(dict :[String:Any]) {
        LGA = dict["LGA"] as? String ?? ""
        id = dict["_id"] as? String ?? ""
        activeStatus = dict["activeStatus"] as? Bool ?? false
        address = dict["address"] as? String ?? ""
        dotsAvailability = dict["dotsAvailability"] as? Int ?? 0
        facilityName = dict["facilityName"] as? String ?? ""
        facilityState = dict["facilityState"] as? String ?? ""
        facilityType = dict["facilityType"] as? String ?? ""
        phoneNumber = dict["phoneNumber"] as? Int ?? 0
        refPatientId = dict["refPatientId"] as? [String]
        specimenId = dict["specimenId"] as? [String]
        userId = dict["userId"] as? [String]
        userTyep = dict["userType"] as? String ?? ""
    }
}
