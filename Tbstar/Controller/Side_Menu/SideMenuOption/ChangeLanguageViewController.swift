//
//  ChangeLanguageViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 23/03/21.
//

import UIKit

protocol ChangeLanguageDelegate {
    func languageChanged()
}

class ChangeLanguageViewController: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var lblHausa: UILabel!
    
    @IBOutlet weak var switchEnglish: UISwitch!
    @IBOutlet weak var switchHausa: UISwitch!
    
    var delegate : ChangeLanguageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        setSelectedLanguage()
    }
    func setupLocalization() {
        
        lblTitle.text = "changeLang".localized
        lblSubtitle.text = "langnote".localized
        
        lblEnglish.text = "eng".localized
        lblHausa.text = "hau".localized
        
        btnSave.setTitle("save".localized, for: .normal)
    }
    
    func setSelectedLanguage()  {
        if let langstr = Defaultss.value(forKey: UDKey.kAppLang) as? String,
           let lang = AppLanguage(rawValue: langstr){
            currentLang = lang
            switchEnglish.isOn = lang == AppLanguage.English ? true : false
            switchHausa.isOn = lang == AppLanguage.Hausa ? true : false
        }else{
            switchEnglish.isOn = currentLang == AppLanguage.English ? true : false
            switchHausa.isOn = currentLang == AppLanguage.Hausa ? true : false
        }
    }
    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSaveTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        Defaultss.setValue(currentLang.rawValue, forKey: UDKey.kAppLang)
        Defaultss.synchronize()
        delegate?.languageChanged()
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "LangChanged"), object: nil, userInfo: nil))
        dismiss(animated: true, completion: nil)
    }
    @IBAction func swtichValueChanged(_ sender: UISwitch) {
        if sender == switchEnglish{
            if sender.isOn{
                currentLang = AppLanguage.English
                switchHausa.isOn = false
            }else{
                currentLang = AppLanguage.Hausa
                switchHausa.isOn = true
            }
        }
        if sender == switchHausa{
            if sender.isOn{
                currentLang = AppLanguage.Hausa
                switchEnglish.isOn = false
            }else{
                currentLang = AppLanguage.English
                switchEnglish.isOn = true
            }
        }
    }
    
}
