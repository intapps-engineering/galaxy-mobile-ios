//
//  SupportNFeedbackViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 23/03/21.
//

import UIKit

class SupportNFeedbackViewController: UIViewController {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSupportDesc: UILabel!
    
    @IBOutlet weak var btnFeedback: UIButton!
    @IBOutlet weak var btnTermsNFeedback: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
    }
    func setupLocalization() {
        
        lblTitle.text = "supportnfeed".localized
        lblSupportDesc.text = "supportDesc".localized
    
        btnFeedback.setTitle("feedback".localized, for: .normal)
        btnTermsNFeedback.setTitle("termsNcon".localized, for: .normal)
        btnClose.setTitle("close".localized, for: .normal)
    }
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDownloadTap(_ sender: Any) {
        guard let url = URL(string: "https://tbstarr.org/ip_man.pdf") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func btnFeedbackTap(_ sender: Any) {
        if let feedbackVC = self.storyboard?.instantiateViewController(withIdentifier: "UserFeedbackViewController") as? UserFeedbackViewController{
            navigationController?.pushViewController(feedbackVC, animated: true)
        }
    }
    
    @IBAction func btnTermsNFeedbackTap(_ sender: Any) {
        if let aggrementVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackAggrementViewController") as? FeedbackAggrementViewController{
            present(aggrementVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnCloseTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
}
