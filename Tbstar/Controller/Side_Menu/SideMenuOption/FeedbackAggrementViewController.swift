//
//  FeedbackAggrementViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 24/03/21.
//

import UIKit
import WebKit

class FeedbackAggrementViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        
        /*if let urlString = independentSupportUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            if let url = URL(string: urlString){
                let request = URLRequest(url: url)
                webView.load(request)
            }
        }*/
        
        if let url = Bundle.main.url(forResource: "feedback", withExtension: "html"){
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
    }
    
    @IBAction func btnOkayTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
extension FeedbackAggrementViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
    }
}
