//
//  ChangePasswordViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 23/03/21.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var txtOldPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNewPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCnfrmNewPswd: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblNotes: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()
        setupLocalization()
    }
    func setupLocalization() {
        
        lblTitle.text = "changePswd".localized
        lblNotes.text = "changePswdInst".localized
        
        txtOldPswd.placeholder = "oldPswd".localized
        txtOldPswd.selectedTitle = "oldPswd".localized
        txtOldPswd.title = "oldPswd".localized
        
        txtNewPswd.placeholder = "newPswd".localized
        txtNewPswd.selectedTitle = "newPswd".localized
        txtNewPswd.title = "newPswd".localized
        
        txtCnfrmNewPswd.placeholder = "cnfmPswd".localized
        txtCnfrmNewPswd.selectedTitle = "cnfmPswd".localized
        txtCnfrmNewPswd.title = "cnfmPswd".localized
        
        btnSave.setTitle("save".localized, for: .normal)
    }
    func setupTextField()  {
        
        txtOldPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtOldPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtOldPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtOldPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtNewPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtNewPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtNewPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtNewPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtCnfrmNewPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmNewPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmNewPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmNewPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    func isDetailValid() -> Bool  {
        
        if let pswd = txtOldPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.eneterOLDPswd)
            return false
        }else{
            if !isValidPassword(password: txtOldPswd.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if let pswd = txtNewPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterNewPswd)
            return false
        }else{
            if !isValidPassword(password: txtNewPswd.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if let pswd = txtCnfrmNewPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.eneterOLDPswd)
            return false
        }else{
            if !isValidPassword(password: txtCnfrmNewPswd.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if txtNewPswd.text! != txtCnfrmNewPswd.text{
            showAlert(titleStr: appName, msg: appMessages.pswdShouldBeSame)
            return false
        }
        return true
    }
    
    @IBAction func btnOldPswdEyeTap(_ sender: UIButton) {
        txtOldPswd.isSecureTextEntry = !txtOldPswd.isSecureTextEntry
        sender.isSelected = !txtOldPswd.isSecureTextEntry
    }
    
    @IBAction func btnNewPswdEyeTap(_ sender: UIButton) {
        txtNewPswd.isSecureTextEntry = !txtNewPswd.isSecureTextEntry
        sender.isSelected = !txtNewPswd.isSecureTextEntry
    }
    
    @IBAction func btnCnfmPswdEyeTap(_ sender: UIButton) {
        txtCnfrmNewPswd.isSecureTextEntry = !txtCnfrmNewPswd.isSecureTextEntry
        sender.isSelected = !txtCnfrmNewPswd.isSecureTextEntry
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    func logout()  {
        
        if isConnectedToNetwork(){
            showHUD()
            let user = getUserDataFromStandard()
            let paraDict:[String:Any] = ["deviceToken" : user?.deviceToken ?? ""]
            
            logOutAPI(paradict: paraDict, accessToken: user?.accessToken ?? "") { (success, strData, message) in
                self.hideHUD()
                if success{
                    self.logOutApplication()
                }else if message != ""{
                    self.view.makeToast(message)
                }
                else{
                    self.view.makeToast(appMessages.somethingWrong)
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    @IBAction func btnSaveTap(_ sender: Any) {
        
        if isDetailValid() {
            
            if isConnectedToNetwork(){
                showHUD()
                let user = getUserDataFromStandard()
                let param : [String : Any] = ["deviceToken" : user?.deviceToken ?? "",
                                              "password" : txtOldPswd.text ?? "",
                                              "newPassword" : txtNewPswd.text ?? "",
                                              "confirmPassword" : txtCnfrmNewPswd.text ?? ""]
                
                changePasswordAPI(paradict: param) { (success, message) in
                    self.hideHUD()
                    self.view.makeToast(message)
                    if success{
                        self.logout()
                    }
                }
            }else {
                view.makeToast(appMessages.noConnection)
            }
        }
        
    }
}
