//
//  UserFeedbackViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 24/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class UserFeedbackViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSubject: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textViewBody: UITextView!
    @IBOutlet weak var lblCharCount: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var arrFeedbackSubject = [FeedbackSubject]()
    let dropDown = DropDown()
    var selectedSubject : FeedbackSubject?
    var placeholderText = "body".localized + "*"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        setupTextFields()
        getFeedbackSubjects()
        textViewBody.delegate = self
        btnSubmit.isUserInteractionEnabled = false
        btnSubmit.backgroundColor = .darkGray
        textViewBody.text = placeholderText
        textViewBody.textColor = UIColor.lightGray
    }
    func setupLocalization() {
        
        lblTitle.text = "userFeedback".localized
        
        txtEmail.placeholder = "email".localized
        txtEmail.selectedTitle = "email".localized
        txtEmail.title = "email".localized
        
        txtSubject.placeholder = "subject".localized
        txtSubject.selectedTitle = "subject".localized
        txtSubject.title = "subject".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
        
    }
    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        if isDetailValid(){
            if isConnectedToNetwork() {
                showHUD()
                let paradict : [String : Any] = ["email" : txtEmail.text ?? "",
                                                 "subjectId" : selectedSubject?._id ?? "",
                                                 "feedbackBody" : textViewBody.text ?? ""]
                
                sendFeedbackAPI(paradict: paradict) { (success, message) in
                    self.hideHUD()
                    if success{
                        self.view.makeToast(message)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.view.makeToast(message)
                    }
                }
            }else{
                view.makeToast(appMessages.noConnection)
            }
        }
    }
    func isDetailValid() -> Bool {
        
        if let email = txtEmail.text, email.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.provideEmail)
            return false
        }
        if selectedSubject == nil{
            showAlert(titleStr: appName, msg: appMessages.selectSubject)
            return false
        }
        return true
    }
    func setupTextFields()  {
        txtEmail.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtEmail.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtEmail.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtEmail.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSubject.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSubject.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSubject.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSubject.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    func setupSubjectDropdown()  {
       
        dropDown.anchorView = txtSubject
        if arrFeedbackSubject.count  > 0{
            dropDown.dataSource = arrFeedbackSubject.map{($0.name)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtSubject.text = item
            selectedSubject = arrFeedbackSubject[index]
            dropDown.hide()
        }
    }
    func getFeedbackSubjects()  {
        
        if isConnectedToNetwork(){
            showHUD()
            getFeedbackSubject { (success, strData) in
                self.hideHUD()
                let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                if let json = self.convertStringToDictionary(text: strResponse){
                    if let arrJson = json["feedbackSubjects"] as? [[String:Any]] {
                        self.arrFeedbackSubject = arrJson.map(FeedbackSubject.init)
                    }
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
}
extension UserFeedbackViewController : UITextFieldDelegate, UITextViewDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtSubject{
            setupSubjectDropdown()
            return false
        }
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = ""
            textView.textColor = txtEmail.textColor
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "body".localized + "*"
            textView.textColor = UIColor.lightGray
            placeholderText = ""
        } else {
            placeholderText = textView.text
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderText = textView.text
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        btnSubmit.isUserInteractionEnabled = numberOfChars <= 9 ? false : true
        btnSubmit.backgroundColor = numberOfChars <= 9 ? UIColor.darkGray : UIColor(hexString: color.appMainBlueColor)
        lblCharCount.text = "\(numberOfChars)/300"
        return true
    }
    
}
