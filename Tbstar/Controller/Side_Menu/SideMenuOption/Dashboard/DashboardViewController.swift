//
//  DashboardViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 25/03/21.
//

import UIKit

class DashboardViewController: BaseViewController {

    @IBOutlet weak var tblvDashboard: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblThisMnth: UILabel!
    @IBOutlet weak var switchMnth: UISwitch!
    @IBOutlet weak var lblShowData: UILabel!
    
    var dashboardDict = [String:Any]()
    var patientCount = 0
    
    override func viewWillAppear(_ animated: Bool) {
        lblTitle.text = "dashboard".localized
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        showHUD()
        
        getDashboardListScreenAPI { (success, strData) in
            self.hideHUD()
            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
            if let json = self.convertStringToDictionary(text: strResponse){
                print(json)
                if let dict = json["dashboard_result"] as? [String : Any] {
                    self.dashboardDict = dict
                }
                else{
                    
                    
                }
            }
            else{
                
            }
            self.tblvDashboard.reloadData()
        }
    }
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    
}
extension DashboardViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dashboardDict.keys.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardCell") as? DashboardCell{
            cell.selectionStyle = .none
            if indexPath.section == 0{
                
                cell.lblTitle.text = "patientScreened".localized
                cell.lblCount.text = "\(dashboardDict["patients_screened"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "patients_screened")
            }else if indexPath.section == 1{
                cell.lblTitle.text = "presumptivePatient".localized
                cell.lblCount.text = "\(dashboardDict["presumptive_patients"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "presumptive_patients")
            }else if indexPath.section == 2 {
                cell.lblTitle.text = "confirmCase".localized
                cell.lblCount.text = "\(dashboardDict["confirmed_TB_cases"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "confirmed_tb_cases")
            }else if indexPath.section == 3 {
                cell.lblTitle.text = "onTreatment".localized
                cell.lblCount.text = "\(dashboardDict["patients_on_treatment"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "patients_on_treatment")
            }else if indexPath.section == 4 {
                cell.lblTitle.text = "treatmentCompleted".localized
                cell.lblCount.text = "\(dashboardDict["treatment_completed"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "treatment_complete")
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let detailvc = storyboard?.instantiateViewController(withIdentifier: "DashboardDetailViewController") as? DashboardDetailViewController{
            
            if indexPath.section == 0 {
                detailvc.screenTitle = "patientScreened".localized
                detailvc.apiUrl = patientScreenedUrl
                patientCount = dashboardDict["patients_screened"] as? Int ?? 0
            }else if indexPath.section == 1{
                detailvc.screenTitle = "presumptivePatient".localized
                detailvc.apiUrl = presumptivePatientUrl
                patientCount = dashboardDict["presumptive_patients"] as? Int ?? 0
            }else if indexPath.section == 2 {
                detailvc.screenTitle = "confirmCase".localized
                detailvc.apiUrl = confirmedPatientUrl
                patientCount = dashboardDict["confirmed_TB_cases"] as? Int ?? 0
            }else if indexPath.section == 3 {
                detailvc.screenTitle = "onTreatment".localized
                detailvc.apiUrl = patientOnTreatmentUrl
                patientCount = dashboardDict["patients_on_treatment"] as? Int ?? 0
            }else if indexPath.section == 4 {
                detailvc.screenTitle = "treatmentCompleted".localized
                detailvc.apiUrl = treatmentCompletedUrl
                patientCount = dashboardDict["treatment_completed"] as? Int ?? 0
            }
            if patientCount > 0 {
                navigationController?.pushViewController(detailvc, animated: true)
            }
            
        }
    }
    
}
