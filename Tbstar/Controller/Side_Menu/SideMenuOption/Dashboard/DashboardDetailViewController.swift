//
//  DashboardDetailViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 31/03/21.
//

import UIKit

class DashboardDetailViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvList: UITableView!
    
    var apiUrl = ""
    var screenTitle = ""
    var pageIndex = 0
    var totalPages = 0
    var arrCaseList = [PatientInformation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = screenTitle
        getListOfPatients()
        
    }

    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getListOfPatients()  {
        
        if isConnectedToNetwork(){
            showHUD()
            
            let paradict : [String : Any]  = ["page" : 10,
                                              "pageIndex" : pageIndex]
            
            
            getPatientTreatedList(paradict: paradict, apiUrl: apiUrl) { (success, msg, strdata) in
                self.hideHUD()
                if success {
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                       print(json)
                        self.totalPages = json["totalPage"] as? Int ?? 0
                        if let list = json["patients"] as? [[String :Any]]{
                            if self.pageIndex == 0{
                                self.arrCaseList = list.map(PatientInformation.init)
                            }else{
                                self.arrCaseList.append(contentsOf: list.map(PatientInformation.init))
                            }
                        }
                    }
                }else{
                    if self.pageIndex == 0{
                        self.view.makeToast(msg)
                    }
                }
                self.tblvList.reloadData()
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
}
extension DashboardDetailViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCaseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //caseCell
        if let cell = tableView.dequeueReusableCell(withIdentifier: "caseCell") as? FindCaseCell{
            cell.setCellData(info: arrCaseList[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrCaseList.count - 1{
            if pageIndex < totalPages {
                pageIndex += 1
                getListOfPatients()
            }
        }
    }
    
}
