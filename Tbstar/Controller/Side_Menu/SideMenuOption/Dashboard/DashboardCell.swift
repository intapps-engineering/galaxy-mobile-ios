//
//  DashboardCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 25/03/21.
//

import UIKit

class DashboardCell: UITableViewCell {

    
    @IBOutlet weak var imgvCell: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

}
