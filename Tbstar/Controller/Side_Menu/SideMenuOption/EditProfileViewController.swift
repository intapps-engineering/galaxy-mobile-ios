//
//  EditProfileViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 22/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class EditProfileViewController: BaseViewController {

    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtGender: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobileNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMobno: SkyFloatingLabelTextField!
    
    var isEditMode = false
    var designationDetail : DesignationDetail?
    let dropdown = DropDown()
    var selectedGender = ""
    var loginUser : User?
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        setupTextField()
        getDetailForIndependentUser()
        
        if let loggedInUser = getUserDataFromStandard() {
            loginUser = loggedInUser
            txtFirstName.text = loggedInUser.firstName
            txtMiddleName.text = loggedInUser.middleName
            txtSurname.text = loggedInUser.surName
            txtDesignation.text = loggedInUser.designation
            txtAddress.text = loggedInUser.address
            txtGender.text = loggedInUser.sex
            selectedGender = loggedInUser.sex
            txtState.text = loggedInUser.state
            txtLGA.text = loggedInUser.lga
            txtMobileNo.text = "\(loggedInUser.phoneNumber)"
            txtAlternateMobno.text = "\(loggedInUser.secondPhoneNumber)"
            vwMain.isUserInteractionEnabled = false
        }
        
    }
    func setupLocalization() {
        lblTitle.text = "eProfile".localized
        
        txtFirstName.placeholder = "firstname".localized + "*"
        txtFirstName.selectedTitle = "firstname".localized + "*"
        txtFirstName.title = "firstname".localized + "*"
        
        txtMiddleName.placeholder = "middlename".localized
        txtMiddleName.selectedTitle = "middlename".localized
        txtMiddleName.title = "middlename".localized
        
        txtSurname.placeholder = "surname".localized + "*"
        txtSurname.selectedTitle = "surname".localized + "*"
        txtSurname.title = "surname".localized + "*"
        
        txtDesignation.placeholder = "designation".localized + "*"
        txtDesignation.selectedTitle = "designation".localized + "*"
        txtDesignation.title = "designation".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtGender.placeholder = "sex".localized  + " " + "optional".localized
        txtGender.selectedTitle = "sex".localized  + " " + "optional".localized
        txtGender.title = "sex".localized  + " " + "optional".localized
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
        
        txtMobileNo.placeholder = "primaryMobNo".localized + "*"
        txtMobileNo.selectedTitle = "primaryMobNo".localized + "*"
        txtMobileNo.title = "primaryMobNo".localized + "*"
        
        txtAlternateMobno.placeholder = "alternateMobNum".localized
        txtAlternateMobno.selectedTitle = "alternateMobNum".localized
        txtAlternateMobno.title = "alternateMobNum".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
    }
    func setupTextField()  {
        
        txtFirstName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMiddleName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDesignation.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDesignation.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDesignation.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDesignation.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtGender.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtGender.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtGender.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtGender.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobileNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMobno.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobno.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobno.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobno.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
        
    }
    func setupDesignationDropdown()  {
        
        dropdown.anchorView = txtDesignation
        if designationDetail != nil{
            dropdown.dataSource = designationDetail!.designationList.map{($0.name)}
        }
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.show()
        
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtDesignation.text = item
            dropdown.hide()
        }
    }
    func setupGenderDropdown()  {
        dropdown.anchorView = txtGender
        dropdown.dataSource = ["Female","Male"]
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.show()
        
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtGender.text = item
            selectedGender = item
            dropdown.hide()
        }
    }
    func setupStateDropdown()  {
        dropdown.anchorView = txtState
        dropdown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.show()
        
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            txtLGA.text = ""
            dropdown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropdown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropdown.dataSource = addressDetail.LGA ?? []
            }
        }
        dropdown.direction = .bottom
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.show()
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropdown.hide()
        }
    }
    func isValidDetail() -> Bool {
        if let name = txtFirstName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        
        if let designation = txtDesignation.text, designation.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectDesignation)
            return false
        }
        
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        
        if let mobNo = txtMobileNo.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMobno.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        return true
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditTap(_ sender: Any) {
        isEditMode = true
        vwMain.isUserInteractionEnabled = true
        btnEdit.isHidden = true
        btnSubmit.backgroundColor = UIColor(hexString: color.appMainBlueColor)
    }
    func updateLoginUserDetail()  {
        loginUser?.firstName = txtFirstName.text  ?? ""
        loginUser?.middleName = txtMiddleName.text ?? ""
        loginUser?.surName = txtSurname.text ?? ""
        loginUser?.state = txtState.text ?? ""
        loginUser?.lga = txtLGA.text ?? ""
        loginUser?.sex = selectedGender
        loginUser?.designation = txtDesignation.text ?? ""
        loginUser?.address = txtAddress.text ?? ""
        loginUser?.phoneNumber = txtMobileNo.text ?? ""
        loginUser?.secondPhoneNumber = txtAlternateMobno.text ?? ""
        saveUserDataToStandard(user: loginUser!)
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isValidDetail(){
            if isConnectedToNetwork(){
                showHUD()
                let paradict : [String : Any] = ["deviceToken" : Defaultss.value(forKey: UDKey.kDeviceToken) ?? "",
                                "firstName" : txtFirstName.text  ?? "",
                                "state" : txtState.text ?? "",
                                "lga" : txtLGA.text ?? "",
                                "surName" : txtSurname.text ?? "",
                                "middleName" : txtMiddleName.text ?? "",
                                "designation" : txtDesignation.text ?? "",
                                "sex" : selectedGender,
                                "address" : txtAddress.text ?? "",
                                "phoneNumber" : txtMobileNo.text ?? "",
                                "secondPhoneNumber" : txtAlternateMobno.text ?? ""]
                                
                
                editProfileAPI(paradict: paradict) { (success, message) in
                    self.hideHUD()
                    
                    if success {
                        self.view.makeToast(message)
                        self.updateLoginUserDetail()
                    }
                    print(message)
                }
            }else{
                view.makeToast(appMessages.noConnection)
            }
        }
    }
    
}
extension EditProfileViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDesignation{
            setupDesignationDropdown()
            return false
        }else if textField == txtGender{
            setupGenderDropdown()
            return false
        }else if textField == txtState{
            setupStateDropdown()
            return false
        }else if textField == txtLGA{
            setupLGADropdown()
            return false
        }
        return true
    }
}
