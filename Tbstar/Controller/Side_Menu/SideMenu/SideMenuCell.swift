//
//  SideMenuCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 16/02/21.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var lblMenuName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
