//
//  SideMenuViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 16/02/21.
//

import UIKit

class SideMenuViewController: BaseViewController {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var tblvMenu: UITableView!
    @IBOutlet weak var lblVersion: UILabel!
    
    var isExpanded = false
    var arrMenu = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        if let user = getUserDataFromStandard(){
            lblUserName.text = "\(user.firstName) \(user.surName)"
            if userType == USER_TYPE_INDEPENDENT {
                lblUserType.text = "\(user.userType) Provider".capitalized
            }
            else if userType == USER_TYPE_PPMV {
                lblUserType.text = "\(user.userType)/CP".uppercased()
            }
            lblVersion.text = "\("version".localized) \(getAppVersion())"
        }
    }
    func setupLocalization() {
        
        if userType == USER_TYPE_INDEPENDENT {
            arrMenu = ["home".localized,"dashboard".localized,"profile".localized,"offlineSync".localized,"supportnfeed".localized,"logout".localized]
            
        } else {
            arrMenu = ["home".localized,"Screen New Client".localized,"My Clients".localized,"dashboard".localized,"profile".localized,"offlineSync".localized,"supportnfeed".localized,"logout".localized]
        }
        
        tblvMenu.reloadData()
    }
    
    func IndependetUserDidSelect(indexPath : IndexPath) {
        
        if arrMenu[indexPath.row] == "home".localized {
            
            if currentMenu == MenuOption.Home{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController{
                    currentMenu = MenuOption.Home
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        }else if  arrMenu[indexPath.row] == "dashboard".localized{
            
            if currentMenu == MenuOption.Dashboard{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController{
                    currentMenu = MenuOption.Dashboard
                    self.navigationController?.pushViewController(dashboardVC, animated: true)
                }
            }
        }else if  arrMenu[indexPath.row] == "profile".localized {
            
            isExpanded = !isExpanded
            
            tblvMenu.reloadRows(at: [indexPath], with: .automatic)
            
        }else if  arrMenu[indexPath.row] == "offlineSync".localized{
            if currentMenu == MenuOption.OfflineSync{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let supportVC = self.storyboard?.instantiateViewController(withIdentifier: "OfflineSyncViewController") as? OfflineSyncViewController{
                    currentMenu = MenuOption.OfflineSync
                    self.navigationController?.pushViewController(supportVC, animated: true)
                }
            }
            
        }else if  arrMenu[indexPath.row] == "supportnfeed".localized{
            
            if currentMenu == MenuOption.SupportNFeedback{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let supportVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportNFeedbackViewController") as? SupportNFeedbackViewController{
                    currentMenu = MenuOption.SupportNFeedback
                    self.navigationController?.pushViewController(supportVC, animated: true)
                }
            }
            
        }else if  arrMenu[indexPath.row] == "logout".localized{
            askForLogout()
        }
        
    }
    
    func PPMVUserDidSelect(indexPath : IndexPath) {
        
        if arrMenu[indexPath.row] == "home".localized {
            
            if currentMenu == MenuOption.Home {
                self.dismiss(animated: true, completion: nil)
            }else{
                if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "UserHomeVC") as? UserHomeVC{
                    currentMenu = MenuOption.Home
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        } else if arrMenu[indexPath.row] == "Screen New Client".localized {
            
            if currentMenu == MenuOption.ScreenNewClient {
                self.dismiss(animated: true, completion: nil)
            }else{
                if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenNewClientVC") as? ScreenNewClientVC {
                    currentMenu = MenuOption.ScreenNewClient
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        } else if arrMenu[indexPath.row] == "My Clients".localized {
            
            if currentMenu == MenuOption.MyClient {
                self.dismiss(animated: true, completion: nil)
            }else{
                if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "MyClientVC") as? MyClientVC {
                    currentMenu = MenuOption.MyClient
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        } else if  arrMenu[indexPath.row] == "dashboard".localized{
            
            if currentMenu == MenuOption.Dashboard{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "UserDashboardVC") as? UserDashboardVC {
                    currentMenu = MenuOption.Dashboard
                    self.navigationController?.pushViewController(dashboardVC, animated: true)
                }
            }
        } else if  arrMenu[indexPath.row] == "profile".localized{
            isExpanded = !isExpanded
            tblvMenu.reloadRows(at: [indexPath], with: .automatic)
            
        }else if  arrMenu[indexPath.row] == "offlineSync".localized{
            if currentMenu == MenuOption.OfflineSync{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let supportVC = self.storyboard?.instantiateViewController(withIdentifier: "OfflineSyncVC") as? OfflineSyncVC{
                    currentMenu = MenuOption.OfflineSync
                    self.navigationController?.pushViewController(supportVC, animated: true)
                }
            }
            
        }else if  arrMenu[indexPath.row] == "supportnfeed".localized{
            
            if currentMenu == MenuOption.SupportNFeedback{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let supportVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportNFeedbackViewController") as? SupportNFeedbackViewController{
                    currentMenu = MenuOption.SupportNFeedback
                    self.navigationController?.pushViewController(supportVC, animated: true)
                }
            }
            
        }else if  arrMenu[indexPath.row] == "logout".localized{
            askForLogout()
        }
    }
    
    @IBAction func btnEditProfileTap(_ sender: Any) {
        
        if currentMenu == MenuOption.EditProfile{
            self.dismiss(animated: true, completion: nil)
        }else{
            if let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as? EditProfileViewController{
                currentMenu = MenuOption.EditProfile
                self.navigationController?.pushViewController(editProfileVC, animated: true)
            }
        }
    }
    
    @IBAction func btnChangePswdTap(_ sender: Any) {
        if currentMenu == MenuOption.ChangePswd{
            self.dismiss(animated: true, completion: nil)
        }else{
            if let changePswdVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController{
                currentMenu = MenuOption.ChangePswd
                self.navigationController?.pushViewController(changePswdVC, animated: true)
            }
        }
    }
    
    @IBAction func btnLanguageTap(_ sender: Any) {
        
        if currentMenu == MenuOption.ChangeLang{
            self.dismiss(animated: true, completion: nil)
        }else{
            if let languagePopup = self.storyboard?.instantiateViewController(withIdentifier: "ChangeLanguageViewController") as? ChangeLanguageViewController{
                currentMenu = MenuOption.ChangeLang
                languagePopup.delegate = self
                languagePopup.modalPresentationStyle = .overFullScreen
                present(languagePopup, animated: true, completion: nil)
            }
        }
    }
    func logout()  {
        
        if isConnectedToNetwork(){
            showHUD()
            let user = getUserDataFromStandard()
            let paraDict:[String:Any] = ["deviceToken" : Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""]
            logOutAPI(paradict: paraDict, accessToken: user?.accessToken ?? "") { (success, strData, message) in
                self.hideHUD()
                if success{
                    self.logOutApplication()
                }else if message != ""{
                    self.view.makeToast(message)
                }
                else{
                    self.view.makeToast(appMessages.somethingWrong)
                }
            }
            
        }else{
            logOutApplication()
            //view.makeToast(appMessages.noConnection)
        }
    }
    
    func askForLogout()  {
        
        if let popup = self.storyboard?.instantiateViewController(withIdentifier: "AskForCovid19PopupViewController") as? AskForCovid19PopupViewController{
            popup.popupTitle = "confirmation".localized
            popup.popupSubtitle = "areYouSure".localized
            popup.delegate = self
            popup.modalPresentationStyle = .overFullScreen
            present(popup, animated: true, completion: nil)
        }
    }
    
}
extension SideMenuViewController : ChangeLanguageDelegate{
    func languageChanged() {
        setupLocalization()
    }
}
extension SideMenuViewController : AskForCovid19PopupViewControllerDelegate{
    func selectedOption(isYes: Bool) {
        if isYes{
            logout()
        }
    }
}
extension SideMenuViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if userType == USER_TYPE_INDEPENDENT {
            
            if indexPath.row == 2{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell") as? ProfileCell{
                    cell.lblMenuName.text = arrMenu[indexPath.row]
                    cell.btnEditProfile.setTitle("eProfile".localized, for: .normal)
                    cell.btnChangePswd.setTitle("changePswd".localized, for: .normal)
                    cell.btnChangeLang.setTitle("changeLang".localized, for: .normal)
                    cell.expandView(shouldExpand: isExpanded)
                    return cell
                }
            } 
            else if let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenuCell") as? SideMenuCell{
                cell.lblMenuName.text = arrMenu[indexPath.row]
                return cell
            }
            
        } else {
            
            if indexPath.row == 4 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell") as? ProfileCell{
                    cell.lblMenuName.text = arrMenu[indexPath.row]
                    cell.btnEditProfile.setTitle("eProfile".localized, for: .normal)
                    cell.btnChangePswd.setTitle("changePswd".localized, for: .normal)
                    cell.btnChangeLang.setTitle("changeLang".localized, for: .normal)
                    cell.expandView(shouldExpand: isExpanded)
                    return cell
                }
            }
            else if let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenuCell") as? SideMenuCell{
                cell.lblMenuName.text = arrMenu[indexPath.row]
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if userType == USER_TYPE_INDEPENDENT {
            
            IndependetUserDidSelect(indexPath: indexPath)
            
        } else {
            
            PPMVUserDidSelect(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if userType == USER_TYPE_INDEPENDENT {
            
            if indexPath.row == 2{ // Profile{
                if isExpanded{
                    return 170.0
                }else{
                    return 50.0
                }
            }
            
        } else {
            
            if indexPath.row == 4{ // Profile{
                if isExpanded{
                    return 170.0
                }else{
                    return 50.0
                }
            }
            
        }
        return 50.0
    }
}
