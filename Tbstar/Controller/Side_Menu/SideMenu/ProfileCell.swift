//
//  ProfileCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 17/02/21.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var imgvDropdown: UIImageView!
    @IBOutlet weak var vwExpandable: UIView!
    @IBOutlet weak var heightCnstOfExpVw: NSLayoutConstraint!
    @IBOutlet weak var lblMenuName: UILabel!
    var isExpanded = false
    
    @IBOutlet weak var btnEditProfile: UIButton!
    @IBOutlet weak var btnChangePswd: UIButton!
    @IBOutlet weak var btnChangeLang: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func expandView(shouldExpand : Bool) {
        heightCnstOfExpVw.constant = shouldExpand ? 120 : 0
        vwExpandable.isHidden = !shouldExpand
        imgvDropdown.image = shouldExpand ? UIImage(named: "down") : UIImage(named: "up")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
