//
//  NewRegistarationVC.swift
//  Tbstar
//
//  Created by Mavani on 21/05/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class NewRegistarationVC: BaseViewController {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblGender : UILabel!
    @IBOutlet var lblMale : UILabel!
    @IBOutlet var lblFemale : UILabel!

    @IBOutlet weak var txtFName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternetMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmPassword: SkyFloatingLabelTextField!

    @IBOutlet var btnExit : UIButton!
    @IBOutlet var btnConfirm : UIButton!
    @IBOutlet var btnMale : UIButton!
    @IBOutlet var btnFemale : UIButton!

    var designationDetail : DesignationDetail?
    var facilityDetailDict : [String : Any]?
    var selectedGender = ""

    let dropDown = DropDown()
    var otpVerification : OTP?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
        setupTextField()
        setFacilityDetail()
    }

    func setupLocalization() {
        
    
        lblTitle.text = "newRegistration".localized
        
        txtFName.placeholder = "firstname".localized + "*"
        txtFName.selectedTitle = "firstname".localized + "*"
        txtFName.title = "firstname".localized + "*"
        
        txtMName.placeholder = "middlename".localized
        txtMName.selectedTitle = "middlename".localized
        txtMName.title = "middlename".localized
        
        txtSurName.placeholder = "surname".localized + "*"
        txtSurName.selectedTitle = "surname".localized + "*"
        txtSurName.title = "surname".localized + "*"
        
        txtDesignation.placeholder = "designation".localized + "*"
        txtDesignation.selectedTitle = "designation".localized + "*"
        txtDesignation.title = "designation".localized + "*"
        
        lblGender.text = "sex".localized + " " + "optional".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        txtMobileNumber.placeholder = "primaryMobNo".localized + "*"
        txtMobileNumber.selectedTitle = "primaryMobNo".localized + "*"
        txtMobileNumber.title = "primaryMobNo".localized + "*"
        
        txtAlternetMobileNumber.placeholder = "alternateMobNo".localized
        txtAlternetMobileNumber.selectedTitle = "alternateMobNo".localized
        txtAlternetMobileNumber.title = "alternateMobNo".localized
        
        txtFacilityName.placeholder = "facilityName".localized + "*"
        txtFacilityName.selectedTitle = "facilityName".localized + "*"
        txtFacilityName.title = "facilityName".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtPassword.placeholder = "password".localized + "*"
        txtPassword.selectedTitle = "password".localized + "*"
        txtPassword.title = "password".localized + "*"
        
        txtConfirmPassword.placeholder = "cnfmPswd".localized + "*"
        txtConfirmPassword.selectedTitle = "cnfmPswd".localized + "*"
        txtConfirmPassword.title = "cnfmPswd".localized + "*"
        
        btnExit.setTitle("exit".localized, for: .normal)
        btnConfirm.setTitle("confirm".localized, for: .normal)
    }
    func setupTextField()  {
        
        txtFName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobileNumber.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNumber.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNumber.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNumber.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternetMobileNumber.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternetMobileNumber.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternetMobileNumber.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternetMobileNumber.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtFacilityName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
    
        txtPassword.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPassword.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtPassword.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPassword.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtConfirmPassword.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtConfirmPassword.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtConfirmPassword.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtConfirmPassword.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        /*tfFirstName.text = "test"
        tfSurname.text = "iOS"
        txtMobileNum.text = "1234567890"
        txtPswd.text = "Test@123"
        txtCnfmPswd.text = "Test@123"
        txtFacilityName.text = "test"
        txtAddress.text = "test"*/
    }
    func setFacilityDetail()  {
        
        if let facilityDict = facilityDetailDict {
            txtFacilityName.text    = facilityDict["facilityName"] as? String ?? ""
            txtAddress.text    = facilityDict["facility_address"] as? String ?? ""
            txtFacilityName.isUserInteractionEnabled = false
            txtAddress.isUserInteractionEnabled = false
            
        }
    }
    func setupDesignationDropdown()  {
        
        dropDown.anchorView = txtDesignation
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.designationList.map{($0.name)}
        }else{
            dropDown.dataSource = ["Community Health Worker", "Community Health Extension Workers(CHEW)", "Community Pharmacist", "Doctor", "Laboratory Scientist","Nurse","Patent Medicine Vendor", "Others", "Network Officer"]
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtDesignation.text = item
            dropDown.hide()
        }
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
        
    }
    func isValidDetail() -> Bool {
        if let name = txtFName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurName.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        
        if let designation = txtDesignation.text, designation.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectDesignation)
            return false
        }
        /*if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }*/
        if let mobNo = txtMobileNumber.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternetMobileNumber.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        
        if let facilityName = txtFacilityName.text, facilityName.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterFacilityName)
            return false
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let pswd = txtPassword.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterPswd)
            return false
        }else{
            if !isValidPassword(password: txtPassword.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if let pswd = txtConfirmPassword.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterCnfmPswd)
            return false
        }
        if txtPassword.text! != txtConfirmPassword.text{
            showAlert(titleStr: appName, msg: appMessages.pswdShouldBeSame)
            return false
        }
        return true
    }
    
    //MARK:-
    @IBAction func btnExitPressed(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCOnfirmPressed(_ sender : UIButton) {
        if isValidDetail(){
            verifyAccount()
        }
    }
    func verifyAccount(){
        
        if isConnectedToNetwork(){
            showHUD()
            
            //let token = Defaultss.value(forKey: UDKey.kNotificationToken) as? String ?? ""
            let paraDict = ["phoneNumber" : txtMobileNumber.text!,
                            "userType": facilityDetailDict!["userType"] as? String ?? "",
                            "deviceToken":Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""]
            
            verifyAccountAPI(paradict: paraDict) { (success, strData,message) in
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    self.hideHUD()
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse),
                           let otpJson = json["otp"] as? [String:Any] {
                            print(otpJson)
                            self.otpVerification = OTP(dict: otpJson)
                            self.moveToVerificationCodeScreen()
                        }
                        else{
                            self.view.makeToast(appMessages.somethingWrong)
                        }
                    }
                    else{
                        self.hideHUD()
                        self.view.makeToast(strResponse)
                    }
                }else if message != ""{
                    self.view.makeToast(message)
                    self.hideHUD()
                }else{
                    self.hideHUD()
                }
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
    func moveToVerificationCodeScreen()  {
        
        if let verificationCodeVc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as? VerificationCodeViewController{
            verificationCodeVc.otpCode = otpVerification
            verificationCodeVc.signupDict = makeSignupDict()
            verificationCodeVc.ISFROMVERIFICATION = IndependentSelected
            self.navigationController?.pushViewController(verificationCodeVc, animated: true)
        }
    }
    @IBAction func btnMalePressed(_ sender : UIButton) {
        
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }

    @IBAction func btnFemalePressed(_ sender : UIButton) {
        
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }

    @IBAction func btnShowPasswordPressed(_ sender : UIButton) {
        
        if sender.tag == 101 {
            txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
            sender.isSelected = !txtPassword.isSecureTextEntry
        } else {
            txtConfirmPassword.isSecureTextEntry = !txtConfirmPassword.isSecureTextEntry
            sender.isSelected = !txtConfirmPassword.isSecureTextEntry
        }
    }
    
    func makeSignupDict() -> [String:Any] {
        
        let token = Defaultss.value(forKey: UDKey.kFCMNotificationToken) as? String ?? ""
        var signupDict = [String:Any]()
        signupDict["facilityId"] = facilityDetailDict!["faciltyid"] as? String ?? ""
        if !selectedGender.isEmpty {
            signupDict["sex"] = selectedGender
        }
        signupDict["designation"] = txtDesignation.text ?? ""
        signupDict["userType"] = facilityDetailDict!["userType"] as? String ?? ""
        signupDict["firstName"] = txtFName.text ?? ""
        signupDict["surName"] = txtSurName.text ?? ""
        signupDict["middleName"] = txtMName.text ?? ""
        signupDict["phoneNumber"] = txtMobileNumber.text ?? ""
        signupDict["secondPhoneNumber"] = txtAlternetMobileNumber.text ?? ""
        signupDict["facilityType"] =  facilityDetailDict!["facility_type"] as? String ?? ""
        signupDict["facilityName"] = txtFacilityName.text ?? ""
        signupDict["address"] = txtAddress.text ?? ""
        signupDict["state"] =  facilityDetailDict!["facility_state"] as? String ?? ""
        signupDict["lga"] =  facilityDetailDict!["lga"] as? String ?? ""
        signupDict["deviceToken"] = Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""
        signupDict["deviceId"] = Defaultss.value(forKey: UDKey.kDeviceId) ?? ""
        signupDict["deviceTokenForNotification"] = token
        signupDict["verificationOtp"] = otpVerification?.verificationOtp ?? ""
        signupDict["password"] = txtPassword.text ?? ""
        signupDict["confirmPassword"] = txtConfirmPassword.text ?? ""
        signupDict["termsConditionStatus"] = "1"
        return signupDict
    }
}

extension NewRegistarationVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobileNumber || textField == txtAlternetMobileNumber{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDesignation{
            setupDesignationDropdown()
            return false
        }
        return true
    }

}
