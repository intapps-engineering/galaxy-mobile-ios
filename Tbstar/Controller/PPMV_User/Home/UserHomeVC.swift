//
//  UserHomeVC.swift
//  Tbstar
//
//  Created by Mavani on 22/05/21.
//

import UIKit

class UserHomeVC: BaseViewController {

    @IBOutlet weak var btnScreenNewClient: UIButton!
    @IBOutlet weak var btnMyClient: UIButton!
    @IBOutlet weak var btnDashboard: UIButton!
    @IBOutlet weak var btnAlert: UIButton!

    @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name:NSNotification.Name(rawValue: "LangChanged"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateAppVersion()
        setupLocalization()
    }
    
    @objc func languageChanged()  {
        setupLocalization()
    }

    func setupLocalization() {
        
        lblTitle.text = "home".localized
        
        btnScreenNewClient.setTitle("Screen New Client".localized, for: .normal)
        btnMyClient.setTitle("My Clients".localized, for: .normal)
        btnDashboard.setTitle("Dashboard".localized, for: .normal)
        btnAlert.setTitle("High Priority Aletrs".localized, for: .normal)
    }
    
    func updateAppVersion()  {
        if isConnectedToNetwork(){
            let paraDict = ["apkVersion" : getAppVersion()]
            let user = getUserDataFromStandard()
            updateAppVersionAPI(paradict: paraDict, accessToken: user?.accessToken ?? "") { (success,code,message) in
                if code == 402{
                    self.view.makeToast(message)
                    self.logOutApplication()
                }
            }
        }
    }
    
    @IBAction func btnScreenNewClientPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenNewClientVC") as? ScreenNewClientVC {
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnMyClientPressed(_ sender : UIButton) {
     
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "MyClientVC") as? MyClientVC {
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }

    @IBAction func btnDashboardPressed(_ sender : UIButton) {
        
        if let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "UserDashboardVC") as? UserDashboardVC {
            self.navigationController?.pushViewController(dashboardVC, animated: true)
        }
    }

    @IBAction func btnAlertPressed(_ sender : UIButton) {
        
        if let dashboardVC = self.storyboard?.instantiateViewController(withIdentifier: "HighPriorityAlertsVC") as? HighPriorityAlertsVC {
            self.navigationController?.pushViewController(dashboardVC, animated: true)
        }

    }

    @IBAction func btnNotificationPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "PPMV_NotificationVC") as? PPMV_NotificationVC{
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnMenuTap(_ sender: Any) {

        showMenu()
    }
}
