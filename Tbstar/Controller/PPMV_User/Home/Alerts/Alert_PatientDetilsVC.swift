//
//  Alert_PatientDetilsVC.swift
//  Tbstar
//
//  Created by user1 on 16/06/21.
//

import UIKit

class Alert_PatientDetilsVC: UIViewController {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblProcessing : UILabel!
    @IBOutlet var lblFacility : UILabel!
    @IBOutlet var lblPatientID : UILabel!
    @IBOutlet var lblPatientName : UILabel!
    @IBOutlet var lblStatus : UILabel!
    @IBOutlet var lblFacilityPhone : UILabel!
    @IBOutlet var lblFacilityState : UILabel!
    @IBOutlet var lblFacilityLGA : UILabel!
    @IBOutlet var lblFacilityName : UILabel!
    @IBOutlet var lblLabPhone : UILabel!
    @IBOutlet var lblLabState : UILabel!
    @IBOutlet var lblLabLGA : UILabel!

    @IBOutlet var viewDetails : UIView!
    
    var strTitle = ""

    var selectedPatientDetails : PPMV_HighPriority_Alerts_Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblTitle.text = strTitle
        lblProcessing.text = "\("PROCESSING".localized)"
        lblFacility.text = "\("Facility".localized) : \(selectedPatientDetails?.facilityScreenedAtName ?? "")"
        lblPatientID.text = "\("Patient ID".localized) : \(selectedPatientDetails?.recordNumber ?? "")"
        lblPatientName.text = "\("Patient Name".localized) : \(selectedPatientDetails?.firstName ?? "") \(selectedPatientDetails?.lastName ?? "")"
        lblStatus.text = "\("Status".localized) : \(selectedPatientDetails?.status  ?? "")"
        lblFacilityPhone.text = "\("Facility Phone".localized) : \(selectedPatientDetails?.phoneNumber ?? "")"
        lblFacilityState.text = "\("Facility State".localized) : \(selectedPatientDetails?.stateName ?? "")"
        lblFacilityLGA.text = "\("Facility LGA".localized) : \(selectedPatientDetails?.lgaName ?? "")"
        lblFacilityName.text = "\("facilityName".localized) : \(selectedPatientDetails!.sampledetails.map{($0.labName)}.joined())"
        lblLabPhone.text = "\("labphone".localized) : \(selectedPatientDetails!.sampledetails.map{($0.labPhoneNumber)}.joined())"
        lblLabState.text = "\("labstate".localized) : \(selectedPatientDetails!.sampledetails.map{($0.labStateName)}.joined())"
        lblLabLGA.text = "\("lablga".localized) : \(selectedPatientDetails!.sampledetails.map{($0.labLgaName)}.joined())"
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
         
        self.navigationController?.popViewController(animated: true)
    }
}
