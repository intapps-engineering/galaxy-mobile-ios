//
//  SampleVC.swift
//  Tbstar
//
//  Created by Mavani on 25/05/21.
//

import UIKit

class SampleVC: UIViewController {

    @IBOutlet var btnFirstOption : UIButton!
    @IBOutlet var lblSecondOption : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setLocalization()
    }
    
    func setLocalization() {
        
        lblSecondOption.text = "Sample Collected Not Accepted By Lab".localized
        
        btnFirstOption.setTitle("Sent To Lab - No Result".localized, for: .normal)
    }


    @IBAction func btnFirstOptionPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "AlertListVC") as? AlertListVC{
            VC.strTitle = "Sent To Lab - No Result".localized
            VC.apiURL = "np/alerts/sample-accepted-no-result-cases"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnSecondOptionPressed(_ sender : UIButton) {
     
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "AlertListVC") as? AlertListVC{
            VC.strTitle = "Sample Collected Not Accepted By Lab".localized
            VC.apiURL = "np/alerts/sample-not-accepted-cases"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}
