//
//  HighPriorityAlertsVC.swift
//  Tbstar
//
//  Created by Mavani on 25/05/21.
//

import UIKit
import CarbonKit

class HighPriorityAlertsVC: UIViewController {

    @IBOutlet var videChildAdd : UIView!
    
    var tabSwipe = CarbonTabSwipeNavigation()

    override func viewDidLoad() {
        super.viewDidLoad()

        TopSliderSetUp()
    }
    
    override func viewDidLayoutSubviews() {
        
        tabSwipe.toolbar.barTintColor = .white
        tabSwipe.toolbar.clipsToBounds = true
    }
    
    func TopSliderSetUp() {
        
        tabSwipe = CarbonTabSwipeNavigation(items: ["PATIENTS".localized, "SAMPLES".localized], delegate: self)

        var frameRect: CGRect = (tabSwipe.carbonSegmentedControl?.frame)!
        frameRect.size.width = videChildAdd.frame.size.width
        tabSwipe.carbonSegmentedControl?.frame = frameRect
        tabSwipe.carbonSegmentedControl?.apportionsSegmentWidthsByContent = false
        tabSwipe.setTabBarHeight(50)
        tabSwipe.setNormalColor(.UnselectedBlueColor)
        tabSwipe.setSelectedColor(.SlectedBlueColor)
        tabSwipe.setIndicatorColor(.SlectedBlueColor)
        let font =  UIFont(name:"Quicksand-Medium",size:16)
        tabSwipe.setNormalColor(.UnselectedBlueColor, font: font!)
        tabSwipe.setSelectedColor(.SlectedBlueColor, font: font!)
        tabSwipe.insert(intoRootViewController: self, andTargetView: videChildAdd)
        
        videChildAdd.addSubview(tabSwipe.view)
        tabSwipe.view.frame = videChildAdd.bounds
        tabSwipe.didMove(toParent: self)
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
         
        currentMenu = MenuOption.Home
        self.navigationController?.popViewController(animated: true)
    }
}

extension HighPriorityAlertsVC : CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard storyboard != nil else { return UIViewController() }
        switch index {
            case 0:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientsVC")
                    as! PatientsVC
                return VC
            case 1:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "SampleVC") as! SampleVC
                return VC
            default: break
        }
        return UIViewController()
    }
}
