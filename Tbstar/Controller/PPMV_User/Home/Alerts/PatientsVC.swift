//
//  PatientsVC.swift
//  Tbstar
//
//  Created by Mavani on 25/05/21.
//

import UIKit

class PatientsVC: UIViewController {

    @IBOutlet var btnFirstOption : UIButton!
    @IBOutlet var btnSecondOption : UIButton!
    @IBOutlet var btnThirdOption : UIButton!
    
    @IBOutlet var lblSecondOption : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setLocalization()

    }
    
    func setLocalization() {
        
        lblSecondOption.text = "Confirmed, Referred, No Treatment".localized
        
        btnFirstOption.setTitle("Confirmed, No Treatment".localized, for: .normal)
        btnThirdOption.setTitle("Confirmed Not Referred".localized, for: .normal)
    }

    @IBAction func btnFirstOptionPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "AlertListVC") as? AlertListVC{
            VC.strTitle = "Confirmed, No Treatment".localized
            VC.apiURL = "np/alerts/confirmed-no-treatment-cases"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnSecondOptionPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "AlertListVC") as? AlertListVC{
            VC.strTitle = "Confirmed, Referred, No Treatment".localized
            VC.apiURL = "np/alerts/confirmed-referred-no-treatment-cases"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnThirdOptionPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "AlertListVC") as? AlertListVC{
            VC.strTitle = "Confirmed Not Referred".localized
            VC.apiURL = "np/alerts/confirmed-not-referred-cases"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}
