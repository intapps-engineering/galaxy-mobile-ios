//
//  AlertListVC.swift
//  Tbstar
//
//  Created by user1 on 15/06/21.
//

import UIKit

class AlertListVC: BaseViewController {

    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet weak var tblvSearch: UITableView!

    var strTitle = ""
    var apiURL = ""
    var selectedSearchType = ""
    var pageIndex = 1
    var totalPages = 0
    
    var arrCaseList = [PPMV_HighPriority_Alerts_Data]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        pageIndex = 1
        lblTitle.text = strTitle
        GetAlertCasesData()
    }
    
    func GetAlertCasesData() {
        
        if isConnectedToNetwork() {
            
            self.showHUD()
            let userData = getUserDataFromStandard()
            var paradict = [String : Any]()
            paradict["page-number"] = pageIndex
            paradict["page-size"] = 10
            paradict["user-id"] = userData?.userId
            paradict["token"] = userData?.accessToken
            
            PPMV_GET_ALETS_CASES_LIST(apiUrl: apiURL, paradict: paradict, completionHandler: { (json , success) in
                self.hideHUD()
                if success {
                    
                    print(json)
                        self.totalPages = json["totalCases"] as? Int ?? 0
                        
                        if let list = json["allPatients"] as? [[String :Any]]{
                            if self.pageIndex == 1 {
                                self.arrCaseList = list.map(PPMV_HighPriority_Alerts_Data.init)
                            }else{
                                self.arrCaseList.append(contentsOf: list.map(PPMV_HighPriority_Alerts_Data.init))
                            }
                        }
                self.tblvSearch.reloadData()
                } else {
                    self.hideHUD()
                    self.arrCaseList.removeAll()
                }
                if json.count == 0{
                    self.tblvSearch.setNoDataPlaceholder("noclientfound".localized)
                } else {
                    self.tblvSearch.removeNoDataPlaceholder()
                }
                self.tblvSearch.reloadData()
            })
        } else {
            view.makeToast(appMessages.noConnection)
        }
    }
    
     @IBAction func btnBackPressed(_ sender : UIButton) {
         
        self.navigationController?.popViewController(animated: true)
    }
}

extension AlertListVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrCaseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblvSearch.dequeueReusableCell(withIdentifier: "cell_PPMV_MyClient") as! cell_PPMV_MyClient
        cell.setAlertCellData(info: arrCaseList[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrCaseList.count - 1{
            if pageIndex < totalPages {
                pageIndex += 1
                GetAlertCasesData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "Alert_PatientDetilsVC") as? Alert_PatientDetilsVC {
            VC.strTitle = strTitle
            VC.selectedPatientDetails = arrCaseList[indexPath.row]
            navigationController?.pushViewController(VC, animated: true)
        }
    }
}
