//
//  MyClientVC.swift
//  Tbstar
//
//  Created by Mavani on 25/05/21.
//

import CarbonKit
import UIKit

class MyClientVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewChildAdd: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let controller = storyboard!.instantiateViewController(withIdentifier: "PPMVSearchVC") as! PPMVSearchVC
        addChild(controller)
        controller.view.frame = CGRect(x: 0, y: 0, width: viewChildAdd.frame.size.width, height: viewChildAdd.frame.size.height)
        viewChildAdd.addSubview(controller.view)
        controller.didMove(toParent: self)
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
         
        currentMenu = MenuOption.Home
        self.navigationController?.popViewController(animated: true)
    }
}
