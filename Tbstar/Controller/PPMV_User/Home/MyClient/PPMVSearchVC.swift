//
//  PPMVSearchVC.swift
//  Tbstar
//
//  Created by Mavani on 10/06/21.
//

import UIKit

class PPMVSearchVC : BaseViewController {
    
    @IBOutlet weak var btnserachBySname: UIButton!
    @IBOutlet weak var btnsearchByCase: UIButton!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var tblvSearch: UITableView!
    
    var selectedSearchType = ""
    var pageIndex = 0
    var totalPages = 0
    
    var SearchFrom : String?
    
    var arrClientList = [PPMV_PatientInformation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLocalization()
        SearchFrom = "Display_All"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        txtSearch.resignFirstResponder()
        txtSearch.text = ""
        btnserachBySname.isSelected = false
        btnsearchByCase.isSelected = false  
        arrClientList.removeAll()
        pageIndex = 0
        GetClientList(page: 10, PAGEIndex: pageIndex, lastName: "", patientRecordNumber: "", search: false)
    }
    
    func GetClientList(page : Int , PAGEIndex : Int , lastName : String , patientRecordNumber : String , search : Bool) {
        
        if isConnectedToNetwork() {
            self.showHUD()
            var paradict = [String : Any]()
            paradict["page"] = page
            paradict["pageIndex"] = PAGEIndex
            paradict["lastName"] = lastName
            paradict["patientRecordNumber"] = patientRecordNumber
            paradict["search"] = search
            
            PPMV_GET_PATIENT_LIST(paradict: paradict, completionHandler: { (success, strData, message) in
                if success {
                    
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.totalPages = json["totalPage"] as? Int ?? 0
                        print(json)
                        if let list = json["PatientList"] as? [[String :Any]]{
                            if self.pageIndex == 0 {
                                self.arrClientList = list.map(PPMV_PatientInformation.init)
                            }else{
                                self.arrClientList.append(contentsOf: list.map(PPMV_PatientInformation.init))
                            }
                        }
                    }else{
                        self.view.makeToast(strResponse)
                    }
                    self.hideHUD()
                }else{
                    self.hideHUD()
                    self.arrClientList.removeAll()
                }
                if self.arrClientList.count == 0{
                    self.tblvSearch.setNoDataPlaceholder("noclientfound".localized)
                } else {
                    self.tblvSearch.removeNoDataPlaceholder()
                }
                self.tblvSearch.reloadData()
            })
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func setupLocalization() {
        
        btnserachBySname.setTitle("searchSurname".localized, for: .normal)
        btnserachBySname.setTitle("searchSurname".localized, for: .selected)
        btnsearchByCase.setTitle("searchCase".localized, for: .normal)
        btnsearchByCase.setTitle("searchCase".localized, for: .selected)
        txtSearch.placeholder = "searchText".localized
    }
    
    func CallApi() -> Bool {
        arrClientList.removeAll()
        pageIndex = 0
        if selectedSearchType == "" && txtSearch.text != ""{
            showAlert(titleStr: appName, msg: appMessages.selectSearchType)
            return false
        }
        if selectedSearchType != "" && txtSearch.text != "" {
            if txtSearch.text?.count ?? 0 >= 3 {
                txtSearch.resignFirstResponder()
                SearchFrom = "Display_Not_All"
                GetClientList(page: 10, PAGEIndex: pageIndex, lastName: selectedSearchType == "Surname" ? txtSearch.text! : "", patientRecordNumber: selectedSearchType == "Case" ? txtSearch.text! : "", search: true)
                return true
            } else {
                txtSearch.resignFirstResponder()
                SearchFrom = "Display_All"
                GetClientList(page: 10, PAGEIndex: pageIndex, lastName: "", patientRecordNumber: "", search: false)
                return true
            }
        } else {
            txtSearch.resignFirstResponder()
            SearchFrom = "Display_All"
            GetClientList(page: 10, PAGEIndex: pageIndex, lastName: "", patientRecordNumber: "", search: false)
            return true
        }
    }
    
    @IBAction func btnSearchTypeTap(_ sender: UIButton) {
        arrClientList.removeAll()
        btnserachBySname.isSelected = false
        btnsearchByCase.isSelected = false
        sender.isSelected = true
        selectedSearchType = sender.tag == 1 ? "Surname" : "Case"
    }
}

extension PPMVSearchVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let callapi = CallApi()
        return callapi
    }
}

extension PPMVSearchVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrClientList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblvSearch.dequeueReusableCell(withIdentifier: "cell_PPMV_MyClient") as! cell_PPMV_MyClient
        //cell.lblSatus.roundCorners(corners: [.topLeft], radius: 5.0)
        cell.setCellData(info: arrClientList[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrClientList.count - 1{
            if pageIndex < totalPages {
                pageIndex += 1
                if SearchFrom == "Display_All" {
                    GetClientList(page: 10, PAGEIndex: pageIndex, lastName: "", patientRecordNumber: "", search: false)
                } else {
                    GetClientList(page: 10, PAGEIndex: pageIndex, lastName: selectedSearchType == "Surname" ? txtSearch.text! : "", patientRecordNumber: selectedSearchType == "Case" ? txtSearch.text! : "", search: true)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let clinicRegister = self.storyboard?.instantiateViewController(withIdentifier: "MyClientDetailsVC") as? MyClientDetailsVC {
            clinicRegister.PATIENT_DETAIL = arrClientList[indexPath.row]
            navigationController?.pushViewController(clinicRegister, animated: true)
        }
    }
}
