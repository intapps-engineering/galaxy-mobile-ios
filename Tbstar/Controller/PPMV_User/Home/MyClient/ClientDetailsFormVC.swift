//
//  ClientDetailsFormVC.swift
//  Tbstar
//
//  Created by Mavani on 10/06/21.
//

import UIKit
import DropDown
import SkyFloatingLabelTextField

class ClientDetailsFormVC: BaseViewController {
    
    @IBOutlet weak var lblAgeTitle: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblSex: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblPatientStatusTitle: UILabel!
    @IBOutlet weak var lblCovidStatusTitle: UILabel!
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfVisit: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtDOB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMobNo: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtNotes: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var btnPatientStatus: UIButton!
    @IBOutlet weak var btnCovidStatus: UIButton!
    @IBOutlet weak var btnFacilityName: UIButton!
    
    @IBOutlet weak var vwMainView: UIView!
    @IBOutlet weak var viewButtons: UIView!
    
    @IBOutlet var Constraint_ButtonsHeight : NSLayoutConstraint!
    @IBOutlet var Constraint_FacilityHeight : NSLayoutConstraint!
    
    var TBDict : [String:Any]?
    var covidDict : [String :Any]?
    var selectedGender = ""
    
    var dateFormatter = DateFormatter()
    var designationDetail : DesignationDetail?
    let dropDown = DropDown()
    
    var visitDate = Date()
    var birthDate = Date()
    
    var ISFORM : String?
    
    var SelectedLabDetail : PPMV_Lab_List?
    var dictPatirntDetail : PPMV_PatientInformation?
    
    var isCovidScreened = false
    var isFromScreening = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupLocalization()
        getDetailForIndependentUser()
        setupTextField()
        askForLocation()
        dateFormatter.dateFormat = displayDateFormat
        txtDateOfVisit.text = dateFormatter.string(from: Date())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.EditButtonPressed(notification:)), name: Notification.Name("EditButtonPressed"), object: nil)
        
        txtFirstName.text =  dictPatirntDetail?.firstName
        txtMiddleName.text =  dictPatirntDetail?.middleName
        txtSurname.text =  dictPatirntDetail?.surname
        txtDateOfVisit.text = ChangeDisplayingDateFoemate(strDate: dictPatirntDetail?.visitDate ?? "")
        txtDOB.text = ChangeDisplayingDateFoemate(strDate: dictPatirntDetail?.dateOfBirth ?? "")
        
        selectedGender = dictPatirntDetail?.sex ?? ""
        
        if selectedGender == Gender.Female.rawValue{
            btnFemaleTap(btnFemale)
        }else if selectedGender == Gender.Male.rawValue{
            btnMaleTap(btnMale)
        }
        
        txtMobNo.text = dictPatirntDetail?.phoneNumber
        txtAddress.text = dictPatirntDetail?.address
        txtState.text = dictPatirntDetail?.state
        txtLGA.text = dictPatirntDetail?.lga
        
        let newDateFormate = DateFormatter()
        newDateFormate.dateFormat = apiDateFormatFromAPI
        let sDate = newDateFormate.date(from: dictPatirntDetail?.dateOfBirth ?? "") ?? Date()
        let age = Calendar.current.dateComponents([.year], from: sDate, to: Date()).year!
        birthDate = sDate
        lblAge.text = "\(age)"
        
        if makeParametrForAPI(){
            btnCovidStatus.setTitle("Screened : View Status", for: .normal)
            isCovidScreened = true
        }else{
            btnCovidStatus.setTitle("Not Screened : Screen now.", for: .normal)
            isCovidScreened = false
        }
        
        if dictPatirntDetail?.patientReferredInPersonToFacilityName != "" || dictPatirntDetail?.patientReferredInPersonToFacilityName.isEmpty == false  {
            
            Constraint_FacilityHeight.constant = 40
            btnFacilityName.isHidden = false
            btnFacilityName.setTitle(dictPatirntDetail?.patientReferredInPersonToFacilityName, for: .normal)
        } else {
            Constraint_FacilityHeight.constant = 0
            btnFacilityName.isHidden = true
        }
        
        Constraint_ButtonsHeight.constant = 0
        viewButtons.isHidden = true
        
        disableAllViews(shouldDisable: true)
    }
    
    func ChangeDisplayingDateFoemate(strDate : String) -> String {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = apiDateFormatFromAPI
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = displayDateFormat
        let date = dateFormatterGet.date(from: strDate)
        
        return dateFormatter.string(from: date ?? Date())
    }
    
    func setupLocalization()  {
        
        lblPatientStatusTitle.text = "patientStatus".localized
        lblCovidStatusTitle.text = "covidStatus".localized
        
        txtFirstName.placeholder = "pFirstName".localized + "*"
        txtFirstName.selectedTitle = "pFirstName".localized + "*"
        txtFirstName.title = "pFirstName".localized + "*"
        
        txtMiddleName.placeholder = "pMiddleName".localized
        txtMiddleName.selectedTitle = "pMiddleName".localized
        txtMiddleName.title = "pMiddleName".localized
        
        txtSurname.placeholder = "pSurname".localized + "*"
        txtSurname.selectedTitle = "pSurname".localized + "*"
        txtSurname.title = "pSurname".localized + "*"
        
        txtDateOfVisit.placeholder = "visitDate".localized + "*"
        txtDateOfVisit.selectedTitle = "visitDate".localized + "*"
        txtDateOfVisit.title = "visitDate".localized + "*"
        
        lblAgeTitle.text = "age".localized
        lblAge.text = "\(0)"
        lblDOB.text = "dob".localized
        lblSex.text = "sex".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        txtMobNo.placeholder = "mobNO".localized + "*"
        txtMobNo.selectedTitle = "mobNO".localized + "*"
        txtMobNo.title = "mobNO".localized + "*"
        
        txtAlternateMobNo.placeholder = "secondMobileNum".localized
        txtAlternateMobNo.selectedTitle = "secondMobileNum".localized
        txtAlternateMobNo.title = "secondMobileNum".localized
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
        
        btnExit.setTitle("exit".localized, for: .normal)
        btnSave.setTitle("save".localized, for: .normal)
        
        btnPatientStatus.setTitle(dictPatirntDetail?.patientStatusLabel ?? "", for: .normal)
        btnPatientStatus.isUserInteractionEnabled = false
        
    }
    
    func makeParametrForAPI() -> Bool  {
        
        if dictPatirntDetail?.patientCovidSymptoms?.covidCough == "no" && dictPatirntDetail?.patientCovidSymptoms?.covidFever == "no" && dictPatirntDetail?.patientCovidSymptoms?.covidLossOfTaste == "no" && dictPatirntDetail?.patientCovidSymptoms?.covidRunnyNose == "no" && dictPatirntDetail?.patientCovidSymptoms?.covidDiarrhea == "no" && dictPatirntDetail?.patientCovidSymptoms?.covidSoreThroat == "no" &&
            dictPatirntDetail?.patientCovidSymptoms?.covidShaking == "no"{
            return false
        } else {
            return true
        }
    }
    
    func setupTextField()  {
        
        txtFirstName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMiddleName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDOB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    @objc func EditButtonPressed(notification: Notification) {
        if let strEdit = notification.userInfo?["EDIT"] as? String {
            if strEdit == "ClientDetailsFormVC" {
                Constraint_ButtonsHeight.constant = 70
                viewButtons.isHidden = false
                setupEditModeView()
                disableAllViews(shouldDisable: false)
            }
        }
    }
    
    func SubmitForm() {
        
        dateFormatter.dateFormat = APIDateFormate
        
        var paradict = [String : Any]()
        paradict["patientId"] = dictPatirntDetail?._id
        paradict["firstName"] = txtFirstName.text
        paradict["middleName"] = txtMiddleName.text
        paradict["lastName"] = txtSurname.text
        paradict["phoneNumber"] = Int(txtMobNo.text ?? "")
        paradict["alternatePhoneNumber"] = Int(txtAlternateMobNo.text ?? "")
        paradict["address"] = txtAddress.text
        paradict["visitDate"] = dateFormatter.string(from: visitDate)
        paradict["dateOfBirth"] = dateFormatter.string(from: birthDate)
        paradict["sex"] = selectedGender
        paradict["age"] = Int(lblAge.text ?? "0")
        paradict["state"] = txtState.text
        paradict["lga"] = txtLGA.text
        paradict["note"] = txtNotes.text
        
        USER_UPDATE_PATIENT_DETAILS(paradict: paradict, completionHandler: { (isSucces , strData , strMessage) in
            if strData != "" {
                if isSucces {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    func makeParametrForAPI() -> [String:String]  {
        
        return ["covidCough" : dictPatirntDetail?.patientCovidSymptoms?.covidCough ?? "no" ,
                "covidFever" : dictPatirntDetail?.patientCovidSymptoms?.covidFever ?? "no",
                "covidLossOfTaste" :  dictPatirntDetail?.patientCovidSymptoms?.covidLossOfTaste ?? "no",
                "covidRunnyNose" :  dictPatirntDetail?.patientCovidSymptoms?.covidRunnyNose ?? "no",
                "covidDiarrhea" :  dictPatirntDetail?.patientCovidSymptoms?.covidDiarrhea ?? "no",
                "covidSoreThroat" :  dictPatirntDetail?.patientCovidSymptoms?.covidSoreThroat ?? "no",
                "covidShaking" :  dictPatirntDetail?.patientCovidSymptoms?.covidShaking ?? "no"]
    }
    
    func PatientSymtoms(value : [String : String]) -> String {
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(value) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        }
        return ""
    }
    
    func isValidDetail() -> Bool {
        if let name = txtFirstName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        if let dob = txtDOB.text, dob.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterDOB)
            return false
        }
        if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }
        if let mobNo = txtMobNo.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMobNo.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        return true
    }
    
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            present(datePickerVC, animated: true, completion: nil)
        }
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            dropDown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropDown.dataSource = addressDetail.LGA ?? []
            }
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropDown.hide()
        }
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
            getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                if success {
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                        self.designationDetail = DesignationDetail(dict: json)
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                    
                }else{
                    self.hideHUD()
                }
            })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func setupEditModeView() {
        
        disableAllViews(shouldDisable: false)
    }
    
    func disableAllViews(shouldDisable : Bool) {
        
        for i in 1..<5{
            let view = vwMainView.viewWithTag(i)
            view?.isUserInteractionEnabled = !shouldDisable
        }
    }
    
    @IBAction func btnMaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }
    
    @IBAction func btnFemaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        
        if isValidDetail() {
            SubmitForm()
        }
    }
    
    @IBAction func btnExitPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPatientStatusTap(_ sender: Any) {
        
    }
    
    @IBAction func btnCovidStatusTap(_ sender: Any) {
        
        if isCovidScreened{
            
            if let covidScreenedVC = self.storyboard?.instantiateViewController(withIdentifier: "CovidSymptomsDetailViewController") as? CovidSymptomsDetailViewController{
                covidScreenedVC.covidSymptom = dictPatirntDetail?.patientCovidSymptoms
                covidScreenedVC.modalPresentationStyle = .overCurrentContext
                view.window?.rootViewController?.present(covidScreenedVC, animated: true, completion: nil)
            }
        }else{
            
            if let askForCovid19 = self.storyboard?.instantiateViewController(withIdentifier: "Covid19ScreeningViewController") as? Covid19ScreeningViewController{
                askForCovid19.isFromEdit = true
                askForCovid19.delegate = self
                askForCovid19.patientRecordNumber = dictPatirntDetail?.patientRecordNumber ?? ""
                view.window?.rootViewController?.present(askForCovid19, animated: true, completion: nil)
            }
        }
    }
}

extension ClientDetailsFormVC  : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDOB{
            openDatePicker(dateType: .BirthDate)
            return false
        }
        if textField == txtDateOfVisit{
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtState{
            setupStateDropdown()
            return false
        }
        if textField == txtLGA{
            setupLGADropdown()
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobNo || textField == txtAlternateMobNo{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension ClientDetailsFormVC : DatePickerDelegate{
    
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDOB.text = dateFormatter.string(from: sDate)
            let age = Calendar.current.dateComponents([.year], from: sDate, to: Date()).year!
            lblAge.text = "\(age)"
            birthDate = sDate
        }else{
            txtDateOfVisit.text = dateFormatter.string(from: sDate)
            visitDate = sDate
        }
    }
}

extension ClientDetailsFormVC : ThanksPopupDelegate {
    
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if let homevc = vc as? UserHomeVC{
                    navigationController?.popToViewController(homevc, animated: true)
                }
            }
        }
    }
}

extension ClientDetailsFormVC : Covid19ScreenDelegate {
    func updatePatientRecord(covidDict: [String : Any]) {
        let covidDetail = PatientCovidSymptoms(dict: covidDict)
        dictPatirntDetail?.patientCovidSymptoms = covidDetail
        setupEditModeView()
    }
}
