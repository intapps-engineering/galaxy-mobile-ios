//
//  MyClientDetailsVC.swift
//  Tbstar
//
//  Created by Mavani on 10/06/21.
//

import UIKit
import CarbonKit

class MyClientDetailsVC: UIViewController {

    @IBOutlet var lblClientName : UILabel!
    
    @IBOutlet weak var viewChildAdd: UIView!
        
    @IBOutlet var btnEdit : UIButton!
    
    var tabSwipe = CarbonTabSwipeNavigation()

    var PATIENT_DETAIL : PPMV_PatientInformation?
    
    var whichEditPressed = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TopSliderSetUp()
    }
    
    override func viewDidLayoutSubviews() {
        
        tabSwipe.toolbar.barTintColor = .white
        tabSwipe.toolbar.clipsToBounds = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        lblClientName.text = "\(PATIENT_DETAIL?.firstName ?? "") \(PATIENT_DETAIL?.surname ?? "")"
    }
    
    //MARK:- TabBar SetUp
    func TopSliderSetUp() {
        
        tabSwipe = CarbonTabSwipeNavigation(items: ["clientDetails".localized, "Specimen_Dispatch".localized], delegate: self)

        var frameRect: CGRect = (tabSwipe.carbonSegmentedControl?.frame)!
        frameRect.size.width = viewChildAdd.frame.size.width
        tabSwipe.carbonSegmentedControl?.frame = frameRect
        tabSwipe.carbonSegmentedControl?.apportionsSegmentWidthsByContent = false
        tabSwipe.setTabBarHeight(50)
        tabSwipe.setNormalColor(.UnselectedBlueColor)
        tabSwipe.setSelectedColor(.SlectedBlueColor)
        tabSwipe.setIndicatorColor(.SlectedBlueColor)
        let font =  UIFont(name:"Quicksand-Medium",size:16)
        tabSwipe.setNormalColor(.UnselectedBlueColor, font: font!)
        tabSwipe.setSelectedColor(.SlectedBlueColor, font: font!)
        tabSwipe.insert(intoRootViewController: self, andTargetView: viewChildAdd)
        
        viewChildAdd.addSubview(tabSwipe.view)
        tabSwipe.view.frame = viewChildAdd.bounds
        tabSwipe.didMove(toParent: self)
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditPressed(_ sender : UIButton) {
        
        btnEdit.isHidden = true
        lblClientName.text = "\("Update".localized) \(PATIENT_DETAIL?.firstName ?? "")"
        let dictEdit:[String: String] = ["EDIT": whichEditPressed]
        NotificationCenter.default.post(name: Notification.Name("EditButtonPressed"), object: nil ,  userInfo: dictEdit)
    }
}

//MARK:- TabBar delegates
extension MyClientDetailsVC : CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard storyboard != nil else { return UIViewController() }
        switch index {
            case 0:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "ClientDetailsFormVC")
                    as! ClientDetailsFormVC
                VC.dictPatirntDetail = PATIENT_DETAIL
                return VC
            case 1:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "SpecimenDispatchVC") as! SpecimenDispatchVC
                VC.dictPatirntDetail = PATIENT_DETAIL
                return VC
            default: break
        }
        return UIViewController()
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        switch index {
            case 0:
                btnEdit.isHidden = false
                whichEditPressed = "ClientDetailsFormVC"
            case 1:
                btnEdit.isHidden = false
                whichEditPressed = "SpecimenDispatchVC"
            default: break
        }
    }
}
