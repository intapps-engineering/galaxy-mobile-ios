//
//  SpecimenDispatchVC.swift
//  Tbstar
//
//  Created by Mavani on 10/06/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
import StepSlider

class SpecimenDispatchVC: BaseViewController , UIScrollViewDelegate {
    
    @IBOutlet weak var vwMainView: UIView!
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var viewEmpty: UIView!
    
    @IBOutlet var lblTemprature : UILabel!
    @IBOutlet var lblTempratureValue : UILabel!
    @IBOutlet var lblNumberOfSampleTitle : UILabel!
    @IBOutlet var lblNumberOfSampleSend : UILabel!
    @IBOutlet var lblApprove : UILabel!
    @IBOutlet var lblEmpty : UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    
    @IBOutlet weak var txtIdentifySample : SkyFloatingLabelTextField!
    @IBOutlet weak var txtSpecimenIDNumber : SkyFloatingLabelTextField!
    @IBOutlet weak var txtType : SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfCollection : SkyFloatingLabelTextField!
    @IBOutlet weak var txtLabName : SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfDispatch : SkyFloatingLabelTextField!
    @IBOutlet weak var txtHIVStatus: SkyFloatingLabelTextField!
    @IBOutlet weak var txtReasonForExamination: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTestedTB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTypeOfTestRequest : SkyFloatingLabelTextField!
    @IBOutlet weak var txtTemprature: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNameOfDispachingOfficer: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumberOfDispachingOfficer: SkyFloatingLabelTextField!
    
    @IBOutlet var viewSider : StepSlider!
    
    @IBOutlet var Constraint_ButtonsHeight : NSLayoutConstraint!
    
    @IBOutlet var scrollview : UIScrollView!
    
    var DateOfCollection = Date()
    var DateOfDispatch = Date()
    
    var dateFormatter = DateFormatter()
    
    let dropDown = DropDown()
    
    var PPMVDropDown : PPMV_Lab_Drop_Down?
    var dictPatirntDetail : PPMV_PatientInformation?
    
    var SampleCount = 1
    var ISFORM : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = displayDateFormat
        
        setupLocalization()
        askForLocation()
        setupTextField()
        Get_DropDown_Data()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblTempratureValue.text = "Celsius".localized
        
        scrollview.isHidden = true
        viewEmpty.isHidden = true
        
        SetVlaueInForm()
    }
    
    func SetVlaueInForm() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.EditButtonPressed(notification:)), name: Notification.Name("EditButtonPressed"), object: nil)
        
        Constraint_ButtonsHeight.constant = 0
        viewButtons.isHidden = true
        
        if dictPatirntDetail?.specimenDetails.count ?? 0 > 0 {
            
            scrollview.isHidden = false
            let sample = dictPatirntDetail!.specimenDetails.map{($0.numberOfSample)}.map(String.init).joined()
            if let samples = Int(sample) {
                viewSider.maxCount = 6
                viewSider.index = UInt(samples)
                SampleCount = samples
                
                if SampleCount <= 1 {
                    lblNumberOfSampleSend.text = "\(SampleCount) \("Sample".localized)"
                } else {
                    lblNumberOfSampleSend.text = "\(SampleCount)  \("Samples".localized)"
                }
            }
            
            let sampleExternalId = dictPatirntDetail!.specimenDetails.map{($0.sampleExternalId)}.joined()
            txtIdentifySample.text = sampleExternalId
            
            let specimenId = dictPatirntDetail!.specimenDetails.map{($0.specimenId)}.joined()
            txtSpecimenIDNumber.text = specimenId
            
            let typeofspecimen = dictPatirntDetail!.specimenDetails.map{($0.typesOfSpecimen)}.joined()
            txtType.text = typeofspecimen
            
            let dateofcollection = dictPatirntDetail!.specimenDetails.map{($0.dateOfCollection)}.joined()
            txtDateOfCollection.text = ChangeDisplayingDateFoemate(strDate:dateofcollection)
            DateOfCollection = dateFormatter.date(from: dateofcollection) ?? Date()
            
            let dateofdispatch = dictPatirntDetail!.specimenDetails.map{($0.dateOfDispatch)}.joined()
            txtDateOfDispatch.text = ChangeDisplayingDateFoemate(strDate:dateofdispatch)
            DateOfDispatch = dateFormatter.date(from: dateofdispatch) ?? Date()
            
            let labname = dictPatirntDetail!.specimenDetails.map{($0.laboratoryName)}.joined()
            txtLabName.text = labname
            
            let hivstatus = dictPatirntDetail!.specimenDetails.map{($0.hivStatus)}.joined()
            txtHIVStatus.text = hivstatus
            
            let reasonforexamination = dictPatirntDetail!.specimenDetails.map{($0.reasonOfExamination)}.joined()
            txtReasonForExamination.text = reasonforexamination
            
            let TbTested = dictPatirntDetail!.specimenDetails.map{($0.previousTreatmentForTb)}.joined()
            txtTestedTB.text = TbTested
            
            let requesttype = dictPatirntDetail!.specimenDetails.map{($0.typeOfTest)}.joined()
            txtTypeOfTestRequest.text = requesttype
            
            let temprature = dictPatirntDetail!.specimenDetails.map{($0.temperature)}.joined()
            print(temprature)
            txtTemprature.text = temprature
            
            let officername = dictPatirntDetail!.specimenDetails.map{($0.nameOfCollectOffcer)}.joined()
            txtNameOfDispachingOfficer.text = officername
            
            let officerphonenumber = dictPatirntDetail!.specimenDetails.map{($0.nameOfCollectOffcerPhNo)}.joined()
            txtPhoneNumberOfDispachingOfficer.text = officerphonenumber
            
            disableAllViews(shouldDisable: true)
            
        } else {
            viewEmpty.isHidden = false
            lblEmpty.text = "Empty_Facility".localized
        }
    }
    
    @objc func EditButtonPressed(notification: Notification) {
        if let strEdit = notification.userInfo?["EDIT"] as? String {
            if strEdit == "SpecimenDispatchVC" {
                Constraint_ButtonsHeight.constant = 120
                viewButtons.isHidden = false
                setupEditModeView()
                disableAllViews(shouldDisable: false)
            }
        }
    }
    
    func ChangeDisplayingDateFoemate(strDate : String) -> String {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = apiDateFormatFromAPI
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = displayDateFormat
        let date = dateFormatterGet.date(from: strDate)
        
        return dateFormatter.string(from: date ?? Date())
    }
    
    func disableAllViews(shouldDisable : Bool) {
        
        for i in 1..<5{
            let view = vwMainView.viewWithTag(i)
            view?.isUserInteractionEnabled = !shouldDisable
        }
    }
    
    func setupEditModeView() {
        
        disableAllViews(shouldDisable: false)
    }
    
    func setupLocalization() {
        
        lblTemprature.text = "Temperature".localized
        lblNumberOfSampleTitle.text = "Number_of_Samples_Sent_to_Lab".localized
        lblNumberOfSampleSend.text = "\(0) \("Sample".localized)"
        lblApprove.text = "I_approve_the_submission_of_this_sample".localized
        
        txtIdentifySample.placeholder = "Use this to identify samples for testing".localized
        txtSpecimenIDNumber.placeholder = "Specimen ID Number".localized
        txtType.placeholder = "Type_of_Specimen".localized
        txtDateOfCollection.placeholder = "Date_of_Collection".localized
        txtLabName.placeholder = "Laboratory_Name".localized
        txtDateOfDispatch.placeholder = "Date_of_Dispatch_to_Lab".localized
        txtHIVStatus.placeholder = "HIV_Status".localized + "*"
        txtReasonForExamination.placeholder = "Reason_for_Examination".localized + "*"
        txtTestedTB.placeholder = "Ever_Previously_been_Treated_for_TB".localized + "*"
        txtTypeOfTestRequest.placeholder = "Type_of_Test_Requested".localized + "*"
        txtTemprature.placeholder = "Temperature".localized + "*"
        txtNameOfDispachingOfficer.placeholder = "Name_of_Dispatching_Officer".localized
        txtPhoneNumberOfDispachingOfficer.placeholder = "Phone_Number_of_Dispatching_Officer".localized
        
        btnExit.setTitle("exit".localized, for: .normal)
        btnSave.setTitle("save".localized, for: .normal)
        
        lblTempratureValue.text = "".localized
    }
    
    func setupTextField()  {
        
        txtIdentifySample.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtIdentifySample.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtIdentifySample.titleColor = UIColor(hexString: color.appRedColor)
        
        txtType.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtType.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtType.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfCollection.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfCollection.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfCollection.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLabName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLabName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLabName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfDispatch.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfDispatch.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfDispatch.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtHIVStatus.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtHIVStatus.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtHIVStatus.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtReasonForExamination.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtReasonForExamination.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtReasonForExamination.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTestedTB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTestedTB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTestedTB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTypeOfTestRequest.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTypeOfTestRequest.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTypeOfTestRequest.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTemprature.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTemprature.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTemprature.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtNameOfDispachingOfficer.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtNameOfDispachingOfficer.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtNameOfDispachingOfficer.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtPhoneNumberOfDispachingOfficer.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPhoneNumberOfDispachingOfficer.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPhoneNumberOfDispachingOfficer.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    func Get_DropDown_Data() {
        
        if isConnectedToNetwork() {
            showHUD()
            USER_GET_ForDispatchForm(completionHandler: { (isSuccess,strdata,message) in
                
                if strdata != ""{
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.savePPMVdeatilsForDispatchToStandard(detail: json as NSDictionary)
                        self.PPMVDropDown = PPMV_Lab_Drop_Down(dict: json)
                        self.hideHUD()
                    }
                } else {
                    self.hideHUD()
                }
            })
        }else{
            self.PPMVDropDown = getPPMVdeatilsForDispatchToStandard()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func isValidDetail() -> Bool {
        
        if let name = txtType.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.selectSpecimanType)
            return false
        }
        if let datecollection = txtDateOfCollection.text, datecollection.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.validCollectionDate)
            return false
        }
        
        if let dispatchdate = txtDateOfDispatch.text , dispatchdate.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.CompareCollectionDate)
            return false
        }
        
        if DateOfCollection >= DateOfDispatch {
            showAlert(titleStr: appName, msg: appMessages.CompareCollectionDate)
            return false
        }
        if let state = txtHIVStatus.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectHIV)
            return false
        }
        if let lga = txtReasonForExamination.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectReasonForExamination)
            return false
        }
        if let TB = txtTestedTB.text, TB.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectTReatedForTB)
            return false
        }
        if let name = txtTypeOfTestRequest.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.SelectTypeRequest)
            return false
        }
        if SampleCount <= 0 {
            showAlert(titleStr: appName, msg: appMessages.SelectSample)
            return false
        }
        return true
    }
    
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            present(datePickerVC, animated: true, completion: nil)
        }
    }
    
    func setupTypeOFSpecimenDropdown()  {
        
        dropDown.anchorView = txtType
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.SpecimanType.map{($0.specimenTypes)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtType.text = item
            dropDown.hide()
        }
    }
    
    func setupHIVStatusDropdown() {
        
        dropDown.anchorView = txtHIVStatus
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.hivstatus.map{($0.hivStatus)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtHIVStatus.text = item
            dropDown.hide()
        }
    }
    
    func setupReasonForExaminationDropdown()  {
        
        dropDown.anchorView = txtReasonForExamination
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TestReason.map{($0.reasonOfTest)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtReasonForExamination.text = item
            dropDown.hide()
        }
    }
    
    func setupTreatedForTBDropdown()  {
        
        dropDown.anchorView = txtTestedTB
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TbStatus.map{($0.TBCheck)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtTestedTB.text = item
            dropDown.hide()
        }
    }
    
    func setupTypeOfTestReuestDropdown()  {
        
        dropDown.anchorView = txtTypeOfTestRequest
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TestType.map{($0.typeOfTest)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            txtTypeOfTestRequest.text = item
            dropDown.hide()
        }
    }
    
    func SubmitForm() {
        
        dateFormatter.dateFormat = APIDateFormate
        
        let _id = dictPatirntDetail!.specimenDetails.map{($0._id)}.joined()
        let specimenId = dictPatirntDetail!.specimenDetails.map{($0.specimenId)}.joined()
        let labID = dictPatirntDetail!.specimenDetails.map{($0.labId)}.joined()
        let labName = dictPatirntDetail!.specimenDetails.map{($0.laboratoryName)}.joined()
        
        var boolreferToCLinic = false
        if specimenId == "" {
            boolreferToCLinic  = true
        }
        var paradict = [String : Any]()
        paradict["patientId"] = dictPatirntDetail?._id
        paradict["specimen_id"] = _id
        paradict["labId"] = labID
        paradict["typesOfSpecimen"] = txtType.text
        paradict["dateOfDispatch"] = dateFormatter.string(from: DateOfDispatch)
        paradict["dateOfCollection"] = dateFormatter.string(from: DateOfCollection)
        paradict["hivStatus"] = txtHIVStatus.text
        paradict["reasonOfExamination"] = txtReasonForExamination.text
        paradict["previousTreatmentForTb"] = txtTestedTB.text
        paradict["typeOfTest"] = txtTypeOfTestRequest.text
        paradict["numberOfSample"] = SampleCount
        paradict["specimenId"] = specimenId
        paradict["referToCLinic"] = boolreferToCLinic
        paradict["laboratoryName"] = labName
        
        USER_UPDATE_SPECIMEN_DETAILS(paradict: paradict, completionHandler: { (isSucces , strData , strMessage) in
            if strData != "" {
                if isSucces {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        })
    }
    
    @IBAction func changeValue(_ sender: StepSlider) {
        
        let count = UInt(sender.index)
        if count <= 1 {
            lblNumberOfSampleSend.text = "\(count) \("Sample".localized)"
        } else {
            lblNumberOfSampleSend.text = "\(count)  \("Samples".localized)"
        }
        
        SampleCount  =  Int(count)
    }
    
    @IBAction func btnSavePressed(_ sender: Any) {
        
        if isValidDetail() {
            SubmitForm()
        }
    }
    
    @IBAction func btnExitPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension SpecimenDispatchVC  : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDateOfCollection {
            openDatePicker(dateType: .BirthDate)
            return false
        }
        if textField == txtDateOfDispatch {
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtType{
            setupTypeOFSpecimenDropdown()
            return false
        }
        if textField == txtHIVStatus{
            setupHIVStatusDropdown()
            return false
        }
        if textField == txtReasonForExamination{
            setupReasonForExaminationDropdown()
            return false
        }
        if textField == txtTestedTB{
            setupTreatedForTBDropdown()
            return false
        }
        if textField == txtTypeOfTestRequest{
            setupTypeOfTestReuestDropdown()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNameOfDispachingOfficer {
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

extension SpecimenDispatchVC : DatePickerDelegate{
    
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDateOfCollection.text = dateFormatter.string(from: sDate)
            DateOfCollection = sDate
        }else{
            txtDateOfDispatch.text = dateFormatter.string(from: sDate)
            DateOfDispatch = sDate
        }
    }
}
