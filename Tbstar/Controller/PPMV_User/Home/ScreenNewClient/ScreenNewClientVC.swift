//
//  ScreenNewClientVC.swift
//  Tbstar
//
//  Created by Mavani on 24/05/21.
//

import UIKit

class ScreenNewClientVC: BaseViewController {

    @IBOutlet var lblHeader : UILabel!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDesc : UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNO: UIButton!

    var isPatientHaveTB = false
    var strIsPatientHaveTB = "no"

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
    }
    
    func setupLocalization() {
        
        lblTitle.text = "Patient's Symptoms".localized
        lblHeader.text = "Screen New Client".localized
        lblDesc.text = "Has client had persistent cough for 2 or more weeks?".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
        
        btnYes.setTitle("yes".localized, for: .normal)
        btnYes.setTitle("yes".localized, for: .selected)
        
        btnNO.setTitle("no".localized, for: .normal)
        btnNO.setTitle("no".localized, for: .selected)
    }
    
    @IBAction func btnYESNOTap(_ sender: UIButton) {
        
        btnYes.isSelected = false
        btnNO.isSelected = false
        sender.isSelected = true
        if sender.tag == 1 {
            isPatientHaveTB = true
            strIsPatientHaveTB = "yes"
        } else {
            isPatientHaveTB = false
            strIsPatientHaveTB = "no"
        }
    }

    @IBAction func btnSubmitPressed(_ sender : UIButton) {
        
        if btnYes.isSelected == false &&  btnNO.isSelected == false {
            showAlert(titleStr: appName, msg: appMessages.questionMustAns)
            return
        }
        
        if let askForCovid19Screen = self.storyboard?.instantiateViewController(withIdentifier: "AskForCovid19PopupViewController") as? AskForCovid19PopupViewController{
            
            askForCovid19Screen.popupTitle = "Screen For COVID19"
            if isPatientHaveTB{
                askForCovid19Screen.popupSubtitle = "Patient has presumptive TB, would you like to screen patient for COVID19?"
            }else{
                askForCovid19Screen.popupSubtitle = "Patient does not have presumptive TB, would you like to screen patient for COVID19?"
            }
            askForCovid19Screen.modalPresentationStyle = .overCurrentContext
            askForCovid19Screen.delegate = self
            present(askForCovid19Screen, animated: true, completion: nil)
        }
    }
    
    func makeParametrForAPI() -> [String:Any]  {
        
        return ["CoughFor2WeeksOrMore" : strIsPatientHaveTB]
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
        
        currentMenu = MenuOption.Home
        self.navigationController?.popViewController(animated: true)
    }
}

extension ScreenNewClientVC : AskForCovid19PopupViewControllerDelegate {
    
    func selectedOption(isYes: Bool) {
        
        if isYes{
            
            if let covid19ScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "Covid19ScreeningViewController") as? Covid19ScreeningViewController{
                covid19ScreenVC.isPatientHaveTB = isPatientHaveTB
                covid19ScreenVC.TBDict = makeParametrForAPI()
                navigationController?.pushViewController(covid19ScreenVC, animated: true)
            }
            
        }else{
            
            if isPatientHaveTB {
                
                if let addNewClientVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAddNewClientDetailVC") as? UserAddNewClientDetailVC {
                    addNewClientVC.TBDict = makeParametrForAPI()
                    navigationController?.pushViewController(addNewClientVC, animated: true)
                }
                
            }else{
                
                if let noTBDetectedVC = self.storyboard?.instantiateViewController(withIdentifier: "TBNotDetectedViewController") as? TBNotDetectedViewController{
                    noTBDetectedVC.TBDict = makeParametrForAPI()
                    navigationController?.pushViewController(noTBDetectedVC, animated: true)
                }
            }
        }
    }
}
