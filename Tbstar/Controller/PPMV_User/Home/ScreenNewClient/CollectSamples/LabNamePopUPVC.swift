//
//  LabNamePopUPVC.swift
//  Tbstar
//
//  Created by Mavani on 28/05/21.
//

import UIKit

protocol LabSelectedDelegate : class {
    
    func btnSubmitPressed(index : Int , ISFrom : String , ISFormLab : String)
}

class LabNamePopUPVC: UIViewController {

    @IBOutlet var lblSelect : UILabel!
    @IBOutlet var lblLabName : UILabel!
    @IBOutlet var lblLabAddress : UILabel!
    
    @IBOutlet var btnSubmit : UIButton!
    
    var delegate : LabSelectedDelegate?
    
    var selectedLab : PPMV_Lab_List?
    
    var SelectedINDEX : Int?
    
    var ISFormLab : String?
    var ISFROMLabSelected : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLocalization()
    }
    
    func setupLocalization() {
        
        if ISFormLab == "Laboratory" {
            if ISFROMLabSelected == "Not_Lab_Selected" {
                lblTextSetup(lblName: "", lblAddress: "Send Sample to lab later".localized, lblTitle: "Lab_Not_Selected".localized)
            } else {
                lblTextSetup(lblName: selectedLab?.facilityName ?? "", lblAddress: "\(selectedLab?.address ?? "") ,\(selectedLab?.LGA ?? ""),\(selectedLab?.facilityState ?? "")", lblTitle: "Lab_Selected".localized)
            }
        } else {
            if ISFROMLabSelected == "Not_Lab_Selected" {
                lblTextSetup(lblName: "", lblAddress: "Send Sample to lab later".localized, lblTitle: "Clinic_Not_Selected".localized)
            } else {
                lblTextSetup(lblName: selectedLab?.facilityName ?? "", lblAddress: "\(selectedLab?.address ?? "") ,\(selectedLab?.LGA ?? ""),\(selectedLab?.facilityState ?? "")", lblTitle: "Lab_Selected".localized)
            }
        }
        
        btnSubmit.setTitle("Select".localized, for: .normal)
    }

    func lblTextSetup(lblName : String , lblAddress : String , lblTitle : String) {
        
        lblSelect.text = lblTitle
        lblLabName.text = lblName
        lblLabAddress.text = lblAddress
    }
    
    @IBAction func btnShadowPressed(_ sender : UIButton) {

        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitPressed(_ sender : UIButton) {
        
        dismiss(animated: true, completion: nil)
        if ISFROMLabSelected == "Not_Lab_Selected" {
            delegate?.btnSubmitPressed(index : 0, ISFrom: ISFROMLabSelected ?? "", ISFormLab: ISFormLab ?? "")
        } else {
            delegate?.btnSubmitPressed(index : SelectedINDEX ?? 0, ISFrom: ISFROMLabSelected ?? "", ISFormLab: ISFormLab ?? "")
        }
    }
}
