//
//  CollectSampleInstaructionVC.swift
//  Tbstar
//
//  Created by Mavani on 27/05/21.
//

import UIKit

class CollectSampleInstaructionVC: UIViewController {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblImportant : UILabel!
    @IBOutlet var lblNotes : UILabel!

    @IBOutlet weak var btnSubmit: UIButton!

    var PATIENT_DETAIL : [String : Any]?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
    }
    
    func setupLocalization() {
        
        lblTitle.text = "Instructions".localized
        lblImportant.text = "IMPORTANT".localized
        lblNotes.text = "Notes".localized
        btnSubmit.setTitle("ok".localized, for: .normal)
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
     
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SelectLabVC") as? SelectLabVC {
            VC.PATIENT_DETAIL = PATIENT_DETAIL
            VC.ISFROM = "Laboratory"
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}
