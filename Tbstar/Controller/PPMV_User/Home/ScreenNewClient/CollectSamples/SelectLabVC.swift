//
//  SelectLabVC.swift
//  Tbstar
//
//  Created by Mavani on 27/05/21.
//

import UIKit


class CellSelectLab : UITableViewCell {
    
    @IBOutlet var lblLabName : UILabel!
}

class SelectLabVC: BaseViewController {
    
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var btnCollectSample : UIButton!
    
    @IBOutlet var tblLabName : UITableView!
    
    @IBOutlet var constraintCollectSampleHeight : NSLayoutConstraint!
    @IBOutlet var constrainttblData : NSLayoutConstraint!

    var totalPages = 0
    var pageIndex = 0
    
    var arrLabList = [PPMV_Lab_List]()
    
    var PATIENT_DETAIL : [String : Any]?
    var dictSelectedLab : PPMV_Lab_List?
    
    var ISFROM : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isConnectedToNetwork() {
            
            self.showHUD()
            tblLabName.isHidden = false
            if ISFROM == "Laboratory" {
                btnCollectSample.isHidden = false
                constraintCollectSampleHeight.constant = 50
                constrainttblData.constant = 20
                GetLabName(boolForCheckLabORClinic: false)
                lblTitle.text = "Select_Laboratory".localized
            } else {
                btnCollectSample.isHidden = true
                constrainttblData.constant = 0
                constraintCollectSampleHeight.constant = 0
                GetLabName(boolForCheckLabORClinic: true)
                lblTitle.text = "Select_Clinic".localized
            }
            
        } else {
            if ISFROM == "Laboratory" {
                lblTitle.text = "Select_Laboratory".localized
            } else {
                lblTitle.text = "Select_Clinic".localized
            }
            tblLabName.isHidden = true
        }
        btnCollectSample.setTitle("CollectSampleNowAndSendLater".localized, for: .normal)
    }
    
    func GetLabName(boolForCheckLabORClinic : Bool) {
        
        if isConnectedToNetwork() {
            
            var paradict = [String : Any]()
            paradict["page"] = 15
            paradict["pageIndex"] = pageIndex
            paradict["referToCLinic"] = boolForCheckLabORClinic
            
            USER_Get_Lab_List(paradict: paradict, completionHandler: { (success , strData, message) in
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if success == "true"{
                        if let json = self.convertStringToDictionary(text: strResponse) {
                            self.totalPages = json["totalPage"] as? Int ?? 0
                            if let list = json["labList"] as? [[String :Any]]{
                                if self.pageIndex == 0{
                                    self.arrLabList = list.map(PPMV_Lab_List.init)
                                }else{
                                    self.arrLabList.append(contentsOf: list.map(PPMV_Lab_List.init))
                                }
                                self.tblLabName.reloadData()
                                self.hideHUD()
                            }
                        }
                    }  else{
                        if self.pageIndex == 0{
                            self.view.makeToast(strResponse)
                        }
                        self.hideHUD()
                    }
                }
            })
            
        } else {
            self.hideHUD()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func AddRecentSearchApi(index : Int , strSearchTYpe : String) {
        
        var paradict = [String : Any]()
        paradict["labId"] = arrLabList[index]._id
        paradict["searchType"] = strSearchTYpe

        USER_ADD_RECENT_SERCH_LAB(paradict: paradict, completionHandler: { (success , strData , message) in
            
            if success == "true" {
                print(strData)
            }
        })
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSearchPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "RecentSearchLabVC") as? RecentSearchLabVC {
            VC.ISForm = ISFROM
            VC.PATIENT_DETAIL = PATIENT_DETAIL
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnCollectSamplePressed(_ sender : UIButton) {
        
        if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"LabNamePopUPVC") as? LabNamePopUPVC {
            selectionPopup.modalPresentationStyle = .overCurrentContext
            selectionPopup.delegate = self
            selectionPopup.ISFormLab = ISFROM
            selectionPopup.ISFROMLabSelected = "Not_Lab_Selected"
            present(selectionPopup, animated: true, completion: nil)
        }
    }
}

extension SelectLabVC : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrLabList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblLabName.dequeueReusableCell(withIdentifier: "CellSelectLab") as! CellSelectLab
        
        let dictLabName = arrLabList[indexPath.row]
        cell.lblLabName.text = dictLabName.facilityName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        dictSelectedLab = arrLabList[indexPath.row]
        if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"LabNamePopUPVC") as? LabNamePopUPVC {
            selectionPopup.modalPresentationStyle = .overCurrentContext
            selectionPopup.selectedLab = arrLabList[indexPath.row]
            selectionPopup.SelectedINDEX = indexPath.row
            selectionPopup.delegate = self
            selectionPopup.ISFROMLabSelected = "Lab_Selected"
            selectionPopup.ISFormLab = ISFROM
            present(selectionPopup, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrLabList.count - 1 {
            if pageIndex < totalPages {
                pageIndex += 1
                if ISFROM == "Laboratory" {
                    GetLabName(boolForCheckLabORClinic: false)
                } else {
                    GetLabName(boolForCheckLabORClinic: true)
                }
            }
        }
    }
}

extension SelectLabVC : LabSelectedDelegate {
    
    func btnSubmitPressed(index: Int, ISFrom: String, ISFormLab: String) {
        if ISFormLab == "Laboratory" {
            if ISFrom == "Not_Lab_Selected" {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SampleDetailsVC") as? SampleDetailsVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            } else {
                AddRecentSearchApi(index: index, strSearchTYpe: "labtech")
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SampleDetailsVC") as? SampleDetailsVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.SelectedLabDetail = dictSelectedLab
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        } else {
            if ISFrom == "Not_Lab_Selected" {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "ClinicSampleDetailVC") as? ClinicSampleDetailVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            } else {
                AddRecentSearchApi(index: index, strSearchTYpe: "clinician")
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "ClinicSampleDetailVC") as? ClinicSampleDetailVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.SelectedLabDetail = dictSelectedLab
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        }
    }
}

