//
//  RecentSearchLabVC.swift
//  Tbstar
//
//  Created by Mavani on 31/05/21.
//

import UIKit


class RecentSearchLabVC: BaseViewController {
    
    @IBOutlet var lblRecentSearch : UILabel!
    
    @IBOutlet var btnCollectSample : UIButton!
    
    @IBOutlet var tblLabName : UITableView!
    @IBOutlet var tblSearchLabName : UITableView!
    
    @IBOutlet var txtSearch : UITextField!
    
    var totalPages = 0
    var pageIndex = 0
    
    var SearchPageIndex = 0
    var SearchTotalPages = 0

    var arrRecentSearchLabList = [PPMV_Lab_List]()
    var arrSearchLabList = [PPMV_Lab_List]()
    
    var dictSelectedLab : PPMV_Lab_List?

    @IBOutlet var tblRecentSearchHeight : NSLayoutConstraint!
    @IBOutlet var tblSearchHeight : NSLayoutConstraint!
    @IBOutlet var constraintCollectSampleHeight : NSLayoutConstraint!
    @IBOutlet var constrainttblSearchesData : NSLayoutConstraint!

    var ISForm : String?

    var PATIENT_DETAIL : [String : Any]?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblRecentSearchHeight.constant = 0
        tblSearchHeight.constant = 0

        arrRecentSearchLabList.removeAll()
        arrSearchLabList.removeAll()
        
        if ISForm == "Laboratory" {
            btnCollectSample.isHidden = false
            constraintCollectSampleHeight.constant = 50
            constrainttblSearchesData.constant = 20
            txtSearch.placeholder = "Laboratory".localized
            GetRecentSearchLabName(strSearchType : "labtech")
        } else {
            btnCollectSample.isHidden = true
            constraintCollectSampleHeight.constant = 0
            constrainttblSearchesData.constant = -20
            txtSearch.placeholder = "Clinic".localized
            GetRecentSearchLabName(strSearchType : "clinician")
        }
    }

    func setupLocalization() {
        
        lblRecentSearch.text = "Recent_Searches".localized
        btnCollectSample.setTitle("CollectSampleNowAndSendLater".localized, for: .normal)
    }

    func GetRecentSearchLabName(strSearchType : String) {
        
        self.showHUD()
        if isConnectedToNetwork() {
            
            var paradict = [String : Any]()
            paradict["searchType"] = strSearchType
            paradict["page"] = 10
            paradict["pageIndex"] = pageIndex
            paradict["dotsclinic"] = 0
            
            USER_Get_Recent_Search_Lab_List(paradict: paradict, completionHandler: { (success , strData, message) in
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse) {
                            self.totalPages = json["totalPage"] as? Int ?? 0
                            if let list = json["labList"] as? [[String :Any]]{
                                if self.pageIndex == 0{
                                    self.arrRecentSearchLabList = list.map(PPMV_Lab_List.init)
                                }else{
                                    self.arrRecentSearchLabList.append(contentsOf: list.map(PPMV_Lab_List.init))
                                }
                                self.tblLabName.reloadData()
                            }
                            self.hideHUD()
                        }
                    }  else{
                        if self.pageIndex == 0{
                            self.view.makeToast(strResponse)
                        }
                        self.hideHUD()
                    }
                } else {
                    self.hideHUD()
                }
            })
            
        } else {
            self.hideHUD()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func GetSearchWiseList(firstName : String , referToCLinic : Bool ,referToCLinicHavingDots : Bool) {

        print(firstName)
        self.showHUD()
        if isConnectedToNetwork() {

            var paradict = [String : Any]()
            paradict["firstName"] = firstName
            paradict["page"] = 10
            paradict["pageIndex"] = SearchPageIndex
            paradict["referToCLinic"] = referToCLinic
            paradict["referToCLinicHavingDots"] = referToCLinicHavingDots
            paradict["search"] = true

            USER_GET_CLINIC_AND_LAB_LIST_BASE_ON_SERCH(paradict: paradict, completionHandler: { (success , strData, message) in
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if success == "true"{
                        if let json = self.convertStringToDictionary(text: strResponse) {
                            self.SearchTotalPages = json["totalPage"] as? Int ?? 0
                            if let list = json["labList"] as? [[String :Any]]{
                                if self.SearchPageIndex == 0{
                                    self.arrSearchLabList = list.map(PPMV_Lab_List.init)
                                }else{
                                    self.arrSearchLabList.append(contentsOf: list.map(PPMV_Lab_List.init))
                                }
                                
                                if self.arrSearchLabList.count > 0 {
                                    
                                    self.constrainttblSearchesData.constant = 20
                                }
                                
                                self.tblSearchLabName.reloadData()
                            }
                            self.hideHUD()
                        }
                    }  else{
                        if self.SearchPageIndex == 0{
                            self.view.makeToast(strResponse)
                        }
                        self.hideHUD()
                    }
                } else {
                    self.hideHUD()
                }
            })

        } else {
            self.hideHUD()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func FindTextWiseList() -> Bool {
        
        SearchPageIndex = 0
        SearchTotalPages = 0
        arrSearchLabList.removeAll()
        if let searchText = txtSearch.text, searchText.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSearchText)
            txtSearch.resignFirstResponder()
            return false

        } else if txtSearch.text?.count ?? 0 >= 3 {
            
            txtSearch.resignFirstResponder()
            if ISForm == "Laboratory" {
                GetSearchWiseList(firstName: txtSearch.text ?? "", referToCLinic: false, referToCLinicHavingDots: false)
            } else {
                GetSearchWiseList(firstName: txtSearch.text ?? "", referToCLinic: true, referToCLinicHavingDots: false)
            }
            return true
        }
        return false
    }
    
    func makeDictForAPI() -> [String:Any] {
        
        let parametrDict : [String : Any] = ["patientDetails" : PATIENT_DETAIL ?? [] , "LabDetails" : dictSelectedLab ?? []]
        
        return parametrDict
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
        

        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCollectSamplePressed(_ sender : UIButton) {
        
        if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"LabNamePopUPVC") as? LabNamePopUPVC {
            
            selectionPopup.modalPresentationStyle = .overCurrentContext
            selectionPopup.delegate = self
            selectionPopup.ISFROMLabSelected = "Not_Lab_Selected"
            selectionPopup.ISFormLab = ISForm
            present(selectionPopup, animated: true, completion: nil)
        }
    }
    
    func AddRecentSearchApi(index : Int , strSearchTYpe : String) {
        
        var paradict = [String : Any]()
        paradict["labId"] = dictSelectedLab?._id
        paradict["searchType"] = strSearchTYpe

        USER_ADD_RECENT_SERCH_LAB(paradict: paradict, completionHandler: { (success , strData , message) in
            
            if success == "true" {
                print(strData)
            }
        })
    }

}

extension RecentSearchLabVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let callApi = FindTextWiseList()
        return callApi
    }
}

extension RecentSearchLabVC : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblLabName {
            
            tblRecentSearchHeight.constant = CGFloat(arrRecentSearchLabList.count * 50)
            return arrRecentSearchLabList.count
            
        } else {
        
            tblSearchHeight.constant = CGFloat(arrSearchLabList.count * 50)
            return arrSearchLabList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblLabName.dequeueReusableCell(withIdentifier: "CellSelectLab") as! CellSelectLab
        
        if tableView == tblLabName {
                
            let dictLabName = arrRecentSearchLabList[indexPath.row]
            cell.lblLabName.text = dictLabName.facilityName

        } else {
            
            let dictLabName = arrSearchLabList[indexPath.row]
            cell.lblLabName.text = dictLabName.facilityName

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"LabNamePopUPVC") as? LabNamePopUPVC {
            selectionPopup.modalPresentationStyle = .overCurrentContext
            if tableView == tblLabName {
                selectionPopup.selectedLab = arrRecentSearchLabList[indexPath.row]
                dictSelectedLab = arrRecentSearchLabList[indexPath.row]
            } else {
                selectionPopup.selectedLab = arrSearchLabList[indexPath.row]
                dictSelectedLab = arrSearchLabList[indexPath.row]
            }
            selectionPopup.ISFROMLabSelected = "Lab_Selected"
            selectionPopup.delegate = self
            selectionPopup.ISFormLab = ISForm
            present(selectionPopup, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == tblLabName {
            
            if indexPath.row == arrRecentSearchLabList.count - 1 {
                if pageIndex < totalPages {
                    pageIndex += 1
                    if ISForm == "Laboratory" {
                        GetRecentSearchLabName(strSearchType : "labtech")
                    } else {
                        GetRecentSearchLabName(strSearchType : "clinician")
                    }
                }
            }
        } else {
            if indexPath.row == arrSearchLabList.count - 1 {
                if SearchPageIndex < SearchTotalPages {
                    SearchPageIndex += 1
                    if ISForm == "Laboratory" {
                        GetRecentSearchLabName(strSearchType : "labtech")
                    } else {
                        GetRecentSearchLabName(strSearchType : "clinician")
                    }
                }
            }
        }
    }
}

extension RecentSearchLabVC : LabSelectedDelegate {
    
    func btnSubmitPressed(index: Int, ISFrom: String, ISFormLab: String) {
        if ISFormLab == "Laboratory" {
            if ISFrom == "Not_Lab_Selected" {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SampleDetailsVC") as? SampleDetailsVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            } else {
                AddRecentSearchApi(index: index, strSearchTYpe: "labtech")
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SampleDetailsVC") as? SampleDetailsVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.SelectedLabDetail = dictSelectedLab
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        } else {
            if ISFrom == "Not_Lab_Selected" {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "ClinicSampleDetailVC") as? ClinicSampleDetailVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            } else {
                AddRecentSearchApi(index: index, strSearchTYpe: "clinician")
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "ClinicSampleDetailVC") as? ClinicSampleDetailVC {
                    VC.PATIENT_DETAIL = PATIENT_DETAIL
                    VC.SelectedLabDetail = dictSelectedLab
                    VC.ISFORM = ISFrom
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
        }
        
    }
}

