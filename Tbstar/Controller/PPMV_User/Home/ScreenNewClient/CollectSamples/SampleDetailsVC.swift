//
//  SampleDetailsVC.swift
//  Tbstar
//
//  Created by Mavani on 28/05/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
import StepSlider

class SampleDetailsVC: BaseViewController {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblTemprature : UILabel!
    @IBOutlet var lblTempratureValue : UILabel!
    @IBOutlet var lblNumberOfSampleTitle : UILabel!
    @IBOutlet var lblNumberOfSampleSend : UILabel!
    @IBOutlet var lblApprove : UILabel!

    @IBOutlet var btnSubmit : UIButton!

    @IBOutlet weak var txtType : SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfCollection : SkyFloatingLabelTextField!
    @IBOutlet weak var txtLabName : SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfDispatch : SkyFloatingLabelTextField!
    @IBOutlet weak var txtHIVStatus: SkyFloatingLabelTextField!
    @IBOutlet weak var txtReasonForExamination: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTestedTB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTypeOfTestRequest : SkyFloatingLabelTextField!
    @IBOutlet weak var txtTemprature: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNameOfDispachingOfficer: SkyFloatingLabelTextField!
    @IBOutlet weak var txtRemarks: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumberOfDispachingOfficer: SkyFloatingLabelTextField!
    
    @IBOutlet var viewSider : StepSlider!
    
    var DateOfCollection = Date()
    var DateOfDispatch = Date()

    var dateFormatter = DateFormatter()

    let dropDown = DropDown()
    
    var PATIENT_DETAIL : [String : Any]?
    var SelectedLabDetail : PPMV_Lab_List?
    var PPMVDropDown : PPMV_Lab_Drop_Down?
    var dictPatirntDetail : PatientInformation?
        
    var SampleCount = 1
    var ISFORM : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateFormatter.dateFormat = displayDateFormat
        
        viewSider.maxCount = 6
        viewSider.index = 1
        setupLocalization()
        askForLocation()
        setupTextField()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        lblTempratureValue.text = "Celsius".localized
        dictPatirntDetail = PatientInformation(dict: PATIENT_DETAIL ?? [:])
        
        txtLabName.isUserInteractionEnabled = false
        if ISFORM == "Not_Lab_Selected" {
            txtLabName.text = "n_a".localized
        } else {
            txtLabName.text = SelectedLabDetail?.facilityName ?? ""
        }
        Get_DropDown_Data()
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func Get_DropDown_Data() {
        
        if isConnectedToNetwork() {
            showHUD()
            USER_GET_ForDispatchForm(completionHandler: { (isSuccess,strdata,message) in
                
                if strdata != ""{
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.savePPMVdeatilsForDispatchToStandard(detail: json as NSDictionary)
                        self.PPMVDropDown = PPMV_Lab_Drop_Down(dict: json)
                        self.hideHUD()
                    }
                } else {
                    self.hideHUD()
                }
            })
        }else{
            self.PPMVDropDown = getPPMVdeatilsForDispatchToStandard()
        }
    }
    
    @IBAction func changeValue(_ sender: StepSlider) {
        
        let count = UInt(sender.index) 
        if count <= 1 {
            lblNumberOfSampleSend.text = "\(count) \("Sample".localized)"
        } else {
            lblNumberOfSampleSend.text = "\(count)  \("Samples".localized)"
        }
        SampleCount  =  Int(count)
    }
        
    func setupLocalization() {
        
        lblTitle.text = "Sample_Details".localized
        lblTemprature.text = "Temperature".localized
        lblNumberOfSampleTitle.text = "Number_of_Samples_Sent_to_Lab".localized
        lblNumberOfSampleSend.text = "\(1) \("Sample".localized)"
        lblApprove.text = "I_approve_the_submission_of_this_sample".localized

        txtType.placeholder = "Type_of_Specimen".localized
        txtDateOfCollection.placeholder = "Date_of_Collection".localized
        txtLabName.placeholder = "Laboratory_Name".localized
        txtDateOfDispatch.placeholder = "Date_of_Dispatch_to_Lab".localized
        txtHIVStatus.placeholder = "HIV_Status".localized + "*"
        txtReasonForExamination.placeholder = "Reason_for_Examination".localized + "*"
        txtTestedTB.placeholder = "Ever_Previously_been_Treated_for_TB".localized + "*"
        txtTypeOfTestRequest.placeholder = "Type_of_Test_Requested".localized + "*"
        txtTemprature.placeholder = "Temperature".localized + "*"
        txtNameOfDispachingOfficer.placeholder = "Name_of_Dispatching_Officer".localized
        txtRemarks.placeholder = "Remarks".localized
        txtPhoneNumberOfDispachingOfficer.placeholder = "Phone_Number_of_Dispatching_Officer".localized

        btnSubmit.setTitle("submit".localized, for: .normal)
    }
    
    func setupTextField()  {
        
        txtType.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtType.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtType.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtType.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfCollection.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfCollection.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfCollection.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfCollection.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLabName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLabName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLabName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLabName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfDispatch.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfDispatch.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfDispatch.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfDispatch.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtHIVStatus.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtHIVStatus.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtHIVStatus.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtHIVStatus.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtReasonForExamination.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtReasonForExamination.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtReasonForExamination.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtReasonForExamination.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTestedTB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTestedTB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtTestedTB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTestedTB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTypeOfTestRequest.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTypeOfTestRequest.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtTypeOfTestRequest.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTypeOfTestRequest.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtTemprature.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTemprature.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtTemprature.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTemprature.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtNameOfDispachingOfficer.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtNameOfDispachingOfficer.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtNameOfDispachingOfficer.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtNameOfDispachingOfficer.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtRemarks.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtRemarks.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtRemarks.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtRemarks.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtPhoneNumberOfDispachingOfficer.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPhoneNumberOfDispachingOfficer.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtPhoneNumberOfDispachingOfficer.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPhoneNumberOfDispachingOfficer.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            present(datePickerVC, animated: true, completion: nil)
        }
    }

    func setupTypeOFSpecimenDropdown()  {
        
        dropDown.anchorView = txtType
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.SpecimanType.map{($0.specimenTypes)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtType.text = item
            dropDown.hide()
        }
    }

    func setupHIVStatusDropdown() {
        
        dropDown.anchorView = txtHIVStatus
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.hivstatus.map{($0.hivStatus)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtHIVStatus.text = item
            dropDown.hide()
        }
    }
    
    func setupReasonForExaminationDropdown()  {
        
        dropDown.anchorView = txtReasonForExamination
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TestReason.map{($0.reasonOfTest)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtReasonForExamination.text = item
            dropDown.hide()
        }
    }

    func setupTreatedForTBDropdown()  {
        
        dropDown.anchorView = txtTestedTB
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TbStatus.map{($0.TBCheck)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtTestedTB.text = item
            dropDown.hide()
        }
    }

    func setupTypeOfTestReuestDropdown()  {
        
        dropDown.anchorView = txtTypeOfTestRequest
        if PPMVDropDown != nil{
            dropDown.dataSource = PPMVDropDown!.TestType.map{($0.typeOfTest)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtTypeOfTestRequest.text = item
            dropDown.hide()
        }
    }

    func SubmitForm() {
        
        let patientDetail  = PATIENT_DETAIL?["patientDetails"] as? [String : Any]
        let patientSymptoms = patientDetail?["patientSymptoms"] as? [String : String]
        
        dateFormatter.dateFormat = APIDateFormate
        
        var paradict = [String : Any]()
        paradict["clientHasNoTB"] = false
        paradict["referToCLinic"] = false
        paradict["patientSymptoms"] = PatientSymtoms(value:patientSymptoms ?? [:])
        paradict["covidSymptoms"] = PatientSymtoms(value:makeParametrForAPI())
        paradict["firstName"] = dictPatirntDetail?.firstName
        paradict["lastName"] = dictPatirntDetail?.surname
        paradict["middleName"] = dictPatirntDetail?.middleName
        paradict["dateOfBirth"] = dictPatirntDetail?.dateOfBirth
        paradict["sex"] = dictPatirntDetail?.sex
        paradict["age"] = Int(dictPatirntDetail?.age ?? "0")
        paradict["phoneNumber"] = Int(dictPatirntDetail?.phoneNumber ?? "")
        paradict["alternatePhoneNumber"] = Int(dictPatirntDetail?.secondPhoneNumber ?? "")
        paradict["address"] = dictPatirntDetail?.address
        paradict["state"] = dictPatirntDetail?.state
        paradict["lga"] = dictPatirntDetail?.lga
        //paradict["typesOfSpecimen"] = "Sputum"
        paradict["typesOfSpecimen"] = txtType.text
        paradict["dateOfCollection"] = dateFormatter.string(from: DateOfCollection)
        paradict["hivStatus"] = txtHIVStatus.text
        paradict["reasonOfExamination"] = txtReasonForExamination.text
        paradict["previousTreatmentForTb"] = txtTestedTB.text
        paradict["typeOfPresumptiveTbCase"] = dictPatirntDetail?.typeOfPresumptiveTbCase
        paradict["siteOfDisease"] = dictPatirntDetail?.siteOfDisease
        paradict["patientWorker"] = dictPatirntDetail?.patientWorker
        paradict["typeOfTest"] = txtTypeOfTestRequest.text
        paradict["numberOfSample"] = String(SampleCount)
        if txtTemprature.text != "" {
            paradict["temperature"] = txtTemprature.text
        } else {
            paradict["temperature"] = "0"
        }
        paradict["nameOfCollectOffcer"] = txtNameOfDispachingOfficer.text
        paradict["nameOfCollectOffcerPhNo"] = Int(txtPhoneNumberOfDispachingOfficer.text ?? "")
        if ISFORM == "Not_Lab_Selected" {
            paradict["isSendingSampleToLabAfterInitialCollection"] = true
            paradict["laboratoryName"] = "n/a"
            paradict["labId"] = "60252408de659117acf7ef5c"
            paradict["dateOfDispatch"] = ""
        } else {
            paradict["isSendingSampleToLabAfterInitialCollection"] = false
            paradict["laboratoryName"] = SelectedLabDetail?.facilityName
            paradict["labId"] = SelectedLabDetail?._id
            paradict["dateOfDispatch"] = dateFormatter.string(from: DateOfDispatch)
        }
        paradict["latitude"] = currentCordinate.latitude
        paradict["longitude"] = currentCordinate.longitude

        USER_PPMV_LAB_SELECT_FORM_DETAILS_PASS(paradict: paradict, completionHandler: { (isSucces , strData , strMessage) in
            
            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
            if strData != "" {
                
                if isSucces {
                    if let json = self.convertStringToDictionary(text: strResponse) {
                        let dict = NSMutableDictionary(dictionary: json)
                        print(dict)
                        let recordNumber : String = dict.value(forKey: "patientRecordNumber") as? String ?? ""
                        if let thankspopup = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController {
                            thankspopup.delegate = self
                            thankspopup.mainTitle = "Thank You!"
                            thankspopup.subTitle = "Screening Completed"
                            thankspopup.caseDetail = "\("yourCase".localized)\n\(recordNumber)\n \("usethisID".localized)"
                            thankspopup.modalPresentationStyle = .overCurrentContext
                            self.present(thankspopup, animated: true, completion: nil)
                        }
                    }
                }
            }
        })
    }
    
    func makeParametrForAPI() -> [String : String]  {
        
        return ["covidCough" : dictPatirntDetail?.patientCovidSymptoms?.covidCough ?? "no" ,
                "covidFever" : dictPatirntDetail?.patientCovidSymptoms?.covidFever ?? "no",
                "covidLossOfTaste" :  dictPatirntDetail?.patientCovidSymptoms?.covidLossOfTaste ?? "no",
                "covidRunnyNose" :  dictPatirntDetail?.patientCovidSymptoms?.covidRunnyNose ?? "no",
                "covidDiarrhea" :  dictPatirntDetail?.patientCovidSymptoms?.covidDiarrhea ?? "no",
                "covidSoreThroat" :  dictPatirntDetail?.patientCovidSymptoms?.covidSoreThroat ?? "no",
                "covidShaking" :  dictPatirntDetail?.patientCovidSymptoms?.covidShaking ?? "no"]
    }

    func PatientSymtoms(value : [String : String]) -> String {
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(value) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        }
        return ""
    }
    
    func SroreDataInDataBase() {
     
        let tblCreate = DatabaseManager().PPMV_Patient_Details_Table_Create()
        
        if tblCreate {
            insertPatientDetailsTODB()
        }
    }
    
    func insertPatientDetailsTODB()  {
        
        let dictData : SaveDataInPPMV_Sqlite_Data?
        let patientDetail = dictPatirntDetail
        
        let databaseDateFormate = DateFormatter()
        databaseDateFormate.dateFormat = apiDateFormatFromAPI
        
        if let tbsymptoms = patientDetail?.patientTBSymptoms{
                
            if let covidSymptoms = patientDetail?.patientCovidSymptoms {
                
                dictData = SaveDataInPPMV_Sqlite_Data(latitude: "\(patientDetail?.lattitude.description ?? "")", longitude: "\(patientDetail?.longitude.description ?? "")", middleName: "\(patientDetail?.middleName.debugDescription ?? "")", lga: "\(patientDetail?.lga.debugDescription ?? "")", siteOfDisease: "\(patientDetail?.siteOfDisease.debugDescription ?? "")", visitDate: "\(patientDetail?.visitDate.debugDescription ?? "")", dateOfBirth: "\(patientDetail?.dateOfBirth.debugDescription ?? "")", phoneNumber: "\(patientDetail?.phoneNumber.debugDescription ?? "")", age: "\(patientDetail?.age.debugDescription ?? "")", hivStatus: "\(txtHIVStatus.text?.debugDescription ?? "")", typeOfPresumptiveTbCase: "\(patientDetail?.typeOfPresumptiveTbCase.debugDescription ?? "")", secondPhoneNumber: "\(patientDetail?.secondPhoneNumber.debugDescription ?? "")", patientWorker: "\(patientDetail?.patientWorker.debugDescription ?? "")", sex: "\(patientDetail?.sex.debugDescription ?? "")", state: "\(patientDetail?.state.debugDescription ?? "")", address: "\(patientDetail?.address.debugDescription ?? "")", lastName: "\(patientDetail?.surname.debugDescription ?? "")", hadTBbefore: "\(patientDetail?.hadTBbefore.debugDescription ?? "")", firstName: "\(patientDetail?.firstName.debugDescription ?? "")", CoughingUpBlood: "\(tbsymptoms.CoughingUpBlood.debugDescription )", Swelling: "\(tbsymptoms.Swelling.debugDescription)", FailureToThrive: "\(tbsymptoms.FailureToThrive.debugDescription)", FeverFor3weeksOrMore: "\(tbsymptoms.FeverFor3weeksOrMore.debugDescription)", CoughFor2WeeksOrMore: "\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription)", NightSweats: "\(tbsymptoms.NightSweats.debugDescription)", UnexplainedWeightLoss: "\(tbsymptoms.UnexplainedWeightLoss.debugDescription)", covidDiarrhea: "\(covidSymptoms.covidDiarrhea.debugDescription)", covidSoreThroat: "\(covidSymptoms.covidSoreThroat.debugDescription)", covidShaking: "\(covidSymptoms.covidShaking.debugDescription)", covidFever: "\(covidSymptoms.covidFever.debugDescription)", covidRunnyNose: "\(covidSymptoms.covidRunnyNose.debugDescription)", covidCough: "\(covidSymptoms.covidCough.debugDescription)", covidLossOfTaste: "\(covidSymptoms.covidLossOfTaste.debugDescription)", createdDate: "\(databaseDateFormate.string(from: Date()).debugDescription)", clientHasNoTB: "".debugDescription, referToCLinic: 0, typesOfSpecimen: "\(txtType.text?.debugDescription ?? "")", dateOfCollection: "\(dateFormatter.string(from: DateOfCollection))", reasonOfExamination: "\(txtReasonForExamination.text?.debugDescription ?? "")", previousTreatmentForTb: "\(txtTestedTB.text?.debugDescription ?? "")", typeOfTest: "\(txtTypeOfTestRequest.text?.debugDescription ?? "")", numberOfSample: "\(String(SampleCount))", temperature: "\(txtTemprature.text?.debugDescription ?? "")", nameOfCollectOffcer: "\(txtNameOfDispachingOfficer.text?.debugDescription ?? "")", nameOfCollectOffcerPhNo: "\(txtPhoneNumberOfDispachingOfficer.text?.debugDescription ?? "")", isSendingSampleToLabAfterInitialCollection: 1, laboratoryName: "n/a".debugDescription, labId: "60252408de659117acf7ef5c".debugDescription, dateOfDispatch: "\(dateFormatter.string(from: DateOfDispatch))")
                
            } else {
                
                dictData = SaveDataInPPMV_Sqlite_Data(latitude: "\(patientDetail?.lattitude.description ?? "")", longitude: "\(patientDetail?.longitude.description ?? "")", middleName: "\(patientDetail?.middleName.debugDescription ?? "")", lga: "\(patientDetail?.lga.debugDescription ?? "")", siteOfDisease: "\(patientDetail?.siteOfDisease.debugDescription ?? "")", visitDate: "\(patientDetail?.visitDate.debugDescription ?? "")", dateOfBirth: "\(patientDetail?.dateOfBirth.debugDescription ?? "")", phoneNumber: "\(patientDetail?.phoneNumber.debugDescription ?? "")", age: "\(patientDetail?.age.debugDescription ?? "")", hivStatus: "\(txtHIVStatus.text?.debugDescription ?? "")", typeOfPresumptiveTbCase: "\(patientDetail?.typeOfPresumptiveTbCase.debugDescription ?? "")", secondPhoneNumber: "\(patientDetail?.secondPhoneNumber.debugDescription ?? "")", patientWorker: "\(patientDetail?.patientWorker.debugDescription ?? "")", sex: "\(patientDetail?.sex.debugDescription ?? "")", state: "\(patientDetail?.state.debugDescription ?? "")", address: "\(patientDetail?.address.debugDescription ?? "")", lastName: "\(patientDetail?.surname.debugDescription ?? "")", hadTBbefore: "\(patientDetail?.hadTBbefore.debugDescription ?? "")", firstName: "\(patientDetail?.firstName.debugDescription ?? "")", CoughingUpBlood: "\(tbsymptoms.CoughingUpBlood.debugDescription )", Swelling: "\(tbsymptoms.Swelling.debugDescription)", FailureToThrive: "\(tbsymptoms.FailureToThrive.debugDescription)", FeverFor3weeksOrMore: "\(tbsymptoms.FeverFor3weeksOrMore.debugDescription)", CoughFor2WeeksOrMore: "\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription)", NightSweats: "\(tbsymptoms.NightSweats.debugDescription)", UnexplainedWeightLoss: "\(tbsymptoms.UnexplainedWeightLoss.debugDescription)", covidDiarrhea: "\("".debugDescription)", covidSoreThroat: "\("".debugDescription)", covidShaking: "\("".debugDescription)", covidFever: "\("".debugDescription)", covidRunnyNose: "\("".debugDescription)", covidCough: "\("".debugDescription)", covidLossOfTaste: "\("".debugDescription)", createdDate: "\(databaseDateFormate.string(from: Date()).debugDescription)", clientHasNoTB: "".debugDescription, referToCLinic: 0, typesOfSpecimen: "\(txtType.text?.debugDescription ?? "")", dateOfCollection: "\(dateFormatter.string(from: DateOfCollection))", reasonOfExamination: "\(txtReasonForExamination.text?.debugDescription ?? "")", previousTreatmentForTb: "\(txtTestedTB.text?.debugDescription ?? "")", typeOfTest: "\(txtTypeOfTestRequest.text?.debugDescription ?? "")", numberOfSample: "\(String(SampleCount))", temperature: "\(txtTemprature.text?.debugDescription ?? "")", nameOfCollectOffcer: "\(txtNameOfDispachingOfficer.text?.debugDescription ?? "")", nameOfCollectOffcerPhNo: "\(txtPhoneNumberOfDispachingOfficer.text?.debugDescription ?? "")", isSendingSampleToLabAfterInitialCollection: 1, laboratoryName: "n/a".debugDescription, labId: "60252408de659117acf7ef5c".debugDescription, dateOfDispatch: "\(dateFormatter.string(from: DateOfDispatch))")
            }
            
            let insert = PPMV_DBManager().SaveDataInPPMV_Sqlite(data: dictData!)
            print(insert)

            if insert {
                if let thankspopup = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController {
                    thankspopup.delegate = self
                    thankspopup.mainTitle = "Thank You!"
                    thankspopup.subTitle = "Screening Completed"
                    thankspopup.caseDetail = "PPMV_Offline_Screening_Completed".localized
                    thankspopup.modalPresentationStyle = .overCurrentContext
                    self.present(thankspopup, animated: true, completion: nil)
                }
            } else {
                view.makeToast(appMessages.somethingWrong)
            }
        }
    }
    
    func isValidDetail() -> Bool {
        
        if let name = txtType.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.selectSpecimanType)
            return false
        }
        if let datecollection = txtDateOfCollection.text, datecollection.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.validCollectionDate)
            return false
        }
        
        if ISFORM != "Not_Lab_Selected" {
            
            if let dispatchdate = txtDateOfDispatch.text , dispatchdate.isEmpty {
                showAlert(titleStr: appName, msg: appMessages.CompareCollectionDate)
                return false
            }
            
            if DateOfCollection >= DateOfDispatch {
                showAlert(titleStr: appName, msg: appMessages.CompareCollectionDate)
                return false
            }
        }
        
         if let state = txtHIVStatus.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectHIV)
            return false
        }
        if let lga = txtReasonForExamination.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectReasonForExamination)
            return false
        }
        if let TB = txtTestedTB.text, TB.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.SelectTReatedForTB)
            return false
        }
        if let name = txtTypeOfTestRequest.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.SelectTypeRequest)
            return false
        }
        if SampleCount <= 0 {
            showAlert(titleStr: appName, msg: appMessages.SelectSample)
            return false
        }
        return true
    }
    
    @IBAction func btnSubmitPressed(_ sender : UIButton) {
        
        if isValidDetail()  {
            
            if isConnectedToNetwork() {
                SubmitForm()
            } else {
                SroreDataInDataBase()
            }
            
        }
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
     
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SelectLabVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}

extension SampleDetailsVC : ThanksPopupDelegate {
    
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if let homevc = vc as? UserHomeVC{
                    navigationController?.popToViewController(homevc, animated: true)
                }
            }
        }
    }
}

extension SampleDetailsVC  : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDateOfCollection {
            openDatePicker(dateType: .BirthDate)
            return false
        }
        if textField == txtDateOfDispatch {
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtType{
            setupTypeOFSpecimenDropdown()
            return false
        }
        if textField == txtHIVStatus{
            setupHIVStatusDropdown()
            return false
        }
        if textField == txtReasonForExamination{
            setupReasonForExaminationDropdown()
            return false
        }
        if textField == txtTestedTB{
            setupTreatedForTBDropdown()
            return false
        }
        if textField == txtTypeOfTestRequest{
            setupTypeOfTestReuestDropdown()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNameOfDispachingOfficer {
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

extension SampleDetailsVC : DatePickerDelegate{
    
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDateOfCollection.text = dateFormatter.string(from: sDate)
            DateOfCollection = sDate
        }else{
            txtDateOfDispatch.text = dateFormatter.string(from: sDate)
            DateOfDispatch = sDate
        }
    }
}

