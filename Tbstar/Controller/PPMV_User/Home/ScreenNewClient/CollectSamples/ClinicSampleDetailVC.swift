//
//  ClinicSampleDetailVC.swift
//  Tbstar
//
//  Created by Mavani on 06/06/21.
//

import UIKit
import DropDown
import SkyFloatingLabelTextField


class ClinicSampleDetailVC: BaseViewController {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var lblAgeTitle: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblSex: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblPaperResult : UILabel!
    
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfVisit: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtDOB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMobNo: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtNotes: SkyFloatingLabelTextField!

    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!

    @IBOutlet weak var imgvCheckmark: UIImageView!
    var isAgree = false

    var TBDict : [String:Any]?
    var covidDict : [String :Any]?
    var selectedGender = ""
    
    var dateFormatter = DateFormatter()
    var designationDetail : DesignationDetail?
    let dropDown = DropDown()
    
    var visitDate = Date()
    var birthDate = Date()
    
    var PATIENT_DETAIL : [String : Any]?
    var SelectedLabDetail : PPMV_Lab_List?
    var dictPatirntDetail : PatientInformation?
        
    var ISFORM : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        getDetailForIndependentUser()
        setupTextField()
        askForLocation()
        dateFormatter.dateFormat = displayDateFormat
        txtDateOfVisit.text = dateFormatter.string(from: Date())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        dictPatirntDetail = PatientInformation(dict: PATIENT_DETAIL ?? [:])
        
        txtFirstName.text =  dictPatirntDetail?.firstName
        txtMiddleName.text =  dictPatirntDetail?.middleName
        txtSurname.text =  dictPatirntDetail?.surname
        txtDateOfVisit.text = dictPatirntDetail?.visitDate
        txtDOB.text = dictPatirntDetail?.dateOfBirth
        
        selectedGender = dictPatirntDetail?.sex ?? ""
        
        if selectedGender == Gender.Female.rawValue{
            btnFemaleTap(btnFemale)
        }else if selectedGender == Gender.Male.rawValue{
            btnMaleTap(btnMale)
        }
        
        txtMobNo.text = dictPatirntDetail?.phoneNumber
        txtAddress.text = dictPatirntDetail?.address
        txtState.text = dictPatirntDetail?.state
        txtLGA.text = dictPatirntDetail?.lga
        
        let newDateFormate = DateFormatter()
        newDateFormate.dateFormat = APIDateFormate
        let sDate = newDateFormate.date(from: dictPatirntDetail?.dateOfBirth ?? "") ?? Date()
        let age = Calendar.current.dateComponents([.year], from: sDate, to: Date()).year!
        lblAge.text = "\(age)"
    }
    
    func setupLocalization()  {
        
        //Add new client detail view controller
        lblScreenTitle.text = "clientDetails".localized
        lblPaperResult.text = "PaperResult".localized
        
        txtFirstName.placeholder = "pFirstName".localized + "*"
        txtFirstName.selectedTitle = "pFirstName".localized + "*"
        txtFirstName.title = "pFirstName".localized + "*"
        
        txtMiddleName.placeholder = "pMiddleName".localized
        txtMiddleName.selectedTitle = "pMiddleName".localized
        txtMiddleName.title = "pMiddleName".localized
        
        txtSurname.placeholder = "pSurname".localized + "*"
        txtSurname.selectedTitle = "pSurname".localized + "*"
        txtSurname.title = "pSurname".localized + "*"
        
        txtDateOfVisit.placeholder = "visitDate".localized + "*"
        txtDateOfVisit.selectedTitle = "visitDate".localized + "*"
        txtDateOfVisit.title = "visitDate".localized + "*"
        
        lblAgeTitle.text = "age".localized
        lblAge.text = "\(0)"
        lblDOB.text = "dob".localized
        lblSex.text = "sex".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        txtMobNo.placeholder = "mobNO".localized + "*"
        txtMobNo.selectedTitle = "mobNO".localized + "*"
        txtMobNo.title = "mobNO".localized + "*"
        
        txtAlternateMobNo.placeholder = "secondMobileNum".localized
        txtAlternateMobNo.selectedTitle = "secondMobileNum".localized
        txtAlternateMobNo.title = "secondMobileNum".localized
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
                
        btnSubmit.setTitle("submit".localized, for: .normal)
            
    }
    
    func setupTextField()  {

        txtFirstName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMiddleName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDOB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    func SubmitForm() {

        let patientDetail  = PATIENT_DETAIL?["patientDetails"] as? [String : Any]
        let patientSymptoms = patientDetail?["patientSymptoms"] as? [String:String]
        dateFormatter.dateFormat = APIDateFormate

        var paradict = [String : Any]()
        paradict["clientHasNoTB"] = false
        paradict["referToCLinic"] = true
        paradict["patientSymptoms"] = PatientSymtoms(value:patientSymptoms ?? [:])
        paradict["covidSymptoms"] = PatientSymtoms(value:makeParametrForAPI())
        paradict["firstName"] = dictPatirntDetail?.firstName
        paradict["lastName"] = txtFirstName.text
        paradict["middleName"] = txtMiddleName.text
        paradict["dateOfBirth"] = dateFormatter.string(from: birthDate)
        paradict["visitDate"] = dateFormatter.string(from: visitDate)
        paradict["sex"] = selectedGender
        paradict["age"] = Int(lblAge.text ?? "0")
        paradict["phoneNumber"] = Int(dictPatirntDetail?.phoneNumber ?? "")
        paradict["alternatePhoneNumber"] = Int(dictPatirntDetail?.secondPhoneNumber ?? "")
        paradict["address"] = txtAddress.text
        paradict["state"] = txtState.text
        paradict["lga"] = txtLGA.text
        if ISFORM == "Not_Lab_Selected" {
            paradict["labId"] = "60252408de659117acf7ef5c"
        } else {
            paradict["labId"] = SelectedLabDetail?._id
        }
        paradict["hasPhysicalResult"] = isAgree
        paradict["note"] = txtNotes.text
        paradict["typeOfPresumptiveTbCase"] = dictPatirntDetail?.typeOfPresumptiveTbCase
        paradict["siteOfDisease"] = dictPatirntDetail?.siteOfDisease
        paradict["patientWorker"] = dictPatirntDetail?.patientWorker
        paradict["latitude"] = currentCordinate.latitude
        paradict["longitude"] = currentCordinate.longitude

        USER_PPMV_LAB_SELECT_FORM_DETAILS_PASS(paradict: paradict, completionHandler: { (isSucces , strData , strMessage) in
            
            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
            if strData != "" {
                
                if isSucces {
                    if let json = self.convertStringToDictionary(text: strResponse) {
                        let dict = NSMutableDictionary(dictionary: json)
                        print(dict)
                        let recordNumber : String = dict.value(forKey: "patientRecordNumber") as? String ?? ""
                        if let thankspopup =        self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController {
                            thankspopup.delegate = self
                            thankspopup.mainTitle = "Thank You!"
                            thankspopup.subTitle = "Screening Completed"
                            thankspopup.caseDetail = "\("yourCase".localized)\n\(recordNumber)\n \("usethisID".localized)"
                            thankspopup.modalPresentationStyle = .overCurrentContext
                            self.present(thankspopup, animated: true, completion: nil)
                        }
                    }
                }
            }
        })
    }

    func makeParametrForAPI() -> [String:String]  {
        
        return ["covidCough" : dictPatirntDetail?.patientCovidSymptoms?.covidCough ?? "no" ,
                "covidFever" : dictPatirntDetail?.patientCovidSymptoms?.covidFever ?? "no",
                "covidLossOfTaste" :  dictPatirntDetail?.patientCovidSymptoms?.covidLossOfTaste ?? "no",
                "covidRunnyNose" :  dictPatirntDetail?.patientCovidSymptoms?.covidRunnyNose ?? "no",
                "covidDiarrhea" :  dictPatirntDetail?.patientCovidSymptoms?.covidDiarrhea ?? "no",
                "covidSoreThroat" :  dictPatirntDetail?.patientCovidSymptoms?.covidSoreThroat ?? "no",
                "covidShaking" :  dictPatirntDetail?.patientCovidSymptoms?.covidShaking ?? "no"]
    }

    func PatientSymtoms(value : [String : String]) -> String {
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(value) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        }
        return ""
    }

    @IBAction func btnBackTap(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SelectLabVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func btnMaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }
    
    @IBAction func btnFemaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isValidDetail() {
            SubmitForm()
        }
    }
        
    func isValidDetail() -> Bool {
        if let name = txtFirstName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        if let dob = txtDOB.text, dob.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterDOB)
            return false
        }
        if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }
        if let mobNo = txtMobNo.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMobNo.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        return true
    }
    
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            present(datePickerVC, animated: true, completion: nil)
        }
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            dropDown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropDown.dataSource = addressDetail.LGA ?? []
            }
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropDown.hide()
        }
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    @IBAction func btnAgreeTap(_ sender: Any) {
        isAgree = !isAgree
        if isAgree {
            imgvCheckmark.image = UIImage(named: "checked")
        }else {
            imgvCheckmark.image = UIImage(named: "uncheck")
        }
    }
}

extension ClinicSampleDetailVC  : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDOB{
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtDateOfVisit{
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtState{
            setupStateDropdown()
            return false
        }
        if textField == txtLGA{
            setupLGADropdown()
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobNo || textField == txtAlternateMobNo{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension ClinicSampleDetailVC : DatePickerDelegate{
    
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDOB.text = dateFormatter.string(from: sDate)
            let age = Calendar.current.dateComponents([.year], from: sDate, to: Date()).year!
            lblAge.text = "\(age)"
            birthDate = sDate
        }else{
            txtDateOfVisit.text = dateFormatter.string(from: sDate)
            visitDate = sDate
        }
    }
}

extension ClinicSampleDetailVC : ThanksPopupDelegate {
    
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if let homevc = vc as? UserHomeVC{
                    navigationController?.popToViewController(homevc, animated: true)
                }
            }
        }
    }
}
