//
//  UserSampleOptionsVC.swift
//  Tbstar
//
//  Created by Mavani on 27/05/21.
//

import UIKit

class UserSampleOptionsVC: BaseViewController {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblCollectSamples : UILabel!
    @IBOutlet var lblReferToClinic : UILabel!

    var clientDetailDict : [String : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
    }
    
    func setupLocalization() {
        
        lblTitle.text = "sampleOptions".localized
        lblCollectSamples.text = "Collect Samples & Send to Lab".localized
        lblReferToClinic.text = "Refer to Clinic".localized
    }
    
    @IBAction func btnCollectSamplePressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "CollectSampleInstaructionVC") as?  CollectSampleInstaructionVC {
            VC.PATIENT_DETAIL = clientDetailDict
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    @IBAction func btnReferToClinicPressed(_ sender : UIButton) {
        
        if isConnectedToNetwork() {
            
            if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SelectLabVC") as? SelectLabVC {
                VC.PATIENT_DETAIL = clientDetailDict
                VC.ISFROM = "Clinic"
                self.navigationController?.pushViewController(VC, animated: true)
            }
        }
    }

    @IBAction func btnBackPressed(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
