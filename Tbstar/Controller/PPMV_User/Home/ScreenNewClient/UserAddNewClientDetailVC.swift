//
//  UserAddNewClientDetailVC.swift
//  Tbstar
//
//  Created by Mavani on 26/05/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class UserAddNewClientDetailVC: BaseViewController {
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    
    @IBOutlet weak var lblPatientHasTB: UILabel!
    
    @IBOutlet weak var lblAgeTitle: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var lblSex: UILabel!
    
    @IBOutlet weak var lblMale: UILabel!
    
    @IBOutlet weak var lblFemale: UILabel!
        
    @IBOutlet weak var lblTypeOFTB: UILabel!
    @IBOutlet weak var lblSuspected: UILabel!
    
    @IBOutlet weak var lblSiteOfDisease: UILabel!
    
    @IBOutlet weak var lblisHealthWorker: UILabel!
        
    @IBOutlet weak var txtFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDateOfVisit: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtDOB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMobNo: SkyFloatingLabelTextField!
    
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var heightCnstOFvwLGA: NSLayoutConstraint!
    
    @IBOutlet weak var btnDSTB: UIButton!
    @IBOutlet weak var btnDRTB: UIButton!
    
    @IBOutlet weak var btnPTBDisease: UIButton!
    @IBOutlet weak var btnEPTBDisease: UIButton!
    @IBOutlet weak var btnWorkerYes: UIButton!
    @IBOutlet weak var btnWorkerNO: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var lblDOB: UILabel!
    
    var TBDict : [String:Any]?
    var covidDict : [String :Any]?
    var selectedGender = ""
    var typeOfTBCase = ""
    var siteOFDisease = ""
    var isHealthWorker = ""
    
    var dateFormatter = DateFormatter()
    var designationDetail : DesignationDetail?
    let dropDown = DropDown()
    
    var visitDate = Date()
    var birthDate = Date()
    
    var clientDetailDict : [String : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        heightCnstOFvwLGA.constant = 0
        getDetailForIndependentUser()
        setupTextField()
        askForLocation()
        dateFormatter.dateFormat = displayDateFormat
        txtDateOfVisit.text = dateFormatter.string(from: Date())
    }
    
    func setupLocalization()  {
        
        //Add new client detail view controller
        lblScreenTitle.text = "clientDetails".localized
        lblPatientHasTB.text = "patientHasTBTitle".localized
        
        txtFirstName.placeholder = "pFirstName".localized + "*"
        txtFirstName.selectedTitle = "pFirstName".localized + "*"
        txtFirstName.title = "pFirstName".localized + "*"
        
        txtMiddleName.placeholder = "pMiddleName".localized
        txtMiddleName.selectedTitle = "pMiddleName".localized
        txtMiddleName.title = "pMiddleName".localized
        
        txtSurname.placeholder = "pSurname".localized + "*"
        txtSurname.selectedTitle = "pSurname".localized + "*"
        txtSurname.title = "pSurname".localized + "*"
        
        txtDateOfVisit.placeholder = "visitDate".localized + "*"
        txtDateOfVisit.selectedTitle = "visitDate".localized + "*"
        txtDateOfVisit.title = "visitDate".localized + "*"
        
        lblAgeTitle.text = "age".localized
        lblDOB.text = "dob".localized
        lblSex.text = "sex".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        txtMobNo.placeholder = "mobNO".localized + "*"
        txtMobNo.selectedTitle = "mobNO".localized + "*"
        txtMobNo.title = "mobNO".localized + "*"
        
        txtAlternateMobNo.placeholder = "secondMobileNum".localized
        txtAlternateMobNo.selectedTitle = "secondMobileNum".localized
        txtAlternateMobNo.title = "secondMobileNum".localized
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
                
        lblTypeOFTB.text = "typeOfTBCase".localized
        
        btnDSTB.setTitle("dstb".localized, for: .normal)
        btnDSTB.setTitle("dstb".localized, for: .selected)
        
        btnDRTB.setTitle("drtb".localized, for: .normal)
        btnDRTB.setTitle("drtb".localized, for: .selected)
        
        lblSiteOfDisease.text = "siteDisease".localized
        btnPTBDisease.setTitle("ptb".localized, for: .normal)
        btnPTBDisease.setTitle("ptb".localized, for: .selected)
        
        btnEPTBDisease.setTitle("eptb".localized, for: .normal)
        btnEPTBDisease.setTitle("eptb".localized, for: .selected)
        
        lblisHealthWorker.text = "isHealthworker".localized
        btnWorkerYes.setTitle("yes".localized, for: .normal)
        btnWorkerYes.setTitle("yes".localized, for: .selected)
        
        btnWorkerNO.setTitle("no".localized, for: .normal)
        btnWorkerNO.setTitle("no".localized, for: .selected)
                
        btnSubmit.setTitle("submit".localized, for: .normal)
    
    }
    func setupTextField()  {
        
        txtFirstName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFirstName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMiddleName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMiddleName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDOB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }
    
    @IBAction func btnFemaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }
    
    @IBAction func btnTypesOFTBTap(_ sender: UIButton) {
        btnDSTB.isSelected = false
        btnDRTB.isSelected = false
        sender.isSelected = true
        typeOfTBCase = sender.tag == 1 ? TBCase.DSTB.rawValue : TBCase.DRTB.rawValue
    }
    
    @IBAction func btnSiteOFDiseaseTap(_ sender: UIButton) {
        btnPTBDisease.isSelected = false
        btnEPTBDisease.isSelected = false
        sender.isSelected = true
        siteOFDisease = sender.tag == 1 ? Disease.PTB.rawValue : Disease.EPTB.rawValue
    }
    
    @IBAction func btnIsWorkerTap(_ sender: UIButton) {
        btnWorkerYes.isSelected = false
        btnWorkerNO.isSelected = false
        sender.isSelected = true
        isHealthWorker = sender.tag == 1 ? isHealthCareWorker.Yes.rawValue : isHealthCareWorker.No.rawValue
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isValidDetail() {
            
            if let listingVC = self.storyboard?.instantiateViewController(withIdentifier: "UserSampleOptionsVC") as? UserSampleOptionsVC {
                listingVC.clientDetailDict = makeDictForAPI()
                navigationController?.pushViewController(listingVC, animated: true)
            }
        }
    }
    
    func makeDictForAPI() -> [String:Any] {
        
        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : TBDict ?? [:],
                                                                 "covidSymptoms": covidDict ?? [:],
                                                                 "clientHasNoTB" : "",
                                                                 "clientHasTB" : ["patientInformation" : makeClientDetailDict()],
                                                                 "latitude" : currentCordinate.latitude,
                                                                 "longitude" : currentCordinate.longitude]]
        
        return parametrDict
    }
    
    func makeClientDetailDict() -> [String:Any] {
        
        dateFormatter.dateFormat = APIDateFormate
        
        let parametrDict : [String : Any] =  ["firstName" : txtFirstName.text ?? "",
                "lastName" : txtSurname.text ?? "",
                "middleName" : txtMiddleName.text ?? "",
                "visitDate" : dateFormatter.string(from: visitDate),
                "dateOfBirth" : dateFormatter.string(from: birthDate),
                "phoneNumber" : txtMobNo.text ?? "",
                "age" : lblAge.text ?? "" ,
                "sex" : selectedGender,
                "secondPhoneNumber":txtAlternateMobNo.text ?? "",
                "typeOfPresumptiveTbCase": typeOfTBCase,
                "siteOfDisease" : siteOFDisease,
                "patientWorker": isHealthWorker,
                "state" : txtState.text ?? "",
                "lga" : txtLGA.text ?? "",
                "address" : txtAddress.text ?? ""]
        
        return parametrDict
    }
    
    func isValidDetail() -> Bool {
        if let name = txtFirstName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        if let dob = txtDOB.text, dob.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterDOB)
            return false
        }
        if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }
        if let mobNo = txtMobNo.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMobNo.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        if typeOfTBCase.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectTypeOfTBCase)
            return false
        }
        if siteOFDisease.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectTypeOfDisease)
            return false
        }
        if isHealthWorker.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectIfHealthWorker)
            return false
        }
        return true
    }
    
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            present(datePickerVC, animated: true, completion: nil)
        }
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            txtLGA.text = ""
            heightCnstOFvwLGA.constant = 80
            txtLGA.isHidden = false
            dropDown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropDown.dataSource = addressDetail.LGA ?? []
            }
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropDown.hide()
        }
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
        }
    }
}

extension UserAddNewClientDetailVC  : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDOB{
            openDatePicker(dateType: .BirthDate)
            return false
        }
        if textField == txtDateOfVisit{
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtState{
            txtAddress.resignFirstResponder()
            setupStateDropdown()
            return false
        }
        if textField == txtLGA{
            txtAddress.resignFirstResponder()
            setupLGADropdown()
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobNo || textField == txtAlternateMobNo{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension UserAddNewClientDetailVC : DatePickerDelegate{
    
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDOB.text = dateFormatter.string(from: sDate)
            let age = Calendar.current.dateComponents([.year], from: sDate, to: Date()).year!
            lblAge.text = "\(age)"
            birthDate = sDate
        }else{
            txtDateOfVisit.text = dateFormatter.string(from: sDate)
            visitDate = sDate
        }
    }
}

