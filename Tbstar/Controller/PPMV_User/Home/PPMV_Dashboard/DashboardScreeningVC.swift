//
//  DashboardScreeningVC.swift
//  Tbstar
//
//  Created by Mavani on 10/06/21.
//

import UIKit

class DashboardScreeningVC: BaseViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvList: UITableView!
    
    var SearchType = ""
    var apiUrl = ""
    var screenTitle = ""
    var pageIndex = 0
    var totalPages = 0
    var arrCaseList = [PPMV_PatientInformation]()
    var arrAllCaseList = [PPMV_PatientInformation]()
    
    @IBOutlet var txtSearch : UITextField!
    
    var isSearch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageIndex = 0
        
        txtSearch.resignFirstResponder()
        lblTitle.text = screenTitle
        getListOfPatients()
        tblvList.reloadData()
    }
    
    func getListOfPatients()  {
        
        if isConnectedToNetwork(){
            showHUD()
            
            print(SearchType)
            let paradict : [String : Any]  = ["page" : 10,
                                              "pageIndex" : pageIndex ,
                                              "searchtype": SearchType]
            
            getPPMV_PatientTreatedList(paradict: paradict) { (success, msg, strdata) in
                self.hideHUD()
                if success {
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        print(json)
                        self.totalPages = json["totalPage"] as? Int ?? 0
                        if let list = json["PatientList"] as? [[String :Any]]{
                            if self.pageIndex == 0{
                                self.arrCaseList = list.map(PPMV_PatientInformation.init)
                                self.arrAllCaseList = list.map(PPMV_PatientInformation.init)
                            }else{
                                self.arrCaseList.append(contentsOf: list.map(PPMV_PatientInformation.init))
                                self.arrAllCaseList.append(contentsOf: list.map(PPMV_PatientInformation.init))
                            }
                        }
                        self.tblvList.reloadData()
                    }
                }else{
                    
                    if self.pageIndex == 0{
                        self.view.makeToast(msg)
                    }
                }
                self.tblvList.reloadData()
            }
        }else{
            hideHUD()
            view.makeToast(appMessages.noConnection)
        }
        
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension DashboardScreeningVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCaseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell_PPMV_Dashboard") as? cell_PPMV_Dashboard{
            cell.setCellData(info: arrCaseList[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrCaseList.count - 1{
            if pageIndex < totalPages {
                pageIndex += 1
                getListOfPatients()
            }
        }
    }
}

extension DashboardScreeningVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if let textFieldString = txtSearch.text, let swtRange = Range(range, in: textFieldString)
        {
            let strCorrectText = textFieldString.replacingCharacters(in: swtRange, with: string)
            let trimmedString = strCorrectText.trimmingCharacters(in: .whitespaces)
            let arrFiltered = arrAllCaseList
            arrCaseList = arrFiltered.filter({$0.firstName.contains(trimmedString) || $0.surname.contains(trimmedString)})
            if strCorrectText.isEmpty {
                arrCaseList = arrAllCaseList
            }
            tblvList.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtSearch {
            txtSearch.resignFirstResponder()
        }
        return true
    }
}
