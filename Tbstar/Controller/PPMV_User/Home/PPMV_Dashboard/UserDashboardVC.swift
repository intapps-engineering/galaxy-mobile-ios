//
//  UserDashboardVC.swift
//  Tbstar
//
//  Created by Mavani on 25/05/21.
//

import UIKit

class UserDashboardVC: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblFacility: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet var switchMonth : UISwitch!
    @IBOutlet var switchFacility : UISwitch!
    
    @IBOutlet weak var tblvDashboard: UITableView!

    var dashboardDict = [String:Any]()
    var patientCount = 0

    var boolMonth = false
    var boolFacility = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SetLocalization()
        Get_Dashboard_Result(ThisMonth: false, FacilityLavel: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switchMonth.isOn = false
        switchFacility.isOn = false
    }
    
    func Get_Dashboard_Result(ThisMonth : Bool , FacilityLavel : Bool) {

        if isConnectedToNetwork() {
            showHUD()
            var paradict = [String : Any]()
            paradict["calendarMonth"] = ThisMonth
            paradict["facilityLevel"] = FacilityLavel
            
            getPPMV_DashboardListScreenAPI(paradict: paradict, completionHandler: { (success, strData) in
                self.hideHUD()
                let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                print(strResponse)
                if let json = self.convertStringToDictionary(text: strResponse){
                    print(json)
                    if let dict = json["dashboard_result"] as? [String : Any] {
                        self.dashboardDict = dict
                    }
                }
                self.tblvDashboard.reloadData()
            })
        } else {
            self.hideHUD()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func SetLocalization() {
        
        lblTitle.text = "dashboard".localized
        lblMonth.text = "This Month".localized
        lblFacility.text = "Facility level".localized
        lblDesc.text = "Showing all time data for user".localized
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
         
        currentMenu = MenuOption.Home
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func switchMonthPressed(_ sender : UISwitch) {
     
        if switchMonth.isOn {
            switchMonth.isOn = true
            boolMonth = true
        } else {
            switchMonth.isOn = false
            boolMonth = false
        }
        Get_Dashboard_Result(ThisMonth: boolMonth, FacilityLavel: boolFacility)
    }
    
    @IBAction func switchFacilityPressed(_ sender : UISwitch) {
        
        if switchFacility.isOn {
            switchFacility.isOn = true
            boolFacility = true
        } else {
            switchFacility.isOn = false
            boolFacility = false
        }
        Get_Dashboard_Result(ThisMonth: boolMonth, FacilityLavel: boolFacility)
    }
}

extension UserDashboardVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dashboardDict.keys.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardCell") as? DashboardCell{
            cell.selectionStyle = .none
            if indexPath.section == 0{
                cell.lblTitle.text = "patientScreened".localized
                cell.lblCount.text = "\(dashboardDict["patients_screened"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "PPMV_D_1")
            }else if indexPath.section == 1{
                cell.lblTitle.text = "Presumptive Cases".localized
                cell.lblCount.text = "\(dashboardDict["presumptive_patients"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "screen TB")
            }else if indexPath.section == 2 {
                cell.lblTitle.text = "Samples Processed".localized
                cell.lblCount.text = "\(dashboardDict["sample_processed"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "PPMV_D_3")
            }else if indexPath.section == 3 {
                cell.lblTitle.text = "Positive Diagnoses".localized
                cell.lblCount.text = "\(dashboardDict["positive_diagnoses"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "PPMV_D_4")
            }else if indexPath.section == 4 {
                cell.lblTitle.text = "Referrals Made".localized
                cell.lblCount.text = "\(dashboardDict["referrals_made"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "PPMV_D_5")
            } else  if indexPath.section == 5 {
                cell.lblTitle.text = "Completed Referrals".localized
                cell.lblCount.text = "\(dashboardDict["completed_referrals_made"] as? Int ?? 0)"
                cell.imgvCell.image = UIImage(named: "PPMV_D_6")
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let detailvc = storyboard?.instantiateViewController(withIdentifier: "DashboardScreeningVC") as? DashboardScreeningVC{
            
            if indexPath.section == 0 {
                detailvc.screenTitle = "patientScreened".localized
                detailvc.SearchType = "patients_screened"
                patientCount = dashboardDict["patients_screened"] as? Int ?? 0
            }else if indexPath.section == 1{
                detailvc.screenTitle = "Presumptive Cases".localized
                detailvc.SearchType = "presumptive_patients"
                patientCount = dashboardDict["presumptive_patients"] as? Int ?? 0
            }else if indexPath.section == 2 {
                detailvc.screenTitle = "Samples Processed".localized
                detailvc.SearchType = "sample_processed"
                patientCount = dashboardDict["sample_processed"] as? Int ?? 0
            }else if indexPath.section == 3 {
                detailvc.screenTitle = "Positive Diagnoses".localized
                detailvc.SearchType = "positive_diagnoses"
                patientCount = dashboardDict["positive_diagnoses"] as? Int ?? 0
            }else if indexPath.section == 4 {
                detailvc.screenTitle = "Referrals Made".localized
                detailvc.SearchType = "referrals_made"
                patientCount = dashboardDict["referrals_made"] as? Int ?? 0
            }else if indexPath.section == 5 {
                detailvc.screenTitle = "Completed Referrals".localized
                detailvc.SearchType = "completed_referrals"
                patientCount = dashboardDict["completed_referrals_made"] as? Int ?? 0
            }
            if patientCount > 0 {
                navigationController?.pushViewController(detailvc, animated: true)
            }
        }
    }
    
}
