//
//  FacilityDetailVC.swift
//  Tbstar
//
//  Created by Mavani on 21/05/21.
//

import UIKit
import SkyFloatingLabelTextField

class FacilityDetailVC: UIViewController {

    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtType: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!

    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var btnExit : UIButton!
    @IBOutlet var btnSubmit : UIButton!
    
    var detailDict : [String : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()
        setupLocalization()
        setDetails()
    }
    func setDetails()  {
        
        if let facilityDict = detailDict {
            txtName.text    = facilityDict["facilityName"] as? String ?? ""
            txtType.text    = facilityDict["facility_type"] as? String ?? ""
            txtState.text   = facilityDict["facility_state"] as? String ?? ""
            txtLGA.text     = facilityDict["lga"] as? String ?? ""
            
            txtName.isUserInteractionEnabled = false
            txtType.isUserInteractionEnabled = false
            txtState.isUserInteractionEnabled = false
            txtLGA.isUserInteractionEnabled = false
        }
        
    }
    func setupLocalization() {
        
        txtName.placeholder = "facilityName".localized
        txtName.selectedTitle = "facilityName".localized
        txtName.title = "facilityName".localized
        
        txtType.placeholder = "facilityType".localized
        txtType.selectedTitle = "facilityType".localized
        txtType.title = "facilityType".localized
        
        txtState.placeholder = "Facility State".localized
        txtState.selectedTitle = "Facility State".localized
        txtState.title = "Facility State".localized
        
        txtLGA.placeholder = "LGA".localized
        txtLGA.selectedTitle = "LGA".localized
        txtLGA.title = "LGA".localized
        
        lblTitle.text = "Facility Details".localized
        
        btnExit.setTitle("Exit".localized, for: .normal)
        btnSubmit.setTitle("Submit".localized, for: .normal)
    }
    
    func setupTextField()  {
        
        txtName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtType.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtType.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtType.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtType.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
    }
    @IBAction func btnExitPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitPressed(_ sender : UIButton) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "NewRegistarationVC") as? NewRegistarationVC {
            VC.facilityDetailDict = detailDict
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}

extension FacilityDetailVC : UITextFieldDelegate {
    
}
