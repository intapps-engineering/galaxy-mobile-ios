//
//  OfflineSyncVC.swift
//  Tbstar
//
//  Created by user1 on 18/06/21.
//

import UIKit

class OfflineSyncVC : BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvSyncList: UITableView!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var heightCnstraint: NSLayoutConstraint!

    var arrPatientInfoDicts = [[String : Any]]()
    var arrPatientList = [PPMV_PatientInformation]()
    var arrTbCollectionDetail = [PatientInformation]()
    var arrTBCollectionDictList = [[String : Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLocalization()
        getListFromDatabase()
    }
    
    func setupLocalization() {
        lblTitle.text = "offline_sync".localized
        btnSync.setTitle("sync".localized, for: .normal)
    }

    func getListFromDatabase()  {
        
        arrPatientInfoDicts = PPMV_DBManager().fetchPPMV_PatientInfoData(query: "select * from PPMV_Patient_Details")
        arrPatientList = arrPatientInfoDicts.map(PPMV_PatientInformation.init)
        tblvSyncList.reloadData()
        getListFromUserDefaultss()
    }

    func getListFromUserDefaultss()  {
        
        if let arrDetails = Defaultss.value(forKey: UDKey.kTbCollecionDetail) as? [[String : Any]]{
            arrTBCollectionDictList = arrDetails
            arrTbCollectionDetail = arrDetails.map(PatientInformation.init)
        }
        
        tblvSyncList.reloadData()
        if arrPatientList.count == 0 && arrTbCollectionDetail.count == 0{
            heightCnstraint.constant = 0
            tblvSyncList.setNoDataPlaceholder("noRecord".localized)
            btnSync.isHidden = true
        }
    }

    func deleteFromDatabase()  {
        
        let ans = DatabaseManager().ExecuteQuery(query: "delete from PPMV_Patient_Details")
        if ans {
            currentMenu = MenuOption.Home
            navigationController?.popViewController(animated: true)
        }
    }

    
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSyncTap(_ sender: Any) {
        
        if isConnectedToNetwork(){
            //screenClientWithTB()
        }else{
            showAlert(titleStr: appName, msg: appMessages.noConnection)
        }
    }

}

extension OfflineSyncVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return arrPatientList.count + arrTbCollectionDetail.count
        return arrPatientList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell_PPMV_OfflineData") as? cell_PPMV_OfflineData{
            
            cell.setCell(info: arrPatientList[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }

}
