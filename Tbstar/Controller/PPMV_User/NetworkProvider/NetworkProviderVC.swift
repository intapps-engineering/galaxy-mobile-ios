//
//  NetworkProviderVC.swift
//  Tbstar
//
//  Created by Mavani on 20/05/21.
//

import UIKit
import SkyFloatingLabelTextField

class NetworkProviderVC : BaseViewController {
    
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet weak var txtProviderNumber: SkyFloatingLabelTextField!
    @IBOutlet var btnNext : UIButton!
    
    @IBOutlet var viewFacilityCode : UIView!
    
    var selectedOption : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()
        setupLocalization()
        setUpGesture()
    }
    
    func setupTextField()  {
        
        txtProviderNumber.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtProviderNumber.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtProviderNumber.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtProviderNumber.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    func setUpGesture()  {
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = [.right]
        self.viewFacilityCode.addGestureRecognizer(swipeRight)
        viewFacilityCode.isUserInteractionEnabled = true
    }

    @objc func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {

        self.navigationController?.popViewController(animated: true)
    }

    func setupLocalization() {
        
        if selectedOption == NetworkProviderSelected {
            lblTitle.text = "Enter Facility Access Code".localized
            txtProviderNumber.placeholder = "Enter Facility Code".localized
            txtProviderNumber.selectedTitle = "Enter Facility Code".localized
            txtProviderNumber.title = "Enter Facility Code".localized
        }else{
            lblTitle.text = "Enter Network Officer Access Code".localized
            txtProviderNumber.placeholder = "Enter Network Officer Code".localized
            txtProviderNumber.selectedTitle = "Enter Network Officer Code".localized
            txtProviderNumber.title = "Enter Network Officer Code".localized
        }
        btnNext.setTitle("submit".localized, for: .normal)
    }
    
    func ShowErrorDialog(strError : String) {
        
        if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"FacilityErrorVC") as? FacilityErrorVC {
            selectionPopup.modalPresentationStyle = .overCurrentContext
            present(selectionPopup, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnNextPressed(_ sender : UIButton) {
        
        if txtProviderNumber.text != "" {
            if isConnectedToNetwork(){
                getFacilityDetails()
            }else{
                view.makeToast(appMessages.noConnection)
            }
        } else {
            view.makeToast(appMessages.enterFacilityCode)
        }
    }
    func getFacilityDetails()  {
        
        showHUD()
        let paraDict:[String:Any] = ["facilityNumber" : txtProviderNumber.text ?? ""]
        
        USER_FacilityCode_Access(paradict: paraDict, completionHandler: { (success , strData, message) in
            if success {
                self.hideHUD()
                print(strData)
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse) {
                        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "FacilityDetailVC") as? FacilityDetailVC {
                            VC.detailDict = json
                            self.navigationController?.pushViewController(VC, animated: true)
                        }
                    }
                }
            } else {
                self.ShowErrorDialog(strError: strData)
                self.hideHUD()
            }
        })
    }
}

extension NetworkProviderVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtProviderNumber {
            
            txtProviderNumber.resignFirstResponder()
        }
        return true
    }
}
