//
//  FacilityErrorVC.swift
//  Tbstar
//
//  Created by Mavani on 04/06/21.
//

import UIKit

class FacilityErrorVC : UIViewController {

    @IBOutlet var lblError : UILabel!
    
    @IBOutlet var btnOK : UIButton!
    
    var strError : String?
     
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setLocalization()
    }
    
    func setLocalization() {
        
        btnOK.setTitle("ok".localized, for: .normal)
        lblError.text = "facility_code_error".localized
    }

    @IBAction func btnOKPressed(_ sender : UIButton) {
     
        dismiss(animated: true, completion: nil)
    }
}
