//
//  FindCaseCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 08/03/21.
//

import UIKit

class FindCaseCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCaseID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setCellData(info : PatientInformation) {
                
        lblName.text = "\(info.firstName) \(info.surname)"
        lblCaseID.text = "ID:\n\(info.patientRecordNumber)"

        if info.createDate.range(of:"[UTC]") != nil {
            
            info.createDate = info.createDate.replacingOccurrences(of: "[UTC]", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        
        let (strdate,_) = Utility.convertDateformat(date: info.createDate, currentFormate: apiDateFormatFromAPI, requiredFormate: "dd/MM/yyyy")
        lblDate.text = "\("date".localized):\n\(strdate)"
    }
    
}

class cell_PPMV_MyClient : UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCaseID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSatus: UILabel!

    override func layoutSubviews() {
        
        //lblSatus.roundCorners(corners: [.topLeft], radius: 10.0)
    }
    
    func setCellData(info : PPMV_PatientInformation) {
             
        lblName.text = "\(info.firstName) \(info.surname)"
        lblCaseID.text = "ID:\n\(info.patientRecordNumber)"

        if info.createDate.range(of:"[UTC]") != nil {
            
            info.createDate = info.createDate.replacingOccurrences(of: "[UTC]", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        
        let (strdate,_) = Utility.convertDateformat(date: info.createDate, currentFormate: apiDateFormatFromAPI, requiredFormate: "dd/MM/yyyy")
        lblDate.text = "\("date".localized):\n\(strdate)"
        
        lblSatus.text = " \(info.patientStatusLabel) "
    }

    func setAlertCellData(info : PPMV_HighPriority_Alerts_Data) {
             
        lblName.text = "\(info.firstName) \(info.lastName)"
        lblCaseID.text = "ID:\n\(info.id)"

        if info.dateConfirmedPositive.range(of:"[UTC]") != nil {
            
            info.dateConfirmedPositive = info.dateConfirmedPositive.replacingOccurrences(of: "[UTC]", with: "", options: NSString.CompareOptions.literal, range: nil)
        }

        let (strdate,_) = Utility.convertDateformat(date: info.dateConfirmedPositive, currentFormate: apiDateFormatFromAPI, requiredFormate: "dd/MM/yyyy")
        lblDate.text = "\("date".localized):\n\(strdate)"
        
        lblSatus.text = " \(info.status) "
    }

}

class cell_PPMV_Dashboard : UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCaseID: UILabel!
    @IBOutlet weak var lblDate: UILabel!

    func setCellData(info : PPMV_PatientInformation) {
        
        lblName.text = "Name : \(info.firstName) \(info.surname)"
        lblCaseID.text = "ID:\n\(info.patientRecordNumber)"

        if info.createDate.range(of:"[UTC]") != nil {
            
            info.createDate = info.createDate.replacingOccurrences(of: "[UTC]", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        
        let (strdate,_) = Utility.convertDateformat(date: info.createDate, currentFormate: apiDateFormatFromAPI, requiredFormate: "dd/MM/yyyy")
        lblDate.text = "\("date".localized):\n\(strdate)"
        
    }

}

class cellNotification : UITableViewCell {
    
    @IBOutlet var lblNotificationName : UILabel!
    @IBOutlet var lblNotificationDate : UILabel!
    
    func setCell(notification : PUSHNotification) {
        
        lblNotificationName.text = "\(notification.pushNotificationBody)"
        let (strdate,_) = Utility.convertDateformat(date: notification.createDate, currentFormate: apiDateFormatFromAPI, requiredFormate: "EEEE, dd MMMM, YYYY")
        lblNotificationDate.text = "\(strdate)"
    }
}

class cell_PPMV_OfflineData : UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblDate: UILabel!

    func setCell(info : PPMV_PatientInformation) {
        
        if info.firstName.isEmpty == true && info.surname.isEmpty == true {
            
            lblName.text = "Not_Available".localized
            lblStatus.text = " \("NON_PRESUMPTIVE".localized) "

        } else {
            lblName.text = "\(info.firstName) \(info.surname)"
            lblStatus.text = " \("PRESUMPTIVE".localized) "

        }
        
        let (strdate,_) = Utility.convertDateformat(date: info.createDate, currentFormate: apiDateFormatFromAPI, requiredFormate: "yyyy-MM-dd hh:mm a")
        lblDate.text = "Capture Date : \(strdate)"
    }
}
