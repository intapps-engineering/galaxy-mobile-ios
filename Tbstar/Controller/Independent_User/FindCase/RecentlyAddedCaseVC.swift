//
//  RecentlyAddedCaseVC.swift
//  Tbstar
//
//  Created by Mavani on 14/05/21.
//

import UIKit

class RecentlyAddedCaseVC: BaseViewController {
    
    @IBOutlet weak var tblvCase: UITableView!
    
    var arrRecentCaseList = [PatientInformation]()
    
    var pageIndex = 0
    var totalPages = 0
    
    var INDEX : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isConnectedToNetwork(){
            if INDEX == 1 {
                findRecentCases()
            }else{
                let userData = getUserDataFromStandard()
                let UserID = userData?.userId
                self.showHUD()
                
                GetSharePatintList(paradict: ["to-ip-user-id": UserID ?? ""], completionHandler: { (arr) in
                    
                    print(arr)
                    if arr.count > 0 {
                        self.arrRecentCaseList = arr
                        self.tblvCase.removeNoDataPlaceholder()
                    } else {
                        self.tblvCase.setNoDataPlaceholder("noclientfound".localized)
                    }
                    self.tblvCase.reloadData()
                    self.hideHUD()
                    
                }, failure: { (error) in
                    
                    self.hideHUD()
                    self.view.makeToast(error.debugDescription)
                })
            }
        }
    }
    
    func findRecentCases()  {
        
        if isConnectedToNetwork(){
            self.showHUD()
            
            var paradict = [String : Any]()
            
            paradict["search"] = false
            paradict["lastName"] = ""
            paradict["patientRecordNumber"] = ""
            paradict["page"] = 10
            paradict["pageIndex"] = pageIndex
            
            findRecentCasesAPI(paradict: paradict) { (success, strData, message) in
                if success {
                    
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.totalPages = json["totalPage"] as? Int ?? 0
                        
                        if let list = json["PatientList"] as? [[String :Any]]{
                            if self.pageIndex == 0{
                                self.arrRecentCaseList = list.map(PatientInformation.init)
                            }else{
                                self.arrRecentCaseList.append(contentsOf: list.map(PatientInformation.init))
                            }
                        }
                    }else{
                        self.view.makeToast(strResponse)
                    }
                    self.hideHUD()
                }else{
                    self.hideHUD()
                    self.arrRecentCaseList.removeAll()
                }
                if self.arrRecentCaseList.count == 0{
                    self.tblvCase.setNoDataPlaceholder("noclientfound".localized)
                }else {
                    self.tblvCase.removeNoDataPlaceholder()
                }
                self.tblvCase.reloadData()
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
}

extension RecentlyAddedCaseVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRecentCaseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "recentCaseCell") as! FindCaseCell
        cell.setCellData(info: arrRecentCaseList[indexPath.row])
        cell.selectionStyle = .none
        cell.selectionStyle = .default
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrRecentCaseList.count - 1{
            if pageIndex < totalPages {
                pageIndex += 1
                findRecentCases()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let clinicRegister = self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterContainerVC") as? ClinicRegisterContainerVC {
            clinicRegister.isEditMode = false
            clinicRegister.isDetailMode = true
            if INDEX == 1 {
                clinicRegister.ISFROM = "RecentCase"
            } else {
                clinicRegister.ISFROM = "ShareCase"
            }
            clinicRegister.patientDetail = arrRecentCaseList[indexPath.row]
            navigationController?.pushViewController(clinicRegister, animated: true)
        }
    }
}
