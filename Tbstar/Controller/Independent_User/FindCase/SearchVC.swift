//
//  SearchVC.swift
//  Tbstar
//
//  Created by Mavani on 14/05/21.
//

import UIKit

class SearchVC : BaseViewController {
    
    @IBOutlet weak var btnserachBySname: UIButton!
    @IBOutlet weak var btnsearchByCase: UIButton!
        
    @IBOutlet weak var txtSearch: UITextField!
    
    var arrRecentCaseList = [PatientInformation]()
    
    @IBOutlet weak var tblvSearch: UITableView!
    
    var selectedSearchType = ""
    var pageIndex = 0
    var totalPages = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLocalization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedSearchType != "" {
            findRecentCases()
        }
    }
        
    func setupLocalization() {
        
        btnserachBySname.setTitle("searchSurname".localized, for: .normal)
        btnserachBySname.setTitle("searchSurname".localized, for: .selected)
        btnsearchByCase.setTitle("searchCase".localized, for: .normal)
        btnsearchByCase.setTitle("searchCase".localized, for: .selected)
        txtSearch.placeholder = "searchText".localized
    }
    
    func findRecentCases()  {
        
        if isConnectedToNetwork(){
            self.showHUD()
            
            var paradict = [String : Any]()
            
            paradict["search"] = true            
            paradict["lastName"] = selectedSearchType == "Surname" ? txtSearch.text! : ""
            paradict["patientRecordNumber"] = selectedSearchType == "Case" ? txtSearch.text! : ""
            
            findRecentCasesAPI(paradict: paradict) { (success, strData, message) in
                if success {
                    
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.totalPages = json["totalPage"] as? Int ?? 0
                        
                        if let list = json["PatientList"] as? [[String :Any]]{
                            if self.pageIndex == 0{
                                self.arrRecentCaseList = list.map(PatientInformation.init)
                            }else{
                                self.arrRecentCaseList.append(contentsOf: list.map(PatientInformation.init))
                            }
                        }
                    }else{
                        self.view.makeToast(strResponse)
                    }
                    self.hideHUD()
                }else{
                    self.hideHUD()
                    self.arrRecentCaseList.removeAll()
                }
                if self.arrRecentCaseList.count == 0{
                    self.tblvSearch.setNoDataPlaceholder("noclientfound".localized)
                } else {
                    self.tblvSearch.removeNoDataPlaceholder()
                }
                self.tblvSearch.reloadData()
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
    
    //MARK:- Actions
    @IBAction func btnSearchTypeTap(_ sender: UIButton) {
        arrRecentCaseList.removeAll()
        btnserachBySname.isSelected = false
        btnsearchByCase.isSelected = false
        sender.isSelected = true
        selectedSearchType = sender.tag == 1 ? "Surname" : "Case"
    }
    
    
    @IBAction func btnSearchPressed() {
        if let searchText = txtSearch.text, searchText.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterSearchText)
        }else if selectedSearchType == ""{
            showAlert(titleStr: appName, msg: appMessages.selectSearchType)
        }else{
            findRecentCases()
            txtSearch.resignFirstResponder()
        }
    }
    
}

extension SearchVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let searchText = textField.text, searchText.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterSearchText)
            return false
        }else if selectedSearchType == ""{
            showAlert(titleStr: appName, msg: appMessages.selectSearchType)
            return false
        }else{
            textField.resignFirstResponder()
            findRecentCases()
            return true
        }
    }
}

extension SearchVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRecentCaseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblvSearch.dequeueReusableCell(withIdentifier: "findCaseCell") as! FindCaseCell
        cell.setCellData(info: arrRecentCaseList[indexPath.row])
        cell.selectionStyle = .none
        cell.selectionStyle = .default
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let clinicRegister = self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterContainerVC") as? ClinicRegisterContainerVC {
            clinicRegister.isEditMode = false
            clinicRegister.isDetailMode = true
            clinicRegister.patientDetail = arrRecentCaseList[indexPath.row]
            navigationController?.pushViewController(clinicRegister, animated: true)
        }
    }
}
