//
//  FindCaseViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 08/03/21.
//

import UIKit
import CarbonKit

class FindCaseViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewChildAdd: UIView!
        
    var tabSwipe = CarbonTabSwipeNavigation()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupLocalization()
        TopSliderSetUp()
    }
    
    func setupLocalization() {
        
        lblTitle.text = "findCase".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidLayoutSubviews() {
        
        tabSwipe.toolbar.barTintColor = .white
        tabSwipe.toolbar.clipsToBounds = true
    }

    //MARK:- TabBar SetUp
    func TopSliderSetUp() {
        
        tabSwipe = CarbonTabSwipeNavigation(items: ["Search".localized, "Recently Added Case".localized, "Shared with me".localized], delegate: self)

        var frameRect: CGRect = (tabSwipe.carbonSegmentedControl?.frame)!
        frameRect.size.width = screenWidth  
        tabSwipe.carbonSegmentedControl?.frame = frameRect
        tabSwipe.carbonSegmentedControl?.apportionsSegmentWidthsByContent = false
        tabSwipe.setTabBarHeight(50)
        tabSwipe.setNormalColor(.UnselectedBlueColor)
        tabSwipe.setSelectedColor(.SlectedBlueColor)
        tabSwipe.setIndicatorColor(.SlectedBlueColor)
        let font =  UIFont(name:"Quicksand-Medium",size:16)
        tabSwipe.setNormalColor(.UnselectedBlueColor, font: font!)
        tabSwipe.setSelectedColor(.SlectedBlueColor, font: font!)
        tabSwipe.insert(intoRootViewController: self, andTargetView: viewChildAdd)
        
        viewChildAdd.addSubview(tabSwipe.view)
        tabSwipe.view.frame = viewChildAdd.bounds
        tabSwipe.didMove(toParent: self)
    }

    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
        
}
//MARK:- TabBar delegates
extension FindCaseViewController : CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        guard storyboard != nil else { return UIViewController() }
        switch index {
            case 0:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC")
                    as! SearchVC
                return VC
            case 1:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "RecentlyAddedCaseVC") as! RecentlyAddedCaseVC
                VC.INDEX = 1
                return VC
            case 2:
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "RecentlyAddedCaseVC") as! RecentlyAddedCaseVC
                VC.INDEX = 2
                return VC
            default: break
        }
        return UIViewController()
    }
}
