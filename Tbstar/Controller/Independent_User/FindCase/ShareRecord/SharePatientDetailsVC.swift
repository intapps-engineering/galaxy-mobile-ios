//
//  SharePatientDetailsVC.swift
//  Tbstar
//
//  Created by Mavani on 17/05/21.
//

import UIKit
import Alamofire

class SharePatientDetailsVC : BaseViewController {

    @IBOutlet var btnShareRecord : UIButton!
    
    @IBOutlet var txtSearch : UITextField!
    
    @IBOutlet var lblPName : UILabel!
    @IBOutlet var lblPNumber : UILabel!
    @IBOutlet var lblPSex : UILabel!
    @IBOutlet var lblPAge : UILabel!
    @IBOutlet var lblUName : UILabel!
    @IBOutlet var lblUNumber : UILabel!
    @IBOutlet var lblUFacilityNumber : UILabel!
    @IBOutlet var lblUState : UILabel!
    @IBOutlet var lblULGA : UILabel!
    @IBOutlet var lblWith : UILabel!
    @IBOutlet var lblShare : UILabel!
    @IBOutlet var lblTitle : UILabel!

    var birthDate = Date()
    var dateFormatter = DateFormatter()
    
    var patientDetail : PatientInformation?
    var sharePatientDetail : SharePatientDetails?
    var dictUserDiect : SharePatientDetails_To_User?
    
    var isfrom : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.BuildUI()
    }

    func BuildUI() {
        
        btnShareRecord.layer.cornerRadius = btnShareRecord.frame.size.height / 2
        btnShareRecord.backgroundColor = UIColor.lightGray
        btnShareRecord.isEnabled = false
        
        lblWith.text = "With".localized
        lblShare.text = "Share Patient Details".localized
        lblTitle.text = "Share Patient Details".localized
        
        txtSearch.placeholder = "Enter user's phone number".localized
        
        let name = "Patient Name".localized
        let number = "Patient Number".localized
        let gender = "Sex".localized
        let age = "Age".localized

        btnShareRecord.setTitle("Share Record".localized, for: .normal)
        lblPName.text = "\(name): \(patientDetail?.firstName ?? "") \(patientDetail?.surname ?? "")"
        lblPNumber.text = "\(number): \(patientDetail?.phoneNumber ?? "")"
        lblPSex.text = "\(gender): \(patientDetail?.sex ?? "")"
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        if isfrom == "ShareCase" {
            dateFormatter.dateFormat = NewapiDateFormatFromAPI
        } else {
            dateFormatter.dateFormat = apiDateFormatFromAPI
        }
        
        let date = dateFormatter.date(from:patientDetail?.dateOfBirth ?? "") ?? Date()
        lblPAge.text = "\(age): \(calculateAge(bDate: date))"
    }
    
    func FindUserFromPhoneNumber()  {
        if isConnectedToNetwork(){
            self.showHUD()
            GetUserFromPhoneNumber(paradict: ["user-phone-number": txtSearch.text ?? ""], completionHandler: { (dictUSer) in
                self.hideHUD()
                
                print(dictUSer)
                self.dictUserDiect = dictUSer
                self.btnShareRecord.backgroundColor = .SlectedBlueColor
                self.btnShareRecord.isEnabled = true
                self.SetUserDetail(dictUSer: self.dictUserDiect!)
            }, failure: { (error) in
                self.hideHUD()
                self.view.makeToast(error.debugDescription)
            })
        }
    }
    
    func SetUserDetail(dictUSer : SharePatientDetails_To_User) {
        
        let name = "Name".localized
        let number = "Phone Number".localized
        let LGA = "LGA".localized
        let State = "State".localized
        let facility = "Facility Name".localized

        lblUName.text = "\(name): \(dictUSer.firstName ?? "") \(dictUSer.lastName ?? "")"
        lblUNumber.text = "\(number): \(dictUSer.phoneNumber ?? "")"
        lblULGA.text = "\(LGA): \(dictUSer.lgaName ?? "")"
        lblUState.text = "\(State): \(dictUSer.stateName ?? "")"
        lblUFacilityNumber.text = "\(facility): \(dictUSer.facilityName ?? "")"
    }
    
    //MARK:- Actions
    @IBAction func btnShareRecordPressed(_ sender : UIButton) {
        
        if txtSearch.text != "" {
            
            let userData = getUserDataFromStandard()
            let UserID = userData?.userId

            DispatchQueue.main.async(execute: {
                
                let dict : NSMutableDictionary = NSMutableDictionary()
                dict.setValue("\(self.patientDetail?.firstName ?? "") \(self.patientDetail?.surname ?? "")", forKey: "PName")
                dict.setValue("\(self.dictUserDiect?.firstName ?? "") \(self.dictUserDiect?.lastName ?? "")", forKey: "UName")
                dict.setValue(self.dictUserDiect?.id, forKey: "to-ip-user-id")
                dict.setValue(UserID, forKey: "from-ip-user-id")
                dict.setValue(self.patientDetail?._id, forKey: "patient-id")
                
                let dictData:[String: NSMutableDictionary] = ["Dictionary": dict]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationSendNameOfPatientAndUser"), object: nil, userInfo: dictData)
            })

            let sharereportview = Bundle.main.loadNibNamed("ShareReportView", owner: self, options: nil)?.first as? ShareReportView
            sharereportview?.frame = self.view.bounds
            sharereportview?.delegate = self
            self.view.addSubview(sharereportview!)
        }
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

}

extension SharePatientDetailsVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtSearch {
            
            if txtSearch.text != "" {
                FindUserFromPhoneNumber()
            } else {
                btnShareRecord.backgroundColor = UIColor.lightGray
                btnShareRecord.isEnabled = false
            }
            txtSearch.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
}

extension SharePatientDetailsVC : ShareReportViewHide {
    
    func successfullySendReport() {
        self.navigationController?.popViewController(animated: true)
    }
}
