//
//  ShareReportView.swift
//  Tbstar
//
//  Created by Mavani on 17/05/21.
//

import Foundation
import UIKit
import SVProgressHUD

protocol ShareReportViewHide : class {
    
    func successfullySendReport()
}

class ShareReportView : UIView {
    
    @IBOutlet var viewShadow : UIView!
    
    @IBOutlet var btnYes : UIButton!
    @IBOutlet var btnNo : UIButton!
    
    @IBOutlet var lblNameDisply : UILabel!
    @IBOutlet var lblShare : UILabel!

    var dictData : NSMutableDictionary = NSMutableDictionary()

    var delegate : ShareReportViewHide?
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
                
        BuildUI()
    }
    
    func BuildUI() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNameOfPatientAndUser(_:)), name: NSNotification.Name(rawValue: "NotificationSendNameOfPatientAndUser"), object: nil)
        
        lblShare.text = "Share Patient Details".localized
        btnNo.setTitle("No".localized, for: .normal)
        btnYes.setTitle("Yes".localized, for: .normal)

        btnNo.layer.cornerRadius = btnNo.frame.size.height / 2
        btnYes.layer.cornerRadius = btnYes.frame.size.height / 2
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.ShadowTap(_:)))
        viewShadow.addGestureRecognizer(tap)
    }
    
    @objc func ShadowTap(_ sender: UITapGestureRecognizer? = nil) {
     
        self.removeFromSuperview()
    }
    
    @objc func getNameOfPatientAndUser(_ notification: NSNotification) {
        
        if let data = notification.userInfo?["Dictionary"] as? NSMutableDictionary {
            dictData = data
            let Pname = data.value(forKey: "PName")
            let Uname = data.value(forKey: "UName")
            
            let share = "Share".localized
            let desc = "patient record with Independent Provider".localized
            lblNameDisply.text = "\(share) \(Pname ?? "") \(desc) \(Uname ?? "")?"
        }
    }

    //MARK:- Action
    @IBAction func btnYesPeressed(_ sender : UIButton) {
        
        SVProgressHUD.show()
        
        var paradict = [String : String]()
        paradict["from-ip-user-id"] = dictData.value(forKey: "from-ip-user-id") as? String
        paradict["patient-id"] = dictData.value(forKey: "patient-id") as? String
        paradict["to-ip-user-id"] = dictData.value(forKey: "to-ip-user-id") as? String
        
        SharepatientDetails(paradict: paradict, completionHandler: { (success,dict) in

            SVProgressHUD.dismiss()
            if success {
                print(dict)
                self.removeFromSuperview()
                self.delegate?.successfullySendReport()
            }
            
        }, failure: { (error) in
            
            self.makeToast("No User Found.!".localized)
            SVProgressHUD.dismiss()
        })

    }
    
    @IBAction func btnNoPressed(_ sender : UIButton) {
        
        self.removeFromSuperview()
    }
}
