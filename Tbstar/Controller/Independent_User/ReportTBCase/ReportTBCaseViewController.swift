//
//  ReportTBCaseViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 17/02/21.
//

import UIKit

class ReportTBCaseViewController: UIViewController {

    @IBOutlet weak var lblTilte: UILabel!
    @IBOutlet weak var btnFindExisting: UIButton!
    @IBOutlet weak var btnCreateNewCaseTap: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
    }
    func setupLocalization() {
        lblTilte.text = "reportTB".localized
        btnFindExisting.setTitle("findExisting".localized, for: .normal)
        btnCreateNewCaseTap.setTitle("newCase".localized, for: .normal)
    }
    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnFindExistingTap(_ sender: Any) {
        
        if let findCase = self.storyboard?.instantiateViewController(withIdentifier: "FindCaseViewController") as? FindCaseViewController {
            navigationController?.pushViewController(findCase, animated: true)
        }
    }
    @IBAction func btnNewCaseTap(_ sender: Any) {
        
        if let clinicRegister = self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterContainerVC") as? ClinicRegisterContainerVC {
            
            navigationController?.pushViewController(clinicRegister, animated: true)
        }
        
    }
}
