//
//  ListingViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 02/03/21.
//

import UIKit

class ListingViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbldesc: UILabel!
    @IBOutlet weak var btnCmpltScrning: UIButton!
    @IBOutlet weak var btnReportTB: UIButton!
    
    var clientDetailDict : [String : Any]?
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = displayDateFormat
        setupLocalization()
    }
    func setupLocalization() {
        lblTitle.text = "listing".localized
        lbldesc.text = "listingDesc".localized
        btnCmpltScrning.setTitle("completeScreening".localized, for: .normal)
        btnReportTB.setTitle("reportTB".localized, for: .normal)
    }
    @IBAction func btnBackTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCompleteScreeningTap(_ sender: Any) {
        
        if isConnectedToNetwork(){
            screenClientWithTB(shouldOpenThanks: true)
        }else{
            //Save details to DB
            //showAlert(titleStr: appName, msg: appMessages.noConnection)
            insertPatientDetailsTODB(shouldOpenThanks: true)
            //showAlert(titleStr: appName, msg: appMessages.noConnection)
        }
    }
    func openThanksPopup(recordNumber : String, id : String)  {
        if let thankspopup = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController{
            thankspopup.delegate = self
            thankspopup.mainTitle = "thankyou".localized
            thankspopup.subTitle = "screeningCompleted".localized
            thankspopup.caseDetail = "\("yourCase".localized)\n\(recordNumber)\n \("usethisID".localized)"
            thankspopup.modalPresentationStyle = .overCurrentContext
            present(thankspopup, animated: true, completion: nil)
        }
    }
    func openClinicRegistration(detail : PatientInformation, isoffline : Bool)  {
        if let clinicRegister = self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterContainerVC") as? ClinicRegisterContainerVC{
            clinicRegister.isEditMode = true
            clinicRegister.patientDetail = detail
            clinicRegister.isDetailMode = false
            clinicRegister.askPopupOnBack = true
            clinicRegister.isFromScreening = true
            clinicRegister.isFromOfflineData = isoffline
            navigationController?.pushViewController(clinicRegister, animated: true)
        }
    }
    @IBAction func btnReportTbTap(_ sender: Any) {
        
        if isConnectedToNetwork(){
            screenClientWithTB(shouldOpenThanks: false)
        }else{
            insertPatientDetailsTODB(shouldOpenThanks: false)
            //Save details to DB
            //showAlert(titleStr: appName, msg: appMessages.noConnection)
        }
    }
    func insertPatientDetailsTODB(shouldOpenThanks : Bool)  {
        
        let patientDetail = PatientInformation(dict: clientDetailDict ?? [:])
        var strQuerry = ""
        var ans = false
        if let tbsymptoms = patientDetail.patientTBSymptoms{
                
            if let covidSymptoms = patientDetail.patientCovidSymptoms {
                
                strQuerry = "insert or replace into PatientDetails(latitude,longitude,middleName,lga,siteOfDisease,visitDate,dateOfBirth,phoneNumber,age,hivStatus,typeOfPresumptiveTbCase,secondPhoneNumber,patientWorker,sex,state,address,lastName,hadTBbefore,firstName,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea, covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste,createdDate) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\(patientDetail.middleName.debugDescription),\(patientDetail.lga.debugDescription),\(patientDetail.siteOfDisease.debugDescription),\(patientDetail.visitDate.debugDescription),\(patientDetail.dateOfBirth.debugDescription),\(patientDetail.phoneNumber.debugDescription),\(patientDetail.age.debugDescription),\(patientDetail.hivStatus.debugDescription),\(patientDetail.typeOfPresumptiveTbCase.debugDescription),\(patientDetail.secondPhoneNumber.debugDescription),\(patientDetail.patientWorker.debugDescription),\(patientDetail.sex.debugDescription),\(patientDetail.state.debugDescription),\(patientDetail.address.debugDescription),\(patientDetail.surname.debugDescription),\(patientDetail.hadTBbefore.debugDescription),\(patientDetail.firstName.debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription),\(covidSymptoms.covidDiarrhea.debugDescription),\(covidSymptoms.covidSoreThroat.debugDescription),\(covidSymptoms.covidShaking.debugDescription),\(covidSymptoms.covidFever.debugDescription),\(covidSymptoms.covidRunnyNose.debugDescription),\(covidSymptoms.covidCough.debugDescription),\(covidSymptoms.covidLossOfTaste.debugDescription),\(dateFormatter.string(from: Date()).debugDescription))"
                
                ans = DatabaseManager().ExecuteQuery(query: strQuerry)
                print(ans)
                
            }else {
                
                strQuerry = "insert or replace into PatientDetails(latitude,longitude,middleName,lga,siteOfDisease,visitDate,dateOfBirth,phoneNumber,age,hivStatus,typeOfPresumptiveTbCase,secondPhoneNumber,patientWorker,sex,state,address,lastName,hadTBbefore,firstName,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea,covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste,createdDate) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\(patientDetail.middleName.debugDescription),\(patientDetail.lga.debugDescription),\(patientDetail.siteOfDisease.debugDescription),\(patientDetail.visitDate.debugDescription),\(patientDetail.dateOfBirth.debugDescription),\(patientDetail.phoneNumber.debugDescription),\(patientDetail.age.debugDescription),\(patientDetail.hivStatus.debugDescription),\(patientDetail.typeOfPresumptiveTbCase.debugDescription),\(patientDetail.secondPhoneNumber.debugDescription),\(patientDetail.patientWorker.debugDescription),\(patientDetail.sex.debugDescription),\(patientDetail.state.debugDescription),\(patientDetail.address.debugDescription),\(patientDetail.surname.debugDescription),\(patientDetail.hadTBbefore.debugDescription),\(patientDetail.firstName.debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(dateFormatter.string(from: Date()).debugDescription))"
                
                ans = DatabaseManager().ExecuteQuery(query: strQuerry)
                print(ans)
            }
            
        }
        if ans && shouldOpenThanks {
            openThanksPopup(recordNumber: "", id: "")
        }else if ans {
            let parentDetail = PatientInformation(dict: self.clientDetailDict ?? [:])
            parentDetail.patientRecordNumber = ""
            parentDetail._id = ""
            self.openClinicRegistration(detail: parentDetail,isoffline: true)
        }else{
            view.makeToast(appMessages.somethingWrong)
        }
    }
    func screenClientWithTB(shouldOpenThanks : Bool)  {
        
        showHUD()
        screenIndependentUserPatientAPI(paradict: clientDetailDict ?? [:]) { (success, strData, message) in
            self.hideHUD()
        
            if strData != ""{
                
                let key = encKeyForGetAnd
                let strResponse = self.decryptResponse(response: strData,key: key)
                if let json = self.convertStringToDictionary(text: strResponse){
                    if success{
                        let pID = json["_id"] as? String ?? ""
                        let pRN = json["patientRecordNumber"] as? String ?? ""
                        
                        if shouldOpenThanks{
                            self.openThanksPopup(recordNumber: pRN, id: pID)
                        }else{
                            let parentDetail = PatientInformation(dict: self.clientDetailDict ?? [:])
                            parentDetail.patientRecordNumber = pRN
                            parentDetail._id = pID
                            self.openClinicRegistration(detail: parentDetail,isoffline: false)
                        }
                        
                    }
                }
                else{
                    self.view.makeToast(strResponse)
                }
            }else if message != ""{
                self.view.makeToast(message)
                print(message)
            }
            
        }
    }
}
extension ListingViewController : ThanksPopupDelegate{
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if let homevc = vc as? HomeViewController{
                    navigationController?.popToViewController(homevc, animated: true)
                }
            }
        }
    }
}
