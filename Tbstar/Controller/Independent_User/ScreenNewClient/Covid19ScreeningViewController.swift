//
//  Covid19ScreeningViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 18/02/21.
//

import UIKit

protocol Covid19ScreenDelegate {
    func updatePatientRecord(covidDict : [String :Any])
}

class Covid19ScreeningViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvQuestions: UITableView!
    @IBOutlet weak var lblScreeningQuestions: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    var isPatientHaveTB = false
    var TBDict : [String:Any]?
    
    var isFromEdit = false
    
    let arrQuestions = ["Do you have cough?","Do you have fever (Temperature 37.5'c & above)?","Do you experience new loss of taste or smell?","Do you have cold or runny nose?","Do you have diarrhea?","Do you have sore throat?","Do you have repeated shaking with chills?"]
    var answerDict = [Int:String]()
   
    var patientRecordNumber = ""
    var delegate : Covid19ScreenDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        // Do any additional setup after loading the view.
    }
    func setupLocalization() {
        lblScreeningQuestions.text = "screenQues".localized
        btnSubmit.setTitle("submit".localized, for: .normal)
    }
    func isDetailValid() -> Bool {
        
        if answerDict.keys.count != arrQuestions.count{
            showAlert(titleStr: appName, msg: appMessages.questionMustAns)
            return false
        }
        return true
    }
    func updateCovidSymptomForPatient() {
        
        if isConnectedToNetwork(){
            showHUD()
            
            var url = ""
            if userType == USER_TYPE_INDEPENDENT {
                url = updateCovidStatusOfPatientUrl
            } else {
               url = PPMV_updateCovidStatusOfPatientUrl
            }
            updateCovidSymptomsOfPatientAPI(apiURL: url, paradict: ["covidSymptoms" : makeParametrForAPI(),
                                                       "patientRecordNumber" : patientRecordNumber]) { (success, strData, message) in
                self.hideHUD()
                if success{
                    self.view.makeToast(message)
                    self.delegate?.updatePatientRecord(covidDict: self.makeParametrForAPI())
                    self.dismiss(animated: true, completion: nil)
                    print(message)
                }else{
                    self.view.makeToast(message)
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    @IBAction func btnBackTap(_ sender: Any) {
        
        if isFromEdit{
            dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isDetailValid(){
            if let covid19ResultVC = self.storyboard?.instantiateViewController(withIdentifier: "Covid19ResultViewController") as? Covid19ResultViewController{
                covid19ResultVC.totalCount = arrQuestions.count
                covid19ResultVC.symptomCount = answerDict.values.filter( {$0 == "yes"}).count
                covid19ResultVC.delegate = self
                covid19ResultVC.modalPresentationStyle = .overCurrentContext
                present(covid19ResultVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnYesTap(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblvQuestions)
        if let indexPath = self.tblvQuestions.indexPathForRow(at: buttonPosition){
            if let cell = tblvQuestions.cellForRow(at: indexPath) as? QuestionCell{
                cell.btnYes.isSelected = true
                cell.btnNo.isSelected = false
                answerDict[indexPath.row] = "yes"
            }
        }
    }
    
    @IBAction func btnNoTap(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblvQuestions)
        if let indexPath = self.tblvQuestions.indexPathForRow(at: buttonPosition){
            if let cell = tblvQuestions.cellForRow(at: indexPath) as? QuestionCell{
                cell.btnYes.isSelected = false
                cell.btnNo.isSelected = true
                answerDict[indexPath.row] = "no"
            }
        }
    }
    func makeParametrForAPI() -> [String:Any]  {
        
        return ["covidCough" : answerDict[0] ?? "" ,
                "covidFever" : answerDict[1] ?? "",
                "covidLossOfTaste" : answerDict[2] ?? "",
                "covidRunnyNose" : answerDict[3] ?? "",
                "covidDiarrhea" : answerDict[4] ?? "",
                "covidSoreThroat" : answerDict[5] ?? "",
                "covidShaking" : answerDict[6] ?? ""]
    }

}
extension Covid19ScreeningViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "covid19questionCell") as? QuestionCell{
            cell.lblQuestion.text = arrQuestions[indexPath.row]
            cell.btnNo.setTitle("no".localized, for: .normal)
            cell.btnYes.setTitle("yes".localized, for: .normal)
            cell.btnNo.setTitle("no".localized, for: .selected)
            cell.btnYes.setTitle("yes".localized, for: .selected)
            return cell
        }
        return UITableViewCell()
    }
}
extension Covid19ScreeningViewController : Covid19ResultViewControllerDelegate{
    func tappedOnContinue() {
        
        if isFromEdit{
            updateCovidSymptomForPatient()
        } else {
            if isPatientHaveTB {
                if userType == USER_TYPE_INDEPENDENT {
                    if let addNewClientVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewClientDetailViewController") as? AddNewClientDetailViewController{
                        addNewClientVC.covidDict = makeParametrForAPI()
                        addNewClientVC.TBDict = TBDict
                        navigationController?.pushViewController(addNewClientVC, animated: true)
                    }
                    
                } else {
                    
                    if let addNewClientVC = self.storyboard?.instantiateViewController(withIdentifier: "UserAddNewClientDetailVC") as? UserAddNewClientDetailVC {
                        addNewClientVC.covidDict = makeParametrForAPI()
                        addNewClientVC.TBDict = TBDict
                        navigationController?.pushViewController(addNewClientVC, animated: true)
                    }
                }
            }else{
                if let noTBDetectedVC = self.storyboard?.instantiateViewController(withIdentifier: "TBNotDetectedViewController") as? TBNotDetectedViewController{
                    noTBDetectedVC.TBDict = TBDict
                    noTBDetectedVC.covidDict = makeParametrForAPI()
                    navigationController?.pushViewController(noTBDetectedVC, animated: true)
                }
            }
        }
        
    }
}
