//
//  TBNotDetectedViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 05/03/21.
//

import UIKit

class TBNotDetectedViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNotHaveTB: UILabel!
    @IBOutlet weak var lblCollectInfo: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var btnAge0_14: UIButton!
    @IBOutlet weak var btnAge15_above: UIButton!
    
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var btnMAle: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var TBDict : [String:Any]?
    var covidDict : [String :Any]?
    
    var age = ""
    var gender = ""
    var paradict : [String : Any]?
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        askForLocation()
        setupLocalization()
        dateFormatter.dateFormat = displayDateFormat
    }
    func setupLocalization() {
        
        lblTitle.text = "tbnotdetected".localized
        lblNotHaveTB.text = "clientNothaveTB".localized
        lblCollectInfo.text = "collectInfo".localized
        lblAge.text = "age".localized
        btnAge0_14.setTitle("age0-14".localized, for: .normal)
        btnAge0_14.setTitle("age0-14".localized, for: .selected)
        
        btnAge15_above.setTitle("age15".localized, for: .normal)
        btnAge15_above.setTitle("age15".localized, for: .selected)
        
        lblGender.text = "sex".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
    }
    func isValidData() -> Bool{
        
        if age == ""{
            showAlert(titleStr: appName, msg: appMessages.enterAge)
            return false
        }else if gender == ""{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }
        return true
    }
    
    @IBAction func btnAge0_14Tap(_ sender: Any) {
        age = "0-14"
        btnAge0_14.isSelected = true
        btnAge15_above.isSelected = false
    }
    
    @IBAction func btnAge15_aboveTap(_ sender: Any) {
        age = "15 & above"
        btnAge0_14.isSelected = false
        btnAge15_above.isSelected = true
    }
    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnFemaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMAle.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        gender = Gender.Female.rawValue
    }
    
    @IBAction func btnMaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMAle.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        gender = Gender.Male.rawValue
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isValidData(){
            
            if userType == USER_TYPE_INDEPENDENT {
                
                if isConnectedToNetwork(){
                    screenClientWithNOTB()
                }else{
                    insertPatientDetailsTODB()
                }
                
            } else {
                
                if isConnectedToNetwork(){
                    screenPPMVWithNOTB()
                }else{
                    Insert_PPMV_Offline_Data_in_DB()
                }
            }
        }
    }
    func openThanksPopup()  {
        
        if let thankspopup = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController {
            thankspopup.delegate = self
            thankspopup.mainTitle = "Thank You!"
            thankspopup.subTitle = "Screening Completed"
            thankspopup.modalPresentationStyle = .overCurrentContext
            present(thankspopup, animated: true, completion: nil)
        }
    }
    
    func screenClientWithNOTB()  {
        showHUD()
        
        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : TBDict ?? [:],
                                                                 "covidSymptoms": covidDict ?? [:],
                                                                 "clientHasNoTB" : ["patientInformation" : ["age" : age,"sex" : gender]],
                                                                 "latitude" : currentCordinate.latitude,
                                                                 "longitude" : currentCordinate.longitude]]
        print(parametrDict)
        
        screenIndependentUserPatientAPI(paradict: parametrDict) { (success, strData, message) in
            self.hideHUD()
        
            if strData != ""{
                //Defaultss.value(forKey: UDKey.kEncKey) as? String ??
                let key = encKeyForGetAnd
                let strResponse = self.decryptResponse(response: strData,key: key)
                if let json = self.convertStringToDictionary(text: strResponse){
                    print(json)
                }
                if success {
                    self.openThanksPopup()
                }
                else{
                    self.view.makeToast(strResponse)
                }
            }else if message != ""{
                self.view.makeToast(message)
                print(message)
            }
            
        }
    }
    
    func screenPPMVWithNOTB()  {
        showHUD()
        
        let parametrDict : [String : Any] =  ["patientSymptoms" : TBDict ?? [:],
                                              "covidSymptoms": covidDict ?? [:],
                                              "clientHasNoTB" : "",
                                              "age" : age ,
                                              "sex" : gender,
                                              "latitude" : currentCordinate.latitude,
                                              "longitude" : currentCordinate.longitude]

        print(parametrDict)
        
        screeningPPMVUserAPI(paradict: parametrDict) { (success, strData, message) in
            self.hideHUD()
        
            if strData != ""{
                //Defaultss.value(forKey: UDKey.kEncKey) as? String ??
                let key = encKeyForGetAnd
                let strResponse = self.decryptResponse(response: strData,key: key)
                if let json = self.convertStringToDictionary(text: strResponse){
                    print(json)
                }
                if success {
                    self.openThanksPopup()
                }
                else{
                    self.view.makeToast(strResponse)
                }
            }else if message != ""{
                self.view.makeToast(message)
                print(message)
            }
            
        }
    }
    
    func insertPatientDetailsTODB()  {
        
        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : TBDict ?? [:],
                                                                 "covidSymptoms": covidDict ?? [:],
                                                                 "clientHasNoTB" : ["patientInformation" : ["age" : age,"sex" : gender]],
                                                                 "latitude" : currentCordinate.latitude,
                                                                 "longitude" : currentCordinate.longitude]]
        print(parametrDict)
        
        let patientDetail = PatientInformation(dict: parametrDict)
        var strQuerry = ""
        var ans = false
        if let tbsymptoms = patientDetail.patientTBSymptoms{
                
            if let covidSymptoms = patientDetail.patientCovidSymptoms {
                
                /*strQuerry = "insert or replace into PatientDetails(latitude,longitude,age,sex,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea, covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\(patientDetail.age.debugDescription),\(patientDetail.sex.debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription),\(covidSymptoms.covidDiarrhea.debugDescription),\(covidSymptoms.covidSoreThroat.debugDescription),\(covidSymptoms.covidShaking.debugDescription),\(covidSymptoms.covidFever.debugDescription),\(covidSymptoms.covidRunnyNose.debugDescription),\(covidSymptoms.covidCough.debugDescription),\(covidSymptoms.covidLossOfTaste.debugDescription))"*/
                
                strQuerry = "insert or replace into PatientDetails(latitude,longitude,middleName,lga,siteOfDisease,visitDate,dateOfBirth,phoneNumber,age,hivStatus,typeOfPresumptiveTbCase,secondPhoneNumber,patientWorker,sex,state,address,lastName,hadTBbefore,firstName,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea, covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste,createdDate) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\("".debugDescription),\("".debugDescription),\("".debugDescription),\( "".debugDescription),\("".debugDescription),\("".debugDescription),\(age.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(gender.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription),\(covidSymptoms.covidDiarrhea.debugDescription),\(covidSymptoms.covidSoreThroat.debugDescription),\(covidSymptoms.covidShaking.debugDescription),\(covidSymptoms.covidFever.debugDescription),\(covidSymptoms.covidRunnyNose.debugDescription),\(covidSymptoms.covidCough.debugDescription),\(covidSymptoms.covidLossOfTaste.debugDescription),\(dateFormatter.string(from: Date()).debugDescription))"
                
                ans = DatabaseManager().ExecuteQuery(query: strQuerry)
                print(ans)
                
            }else {
                
                /*strQuerry = "insert or replace into PatientDetails(latitude,longitude,age,sex,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\(patientDetail.age.debugDescription),\(patientDetail.sex.debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription))"*/
                
                strQuerry = "insert or replace into PatientDetails(latitude,longitude,middleName,lga,siteOfDisease,visitDate,dateOfBirth,phoneNumber,age,hivStatus,typeOfPresumptiveTbCase,secondPhoneNumber,patientWorker,sex,state,address,lastName,hadTBbefore,firstName,CoughingUpBlood,Swelling,FailureToThrive,FeverFor3weeksOrMore,CoughFor2WeeksOrMore,NightSweats,UnexplainedWeightLoss,covidDiarrhea, covidSoreThroat,covidShaking,covidFever,covidRunnyNose,covidCough,covidLossOfTaste,createdDate) values(\(patientDetail.lattitude.description),\(patientDetail.longitude.description),\("".debugDescription),\("".debugDescription),\("".debugDescription),\( "".debugDescription),\("".debugDescription),\("".debugDescription),\(age.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(gender.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(tbsymptoms.CoughingUpBlood.debugDescription),\(tbsymptoms.Swelling.debugDescription),\(tbsymptoms.FailureToThrive.debugDescription),\(tbsymptoms.FeverFor3weeksOrMore.debugDescription),\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription),\(tbsymptoms.NightSweats.debugDescription),\(tbsymptoms.UnexplainedWeightLoss.debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\("".debugDescription),\(dateFormatter.string(from: Date()).debugDescription))"
                
                ans = DatabaseManager().ExecuteQuery(query: strQuerry)
                print(ans)
            }
            
        }
        if ans {
            openThanksPopup()
        }
    }
    
    
    func Insert_PPMV_Offline_Data_in_DB()  {
        
        let parametrDict : [String : Any] = ["patientDetails" : ["patientSymptoms" : TBDict ?? [:],
                                                                 "covidSymptoms": covidDict ?? [:],
                                                                 "clientHasNoTB" : ["patientInformation" : ["age" : age,"sex" : gender]],
                                                                 "latitude" : currentCordinate.latitude,
                                                                 "longitude" : currentCordinate.longitude]]
        print(parametrDict)
        
        let patientDetail = PatientInformation(dict: parametrDict)
        let dictData : SaveDataInPPMV_Sqlite_Data?
        let databaseDateFormate = DateFormatter()
        databaseDateFormate.dateFormat = apiDateFormatFromAPI

        if let tbsymptoms = patientDetail.patientTBSymptoms{
                
            if let covidSymptoms = patientDetail.patientCovidSymptoms {
                
                dictData = SaveDataInPPMV_Sqlite_Data(latitude: "\("".debugDescription)", longitude: "\("".debugDescription)", middleName: "\("".debugDescription)", lga: "\("".debugDescription)", siteOfDisease: "\("".debugDescription)", visitDate: "\("".debugDescription)", dateOfBirth: "\("".debugDescription)", phoneNumber: "\("".debugDescription)", age: "\(age.debugDescription)", hivStatus: "\("".debugDescription)", typeOfPresumptiveTbCase: "\("".debugDescription)", secondPhoneNumber: "\("".debugDescription)", patientWorker: "\("".debugDescription)", sex: "\(gender.debugDescription)", state: "\("".debugDescription)", address: "\("".debugDescription)", lastName: "\("".debugDescription)", hadTBbefore: "\("".debugDescription)", firstName: "\("".debugDescription)", CoughingUpBlood: "\(tbsymptoms.CoughingUpBlood.debugDescription)", Swelling: "\(tbsymptoms.Swelling.debugDescription)", FailureToThrive: "\(tbsymptoms.FailureToThrive.debugDescription)", FeverFor3weeksOrMore: "\(tbsymptoms.FeverFor3weeksOrMore.debugDescription)", CoughFor2WeeksOrMore: "\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription)", NightSweats: "\(tbsymptoms.NightSweats.debugDescription)", UnexplainedWeightLoss: "\(tbsymptoms.UnexplainedWeightLoss.debugDescription)", covidDiarrhea: "\(covidSymptoms.covidDiarrhea.debugDescription)", covidSoreThroat: "\(covidSymptoms.covidSoreThroat.debugDescription)", covidShaking: "\(covidSymptoms.covidShaking.debugDescription)", covidFever: "\(covidSymptoms.covidFever.debugDescription)", covidRunnyNose: "\(covidSymptoms.covidRunnyNose.debugDescription)", covidCough: "\(covidSymptoms.covidCough.debugDescription)", covidLossOfTaste: "\(covidSymptoms.covidLossOfTaste.debugDescription)", createdDate: "\(databaseDateFormate.string(from: Date()).debugDescription)", clientHasNoTB: "".debugDescription, referToCLinic: 0, typesOfSpecimen: "\("".debugDescription)", dateOfCollection: "\("".debugDescription)", reasonOfExamination: "\("".debugDescription)", previousTreatmentForTb: "\("".debugDescription)", typeOfTest: "\("".debugDescription)", numberOfSample: "\("".debugDescription)", temperature: "\("".debugDescription)", nameOfCollectOffcer: "\("".debugDescription)", nameOfCollectOffcerPhNo: "\("".debugDescription)", isSendingSampleToLabAfterInitialCollection: 0, laboratoryName: "n/a".debugDescription, labId: "\("".debugDescription)", dateOfDispatch: "\("".debugDescription)")

                
            } else {
                
                dictData = SaveDataInPPMV_Sqlite_Data(latitude: "\("".debugDescription)", longitude: "\("".debugDescription)", middleName: "\("".debugDescription)", lga: "\("".debugDescription)", siteOfDisease: "\("".debugDescription)", visitDate: "\("".debugDescription)", dateOfBirth: "\("".debugDescription)", phoneNumber: "\("".debugDescription)", age: "\(age.debugDescription)", hivStatus: "\("".debugDescription)", typeOfPresumptiveTbCase: "\("".debugDescription)", secondPhoneNumber: "\("".debugDescription)", patientWorker: "\("".debugDescription)", sex: "\(gender.debugDescription)", state: "\("".debugDescription)", address: "\("".debugDescription)", lastName: "\("".debugDescription)", hadTBbefore: "\("".debugDescription)", firstName: "\("".debugDescription)", CoughingUpBlood: "\(tbsymptoms.CoughingUpBlood.debugDescription)", Swelling: "\(tbsymptoms.Swelling.debugDescription)", FailureToThrive: "\(tbsymptoms.FailureToThrive.debugDescription)", FeverFor3weeksOrMore: "\(tbsymptoms.FeverFor3weeksOrMore.debugDescription)", CoughFor2WeeksOrMore: "\(tbsymptoms.CoughFor2WeeksOrMore.debugDescription)", NightSweats: "\(tbsymptoms.NightSweats.debugDescription)", UnexplainedWeightLoss: "\(tbsymptoms.UnexplainedWeightLoss.debugDescription)", covidDiarrhea: "\("".debugDescription)", covidSoreThroat: "\("".debugDescription)", covidShaking: "\("".debugDescription)", covidFever: "\("".debugDescription)", covidRunnyNose: "\("".debugDescription)", covidCough: "\("".debugDescription)", covidLossOfTaste: "\("".debugDescription)", createdDate: "\(databaseDateFormate.string(from: Date()).debugDescription)", clientHasNoTB: "".debugDescription, referToCLinic: 0, typesOfSpecimen: "\("".debugDescription)", dateOfCollection: "\("".debugDescription)", reasonOfExamination: "\("".debugDescription)", previousTreatmentForTb: "\("".debugDescription)", typeOfTest: "\("".debugDescription)", numberOfSample: "\("".debugDescription)", temperature: "\("".debugDescription)", nameOfCollectOffcer: "\("".debugDescription)", nameOfCollectOffcerPhNo: "\("".debugDescription)", isSendingSampleToLabAfterInitialCollection: 0, laboratoryName: "n/a".debugDescription, labId: "\("".debugDescription)", dateOfDispatch: "\("".debugDescription)")
            }
            
            let insert = PPMV_DBManager().SaveDataInPPMV_Sqlite(data: dictData!)
            
            if insert {
                openThanksPopup()
            }
        }
    }
}
extension TBNotDetectedViewController : ThanksPopupDelegate{
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if userType == USER_TYPE_INDEPENDENT {
                    if let homevc = vc as? HomeViewController{
                        navigationController?.popToViewController(homevc, animated: true)
                    }
                } else {
                    if let homevc = vc as? UserHomeVC{
                        navigationController?.popToViewController(homevc, animated: true)
                    }
                }
            }
        }
    }
}
