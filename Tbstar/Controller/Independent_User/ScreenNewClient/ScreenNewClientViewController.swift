//
//  ScreenNewClientViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 17/02/21.
//

import UIKit

class ScreenNewClientViewController: BaseViewController {
     
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvQuestions: UITableView!
    @IBOutlet weak var lblScreeningQuestions: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    let arrQuestions = ["cough2week".localized,"fever3week".localized,"nightSweats".localized,"weightLoss".localized,"swelling".localized,"failure".localized,"coughing".localized]
    
    var answerDict = [Int:String]()
    var isPatientHaveTB = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
    }
    func setupLocalization() {
        lblTitle.text = "screenClient".localized
        lblScreeningQuestions.text = "screenQues".localized
        btnSubmit.setTitle("submit".localized, for: .normal)
    }
    func isDetailValid() -> Bool {
       
        if answerDict.keys.count != arrQuestions.count{
            showAlert(titleStr: appName, msg: appMessages.questionMustAns)
            return false
        }
        return true
    }
    @IBAction func btnBackTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isDetailValid(){
           // if arrAnswer.filter({$0[$0.keys.first ?? 1] == "Yes"}).first != nil{
            if answerDict.values.contains("yes") {
                isPatientHaveTB = true
            }else{
                isPatientHaveTB = false
            }
            if let askForCovid19Screen = self.storyboard?.instantiateViewController(withIdentifier: "AskForCovid19PopupViewController") as? AskForCovid19PopupViewController{
                
                askForCovid19Screen.popupTitle = "Screen For COVID19"
                if isPatientHaveTB{
                    askForCovid19Screen.popupSubtitle = "Patient has presumptive TB, would you like to screen patient for COVID19?"
                }else{
                    askForCovid19Screen.popupSubtitle = "Patient does not have presumptive TB, would you like to screen patient for COVID19?"
                }
                askForCovid19Screen.modalPresentationStyle = .overCurrentContext
                askForCovid19Screen.delegate = self
                present(askForCovid19Screen, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnYesTap(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblvQuestions)
        if let indexPath = self.tblvQuestions.indexPathForRow(at: buttonPosition){
            
            if let cell = tblvQuestions.cellForRow(at: indexPath) as? QuestionCell{
                cell.btnYes.isSelected = true
                cell.btnNo.isSelected = false
                answerDict[indexPath.row] = "yes"
                /*if let dict = arrAnswer.filter({$0.keys.contains(indexPath.row)}).first{
                    if let index = arrAnswer.firstIndex(of: dict){
                        arrAnswer[index] = [indexPath.row : "Yes"]
                    }
                }else{
                    arrAnswer.append([indexPath.row : "Yes"])
                }*/
            }
        }
    }
    
    @IBAction func btnNoTap(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.tblvQuestions)
        if let indexPath = self.tblvQuestions.indexPathForRow(at: buttonPosition){
            if let cell = tblvQuestions.cellForRow(at: indexPath) as? QuestionCell{
                cell.btnYes.isSelected = false
                cell.btnNo.isSelected = true
                answerDict[indexPath.row] = "no"
            }
        }
    }
    func makeParametrForAPI() -> [String:Any]  {
        
        return ["CoughFor2WeeksOrMore" : answerDict[0] ?? "" ,
                "FeverFor3weeksOrMore" : answerDict[1] ?? "",
                "NightSweats" : answerDict[2] ?? "",
                "UnexplainedWeightLoss" : answerDict[3] ?? "",
                "Swelling" : answerDict[4] ?? "",
                "FailureToThrive" : answerDict[5] ?? "",
                "CoughingUpBlood" : answerDict[6] ?? ""]
    }
}
extension ScreenNewClientViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell") as? QuestionCell{
            cell.lblQuestion.text = arrQuestions[indexPath.row]
            cell.btnNo.setTitle("no".localized, for: .normal)
            cell.btnYes.setTitle("yes".localized, for: .normal)
            cell.btnNo.setTitle("no".localized, for: .selected)
            cell.btnYes.setTitle("yes".localized, for: .selected)
            return cell
        }
        return UITableViewCell()
    }
}
extension ScreenNewClientViewController : AskForCovid19PopupViewControllerDelegate{
    func selectedOption(isYes: Bool) {
        if isYes{
            if let covid19ScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "Covid19ScreeningViewController") as? Covid19ScreeningViewController{
                covid19ScreenVC.isPatientHaveTB = isPatientHaveTB
                covid19ScreenVC.TBDict = makeParametrForAPI()
                navigationController?.pushViewController(covid19ScreenVC, animated: true)
            }
        }else{
            if isPatientHaveTB{
                if let addNewClientVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewClientDetailViewController") as? AddNewClientDetailViewController{
                    addNewClientVC.TBDict = makeParametrForAPI()
                    navigationController?.pushViewController(addNewClientVC, animated: true)
                }
            }else{
                if let noTBDetectedVC = self.storyboard?.instantiateViewController(withIdentifier: "TBNotDetectedViewController") as? TBNotDetectedViewController{
                    noTBDetectedVC.TBDict = makeParametrForAPI()
                    navigationController?.pushViewController(noTBDetectedVC, animated: true)
                }
            }
            
        }
    }
}
