//
//  QuestionCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 17/02/21.
//

import UIKit

class QuestionCell: UITableViewCell {

    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
