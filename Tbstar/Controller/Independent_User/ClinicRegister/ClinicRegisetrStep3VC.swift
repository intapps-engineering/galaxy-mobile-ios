//
//  ClinicRegisetrStep3.swift
//  Tbstar
//
//  Created by Viprak-Heena on 03/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class ClinicRegisetrStep3VC: BaseViewController {

    
    @IBOutlet weak var lblIntiateTreatment: UILabel!
    @IBOutlet weak var btnTreatmentYes: UIButton!
    
    @IBOutlet weak var btnTreatmentNo: UIButton!
    
    @IBOutlet weak var txtTreatmentDate: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblIsDotProvided: UILabel!
    
    @IBOutlet weak var btnDotNo: UIButton!
    
    @IBOutlet weak var btnDotYes: UIButton!
    @IBOutlet weak var lblLength: UILabel!
    
    @IBOutlet weak var btn6mnth: UIButton!
    @IBOutlet weak var btn12mnth: UIButton!
    
    
    @IBOutlet weak var txtOthercommentIT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var heightCnstOfVwTreatment: NSLayoutConstraint!
    
    @IBOutlet weak var vwTreatmentView: UIView!
    
    @IBOutlet weak var vwSpecifyTreatment: UIView!
    
    @IBOutlet weak var lblSpecifyTreatment: UILabel!
    @IBOutlet weak var btnSTYes: UIButton!
    @IBOutlet weak var btnSTNo: UIButton!
    
    @IBOutlet weak var vwTreatmentOutcome: UIView!
    @IBOutlet weak var lblSTOutcomeTitle: UILabel!
    @IBOutlet weak var lblSTOutcome: UILabel!
    
    
    @IBOutlet weak var lblFixdrugOrLoose: UILabel!
    @IBOutlet weak var btnLooseDrug: UIButton!
    @IBOutlet weak var btnFixedDrug: UIButton!
    @IBOutlet weak var vwFixDrug: UIView!
    @IBOutlet weak var heightCnstOFvwFixDrug: NSLayoutConstraint!
    
    @IBOutlet weak var lblIsFixDrug: UILabel!
    
    @IBOutlet weak var lblFixDrugSelected: UILabel!
    
    @IBOutlet weak var vwLooseDrug: UIView!

    @IBOutlet weak var heightCnstOfvwLooseDrug: NSLayoutConstraint!
    
    @IBOutlet weak var lblIsLooseDrug: UILabel!
    @IBOutlet weak var lblWhichLooseDrug: UILabel!
    @IBOutlet weak var btnLooseDrug1: UIButton!
    @IBOutlet weak var btnLooseDrug2: UIButton!
    
    @IBOutlet weak var btnLooseDrug3: UIButton!
    @IBOutlet weak var btnLooseDrug4: UIButton!
    @IBOutlet weak var btnOtherLooseDrug: UIButton!
    
    @IBOutlet weak var txtOtherLooseDrug: SkyFloatingLabelTextField!
    
    @IBOutlet weak var heightCnstOftxtLD: NSLayoutConstraint!
    
    @IBOutlet weak var txtOtherResultST: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var lblIsReferToOtherFacility: UILabel!
    
    @IBOutlet weak var btnReferYes: UIButton!
    
    @IBOutlet weak var btnReferNo: UIButton!
    @IBOutlet weak var vwOtherFacility: UIView!
    @IBOutlet weak var txtSelectState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectFacility: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityAddress: SkyFloatingLabelTextField!
    
    @IBOutlet weak var vwLGA: UIView!
    @IBOutlet weak var vwFacility: UIView!
    @IBOutlet weak var vwFacilityDetail: UIView!
    
    @IBOutlet weak var heightCnstOfLGA: NSLayoutConstraint!
    
    @IBOutlet weak var heightcnstOfFacility: NSLayoutConstraint!
    
    @IBOutlet weak var heightCnstOFfacilityDetail: NSLayoutConstraint!
    
    @IBOutlet weak var heightCnstOfVeST: NSLayoutConstraint!
    
    @IBOutlet weak var heightCnstOfVwOtherFac: NSLayoutConstraint!
    
    @IBOutlet weak var topCnstOftxtOtherTovwFixdrug: NSLayoutConstraint!
    
    @IBOutlet weak var topCnstOftxtOtherTovwLoosedrug: NSLayoutConstraint!
    
    @IBOutlet weak var topCnstoftxtOtherTovwDrugType: NSLayoutConstraint!
    
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var btnPrevious: UIButton!
    
    
    var isInitiateTreatment = ""
    var isDOTProvided = ""
    var lengthOFRegimen = ""
    var isSpecifyTreatment = ""
    var drugType = ""
    
    var treatmentOutcomeName = ""
    var treatmentOutcomeId = ""
    
    var isReferToOtherClinic = ""
    
    var selectedFixDrug = ""
    var arrSelectedLooseDrugs = [String]()
    
    var dateFormatter = DateFormatter()
    var treatmentInitiateDate = Date()
    var treatmentEndDate = Date()
    let dropDown = DropDown()
    var designationDetail : DesignationDetail?
    var lgaList = [LGA]()
    var treatmentList = [TreatmentOutcomes]()
    var dotCenterList = [DotsCenter]()
    var selectedStateId = ""
    var selectedLGACode = ""
    
    var isEditMode = false
    var isDetailMode = false
    var isFromScreening = false
    
    var patientDetail : PatientInformation?
    
    @IBOutlet weak var vwMainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        dateFormatter.dateFormat = displayDateFormat
        txtTreatmentDate.text = dateFormatter.string(from: Date())
        getDetailForIndependentUser()
        getTreatmentOutcomes()
        setupTextField()
        setViews()
    }
    func setViews() {
        
        vwTreatmentView.isHidden = true
        heightCnstOfVwTreatment.constant = 0
        vwSpecifyTreatment.isHidden = true
        heightCnstOfVeST.constant = 0
        
        vwOtherFacility.isHidden = true
        heightCnstOfVwOtherFac.constant = 0
    }
    
    func setupLocalization() {

        lblIntiateTreatment.text = "initiateTreatment".localized
        
        btnTreatmentYes.setTitle("yes".localized, for: .normal)
        btnTreatmentYes.setTitle("yes".localized, for: .selected)
        
        btnTreatmentNo.setTitle("no".localized, for: .normal)
        btnTreatmentNo.setTitle("no".localized, for: .selected)
        
        txtTreatmentDate.placeholder = "treatmentDate".localized
        txtTreatmentDate.selectedTitle = "treatmentDate".localized
        txtTreatmentDate.title = "treatmentDate".localized
        
        lblIsDotProvided.text = "isDOT".localized
        
        btnDotYes.setTitle("yes".localized, for: .normal)
        btnDotYes.setTitle("yes".localized, for: .selected)
        
        btnDotNo.setTitle("no".localized, for: .normal)
        btnDotNo.setTitle("no".localized, for: .selected)
        
        lblLength.text = "lengthRegimen".localized
        
        txtOthercommentIT.placeholder = "otherTDetails".localized
        txtOthercommentIT.selectedTitle = "otherTDetails".localized
        txtOthercommentIT.title = "otherTDetails".localized
        
        lblSpecifyTreatment.text = "specifyTreatment".localized
        lblSTOutcomeTitle.text = "treatmentOutcome".localized
        lblSTOutcome.text = "selectTreatment".localized
        
        btnSTYes.setTitle("yes".localized, for: .normal)
        btnSTYes.setTitle("yes".localized, for: .selected)
        
        btnSTNo.setTitle("no".localized, for: .normal)
        btnSTNo.setTitle("no".localized, for: .selected)
        
        lblFixdrugOrLoose.text = "fixorlooseDrugcombination".localized
        
        btnFixedDrug.setTitle("fixdrug".localized, for: .normal)
        btnFixedDrug.setTitle("fixdrug".localized, for: .selected)
        
        btnLooseDrug.setTitle("looseDrug".localized, for: .normal)
        btnLooseDrug.setTitle("looseDrug".localized, for: .selected)
        
        lblIsFixDrug.text = "isFixedDrug".localized
        lblFixDrugSelected.text = "fixDrugCombination".localized
        
        lblIsLooseDrug.text = "ifLooseDrug".localized
        lblWhichLooseDrug.text = "whichDrug".localized
        
        txtOtherResultST.placeholder = "otherTDetails".localized
        txtOtherResultST.selectedTitle = "otherTDetails".localized
        txtOtherResultST.title = "otherTDetails".localized
        
        txtOtherLooseDrug.placeholder = "otherLD".localized
        txtOtherLooseDrug.selectedTitle = "otherLD".localized
        txtOtherLooseDrug.title = "otherLD".localized
        
        lblIsReferToOtherFacility.text = "referredToOtherFaci".localized
        
        btnReferNo.setTitle("no".localized, for: .normal)
        btnReferNo.setTitle("no".localized, for: .selected)
        
        btnReferYes.setTitle("yes".localized, for: .normal)
        btnReferYes.setTitle("yes".localized, for: .selected)
        
        txtSelectState.placeholder = "selectState".localized
        txtSelectState.selectedTitle = "selectState".localized
        txtSelectState.title = "selectState".localized
        
        txtLGA.placeholder = "selectLGA".localized
        txtLGA.selectedTitle = "selectLGA".localized
        txtLGA.title = "selectLGA".localized
        
        txtSelectFacility.placeholder = "selectFacility".localized
        txtSelectFacility.selectedTitle = "selectFacility".localized
        txtSelectFacility.title = "selectFacility".localized
        
        txtFacilityName.placeholder = "otherFacName".localized
        txtFacilityName.selectedTitle = "otherFacName".localized
        txtFacilityName.title = "otherFacName".localized
    
        txtFacilityAddress.placeholder = "otherFacAdd".localized
        txtFacilityAddress.selectedTitle = "otherFacAdd".localized
        txtFacilityAddress.title = "otherFacAdd".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
        btnExit.setTitle("exit".localized, for: .normal)
        btnPrevious.setTitle("previous".localized, for: .normal)
    }
    func setupTextField()  {
        
        txtTreatmentDate.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtTreatmentDate.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtTreatmentDate.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtTreatmentDate.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtOthercommentIT.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtOthercommentIT.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtOthercommentIT.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtOthercommentIT.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtOtherResultST.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultST.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultST.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultST.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtOtherLooseDrug.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherLooseDrug.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherLooseDrug.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherLooseDrug.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSelectState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSelectFacility.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectFacility.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectFacility.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSelectFacility.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtFacilityName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtFacilityAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
       
    }
    func setupEditModeView() {
        
        isInitiateTreatment = patientDetail?.initiateTreatmentDetail?.didYouInitiateTreatment ?? ""
        
        if isInitiateTreatment.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame {
            btnIntiateTretmentTap(btnTreatmentYes)
            (txtTreatmentDate.text,treatmentInitiateDate) = Utility.convertDateformat(date: patientDetail?.initiateTreatmentDetail?.dateTreatmentInitiated ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            isDOTProvided = patientDetail?.initiateTreatmentDetail?.isDotProvidedByTreatment ?? ""
            if isDOTProvided == YesNO.Yes.rawValue {
                btnIsDotTap(btnDotYes)
            }else{
                btnIsDotTap(btnDotNo)
            }
            lengthOFRegimen = patientDetail?.initiateTreatmentDetail?.lengthOfProposedRegimen ?? ""
            if lengthOFRegimen == "6"{
                btnLengthTap(btn6mnth)
            }else{
                btnLengthTap(btn12mnth)
            }
            (_,treatmentEndDate) = Utility.convertDateformat(date: patientDetail?.initiateTreatmentDetail?.dateOfTreatmentEnd ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
        }else{
            btnIntiateTretmentTap(btnTreatmentNo)
        }
        
        isSpecifyTreatment = patientDetail?.specifyTreatmentDetail?.specifyTreatment ?? ""
        if let treatmentOutcomeD = patientDetail?.treatmentOutcomeDetail {
            treatmentOutcomeId = treatmentOutcomeD.treatmentOutcomeId
            treatmentOutcomeName = treatmentOutcomeD.treatmentOutcomeLabel
            lblSTOutcome.text = treatmentOutcomeName
        }
        if isSpecifyTreatment.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame  {
            btnSpecifyTreatmentTap(btnSTYes)
            drugType = patientDetail?.specifyTreatmentDetail?.drugType ?? ""
            if drugType == DrugType.FixedDrug.rawValue {
                btnDrugTypeTap(btnFixedDrug)
                lblFixDrugSelected.text = patientDetail?.specifyTreatmentDetail?.fixedDrugCombination ?? ""
                selectedFixDrug = patientDetail?.specifyTreatmentDetail?.fixedDrugCombination ?? ""
            }else{
                btnDrugTypeTap(btnLooseDrug)
                arrSelectedLooseDrugs = (patientDetail?.specifyTreatmentDetail?.specifyCombination ?? "").components(separatedBy: "+")
                if arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare(LooseDrugCombination.Rifampicin.rawValue) == .orderedSame }){
                    btnLooseDrug1.isSelected = true
                }
                if arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare(LooseDrugCombination.Isoniazid.rawValue) == .orderedSame }){
                    btnLooseDrug2.isSelected = true
                }
                if arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare(LooseDrugCombination.Ethambutol.rawValue) == .orderedSame }){
                    btnLooseDrug3.isSelected = true
                }
                if arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare(LooseDrugCombination.Pyrazinamide.rawValue) == .orderedSame }){
                    btnLooseDrug4.isSelected = true
                }
                if (arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare(LooseDrugCombination.Others.rawValue) == .orderedSame })) || (arrSelectedLooseDrugs.contains(where: { $0.caseInsensitiveCompare("Others (Specify)") == .orderedSame })) {
                    btnOtherLooseDrug.isSelected = true
                    txtOtherLooseDrug.text = arrSelectedLooseDrugs.last ?? ""
                }
            }
            txtOtherResultST.text = patientDetail?.specifyTreatmentDetail?.otherTreatmentDetails
        }else{
            btnSpecifyTreatmentTap(btnSTNo)
        }
        
        isReferToOtherClinic = patientDetail?.refferedToOtherFacilityDetail?.patientReferredToOtherFacilty ?? ""
        
        if isReferToOtherClinic.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame  {
            btnIsReferTap(btnReferYes)
            selectedStateId = patientDetail?.refferedToOtherFacilityDetail?.stateId ?? ""
            txtSelectState.text = (designationDetail!.addressList.filter({ $0.stateId == selectedStateId }).first?.stateName ?? "")
            setUpLGAView()
            
            selectedLGACode = patientDetail?.refferedToOtherFacilityDetail?.lgaId ?? ""
            txtLGA.text = lgaList.filter( {$0.code == Int(selectedLGACode)}).first?.name ?? ""
            setUpFacilityView()
            
            txtSelectFacility.text = patientDetail?.refferedToOtherFacilityDetail?.otherFacilityName ?? ""
            txtFacilityName.text = patientDetail?.refferedToOtherFacilityDetail?.otherFacilityName ?? ""
            if patientDetail?.refferedToOtherFacilityDetail?.otherFaciltyAddres == "" {
                txtFacilityAddress.text = "\(txtSelectState.text ?? "") \(txtLGA.text ?? "")"
            }else{
                txtFacilityAddress.text = patientDetail?.refferedToOtherFacilityDetail?.otherFaciltyAddres
            }
            setupFacilityDetailView()
            
        }else{
            btnIsReferTap(btnReferNo)
        }
        disableAllViews(shouldDisable: isDetailMode)
    }
    func disableAllViews(shouldDisable : Bool) {
        
        for i in 3..<6{
            let view = vwMainView.viewWithTag(i)
            view?.isUserInteractionEnabled = !shouldDisable
        }
        btnSubmit.isHidden = shouldDisable
        btnExit.isHidden = !shouldDisable
    }
    
    @IBAction func btnIntiateTretmentTap(_ sender: UIButton) {
        
        btnTreatmentYes.isSelected = false
        btnTreatmentNo.isSelected = false
        
        isInitiateTreatment = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
        
        sender.isSelected = true
        vwTreatmentView.isHidden = isInitiateTreatment == YesNO.Yes.rawValue ? false : true
        heightCnstOfVwTreatment.constant = isInitiateTreatment == YesNO.Yes.rawValue ? 310 : 0
    }
    
    @IBAction func btnIsDotTap(_ sender: UIButton) {
        btnDotYes.isSelected = false
        btnDotNo.isSelected = false
        isDOTProvided = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
        sender.isSelected = true
    }
    
    @IBAction func btnLengthTap(_ sender: UIButton) {
        
        btn6mnth.isSelected = false
        btn12mnth.isSelected = false
        lengthOFRegimen = sender.tag == 1 ? "6" : "12"
        sender.isSelected = true
    }
    @IBAction func btnSpecifyTreatmentTap(_ sender: UIButton) {
        
        btnSTNo.isSelected = false
        btnSTYes.isSelected = false
        
        isSpecifyTreatment = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
        sender.isSelected = true
        
        heightCnstOfvwLooseDrug.constant = 0
        vwLooseDrug.isHidden = true
        
        heightCnstOFvwFixDrug.constant = 0
        vwFixDrug.isHidden = true
        
        heightCnstOfVeST.constant = isSpecifyTreatment == YesNO.Yes.rawValue ? 200 : 0
        vwSpecifyTreatment.isHidden = isSpecifyTreatment == YesNO.Yes.rawValue ? false : true
        
        topCnstoftxtOtherTovwDrugType.isActive = true
        topCnstOftxtOtherTovwFixdrug.isActive = false
        topCnstOftxtOtherTovwLoosedrug.isActive = false
    }
    @IBAction func btnSTOutcomeOptionTap(_ sender: UIButton) {
        
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = treatmentList.map{($0.name)}
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblSTOutcome.text = item
            treatmentOutcomeName = item
            treatmentOutcomeId = treatmentList[index]._id
            dropDown.hide()
        }
    }
    
    @IBAction func btnDrugTypeTap(_ sender: UIButton) {
        
        btnFixedDrug.isSelected = false
        btnLooseDrug.isSelected = false
        sender.isSelected = true
        drugType = sender.tag == 1 ? DrugType.FixedDrug.rawValue : DrugType.LooseDrug.rawValue
        
        heightCnstOFvwFixDrug.constant = drugType == DrugType.FixedDrug.rawValue ? 80 : 0
        vwFixDrug.isHidden = drugType == DrugType.FixedDrug.rawValue ? false : true
        
        heightCnstOfvwLooseDrug.constant = drugType == DrugType.LooseDrug.rawValue ? 280 : 0
        vwLooseDrug.isHidden = drugType == DrugType.LooseDrug.rawValue ? false : true
        
        //heightCnstOfVeST.constant = 
        topCnstOftxtOtherTovwFixdrug.isActive = drugType == DrugType.FixedDrug.rawValue ? true : false
        topCnstOftxtOtherTovwLoosedrug.isActive = drugType == DrugType.LooseDrug.rawValue ? true : false
        topCnstoftxtOtherTovwDrugType.isActive = false
        
        heightCnstOftxtLD.constant = 0
        heightCnstOfVeST.constant = heightCnstOFvwFixDrug.constant + heightCnstOfvwLooseDrug.constant + 200
    }
    
    @IBAction func btnFixDrugTap(_ sender: UIButton) {
        //Open Dropdown
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["Rifampicin + Isoniazid + Pyrazinamide + Ethambutol (RHZE)","Rifampicin + Isoniazid + Pyrazinamide (RHZ)","Rifampicin + Isoniazid (RH)"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblFixDrugSelected.text = item
            selectedFixDrug = FixDrugCombination.allCases[index].rawValue
            dropDown.hide()
        }
    }
    
    @IBAction func btnLooseDrugTypeTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            arrSelectedLooseDrugs.append(sender.titleLabel?.text ?? "")
        }else{
            arrSelectedLooseDrugs.removeAll( where: {$0 == sender.titleLabel?.text ?? ""})
        }
    }
    
    @IBAction func btnOtherLDTap(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected{
            arrSelectedLooseDrugs.append(sender.titleLabel?.text ?? "")
        }else{
            arrSelectedLooseDrugs.removeAll( where: {$0 == sender.titleLabel?.text ?? ""})
        }
        
        txtOtherLooseDrug.isHidden = sender.isSelected ? false : true
        heightCnstOftxtLD.constant = sender.isSelected ? 40 : 0
        
        heightCnstOfvwLooseDrug.constant = 280 + heightCnstOftxtLD.constant
        heightCnstOfVeST.constant = heightCnstOFvwFixDrug.constant + heightCnstOfvwLooseDrug.constant + 200
    }
    
    @IBAction func btnIsReferTap(_ sender: UIButton) {
        
        btnReferNo.isSelected = false
        btnReferYes.isSelected = false
        sender.isSelected = true
        isReferToOtherClinic = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
        
        vwOtherFacility.isHidden = isReferToOtherClinic == YesNO.Yes.rawValue ? false : true
        heightCnstOfLGA.constant = 0
        vwLGA.isHidden = true
        
        heightcnstOfFacility.constant = 0
        vwFacility.isHidden = true
        
        heightCnstOFfacilityDetail.constant = 0
        vwFacilityDetail.isHidden = true
        
        heightCnstOfVwOtherFac.constant = isReferToOtherClinic == YesNO.Yes.rawValue ? 50 : 0
        //heightCnstOFfacilityDetail.
    }
    
    @IBAction func btnPreviousTap(_ sender: Any) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "PreviousTap"), object: nil, userInfo: nil))
    }
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        if isValidDetail(){
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "PopToRoot"), object: nil, userInfo: makePatientInfoDictionary()))
        }
    }
    
    @IBAction func btnExitTap(_ sender: Any) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "PopToFindCase"), object: nil, userInfo: nil))
    }
    func makePatientInfoDictionary() -> [String :Any] {
        
        return [
                "initiateTreatment" : makeInitiateTreatment(),
                "specifyTreatment" : makeSpecifyTreatment(),
                "patientReferredToOtherFacilty" : makeReferredToOtherClinic(),
                "treatmentOutcome" : makeTreatmentOutcome()]
    }
    func makeInitiateTreatment() -> [String : Any] {
        
        dateFormatter.dateFormat = APIDateFormate
        if isInitiateTreatment == YesNO.Yes.rawValue{
            
            if lengthOFRegimen == "6"{
                treatmentEndDate = Calendar.current.date(byAdding: .month, value: 1, to: treatmentInitiateDate) ?? Date()
            }else{
                treatmentEndDate = Calendar.current.date(byAdding: .month, value: 12, to: treatmentInitiateDate) ?? Date()
            }
            
            return ["didYouInitiateTreatment" : isInitiateTreatment,
                    "dateTreatmentInitiated" : dateFormatter.string(from: treatmentInitiateDate),
                    "isDotProvidedByTreatment" : isDOTProvided,
                    "lengthOfProposedRegimen" : lengthOFRegimen,
                    "dateOfTreatmentEnd" : dateFormatter.string(from: treatmentEndDate),
                    "otherTreatmentDetails" : txtOthercommentIT.text ?? ""]
        }else{
            return ["didYouInitiateTreatment" : isInitiateTreatment]
        }
    }
    func makeSpecifyTreatment() -> [String : Any] {
        
        if isSpecifyTreatment == YesNO.Yes.rawValue{
            var selectedLooseDrug = arrSelectedLooseDrugs.joined(separator: "+")
            if let looseDrug = txtOtherLooseDrug.text, !looseDrug.isEmpty{
                selectedLooseDrug =  selectedLooseDrug + "+ \(looseDrug)"
            }
            return ["specifyTreatment" : isSpecifyTreatment,
                    "drugType" : drugType,
                    "fixedDrugCombination" : selectedFixDrug,
                    "specifyCombination" : selectedLooseDrug,
                    "otherTreatmentDetails" : txtOtherResultST.text ?? ""]
        }else{
            return ["specifyTreatment" : isSpecifyTreatment]
        }
        
    }
    func makeTreatmentOutcome() -> [String : Any] {
        return [ "treatmentOutcomeId" : treatmentOutcomeId,
                 "treatmentOutcomeLabel" : treatmentOutcomeName]
    }
    func makeReferredToOtherClinic() -> [String : Any] {
        
        if isReferToOtherClinic == YesNO.Yes.rawValue {
            return ["patientReferredToOtherFacilty" : isReferToOtherClinic,
                    "otherFacilityName" : txtFacilityName.text ?? "",
                    "otherFaciltyAddres" : txtFacilityAddress.text ?? "",
                    "stateId" : selectedStateId,
                    "lgaId" : selectedLGACode]
        }else{
            return ["patientReferredToOtherFacilty" : isReferToOtherClinic]
        }
    }
    func isValidDetail() -> Bool {
        
        if isInitiateTreatment.isEmpty || isSpecifyTreatment.isEmpty || isReferToOtherClinic.isEmpty  {
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        
        if isInitiateTreatment == YesNO.Yes.rawValue{
            if isDOTProvided.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectSupportSystem)
                return false
            }
            if lengthOFRegimen.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectLength)
                return false
            }
        }
        if isSpecifyTreatment == YesNO.Yes.rawValue{
            
            if treatmentOutcomeName.isEmpty {
                showAlert(titleStr: appName, msg: appMessages.selectTreatmentOutcome)
                return false
            }
            if drugType.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectDrugtype)
                return false
            }
            if drugType == DrugType.FixedDrug.rawValue{
                if selectedFixDrug.isEmpty{
                    showAlert(titleStr: appName, msg: appMessages.enterFixdrug)
                    return false
                }
            }
            if drugType == DrugType.LooseDrug.rawValue{
                if arrSelectedLooseDrugs.count == 0{
                    showAlert(titleStr: appName, msg: appMessages.enterLoosedrug)
                    return false
                }
            }
        }
        if isReferToOtherClinic == YesNO.Yes.rawValue{
            
            if selectedStateId.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectFacilityy)
                return false
            }
            if selectedLGACode == ""{
                showAlert(titleStr: appName, msg: appMessages.selectFacilityy)
                return false
            }
            if let facility = txtSelectFacility.text, facility.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectFacilityy)
                return false
            }
        }
        return true
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtSelectState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }
        dropDown.direction = .top
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectState.text = item
            selectedStateId = designationDetail?.addressList[index].stateId ?? ""
            setUpLGAView()
            dropDown.hide()
            
        }
    }
    func setUpLGAView()  {
        txtLGA.text = ""
        txtSelectFacility.text = ""
        txtFacilityAddress.text = ""
        txtFacilityName.text = ""
        heightCnstOfLGA.constant = 80
        heightCnstOfVwOtherFac.constant = txtSelectState.frame.height + heightCnstOfLGA.constant
        vwLGA.isHidden = false
        vwFacility.isHidden = true
        vwFacilityDetail.isHidden = true
        self.getAdminLGAs()
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        dropDown.dataSource = lgaList.map{($0.name)}
        
        dropDown.direction = .top
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            selectedLGACode = "\(lgaList[index].code)"
            setUpFacilityView()
            dropDown.hide()
        }
    }
    func setUpFacilityView()  {
        txtSelectFacility.text = ""
        txtFacilityAddress.text = ""
        txtFacilityName.text = ""
        heightcnstOfFacility.constant = 60
        heightCnstOfVwOtherFac.constant = txtSelectState.frame.height + heightCnstOfLGA.constant + heightcnstOfFacility.constant
        vwFacility.isHidden = false
        vwFacilityDetail.isHidden = true
        self.getDotCenterByLGA()
    }
    func setupFacilityDropdown()  {
        
        dropDown.anchorView = txtSelectFacility
        dropDown.dataSource = dotCenterList.map{($0.name)}
        
        dropDown.direction = .top
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtSelectFacility.text = item
            txtFacilityAddress.text = dotCenterList[index].address == "" ? "\(dotCenterList[index].lga) \(dotCenterList[index].state)" : dotCenterList[index].address
            txtFacilityName.text = item
            setupFacilityDetailView()
            dropDown.hide()
        }
    }
    func setupFacilityDetailView()  {
        heightCnstOFfacilityDetail.constant = 100
        heightCnstOfVwOtherFac.constant = txtSelectState.frame.height + heightCnstOfLGA.constant + heightcnstOfFacility.constant + heightCnstOFfacilityDetail.constant
        vwFacilityDetail.isHidden = false
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                                if self.isEditMode || self.isDetailMode{
                                    self.setupEditModeView()
                                }
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
    }
    func getTreatmentOutcomes()  {
        
        if isConnectedToNetwork(){
            showHUD()
            getTreatmentOutcomesAPI { (success, strData) in
                if success {
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        if let outcomes = json["treatmentOutcomes"] as? [[String:Any]]{
                            self.treatmentList = outcomes.map(TreatmentOutcomes.init)
                        }
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                }else{
                    self.hideHUD()
                }
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    func getAdminLGAs() {
        if isConnectedToNetwork(){
            showHUD()
            fetchLGAListAPI(paradict: ["stateId" : selectedStateId]) { (success, strdata, message) in
                if success {
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        if let lgas = json["lgas"] as? [[String:Any]]{
                            self.lgaList = lgas.map(LGA.init)
                            if self.isEditMode || self.isDetailMode{
                                self.txtLGA.text = self.lgaList.filter( {$0.code == Int(self.selectedLGACode)}).first?.name ?? ""
                            }
                        }
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                }else{
                    self.hideHUD()
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    func getDotCenterByLGA()  {
        if isConnectedToNetwork(){
            showHUD()
            fetchDotCenterListAPI(paradict: ["state" : selectedStateId,
                                             "lga" : selectedLGACode]) { (success, strdata, message) in
                if success {
                    let strResponse = self.decryptResponse(response: strdata,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        if let dotsList = json["dotsCenters"] as? [[String:Any]]{
                            self.dotCenterList = dotsList.map(DotsCenter.init)
                        }
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                    
                }else{
                    self.hideHUD()
                    if message != ""{
                        self.view.makeToast(message)
                    }
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
}
extension ClinicRegisetrStep3VC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtSelectState{
            setupStateDropdown()
            return false
        }
        if textField == txtLGA{
            setupLGADropdown()
            return false
        }
        if textField == txtSelectFacility{
            setupFacilityDropdown()
            return false
        }
        return true
    }
}
extension ClinicRegisetrStep3VC : ThanksPopupDelegate{
    func submitTap() {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "PopToRoot"), object: nil, userInfo: nil))
    }
}
