//
//  ClinicRegisterContainerVC.swift
//  Tbstar
//
//  Created by Viprak-Heena on 03/03/21.
//

import UIKit

class ClinicRegisterContainerVC: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var containerVW: UIView!
    
    @IBOutlet weak var stepIndicator: StepIndicatorView!
    
    var pages: [UIViewController]?
    private var pageViewController: UIPageViewController!
    var currentPage = CurrentPage.resgisterStep1
    var patientInfoDict = [String : Any]()
    
    var isDetailMode = false
    var isEditMode = false
    var patientDetail : PatientInformation?
    var askPopupOnBack = false
    var isFromScreening = false
    
    var offlinePatientId = 0
    var isFromOfflineData = false
    
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    var ISFROM : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = displayDateFormat
        setupPages()
        setupControllers()
        btnEdit.isHidden = !isDetailMode
        btnShare.isHidden = !isDetailMode
        currentPage = CurrentPage.resgisterStep1
        lblTitle.text = "clinicRegister".localized
        if pages != nil{
            
            let controller = pages![0]
            pageViewController.setViewControllers([controller], direction: .forward, animated: false, completion: nil)
            stepIndicator.currentStep = 0
        }
        NotificationCenter.default.addObserver(self, selector: #selector(moveToNextPage), name:NSNotification.Name(rawValue: "NextTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(moveToPreviousPage), name:NSNotification.Name(rawValue: "PreviousTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(poptoRootView), name:NSNotification.Name(rawValue: "PopToRoot"), object: nil)
        //PopToFindCase
        NotificationCenter.default.addObserver(self, selector: #selector(poptoFindCaseScreen), name:NSNotification.Name(rawValue: "PopToFindCase"), object: nil)
        
    }

    @IBAction func btnBackTap(_ sender: Any) {
        
        if askPopupOnBack{
            if let askForCovid19Screen = self.storyboard?.instantiateViewController(withIdentifier: "AskForCovid19PopupViewController") as? AskForCovid19PopupViewController{
                
                askForCovid19Screen.popupTitle = "If you leave this page all the current entered collectSampleDataModel(if any) will be lost. Although you can add them again from the Edit Clinic Register Screen"
                askForCovid19Screen.popupSubtitle = "Do you wish to continue?"
                askForCovid19Screen.modalPresentationStyle = .overCurrentContext
                askForCovid19Screen.delegate = self
                present(askForCovid19Screen, animated: true, completion: nil)
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditTap(_ sender: Any) {
        
        isDetailMode = false
        isEditMode = true
        pageViewController.view.removeFromSuperview()
        setupPages()
        setupControllers()
        btnEdit.isHidden = !isDetailMode
        currentPage = CurrentPage.resgisterStep1
        if pages != nil{
            
            let controller = pages![0]
            pageViewController.setViewControllers([controller], direction: .forward, animated: false, completion: nil)
            stepIndicator.currentStep = 0
        }
    }
    
    @IBAction func btnSharePressed(_ sender : UIButton) {
        
        btnShare.isHidden = !isDetailMode
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "SharePatientDetailsVC") as? SharePatientDetailsVC {
            VC.patientDetail = self.patientDetail
            VC.isfrom = ISFROM
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    func setupPages()
    {
        
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterStep1VC") as?  ClinicRegisterStep1VC)
        vc?.isEditMode = isEditMode
        vc?.isDetailMode = isDetailMode
        vc?.patientDetail = patientDetail
        vc?.isFromScreening = isFromScreening
        vc?.isfrom = ISFROM

        let vc1 = (self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisterStep2VC") as?  ClinicRegisterStep2VC)
        vc1?.isEditMode = isEditMode
        vc1?.isDetailMode = isDetailMode
        vc1?.patientDetail = patientDetail
        vc1?.isFromScreening = isFromScreening
        
        let vc2 = (self.storyboard?.instantiateViewController(withIdentifier: "ClinicRegisetrStep3VC") as?  ClinicRegisetrStep3VC)
        vc2?.isEditMode = isEditMode
        vc2?.isDetailMode = isDetailMode
        vc2?.patientDetail = patientDetail
        vc2?.isFromScreening = isFromScreening
        
        pages = ([
            vc,
            vc1,
            vc2
            ] as? [UIViewController])
    }
    func setupControllers() {
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.view.frame = containerVW.bounds
        pageViewController.view.backgroundColor = UIColor.clear
        containerVW.addSubview(pageViewController.view)
    }
    func reportTBCase()  {
        
        
        
        if isConnectedToNetwork(){
            showHUD()
            
            let paradict:[String : Any] = ["patientId" : isEditMode == true ? patientDetail?._id ?? "" : "",
                                           "all" : "1",
                                           "patientDetails" : ["clientHasTB":["patientRecordNumber" : isEditMode == true ?  patientDetail?.patientRecordNumber ?? "" : "",
                                                                              "patientInformation" : patientInfoDict]]]
            
            print("Api paradict : \(paradict)")
            
            
            reportTBAfterCollectDetailsAPI(paradict: paradict) { (success, strData, message) in
                
                if success {
                        let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                        if let json = self.convertStringToDictionary(text: strResponse){
                            print(json)
                            self.hideHUD()
                            if self.isEditMode == true{
                                self.openThanksPopup(recordNumber: "", id: json["_id"] as? String ?? "")
                            }else{
                                self.openThanksPopup(recordNumber: json["patientRecordNumber"] as? String ?? "", id: json["_id"] as? String ?? "")
                            }
                        }else{
                            self.hideHUD()
                        }
                    
                }else{
                    self.hideHUD()
                    if message != "" {
                        self.view.makeToast(message)
                    }
                }
            }
            
        }else{
            let paradict:[String : Any] = ["patientId" : isEditMode == true ? patientDetail?._id ?? "" : "",
                                           "all" : "1",
                                           "patientDetails" : ["clientHasTB":["patientRecordNumber" : isEditMode == true ?  patientDetail?.patientRecordNumber ?? "" : "",
                                                                              "patientInformation" : patientInfoDict]],
                                           "createDate" :dateFormatter.string(from: Date())]
            saveDataToDB(dict: paradict)
           // view.makeToast(appMessages.noConnection)
        }
    }
    func saveDataToDB(dict : [String:Any])  {
        
        if var arrDetails = Defaultss.value(forKey: UDKey.kTbCollecionDetail) as? [[String : Any]] {
            arrDetails.append(dict)
            Defaultss.setValue(arrDetails, forKey: UDKey.kTbCollecionDetail)
            Defaultss.synchronize()
        }else{
            let arrData = [dict]
            Defaultss.setValue(arrData, forKey: UDKey.kTbCollecionDetail)
            Defaultss.synchronize()
        }
        self.openThanksPopup(recordNumber:"", id:"")
    }
    func openThanksPopup(recordNumber : String, id : String)  {
        if let thankspopup = self.storyboard?.instantiateViewController(withIdentifier: "ThankYouPopupViewController") as? ThankYouPopupViewController{
            thankspopup.delegate = self
            thankspopup.mainTitle = "Thank You!"
            thankspopup.subTitle = "Screening Completed"
            thankspopup.caseDetail = "\("yourCase".localized)\n\(recordNumber)\n \("usethisID".localized)"
            thankspopup.modalPresentationStyle = .overCurrentContext
            present(thankspopup, animated: true, completion: nil)
        }
    }
    @objc func poptoRootView(_ notification: NSNotification){
        
        patientInfoDict.merge((notification.userInfo ?? [:]) as? [String :Any] ?? [:]) { (_, _) in}
        reportTBCase()
      
    }
    @objc func poptoFindCaseScreen(_ notification: NSNotification){
        navigationController?.popViewController(animated: true)
    }
    @objc func moveToNextPage(_ notification: NSNotification)
    {
        if(currentPage == CurrentPage.resgisterStep1) {
            currentPage = CurrentPage.resgisterStep2
            stepIndicator.currentStep = 1
            patientInfoDict = (notification.userInfo ?? [:]) as? [String :Any] ?? [:]
            print("First step dict : \(patientInfoDict)")
            displayNextPage()
        } else if(currentPage == CurrentPage.resgisterStep2) {
            currentPage = CurrentPage.resgisterStep3
            stepIndicator.currentStep = 2
            patientInfoDict.merge((notification.userInfo ?? [:]) as? [String :Any] ?? [:]) { (_, _) in}
            print(patientInfoDict)
            displayNextPage()
        }else{
            print("Last step")
        }
    }
    @objc func moveToPreviousPage(_ notification: NSNotification)
    {
        if(currentPage == CurrentPage.resgisterStep1) {
            print("first step")
        } else if(currentPage == CurrentPage.resgisterStep2) {
            currentPage = CurrentPage.resgisterStep1
            stepIndicator.currentStep = 0
            displayPreviousPage()
        }else if(currentPage == CurrentPage.resgisterStep3) {
            currentPage = CurrentPage.resgisterStep2
            stepIndicator.currentStep = 1
            displayPreviousPage()
        }
    }
    func displayNextPage()
    {
        switch currentPage {
        case .resgisterStep1:
            let controllerToShow = pages![0]
            pageViewController.setViewControllers([controllerToShow], direction: .forward, animated: true, completion: nil)
        case .resgisterStep2:
            let controllerToShow = pages![1]
            pageViewController.setViewControllers([controllerToShow], direction: .forward, animated: true, completion: nil)
        case .resgisterStep3:
            let controllerToShow = pages![2]
            pageViewController.setViewControllers([controllerToShow], direction: .forward, animated: true, completion: nil)
        }
    }
    func displayPreviousPage() {
        
        switch currentPage {
        case .resgisterStep1:
            let controllerToShow = pages![0]
            pageViewController.setViewControllers([controllerToShow], direction: .reverse, animated: true, completion: nil)
        case .resgisterStep2:
            let controllerToShow = pages![1]
            pageViewController.setViewControllers([controllerToShow], direction: .reverse, animated: true, completion: nil)
        case .resgisterStep3:
            let controllerToShow = pages![2]
            pageViewController.setViewControllers([controllerToShow], direction: .reverse, animated: true, completion: nil)
        }
    }
}
extension ClinicRegisterContainerVC : ThanksPopupDelegate{
    
    func submitTap() {
        if let arrVCs = self.navigationController?.viewControllers{
            for vc in arrVCs{
                if let homevc = vc as? HomeViewController{
                    navigationController?.popToViewController(homevc, animated: true)
                }
            }
        }
    }
}
extension ClinicRegisterContainerVC : AskForCovid19PopupViewControllerDelegate{
    func selectedOption(isYes: Bool) {
        if isYes{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.openThanksPopup(recordNumber: self.patientDetail?.patientRecordNumber ?? "", id: self.patientDetail?._id ?? "")
            }
        }
    }
}
