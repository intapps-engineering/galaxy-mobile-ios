//
//  ClinicRegisterStep2VC.swift
//  Tbstar
//
//  Created by Viprak-Heena on 03/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class ClinicRegisterStep2VC: BaseViewController {

    @IBOutlet weak var txtDateOfVisit: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSourceReferal: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblTypeofTBTitle: UILabel!
    @IBOutlet weak var lblSuspected: UILabel!
    @IBOutlet weak var lblSiteOfDisease: UILabel!
    @IBOutlet weak var lblPatientHealthWorker: UILabel!
    @IBOutlet weak var lblHivStatusTitle: UILabel!
    @IBOutlet weak var lblHIVTestingTitle: UILabel!
    @IBOutlet weak var lblTestedForHIVTitle: UILabel!
    
    @IBOutlet weak var lblAFBTestTitle: UILabel!
    @IBOutlet weak var lblAFBResultTitle: UILabel!
    
    @IBOutlet weak var lblxpertTestTitle: UILabel!
    @IBOutlet weak var lblxpertResultTitle: UILabel!
    
    @IBOutlet weak var lblXrayTestTitle: UILabel!
    @IBOutlet weak var lblXrayResultTest: UILabel!
    
    @IBOutlet weak var lblOtherTBTestTitle: UILabel!
    @IBOutlet weak var lblTBTypeOFTestTitle: UILabel!
    
    @IBOutlet weak var lblTBResultTestTitle: UILabel!
    
    @IBOutlet weak var lblDrugResistanceTitle: UILabel!
    
    @IBOutlet weak var lblHIVStatus: UILabel!
    @IBOutlet weak var lblTestedForHIV: UILabel!
    @IBOutlet weak var btnDSTB: UIButton!
    @IBOutlet weak var btnDRTB: UIButton!
    @IBOutlet weak var btnLungs: UIButton!
    @IBOutlet weak var btnEPTB: UIButton!
    @IBOutlet weak var btnWorkerYes: UIButton!
    @IBOutlet weak var btnWorkerNo: UIButton!
    @IBOutlet weak var btnHIVTestYes: UIButton!
    @IBOutlet weak var btnHIVTestNo: UIButton!
    
    @IBOutlet weak var vwHIVResult: UIView!
    @IBOutlet weak var vwHIVStatus: UIView!
    
    @IBOutlet weak var btnAFBYes: UIButton!
    @IBOutlet weak var btnAFBNO: UIButton!
    
    @IBOutlet weak var vwAFBTest: UIView!

    @IBOutlet weak var heightCnstOFAFB: NSLayoutConstraint!
    
    @IBOutlet weak var txtSpecimenDateCollAFB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSpecimenSentToLabAFB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtResultAFB: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblAFBResult: UILabel!
    @IBOutlet weak var vwAFBResult: UIView!
    @IBOutlet weak var btnXpertYes: UIButton!
    @IBOutlet weak var btnXpertNO: UIButton!
    
    @IBOutlet weak var txtSpecimenDateCollXpert: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSpecimenSentToLabXpert: SkyFloatingLabelTextField!
    @IBOutlet weak var txtResultXpert: SkyFloatingLabelTextField!

    @IBOutlet weak var vwXpertTest: UIView!
    
    @IBOutlet weak var heightCnstOFXpert: NSLayoutConstraint!
    
    @IBOutlet weak var lblXpertResult: UILabel!
    
    @IBOutlet weak var btnXRayYes: UIButton!
    @IBOutlet weak var btnXRayNo: UIButton!
    
    @IBOutlet weak var lblXrayResult: UILabel!
    @IBOutlet weak var vwXrayResult: UIView!
    
    @IBOutlet weak var txtXrayResultComment: SkyFloatingLabelTextField!
    
    @IBOutlet weak var vwXray: UIView!
    
    @IBOutlet weak var heightCnstOFvwXray: NSLayoutConstraint!
    
    @IBOutlet weak var btnOtherTestYes: UIButton!
    @IBOutlet weak var btnOtherTestNo: UIButton!
    
    @IBOutlet weak var vwOtherTest: UIView!
    
    @IBOutlet weak var heightCnstOfvwOther: NSLayoutConstraint!
    
    @IBOutlet weak var lblTypeofTest: UILabel!
    @IBOutlet weak var vwTypeOftest: UIView!
    
    @IBOutlet weak var vwResultOftest: UIView!
    
    @IBOutlet weak var lblResultOfTest: UILabel!
    
    @IBOutlet weak var btnDrugYes: UIButton!
    
    @IBOutlet weak var btnDrugNo: UIButton!
    
    
    @IBOutlet weak var btnPrevious: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var txtOtherResultComment: SkyFloatingLabelTextField!
    
    @IBOutlet weak var vwMainView: UIView!
    
    var isAFBTestConducted = ""
    var isXpertTestConducted = ""
    var isXrayPerformed = ""
    var isOtherTestConducted = ""
    var isDrugDetected = ""
    
    var dateFormatter = DateFormatter()
    
    var typeOfTBCase = ""
    var siteOFDisease = ""
    var isHealthWorker = ""
    var hivStatusOnArrival = ""
    var hivTesting = ""
    var testedForHIV = ""
    var afbResult = ""
    var xpertResult = ""
    var xRayREsult = ""
    var otherTypeOfTBTest = ""
    var otherResult = ""
    
    var AfbSampleCollectionDate = Date()
    var AfbSampleSentLabDate = Date()
    var AfbResultDate = Date()
    
    var MtbSampleCollectionDate = Date()
    var MtbSampleSentLabDate = Date()
    var MtbResultDate = Date()
    var visitDate = Date()
    
    var isEditMode = false
    var isDetailMode = false
    var isFromScreening = false
    
    var patientDetail : PatientInformation?
    
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        dateFormatter.dateFormat = displayDateFormat
        setupView()
        setupTextField()
        txtDateOfVisit.text = dateFormatter.string(from: Date())
        if isEditMode || isDetailMode{
            setupEditModeView()
        }
    }
    func setupLocalization() {
        
        txtDateOfVisit.placeholder = "visitDate".localized + "*"
        txtDateOfVisit.selectedTitle = "visitDate".localized + "*"
        txtDateOfVisit.title = "visitDate".localized + "*"
        
        lblTypeofTBTitle.text = "typeOfTBCase".localized
        
        lblSuspected.text = "suspected".localized
        
        btnDSTB.setTitle("dstb".localized, for: .normal)
        btnDSTB.setTitle("dstb".localized, for: .selected)
        
        btnDRTB.setTitle("drtb".localized, for: .normal)
        btnDRTB.setTitle("drtb".localized, for: .selected)
        
        lblSiteOfDisease.text = "siteDisease".localized
        
        btnLungs.setTitle("pulmonary".localized, for: .normal)
        btnLungs.setTitle("pulmonary".localized, for: .selected)
        
        btnEPTB.setTitle("expulmonary".localized, for: .normal)
        btnEPTB.setTitle("expulmonary".localized, for: .selected)
        
        
        lblPatientHealthWorker.text = "isHealthWorker".localized
        
        btnWorkerYes.setTitle("yes".localized, for: .normal)
        btnWorkerYes.setTitle("yes".localized, for: .selected)
        
        btnWorkerNo.setTitle("no".localized, for: .normal)
        btnWorkerNo.setTitle("no".localized, for: .selected)
        
        txtSourceReferal.placeholder = "referaal".localized
        txtSourceReferal.selectedTitle = "referaal".localized
        txtSourceReferal.title = "referaal".localized
        
        lblHivStatusTitle.text = "hivStatusonArrival".localized
        lblHIVTestingTitle.text = "counselledForHIV".localized
        
        btnHIVTestYes.setTitle("yes".localized, for: .normal)
        btnHIVTestYes.setTitle("yes".localized, for: .selected)
        
        btnHIVTestNo.setTitle("no".localized, for: .normal)
        btnHIVTestNo.setTitle("no".localized, for: .selected)
        
        lblTestedForHIVTitle.text = "testedForHIV".localized
        lblAFBTestTitle.text = "afbTested".localized
        
        btnAFBNO.setTitle("no".localized, for: .normal)
        btnAFBNO.setTitle("no".localized, for: .selected)
        
        btnAFBYes.setTitle("yes".localized, for: .normal)
        btnAFBYes.setTitle("yes".localized, for: .selected)
        
        txtSpecimenDateCollAFB.placeholder = "speciDateCollection".localized
        txtSpecimenDateCollAFB.selectedTitle = "speciDateCollection".localized
        txtSpecimenDateCollAFB.title = "speciDateCollection".localized
        
        txtSpecimenSentToLabAFB.placeholder = "speciSenttoLab".localized
        txtSpecimenSentToLabAFB.selectedTitle = "speciSenttoLab".localized
        txtSpecimenSentToLabAFB.title = "speciSenttoLab".localized
        
        txtResultAFB.placeholder = "speciresult".localized
        txtResultAFB.selectedTitle = "speciresult".localized
        txtResultAFB.title = "speciresult".localized
        
        lblAFBResultTitle.text = "afbresults".localized + "*"
        
        lblxpertTestTitle.text = "xpertTestConducted".localized
        
        btnXpertYes.setTitle("yes".localized, for: .normal)
        btnXpertYes.setTitle("yes".localized, for: .selected)
        
        btnXpertNO.setTitle("no".localized, for: .normal)
        btnXpertNO.setTitle("no".localized, for: .selected)
        
        txtSpecimenDateCollXpert.placeholder = "speciDateCollection".localized
        txtSpecimenDateCollXpert.selectedTitle = "speciDateCollection".localized
        txtSpecimenDateCollXpert.title = "speciDateCollection".localized
        
        txtSpecimenSentToLabXpert.placeholder = "speciSenttoLab".localized
        txtSpecimenSentToLabXpert.selectedTitle = "speciSenttoLab".localized
        txtSpecimenSentToLabXpert.title = "speciSenttoLab".localized
        
        txtResultXpert.placeholder = "speciresult".localized
        txtResultXpert.selectedTitle = "speciresult".localized
        txtResultXpert.title = "speciresult".localized
        
        lblxpertResultTitle.text = "xpertResult".localized + "*"
        
        lblXrayTestTitle.text = "chestXray".localized
        
        btnXRayYes.setTitle("yes".localized, for: .normal)
        btnXRayYes.setTitle("yes".localized, for: .selected)
        
        btnXRayNo.setTitle("no".localized, for: .normal)
        btnXRayNo.setTitle("no".localized, for: .selected)
        
        lblXrayResultTest.text = "resultofTest".localized
        
        txtXrayResultComment.placeholder = "xrayComments".localized
        txtXrayResultComment.selectedTitle = "xrayComments".localized
        txtXrayResultComment.title = "xrayComments".localized
        
        lblOtherTBTestTitle.text = "otherTBTest".localized
        
        btnOtherTestYes.setTitle("yes".localized, for: .normal)
        btnOtherTestYes.setTitle("yes".localized, for: .selected)
        
        btnOtherTestNo.setTitle("no".localized, for: .normal)
        btnOtherTestNo.setTitle("no".localized, for: .selected)
        
        lblTBTypeOFTestTitle.text = "typeOfTest".localized
        lblTBResultTestTitle.text = "resultOfTest".localized
        lblDrugResistanceTitle.text = "drugResistance".localized
        
        btnDrugNo.setTitle("no".localized, for: .normal)
        btnDrugNo.setTitle("no".localized, for: .selected)
        
        btnDrugYes.setTitle("yes".localized, for: .normal)
        btnDrugYes.setTitle("yes".localized, for: .selected)
        
        txtOtherResultComment.placeholder = "tbTestComments".localized
        txtOtherResultComment.selectedTitle = "tbTestComments".localized
        txtOtherResultComment.title = "tbTestComments".localized
        
        btnNext.setTitle("next".localized, for: .normal)
        btnPrevious.setTitle("previous".localized, for: .normal)
        
        lblTestedForHIV.text = "finalResult".localized
        lblAFBResult.text = "finalResult".localized + "*"
        lblXpertResult.text = "finalResult".localized + "*"
        lblXrayResult.text = "finalResult".localized + "*"
        lblTypeofTest.text = "typeOfTest".localized
        lblResultOfTest.text = "finalResult".localized + "*"
    }
    
    func setupEditModeView() {
        
        if isFromScreening{
            (txtDateOfVisit.text,visitDate) = Utility.convertDateformat(date: patientDetail?.visitDate ?? "", currentFormate: APIDateFormate, requiredFormate: displayDateFormat)
        }else{
            (txtDateOfVisit.text,visitDate) = Utility.convertDateformat(date: patientDetail?.visitDate ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
        }
        if patientDetail?.ptbCaseDetail?.typeOfPresumtivePtbCase.caseInsensitiveCompare(TBCase.DSTB.rawValue) == .orderedSame {
            btnTypeOfTBCase(btnDSTB)
        }else{
            btnTypeOfTBCase(btnDRTB)
        }
        if patientDetail?.ptbCaseDetail?.siteOfDisease.caseInsensitiveCompare(Disease.PulmonaryLungs.rawValue) == .orderedSame{
            btnDiseaseTap(btnLungs)
        }else{
            btnDiseaseTap(btnEPTB)
        }
        if patientDetail?.ptbCaseDetail?.patientIsAhealthWorker.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame {
            btnWorkerTap(btnWorkerYes)
        }else{
            btnWorkerTap(btnWorkerNo)
        }
        txtSourceReferal.text = patientDetail?.ptbCaseDetail?.sourceofreferral
        lblHIVStatus.text = patientDetail?.ptbCaseDetail?.didPatientKnowHivStatusUponArrival ?? ""
        hivStatusOnArrival = patientDetail?.ptbCaseDetail?.didPatientKnowHivStatusUponArrival ?? ""
        
        hivTesting = patientDetail?.ptbCaseDetail?.wasPatientCounselledForHivTesting ?? ""
        if hivTesting.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame {
            btnHIVConsTap(btnHIVTestYes)
        }else{
            btnHIVConsTap(btnHIVTestNo)
        }
        testedForHIV = patientDetail?.ptbCaseDetail?.wasPatientTestedForHiv ?? ""
        lblTestedForHIV.text = testedForHIV
        
        isAFBTestConducted = patientDetail?.afbTestDetail?.afbTestConducted ?? ""
        if isAFBTestConducted.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame{
            
            btnAFBTap(btnAFBYes)
            
            (txtSpecimenDateCollAFB.text,AfbSampleCollectionDate) = Utility.convertDateformat(date: patientDetail?.afbTestDetail?.dateOfSpecimenCollection ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            (txtSpecimenSentToLabAFB.text ,AfbSampleSentLabDate) = Utility.convertDateformat(date: patientDetail?.afbTestDetail?.dateSpecimenWasSentToLaboratory ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            (txtResultAFB.text ,AfbResultDate) = Utility.convertDateformat(date: patientDetail?.afbTestDetail?.dateResultsReleased ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            lblAFBResult.text  = patientDetail?.afbTestDetail?.afbResults
            afbResult = patientDetail?.afbTestDetail?.afbResults ?? ""
        }else{
            btnAFBTap(btnAFBNO)
        }
        
        isXpertTestConducted = patientDetail?.mtbTestDetail?.rifTestConducted ?? ""

        if isXpertTestConducted.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame{
            btnXpertTestTap(btnXpertYes)
            
            (txtSpecimenDateCollXpert.text,MtbSampleCollectionDate) = Utility.convertDateformat(date: patientDetail?.mtbTestDetail?.dateOfSpecimenCollection ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            (txtSpecimenSentToLabXpert.text ,MtbSampleSentLabDate) = Utility.convertDateformat(date: patientDetail?.mtbTestDetail?.dateSpecimenWasSentToLaboratory ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            (txtResultXpert.text ,MtbResultDate) = Utility.convertDateformat(date: patientDetail?.mtbTestDetail?.dateResultsReleased ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
            
            lblXpertResult.text = patientDetail?.mtbTestDetail?.xpertResults
            xpertResult = patientDetail?.mtbTestDetail?.xpertResults ?? ""
            
        }else{
            btnXpertTestTap(btnXpertNO)
        }
        isXrayPerformed = patientDetail?.xrayPerformedDetail?.chestXrayPerformed ?? ""
        
        if isXrayPerformed.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame {
            btnXrayTap(btnXRayYes)
            lblXrayResult.text = patientDetail?.xrayPerformedDetail?.xRaySuggestiveOfTb
            txtXrayResultComment.text = patientDetail?.xrayPerformedDetail?.otherXrayResultComments ?? ""
            xRayREsult = patientDetail?.xrayPerformedDetail?.xRaySuggestiveOfTb ?? ""
        }else{
            btnXrayTap(btnXRayNo)
        }
        
        isOtherTestConducted = patientDetail?.otherTbTestDetail?.otherTbTestConducted ?? ""
        
        if isOtherTestConducted.caseInsensitiveCompare(YesNO.Yes.rawValue) == .orderedSame {
            btnOtherTBTestTap(btnOtherTestYes)
            lblTypeofTest.text = patientDetail?.otherTbTestDetail?.typeOfTest ?? ""
            otherTypeOfTBTest = patientDetail?.otherTbTestDetail?.typeOfTest ?? ""
            lblResultOfTest.text = patientDetail?.otherTbTestDetail?.resultOfTest ?? ""
            otherResult = patientDetail?.otherTbTestDetail?.resultOfTest ?? ""
            isDrugDetected = patientDetail?.otherTbTestDetail?.drugResistanceDetected ?? ""
            if isDrugDetected == YesNO.Yes.rawValue {
                btnDrugDetectedTap(btnDrugYes)
            }else{
                btnDrugDetectedTap(btnDrugNo)
            }
            txtOtherResultComment.text = patientDetail?.otherTbTestDetail?.otherTestResultComments ?? ""
        }else{
            btnOtherTBTestTap(btnOtherTestNo)
        }
        disableAllViews(shouldDisable: isDetailMode)
    }
    func disableAllViews(shouldDisable : Bool) {
        
        for i in 1..<13{
            let view = vwMainView.viewWithTag(i)
            view?.isUserInteractionEnabled = !shouldDisable
        }
        
    }
    func setupView() {
        heightCnstOFAFB.constant = 0
        vwAFBTest.isHidden = true
        heightCnstOFXpert.constant = 0
        vwXpertTest.isHidden = true
        heightCnstOfvwOther.constant = 0
        vwOtherTest.isHidden = true
        heightCnstOFvwXray.constant = 0
        vwXray.isHidden = true
    }
    func setupTextField()  {
        
        txtDateOfVisit.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDateOfVisit.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSourceReferal.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSourceReferal.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSourceReferal.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSourceReferal.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSpecimenDateCollAFB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollAFB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollAFB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollAFB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSpecimenSentToLabAFB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabAFB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabAFB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabAFB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtResultAFB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtResultAFB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtResultAFB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtResultAFB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSpecimenDateCollXpert.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollXpert.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollXpert.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenDateCollXpert.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSpecimenSentToLabXpert.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabXpert.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabXpert.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSpecimenSentToLabXpert.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtResultXpert.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtResultXpert.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtResultXpert.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtResultXpert.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtXrayResultComment.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtXrayResultComment.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtXrayResultComment.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtXrayResultComment.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtOtherResultComment.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultComment.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultComment.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtOtherResultComment.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    
    @IBAction func btnTypeOfTBCase(_ sender: UIButton) {
        btnDSTB.isSelected = false
        btnDRTB.isSelected = false
        sender.isSelected = true
        typeOfTBCase = sender.tag == 1 ? TBCase.DSTB.rawValue : TBCase.DRTB.rawValue
    }
    
    @IBAction func btnDiseaseTap(_ sender: UIButton) {
        btnLungs.isSelected = false
        btnEPTB.isSelected = false
        sender.isSelected = true
        siteOFDisease = sender.tag == 1 ? Disease.PulmonaryLungs.rawValue : Disease.ExPulmonary.rawValue
    }
    
    @IBAction func btnWorkerTap(_ sender: UIButton) {
        btnWorkerNo.isSelected = false
        btnWorkerYes.isSelected = false
        sender.isSelected = true
        isHealthWorker = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
    }
    
    @IBAction func btnHIVStatusTap(_ sender: UIButton) {
        //open dropdowwn
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["Yes - Positive","Yes - Negative","Did not know status"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblHIVStatus.text = item
            if index == 0{
                hivStatusOnArrival = HIVStatusOnArrival.Positive.rawValue
            }else if index == 1{
                hivStatusOnArrival = HIVStatusOnArrival.Negative.rawValue
            }else if index == 2{
                hivStatusOnArrival = HIVStatusOnArrival.Unknown.rawValue
            }
            dropDown.hide()
        }
    }
    
    @IBAction func btnHIVConsTap(_ sender: UIButton) {
        btnHIVTestNo.isSelected = false
        btnHIVTestYes.isSelected = false
        sender.isSelected = true
        hivTesting = sender.tag == 1 ? YesNO.Yes.rawValue : YesNO.No.rawValue
    }
    @IBAction func btnHIVResultTap(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["Yes - Positive","Yes - Negative","Was not tested"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblTestedForHIV.text = item
            if index == 0{
                testedForHIV = TestedForHIV.Positive.rawValue
            }else if index == 1{
                testedForHIV = TestedForHIV.Negative.rawValue
            }else if index == 2{
                testedForHIV = TestedForHIV.Nottested.rawValue
            }
            dropDown.hide()
        }
        
    }
    @IBAction func btnAFBTap(_ sender: UIButton) {
        btnAFBYes.isSelected = false
        btnAFBNO.isSelected = false
        sender.isSelected = true
        isAFBTestConducted = sender.tag == 1 ? YesNO.Yes.rawValue: YesNO.No.rawValue
        heightCnstOFAFB.constant =  isAFBTestConducted == YesNO.Yes.rawValue ? 280 : 0
        vwAFBTest.isHidden = isAFBTestConducted == YesNO.Yes.rawValue ? false : true
    }
    @IBAction func btnAFBResultTap(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["Negative","1+","2+","3+","1","2","3","4","5","6","7","8","9"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblAFBResult.text = item
            afbResult = item.lowercased()
            dropDown.hide()
        }
    }
    
    @IBAction func btnXpertTestTap(_ sender: UIButton) {
        
        btnXpertNO.isSelected = false
        btnXpertYes.isSelected = false
        sender.isSelected = true
        isXpertTestConducted = sender.tag == 1 ? YesNO.Yes.rawValue: YesNO.No.rawValue
        heightCnstOFXpert.constant =  isXpertTestConducted == YesNO.Yes.rawValue ? 280 : 0
        vwXpertTest.isHidden = isXpertTestConducted == YesNO.Yes.rawValue ? false : true
    }
    
    @IBAction func btnXpertResultTap(_ sender: UIButton) {
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["MTB NOT Detected", "MTB Detected - RIF Resistance not detected","MTB Detected - RIF Resistance detected","MTB Detected - RIF Resistance indeterminate","Error or incompleted/ Invalid result"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblXpertResult.text = item
            xpertResult = XpertResults.allCases[index].rawValue
            dropDown.hide()
        }
    }
    
    @IBAction func btnXrayTap(_ sender: UIButton) {
        btnXRayNo.isSelected = false
        btnXRayYes.isSelected = false
        sender.isSelected = true
        isXrayPerformed = sender.tag == 1 ? YesNO.Yes.rawValue: YesNO.No.rawValue
        heightCnstOFvwXray.constant =  isXrayPerformed == YesNO.Yes.rawValue ? 140 : 0
        vwXray.isHidden = isXrayPerformed == YesNO.Yes.rawValue ? false : true
    }
    
    @IBAction func btnXrayResultTap(_ sender: UIButton) {
        //open dropdowwn
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["X-ray suggestive of TB", "X-ray not suggestive of TB"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblXrayResult.text = item
            xRayREsult = ChestXRayResult.allCases[index].rawValue
            dropDown.hide()
        }
    }
    
    @IBAction func btnOtherTBTestTap(_ sender: UIButton) {
        btnOtherTestNo.isSelected = false
        btnOtherTestYes.isSelected = false
        sender.isSelected = true
        isOtherTestConducted = sender.tag == 1 ? YesNO.Yes.rawValue: YesNO.No.rawValue
        heightCnstOfvwOther.constant =  isOtherTestConducted == YesNO.Yes.rawValue ? 300 : 0
        vwOtherTest.isHidden = isOtherTestConducted == YesNO.Yes.rawValue ? false : true
    }
    
    @IBAction func btnTypeOfOtherTestTap(_ sender: UIButton) {
        //open dropdowwn
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["Culture", "LPA","1st Line DST","2nd Line DST", "Other"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblTypeofTest.text = item
            otherTypeOfTBTest = TypeOfTBCase.allCases[index].rawValue
            dropDown.hide()
        }
    }
    
    @IBAction func btnResultOFTestTap(_ sender: UIButton) {
        //open dropdowwn
        
        dropDown.anchorView = sender
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.dataSource = ["TB Detected","TB Not Detected"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            lblResultOfTest.text = item
            otherResult = ResultOFOtherTBTest.allCases[index].rawValue
            dropDown.hide()
        }
    }
    
    @IBAction func btnDrugDetectedTap(_ sender: UIButton) {
        btnDrugNo.isSelected = false
        btnDrugYes.isSelected = false
        sender.isSelected = true
        isDrugDetected = sender.tag == 1 ? YesNO.Yes.rawValue: YesNO.No.rawValue
    }
    
    @IBAction func btnPreviousTap(_ sender: Any) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "PreviousTap"), object: nil, userInfo: nil))
    }
    
    @IBAction func btnNextTap(_ sender: Any) {
        
        if isDetailMode{
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "NextTap"), object: nil, userInfo: nil))
        }else{
            if isValidDetail(){
                 NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "NextTap"), object: nil, userInfo: makePatientInfoDictionary()))
            }
        }
    }
    func makePatientInfoDictionary() -> [String :Any] {
        dateFormatter.dateFormat = APIDateFormate
        return ["visitDate" : dateFormatter.string(from: visitDate),
                "ptbCase" : ["typeOfPresumtivePtbCase" : typeOfTBCase,
                             "siteOfDisease" : siteOFDisease,
                             "patientIsAhealthWorker" : isHealthWorker,
                             "didPatientKnowHivStatusUponArrival" : hivStatusOnArrival,
                             "wasPatientCounselledForHivTesting" : hivTesting,
                             "wasPatientTestedForHiv" : testedForHIV,
                             "sourceofreferral" : txtSourceReferal.text ?? ""
                ],
                "afbTest" : makeAFBDict(),
                "mtbTest" : makeMTBDict(),
                "xRayPerformed" : makeXrayDict(),
        "otherTbTest" : makeOtherTBTestDict()]
    }
    func makeAFBDict() -> [String : Any] {
        if isAFBTestConducted == YesNO.Yes.rawValue{
            return ["afbTestConducted" : isAFBTestConducted,
                    "dateOfSpecimenCollection" : dateFormatter.string(from: AfbSampleCollectionDate),
                    "dateSpecimenWasSentToLaboratory" : dateFormatter.string(from: AfbSampleSentLabDate),
                    "dateResultsReleased" :dateFormatter.string(from: AfbResultDate),
                    "afbResults" : afbResult]
        }else{
            return ["afbTestConducted" : isAFBTestConducted]
        }
    }
    func makeMTBDict() -> [String : Any] {
        if isXpertTestConducted == YesNO.Yes.rawValue{
            return ["rifTestConducted" : isXpertTestConducted,
                    "dateOfSpecimenCollection" : dateFormatter.string(from: MtbSampleCollectionDate),
                    "dateSpecimenWasSentToLaboratory" : dateFormatter.string(from: MtbSampleSentLabDate),
                    "dateResultsReleased" :dateFormatter.string(from: MtbResultDate),
                    "xpertResults" : xpertResult]
        }else{
            return ["rifTestConducted" : isXpertTestConducted]
        }
    }
    func makeXrayDict() -> [String : Any] {
        if isXrayPerformed == YesNO.Yes.rawValue{
            return ["chestXrayPerformed" : isXrayPerformed,
                    "xRaySuggestiveOfTb" : xRayREsult,
                    "otherXrayResultComments" : txtXrayResultComment.text ?? ""]
        }else{
            return ["chestXrayPerformed" : isXrayPerformed]
        }
    }
    func makeOtherTBTestDict() -> [String : Any] {
        if isOtherTestConducted == YesNO.Yes.rawValue{
            return ["otherTbTestConducted" : isOtherTestConducted,
                    "typeOfTest" : otherTypeOfTBTest,
                    "resultOfTest" : otherResult,
                    "drugResistanceDetected" : isDrugDetected,
                    "otherTestResultComments" : txtOtherResultComment.text ?? ""]
        }else{
            return ["otherTbTestConducted" : isOtherTestConducted]
        }
    }
    func isValidDetail() -> Bool {
        if typeOfTBCase.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if siteOFDisease.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if isHealthWorker.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if hivStatusOnArrival.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if hivTesting.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if testedForHIV.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if isAFBTestConducted.isEmpty || isXrayPerformed.isEmpty || isXpertTestConducted.isEmpty || isOtherTestConducted.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.allQuestionsMustBeAnswered)
            return false
        }
        if isAFBTestConducted == YesNO.Yes.rawValue{
            if let afbdate = txtSpecimenDateCollAFB.text, afbdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterAFBSampleCollectionDate)
                return false
            }
            if let afbSentdate = txtSpecimenSentToLabAFB.text, afbSentdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterValidAFBTestSendDate)
                return false
            }
            if AfbSampleSentLabDate < AfbSampleCollectionDate{
                showAlert(titleStr: appName, msg: appMessages.enterValidAFBTestSendDate)
                return false
            }
            if let afbResultdate = txtResultAFB.text, afbResultdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterValidAFBResultDate)
                return false
            }
            if AfbResultDate < AfbSampleCollectionDate || AfbResultDate < AfbSampleSentLabDate {
                showAlert(titleStr: appName, msg: appMessages.enterValidAFBResultDate)
                return false
            }
            if afbResult.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterAfbtestResult)
                return false
            }
        }
        if isXpertTestConducted == YesNO.Yes.rawValue{
            if let xpertdate = txtSpecimenDateCollXpert.text, xpertdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterMTBSampleCollectionDate)
                return false
            }
            if let xpertdate = txtSpecimenSentToLabXpert.text, xpertdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterValidMTBSendDate)
                return false
            }
            if MtbSampleSentLabDate < MtbSampleCollectionDate{
                showAlert(titleStr: appName, msg: appMessages.enterValidMTBSendDate)
                return false
            }
            if let xpertdate = txtResultXpert.text, xpertdate.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterValidMTBResultDate)
                return false
            }
            if MtbResultDate < MtbSampleCollectionDate || AfbResultDate < MtbSampleSentLabDate {
                showAlert(titleStr: appName, msg: appMessages.enterValidMTBResultDate)
                return false
            }
            if xpertResult.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterMTBtestResult)
                return false
            }
        }
        if isXrayPerformed ==  YesNO.Yes.rawValue{
            if xRayREsult.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterXrayTextResult)
                return false
            }
        }
        if isOtherTestConducted == YesNO.Yes.rawValue{
            if otherTypeOfTBTest.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectOtherTypeOfTBTest)
                return false
            }
            if otherResult.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.enterOtherTestResult)
                return false
            }
            if isDrugDetected.isEmpty{
                showAlert(titleStr: appName, msg: appMessages.selectDrugResistance)
                return false
            }
        }
         
        return true
    }
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            view.window?.rootViewController?.present(datePickerVC, animated: true, completion: nil)
        }
    }
}
extension ClinicRegisterStep2VC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDateOfVisit{
            openDatePicker(dateType: .VisitDate)
            return false
        }
        if textField == txtSpecimenDateCollAFB{
            openDatePicker(dateType: .SpecimenCollectionDateAFB)
            return false
        }
        if textField == txtSpecimenSentToLabAFB{
            openDatePicker(dateType: .SpecimenSentLabDateAFB)
            return false
        }
        if textField == txtResultAFB{
            openDatePicker(dateType: .ResultDateAFB)
            return false
        }
        if textField == txtSpecimenDateCollXpert{
            openDatePicker(dateType: .SpecimenCollectionDateXpert)
            return false
        }
        if textField == txtSpecimenSentToLabXpert{
            openDatePicker(dateType: .SpecimenSentLabDateXpert)
            return false
        }
        if textField == txtResultXpert{
            openDatePicker(dateType: .ResultDateXpert)
            return false
        }
        
        return true
    }
}
extension ClinicRegisterStep2VC : DatePickerDelegate{
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .VisitDate {
            txtDateOfVisit.text = dateFormatter.string(from: sDate)
            visitDate = sDate
        }else if dateType == .SpecimenCollectionDateAFB{
            txtSpecimenDateCollAFB.text = dateFormatter.string(from: sDate)
            AfbSampleCollectionDate = sDate
        }else if dateType == .SpecimenSentLabDateAFB{
            txtSpecimenSentToLabAFB.text = dateFormatter.string(from: sDate)
            AfbSampleSentLabDate = sDate
        }else if dateType == .ResultDateAFB{
            txtResultAFB.text = dateFormatter.string(from: sDate)
            AfbResultDate = sDate
        }else if dateType == .SpecimenCollectionDateXpert{
            txtSpecimenDateCollXpert.text = dateFormatter.string(from: sDate)
            MtbSampleCollectionDate = sDate
        }else if dateType == .SpecimenSentLabDateXpert{
            txtSpecimenSentToLabXpert.text = dateFormatter.string(from: sDate)
            MtbSampleSentLabDate = sDate
        }else if dateType == .ResultDateXpert{
            txtResultXpert.text = dateFormatter.string(from: sDate)
            MtbResultDate = sDate
        }
    }
}
