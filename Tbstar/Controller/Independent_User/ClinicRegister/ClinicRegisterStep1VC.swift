//
//  ClinicRegisterStep1VC.swift
//  Tbstar
//
//  Created by Viprak-Heena on 03/03/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class ClinicRegisterStep1VC: BaseViewController {

    @IBOutlet weak var txtFname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSurname: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDOB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobNO: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMOB: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblAgeTitle: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var lblSexTitle: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var heightCnstOFLGA: NSLayoutConstraint!
    
    @IBOutlet weak var lblPatientStatusTitle: UILabel!
    @IBOutlet weak var btnPatientStatus: UIButton!
    
    @IBOutlet weak var lblCovidStatusTitle: UILabel!
    
    @IBOutlet weak var btnCovidStatus: UIButton!
    
    @IBOutlet weak var vwMainView: UIView!
    @IBOutlet weak var lblPatientIdTitle: UILabel!
    @IBOutlet weak var heightCnstOFvwRecord: NSLayoutConstraint!
    
    @IBOutlet weak var lblPatientRecordNumber: UILabel!
    
    @IBOutlet weak var lblDateOfBirth: UILabel!
    
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    var selectedGender = ""
    var dateFormatter = DateFormatter()
    var designationDetail : DesignationDetail?
    let dropDown = DropDown()
    var birthDate = Date()
    
    var isDetailMode = false
    var isEditMode = false
    var isFromScreening = false
    
    var patientDetail : PatientInformation?
    var isCovidScreened = false
    
    var isfrom : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = displayDateFormat
        setupLocalization()
        setupTextField()
        getDetailForIndependentUser()
        heightCnstOFLGA.constant = 0
        
        if isDetailMode || isEditMode{
            heightCnstOFvwRecord.constant = 80
            setupEditModeView()
        }else{
            heightCnstOFvwRecord.constant = 0
            btnCovidStatus.setTitle("", for: .normal)
            btnCovidStatus.isUserInteractionEnabled = false
            btnPatientStatus.setTitle("", for: .normal)
            btnPatientStatus.isUserInteractionEnabled = false
        }
    }
    func setupLocalization() {
        txtFname.placeholder = "pFirstName".localized + "*"
        txtFname.selectedTitle = "pFirstName".localized + "*"
        txtFname.title = "pFirstName".localized + "*"
        
        txtMName.placeholder = "pMiddleName".localized
        txtMName.selectedTitle = "pMiddleName".localized
        txtMName.title = "pMiddleName".localized
        
        txtSurname.placeholder = "pSurname".localized + "*"
        txtSurname.selectedTitle = "pSurname".localized + "*"
        txtSurname.title = "pSurname".localized + "*"
        
        lblAgeTitle.text = "age".localized
        lblSexTitle.text = "sex".localized + "*"
        
        txtMobNO.placeholder = "mobNO".localized + "*"
        txtMobNO.selectedTitle = "mobNO".localized + "*"
        txtMobNO.title = "mobNO".localized + "*"
        
        txtAlternateMOB.placeholder = "secondMobileNum".localized
        txtAlternateMOB.selectedTitle = "secondMobileNum".localized
        txtAlternateMOB.title = "secondMobileNum".localized
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
        
        lblPatientStatusTitle.text = "patientStatus".localized
        lblCovidStatusTitle.text = "covidStatus".localized
        lblPatientIdTitle.text = "patientID".localized
        lblDateOfBirth.text = "dob".localized + "*"
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        btnNext.setTitle("next".localized, for: .normal)
        
    }
    func setupEditModeView() {
        
        lblPatientRecordNumber.text = patientDetail?.patientRecordNumber ?? ""
        txtFname.text = patientDetail?.firstName ?? ""
        txtMName.text = patientDetail?.middleName ?? ""
        txtSurname.text = patientDetail?.surname ?? ""
        
        if patientDetail?.dateOfBirth.range(of:"[UTC]") != nil {
            
            patientDetail?.dateOfBirth = patientDetail?.dateOfBirth.replacingOccurrences(of: "[UTC]", with: "", options: NSString.CompareOptions.literal, range: nil) ?? ""
            
            if isFromScreening{
                
                (txtDOB.text, birthDate) = Utility.convertDateformat(date: patientDetail?.dateOfBirth ?? "", currentFormate: displayDateFormat, requiredFormate: displayDateFormat)
                
            } else {
                
                (txtDOB.text, birthDate) = Utility.convertDateformat(date: patientDetail?.dateOfBirth ?? "", currentFormate: NewapiDateFormatFromAPI, requiredFormate: displayDateFormat)
            }
            
        } else {
            
            if isFromScreening{
                (txtDOB.text, birthDate) = Utility.convertDateformat(date: patientDetail?.dateOfBirth ?? "", currentFormate: APIDateFormate, requiredFormate: displayDateFormat)
                
            }else{
                
                if isfrom == "RecentCase" {
                    (txtDOB.text, birthDate) = Utility.convertDateformat(date: patientDetail?.dateOfBirth ?? "", currentFormate: apiDateFormatFromAPI, requiredFormate: displayDateFormat)
                } else {
                    (txtDOB.text, birthDate) = Utility.convertDateformat(date: patientDetail?.dateOfBirth ?? "", currentFormate: NewapiDateFormatFromAPI, requiredFormate: displayDateFormat)
                }
            }
        }
            //patientDetail?.dateOfBirth ?? ""
        lblAge.text = "\(calculateAge(bDate: birthDate))"
        selectedGender = patientDetail?.sex ?? ""
        
        if selectedGender == Gender.Female.rawValue{
            btnFemaleTap(btnFemale)
        }else if selectedGender == Gender.Male.rawValue{
            btnMaleTap(btnMale)
        }
        txtMobNO.text  = patientDetail?.phoneNumber ?? ""
        txtAlternateMOB.text = patientDetail?.secondPhoneNumber ?? ""
        txtAddress.text = patientDetail?.address ?? ""
        txtState.text = patientDetail?.state
        heightCnstOFLGA.constant = 80
        txtLGA.isHidden = false
        txtLGA.text = patientDetail?.lga
        
        btnPatientStatus.setTitle(patientDetail?.patientStatusLabel ?? "", for: .normal)
        btnPatientStatus.isUserInteractionEnabled = false
        
        if patientDetail?.patientCovidSymptoms != nil{
            btnCovidStatus.setTitle("Screened : View Status", for: .normal)
            isCovidScreened = true
        }else{
            btnCovidStatus.setTitle("Not Screened : Screen now.", for: .normal)
            isCovidScreened = false
        }
        disableAllViews(shouldDisable: isDetailMode)
    }
    func disableAllViews(shouldDisable : Bool) {
        
        for i in 1..<5{
            let view = vwMainView.viewWithTag(i)
            view?.isUserInteractionEnabled = !shouldDisable
        }
        
    }
    func setupTextField()  {
        
        txtFname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFname.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtDOB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtDOB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobNO.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNO.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNO.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobNO.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMOB.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMOB.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMOB.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMOB.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtState.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtState.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtState.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtLGA.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtLGA.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
    }
    
    @IBAction func btnMaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }
    
    @IBAction func btnFemaleTap(_ sender: UIButton) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }
    @IBAction func btnNextStep(_ sender: Any) {
        //uncomment validation function
        if isDetailMode{
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "NextTap"), object: nil, userInfo: nil))
        }else{
            if isValidDetail(){
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "NextTap"), object: nil, userInfo: makePatientInfoDictionary()))
            }
        }
    }
    
    @IBAction func btnPatientStatusTap(_ sender: Any) {
        
    }
    
    @IBAction func btnCovidStatusTap(_ sender: Any) {
       
        if isCovidScreened{
            
            if let covidScreenedVC = self.storyboard?.instantiateViewController(withIdentifier: "CovidSymptomsDetailViewController") as? CovidSymptomsDetailViewController{
                covidScreenedVC.covidSymptom = patientDetail?.patientCovidSymptoms
                covidScreenedVC.modalPresentationStyle = .overCurrentContext
                view.window?.rootViewController?.present(covidScreenedVC, animated: true, completion: nil)
            }
        }else{
            
            if let askForCovid19 = self.storyboard?.instantiateViewController(withIdentifier: "Covid19ScreeningViewController") as? Covid19ScreeningViewController{
                askForCovid19.isFromEdit = true
                askForCovid19.delegate = self
                askForCovid19.patientRecordNumber = patientDetail?.patientRecordNumber ?? ""
                view.window?.rootViewController?.present(askForCovid19, animated: true, completion: nil)
            }
        }
    }
    
    func makePatientInfoDictionary() -> [String :Any] {
        dateFormatter.dateFormat = APIDateFormate
        return ["firstName" : txtFname.text ?? "",
                "lastName" : txtSurname.text ?? "",
                "middleName" : txtMName.text ?? "",
                "dateOfBirth" : dateFormatter.string(from: birthDate),
                "age" : lblAge.text ?? "",
                "sex" : selectedGender,
                "phoneNumber" : txtMobNO.text ?? "",
                "secondPhoneNumber" : txtAlternateMOB.text ?? "",
                "address" : txtAddress.text ?? "",
                "state" : txtState.text ?? "",
                "lga" : txtLGA.text ?? ""]
    }
    
    func isValidDetail() -> Bool {
        if let name = txtFname.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = txtSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        if let dob = txtDOB.text, dob.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterDOB)
            return false
        }
        if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }
        if let mobNo = txtMobNO.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMOB.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        return true
    }
    func openDatePicker(dateType : DateType)  {
        
        if let datePickerVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerController") as? DatePickerController{
            datePickerVC.dateType = dateType
            datePickerVC.delegate = self
            datePickerVC.modalPresentationStyle = .overCurrentContext
            view.window?.rootViewController?.present(datePickerVC, animated: true, completion: nil)
        }
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            txtLGA.text = ""
            heightCnstOFLGA.constant = 80
            txtLGA.isHidden = false
            dropDown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropDown.dataSource = addressDetail.LGA ?? []
            }
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropDown.hide()
        }
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
    }
}
extension ClinicRegisterStep1VC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDOB{
            openDatePicker(dateType: .BirthDate)
            return false
        }
        if textField == txtState{
            setupStateDropdown()
            return false
        }
        if textField == txtLGA{
            setupLGADropdown()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobNO || textField == txtAlternateMOB{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
extension ClinicRegisterStep1VC : DatePickerDelegate{
    func selectedDate(sDate: Date, dateType: DateType) {
        if dateType == .BirthDate{
            txtDOB.text = dateFormatter.string(from: sDate)
            lblAge.text = "\(calculateAge(bDate: sDate))"
            birthDate = sDate
        }
    }
}
extension ClinicRegisterStep1VC : Covid19ScreenDelegate{
    func updatePatientRecord(covidDict: [String : Any]) {
        let covidDetail = PatientCovidSymptoms(dict: covidDict)
        patientDetail?.patientCovidSymptoms = covidDetail
        setupEditModeView()
    }
}
