//
//  HomeViewController.swift
//  Tbstar
//
//  Created by Apple on 15/02/21.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var btnScreenForTB: UIButton!
    @IBOutlet weak var btnReportTb: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        setupLocalization()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        updateAppVersion()
        getDetailForIndependentUser()
        NotificationCenter.default.addObserver(self, selector: #selector(languageChanged), name:NSNotification.Name(rawValue: "LangChanged"), object: nil)
    }
    func setupLocalization() {
        lblTitle.text = "home".localized
        btnReportTb.setTitle("reportTB".localized, for: .normal)
        btnScreenForTB.setTitle("screenForTB".localized, for: .normal)
    }
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                if success {
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                    }
                }
            })
        }
    }
    @objc func languageChanged()  {
        setupLocalization()
    }
    
    @IBAction func btnScreenForTBTap(_ sender: Any) {
        if let screenClientVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenNewClientViewController") as? ScreenNewClientViewController{
            navigationController?.pushViewController(screenClientVC, animated: true)
        }
    }
    
    @IBAction func btnReportTBTap(_ sender: Any) {
        if let reportTBVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportTBCaseViewController") as? ReportTBCaseViewController{
            navigationController?.pushViewController(reportTBVC, animated: true)
        }
    }
    
    @IBAction func btnMenuTap(_ sender: Any) {
        //logout()
        showMenu()
    }
    
    @IBAction func btnNotificationTap(_ sender: Any) {
        
        if let notificationVC = self.storyboard?.instantiateViewController(withIdentifier: "PPMV_NotificationVC") as? PPMV_NotificationVC {
            navigationController?.pushViewController(notificationVC, animated: true)
        }
        
    }
    
    func updateAppVersion()  {
        if isConnectedToNetwork(){
            let paraDict = ["apkVersion" : getAppVersion()]
            let user = getUserDataFromStandard()
            updateAppVersionAPI(paradict: paraDict, accessToken: user?.accessToken ?? "") { (success,code,message) in
                if code == 402{
                    self.view.makeToast(message)
                    self.logOutApplication()
                }
            }
        }
    }
}
