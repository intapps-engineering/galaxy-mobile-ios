//
//  OfflineSyncViewController.swift
//  Tbstar
//
//  Created by Apple on 13/04/21.
//

import UIKit

class OfflineSyncViewController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblvSyncList: UITableView!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var heightCnstraint: NSLayoutConstraint!
    
    var arrPatientInfoDicts = [[String : Any]]()
    var arrPatientList = [PatientInformation]()
    var arrTbCollectionDetail = [PatientInformation]()
    var arrTBCollectionDictList = [[String : Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        getListFromDatabase()
    }
    func setupLocalization() {
        lblTitle.text = "offline_sync".localized
        btnSync.setTitle("sync".localized, for: .normal)
    }
    @IBAction func btnBackTap(_ sender: Any) {
        currentMenu = MenuOption.Home
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSyncTap(_ sender: Any) {
        
        if isConnectedToNetwork(){
            screenClientWithTB()
        }else{
            showAlert(titleStr: appName, msg: appMessages.noConnection)
        }
    }
    func getListFromUserDefaultss()  {
        
        if let arrDetails = Defaultss.value(forKey: UDKey.kTbCollecionDetail) as? [[String : Any]]{
            arrTBCollectionDictList = arrDetails
            arrTbCollectionDetail = arrDetails.map(PatientInformation.init)
        }
        
        tblvSyncList.reloadData()
        if arrPatientList.count == 0 && arrTbCollectionDetail.count == 0{
            heightCnstraint.constant = 0
            tblvSyncList.setNoDataPlaceholder("noRecord".localized)
            btnSync.isHidden = true
        }
    }
    
    func screenClientWithTB()  {
        
        showHUD()
        if arrPatientInfoDicts.count > 0 {
            let apiDict = ["patient" : arrPatientInfoDicts]
            
            screenIndependentUserPatientOfflineAPI(paradict: apiDict) { (success, strData, message) in
                self.hideHUD()
                
                if success{
                    if self.arrTBCollectionDictList.count > 0{
                        self.reportPatientWithTbCollection()
                    }else{
                        self.view.makeToast(message)
                        self.deleteFromDatabase()
                    }
                }else if message != ""{
                    self.view.makeToast(message)
                    print(message)
                }
                
            }
        }else{
            reportPatientWithTbCollection()
        }
    }
    
    func reportPatientWithTbCollection()  {
        
        showHUD()
        
        let apiDict = ["patient" : arrTBCollectionDictList]
        
        reportTBAfterCollectDetailsOfflineAPI(paradict: apiDict) { (success, strData, message) in
            self.hideHUD()
            if success{
                self.view.makeToast(message)
                self.deletefromUserDefault()
                self.deleteFromDatabase()
            }else if message != ""{
                self.view.makeToast(message)
                print(message)
            }
        }
        
    }
    func deletefromUserDefault() {
        
        Defaultss.setValue(nil, forKey: UDKey.kTbCollecionDetail)
        Defaultss.synchronize()
    }
    func deleteFromDatabase()  {
        
        let ans = DatabaseManager().ExecuteQuery(query: "delete from PatientDetails")
        if ans {
            currentMenu = MenuOption.Home
            navigationController?.popViewController(animated: true)
        }
    }
    func getListFromDatabase()  {
        arrPatientInfoDicts = DatabaseManager().fetchPatientInfoData(query: "select * from PatientDetails")
        arrPatientList = arrPatientInfoDicts.map(PatientInformation.init)
        getListFromUserDefaultss()
        
    }
}
extension OfflineSyncViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPatientList.count + arrTbCollectionDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "syncCell") as? FindCaseCell{
            if indexPath.row < arrPatientList.count{
                let info = arrPatientList[indexPath.row]
                cell.lblName.text = "\(info.firstName) \(info.surname)"
                cell.lblDate.text = "Date:\n \(info.createDate)"
                return cell
            }
            else if indexPath.row - arrPatientList.count < arrTbCollectionDetail.count{
                let info = arrTbCollectionDetail[indexPath.row - arrPatientList.count ]
                cell.lblName.text = "\(info.firstName) \(info.surname)"
                cell.lblDate.text = "Date:\n \(info.createDate)"
                return cell
            }
        }
        return UITableViewCell()
    }

}
