//
//  CovidSymptomsDetailViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 16/03/21.
//

import UIKit

class CovidSymptomsDetailViewController: UIViewController {

    @IBOutlet weak var lblPopupTitle: UILabel!
    @IBOutlet weak var lblSymptomDetail: UILabel!
    @IBOutlet weak var tblvCovidSymptom: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var heightCnstOfTblv: NSLayoutConstraint!
    
    var covidSymptom : PatientCovidSymptoms?
    var yesCount = 0
    
    let arrQuestions = ["Do you have cough?","Do you have fever (Temperature 37.5'c & above)?","Do you experience new loss of taste or smell?","Do you have cold or runny nose?","Do you have diarrhea?","Do you have sore throat?","Do you have repeated shaking with chills?"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tblvCovidSymptom.reloadData()
        print("Yes count \(yesCount)")
        
        
        heightCnstOfTblv.constant = tblvCovidSymptom.contentSize.height
    }
    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnContinueTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension CovidSymptomsDetailViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQuestions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "qustionAnsCell") as? QuestionAnsCell{
            
            cell.lblQuestion.text = arrQuestions[indexPath.row]
            switch indexPath.row {
            case 0:
                cell.lblAns.text = covidSymptom?.covidCough ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 1:
                cell.lblAns.text = covidSymptom?.covidFever ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 2:
                cell.lblAns.text = covidSymptom?.covidLossOfTaste ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 3:
                cell.lblAns.text = covidSymptom?.covidRunnyNose ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 4 :
                cell.lblAns.text = covidSymptom?.covidDiarrhea ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 5 :
                cell.lblAns.text = covidSymptom?.covidSoreThroat ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            case 6 :
                cell.lblAns.text = covidSymptom?.covidShaking ?? "no"
                yesCount += cell.lblAns.text == "yes" ? 1 : 0
                break
            default:
                cell.lblAns.text = "no"
                break
            }
            lblSymptomDetail.text = "Patient has \(yesCount) Symptoms"
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
