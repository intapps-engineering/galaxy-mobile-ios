//
//  Covid19ResultViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 18/02/21.
//

import UIKit

protocol Covid19ResultViewControllerDelegate {
    func tappedOnContinue()
}

class Covid19ResultViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSymptom: UILabel!
    @IBOutlet weak var lblSymptomCount: UILabel!
    @IBOutlet weak var lblStepsToTake: UILabel!
    @IBOutlet weak var lblStepsDesc: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
    
    var totalCount = 0
    var symptomCount = 0
    var delegate : Covid19ResultViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSymptomCount.text = "\(symptomCount)/\(totalCount)"
    }

    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnContinueTap(_ sender: Any) {
        delegate?.tappedOnContinue()
        dismiss(animated: false, completion: nil)
    }
}
