//
//  DatePickerController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 09/03/21.
//

import UIKit

protocol DatePickerDelegate {
    func selectedDate(sDate : Date,dateType : DateType)
}

class DatePickerController: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    
    var delegate : DatePickerDelegate?
    var dateType = DateType.BirthDate
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // datePicker.maximumDate = Date()
        setupLocalization()
    }
    func setupLocalization() {
        btnOk.setTitle("ok".localized, for: .normal)
        btnCancel.setTitle("cancel".localized, for: .normal)
    }
    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOkTap(_ sender: Any) {
        delegate?.selectedDate(sDate: datePicker.date,dateType: dateType)
        dismiss(animated: false, completion: nil)
    }
    @IBAction func btnCancelTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
