//
//  QuestionAnsCell.swift
//  Tbstar
//
//  Created by Viprak-Heena on 16/03/21.
//

import UIKit

class QuestionAnsCell: UITableViewCell {

    
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
