//
//  AskForCovid19PopupViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 18/02/21.
//

import UIKit

protocol AskForCovid19PopupViewControllerDelegate {
    func selectedOption(isYes : Bool)
}

class AskForCovid19PopupViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    var delegate : AskForCovid19PopupViewControllerDelegate?
   
    var popupTitle = ""
    var popupSubtitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = popupTitle
        lblDescription.text = popupSubtitle
        setupLocalization()
    }
    func setupLocalization() {
        btnYes.setTitle("yes".localized, for: .normal)
        btnNo.setTitle("no".localized, for: .normal)
    }
    @IBAction func btnYesTap(_ sender: Any) {
        delegate?.selectedOption(isYes: true)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnNoTap(_ sender: Any) {
        delegate?.selectedOption(isYes: false)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
}
