//
//  ThankYouPopupViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 02/03/21.
//

import UIKit

protocol ThanksPopupDelegate {
    func submitTap()
}

class ThankYouPopupViewController: UIViewController {

    @IBOutlet weak var lblThankyou: UILabel!
    @IBOutlet weak var lblsuccess: UILabel!
    @IBOutlet weak var lblCaseDetail: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var delegate : ThanksPopupDelegate?
    
    var mainTitle = ""
    var subTitle = ""
    var caseDetail = ""
    override func viewDidLoad() {
        
        super.viewDidLoad()
        lblThankyou.text = mainTitle
        lblsuccess.text = subTitle
        
        if caseDetail != ""{
            lblCaseDetail.text = caseDetail
        }else{
            lblCaseDetail.text = ""
        }
        
        btnOk.setTitle("ok".localized, for: .normal)
    }
    
    @IBAction func btnOutsidePopupTap(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnOkTap(_ sender: Any) {
        delegate?.submitTap()
        dismiss(animated: true, completion: nil)
    }
}
