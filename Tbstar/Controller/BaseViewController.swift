//
//  BaseViewController.swift
//  Tbstar
//
//  Created by Apple on 29/01/21.
//

import UIKit
import SVProgressHUD
import SystemConfiguration
import CryptoSwift
import CoreLocation

class BaseViewController: UIViewController {

    let locationManager = CLLocationManager()
    var currentCordinate = CLLocationCoordinate2D()
    
    var enckKey = encKeyForGet
    var userType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setForegroundColor(UIColor(hexString: color.appMainBlueColor))
        
        if let key  = Defaultss.value(forKey: UDKey.kEncKey) as? String {
            enckKey = key
        }
        let userData = getUserDataFromStandard()
        userType = userData?.userType ?? ""
    }
    func askForLocation()  {
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
    }
    func showAlert(titleStr :String ,msg: String)
    {
        let alertController = UIAlertController(title: titleStr, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok".localized, style: .cancel, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    func showHUD()
    {
        SVProgressHUD.show()
    }
    func hideHUD()
    {
        SVProgressHUD.dismiss()
    }
    func showMenu()
    {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    func calculateAge(bDate : Date) -> Int {
        let age = Calendar.current.dateComponents([.year], from: bDate, to: Date()).year!
        return age
    }
    func getAppVersion() -> String {
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String{
            return appVersion
        }
        return "0.0"
    }
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    func convertDictionaryToString(dict : [String:Any]) -> String?{
        
        var Json : String?
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dict,
            options: []) {
            
            let theJSONText = String(data: theJSONData,encoding: .utf8)
            Json = theJSONText
        }
        return Json
        
    }
    public func isValidPassword(password: String) -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
    func decryptResponse(response:String, key : String) -> String {
        do {
            let decodedkey = Array(key.utf8).sha256()
            let aes = try AES(key: decodedkey, blockMode: CBC(iv: ivEnc), padding: .pkcs5)
            let decryptedBytes = try aes.decrypt(Array(hex: response))
            return String(bytes: decryptedBytes, encoding: .utf8) ?? ""
        }catch{
            print(error)
        }
        return ""
    }
    
    func saveUserDataToStandard(user : User)  {
        do{
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: false)
            Defaultss.setValue(encodedData, forKey: UDKey.kLoginData)
            Defaultss.synchronize()
        }catch{
            print(error)
        }
    }
    
    func getUserDataFromStandard() -> User? {
        do{
            if let userData = Defaultss.value(forKey: UDKey.kLoginData) as? Data{
                
                if let user = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(userData) as? User{
                    return user
                }
            }
        }catch (let error){
           
            print("Failed to Data : \(error.localizedDescription)")
            
        }
        return nil
    }
    
    func saveDesignationDetailToStandard(detail : NSDictionary)  {
        /*do{
            let encodedData = try NSKeyedArchiver.archivedData(withRootObject: detail, requiringSecureCoding: false)
            Defaultss.setValue(encodedData, forKey: UDKey.kDesiDetails)
            Defaultss.synchronize()
        }catch{
            print(error)
        }*/
        Defaultss.setValue(detail, forKey: UDKey.kDesiDetails)
        Defaultss.synchronize()
    
    }
    
    func getDesignationDetailFromStandard() -> DesignationDetail? {
        
        if let desiData = Defaultss.value(forKey: UDKey.kDesiDetails) as? NSDictionary{
            let designationDetail = DesignationDetail(dict: desiData as! [String : Any])
            return designationDetail
        }
        return nil
    }
    
    func savePPMVdeatilsForDispatchToStandard(detail : NSDictionary)  {
        Defaultss.setValue(detail, forKey: UDKey.kPPMVdeatilsForDispatch)
        Defaultss.synchronize()
    }

    func getPPMVdeatilsForDispatchToStandard() -> PPMV_Lab_Drop_Down? {
        
        if let desiData = Defaultss.value(forKey: UDKey.kPPMVdeatilsForDispatch) as? NSDictionary{
            let designationDetail = PPMV_Lab_Drop_Down(dict: desiData as! [String : Any])
            return designationDetail
        }
        return nil
    }

    func logOutApplication()  {
        
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController,
           let navVC = self.storyboard?.instantiateViewController(withIdentifier: "rootNavigationController") as? UINavigationController{
            navVC.setViewControllers([loginVC], animated: true)
            
            Defaultss.set(false, forKey: UDKey.kIsLoggedIn)
            Defaultss.setValue(nil, forKey: UDKey.kEncKey)

            if userType == USER_TYPE_INDEPENDENT {
                
                Defaultss.setValue(nil, forKey: UDKey.kLoginData)
                Defaultss.set(nil, forKey: UDKey.kIsUser_Details_Password)
                Defaultss.set(nil, forKey: UDKey.kIsUser_Details_PhoneNo)
            }
            
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController?.dismiss(animated: false, completion: nil)
            appDel.window?.rootViewController = navVC
        }
    }
}
extension BaseViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = manager.location{
            currentCordinate = location.coordinate
            print(currentCordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .denied:
            openSettings()
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .restricted:
            print("restricted")
        @unknown default:
            print("unknown")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        switch CLLocationManager.authorizationStatus() {
        case .denied:
            openSettings()
        case .authorizedAlways, .authorizedWhenInUse:
            print("Access")
        case .notDetermined:
            manager.requestAlwaysAuthorization()
            print("Not determined")
        case .restricted:
            print("restricted")
        @unknown default:
            print("unknown")
        }
    }
    
    func openSettings()
    {
        let alert = UIAlertController(title: appName, message: appMessages.locationAccess, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Not Now", style: .cancel, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) in
            //UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
}
