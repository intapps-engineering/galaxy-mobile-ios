//
//  SelectionPopupViewController.swift
//  Tbstar
//
//  Created by Apple on 28/01/21.
//

import UIKit
protocol SelectionPopupViewControllerDelegate {
    
    func popupoptionSelected(strFrom : String)
}

class SelectionPopupViewController: UIViewController {

    @IBOutlet weak var vwPopup: UIView!
    @IBOutlet weak var viewBottomDesc: UIView!
    
    @IBOutlet weak var imgvIndependent: UIImageView!
    @IBOutlet weak var imgNetworkProvider: UIImageView!
    @IBOutlet weak var imgNetworkOfficer: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblIndependentProvider: UILabel!
    @IBOutlet weak var lblNetworkProvider: UILabel!
    @IBOutlet weak var lblNetworkOfficer: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
        
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnContinue: UIButton!

    @IBOutlet var viewBottomDescHeight : NSLayoutConstraint!
    @IBOutlet var viewNetworkOfficerHeight : NSLayoutConstraint!
    @IBOutlet var viewOptionHeight : NSLayoutConstraint!

    var isfrom : String?
    
    var delegate : SelectionPopupViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        BuildUI()
    }
    
    func setupLocalization() {
        lblTitle.text = "registerAs".localized
        lblIndependentProvider.text = "independentProvider".localized
        lblNetworkProvider.text = "Network Provider".localized
        lblNetworkOfficer.text = "Network Officer".localized
        btnBack.setTitle("Back".localized, for: .normal)
        btnContinue.setTitle("Continue".localized, for: .normal)
    }
    
    func BuildUI() {
        
        viewBottomDesc.isHidden = true
        viewBottomDescHeight.constant = 0
        
        ///Temporary
        viewNetworkOfficerHeight.constant = 0
        viewOptionHeight.constant = viewOptionHeight.constant - 40
        
        btnBack.layer.cornerRadius = btnBack.frame.size.height / 2
        btnContinue.layer.cornerRadius = btnContinue.frame.size.height / 2
    }
    
    @IBAction func btnIndependentTap(_ sender: UIButton) {
        
        if sender.tag == 101 {
            
            Defaultss.set(USER_TYPE_INDEPENDENT, forKey: SPECIFY_USERS)
            selectedImageset(selectedImage: imgvIndependent)
            delegate?.popupoptionSelected(strFrom: IndependentSelected)
            viewBottomDesc.isHidden = true
            viewBottomDescHeight.constant = 0
            dismiss(animated: false, completion: nil)
            
        } else if sender.tag == 102 {
            
            Defaultss.set(USER_TYPE_PPMV, forKey: SPECIFY_USERS)
            viewBottomDesc.isHidden = false
            viewBottomDescHeight.constant = 110
            selectedImageset(selectedImage: imgNetworkProvider)
            isfrom = NetworkProviderSelected
            lblDesc.text = "To continue as a Network Provider you must have a valid Facility Access Code".localized
            
        } else {
            
            Defaultss.set(USER_TYPE_PPMV, forKey: SPECIFY_USERS)
            viewBottomDesc.isHidden = false
            viewBottomDescHeight.constant = 110
            selectedImageset(selectedImage: imgNetworkOfficer)
            isfrom = NetworkOfficerSelected
            lblDesc.text = "To continue as a Network Officer you must have a valid Network Officer Access Code".localized
        }
    }
    
    func selectedImageset(selectedImage : UIImageView) {
        
        imgvIndependent.image = UIImage(named: "unselect")
        imgNetworkProvider.image = UIImage(named: "unselect")
        imgNetworkOfficer.image = UIImage(named: "unselect")
        
        selectedImage.image = UIImage(named: "select")
    }
    
    @IBAction func btnOuterPopupTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBackPressed(_ sender : UIButton) {
        dismiss(animated: false, completion: nil)
    }

    @IBAction func btnContinurPressedPressed(_ sender : UIButton) {
        
        if isfrom == NetworkProviderSelected {
            delegate?.popupoptionSelected(strFrom: NetworkProviderSelected)
        } else {
            delegate?.popupoptionSelected(strFrom: NetworkOfficerSelected)
        }
        dismiss(animated: false, completion: nil)
    }

}
