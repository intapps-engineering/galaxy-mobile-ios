//
//  FacilityListPopupViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 09/02/21.
//

import UIKit

protocol FacilityPopupDelegate {
    func selectedFacility(facility : Facility)
    func manuallyOptionSelected()
}

class FacilityListPopupViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblvList: UITableView!
    @IBOutlet weak var vwPopup: UIView!
    
    var arrFacilityList = [Facility]()
    var arrFilterFacilityList = [Facility]()
    var delegate : FacilityPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwPopup.layer.cornerRadius = 15.0
        arrFilterFacilityList = arrFacilityList
        tblvList.reloadData()
        searchBar.textField?.font = UIFont(name: quicksandRegularFont, size: 14.0)
    }
    
    @IBAction func btnEnetrManuallyTap(_ sender : Any) {
        delegate?.manuallyOptionSelected()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOuterPopuptapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension FacilityListPopupViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterFacilityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "facilityListCell"){
            if let lblName = cell.viewWithTag(1) as? UILabel{
                lblName.text = arrFilterFacilityList[indexPath.row].facilityName
            }
            return cell
        }
        return UITableViewCell()
      }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedFacility(facility: arrFilterFacilityList[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
}
extension FacilityListPopupViewController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            arrFilterFacilityList = arrFacilityList.filter{ $0.facilityName.lowercased().starts(with: searchText.lowercased()) }
            tblvList.reloadData()
        }else{
            arrFilterFacilityList = arrFacilityList
            tblvList.reloadData()
        }
    }
    
}
