//
//  LoginViewController.swift
//  Tbstar
//
//  Created by Apple on 27/01/21.
//

import UIKit
import SkyFloatingLabelTextField
import CryptoKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var tfMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblSignup: UILabel!
    @IBOutlet weak var vwMobileNo: UIView!
    @IBOutlet weak var vwPswd: UIView!
    @IBOutlet weak var txtPswd: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnForgotPswd: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    @IBOutlet weak var leadingCnstOfPswdVW: NSLayoutConstraint!
    @IBOutlet weak var leadingCnstOfMobVW: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        setupTextField()
        setUpGesture()
        /*txtPswd.text = "Test@123"
        tfMobileNumber.text = "8527419630"*/
    }
    func setupLocalization() {
        tfMobileNumber.placeholder = "mobNo".localized
        tfMobileNumber.selectedTitle = "mobNo".localized
        tfMobileNumber.title = "mobNo".localized
        
        txtPswd.placeholder = "pswd".localized
        txtPswd.selectedTitle = "pswd".localized
        txtPswd.title = "pswd".localized
        
        btnSignIn.setTitle("signIn".localized, for: .normal)
        btnNext.setTitle("next".localized, for: .normal)
        btnForgotPswd.setTitle("forgotPswd".localized, for: .normal)
        
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "dontHaveAcc".localized + " " + "signup".localized + " " + "here".localized)
        
        attributedString.setColorForText(textForAttribute: "signup".localized, withColor: UIColor(hexString: color.appRedColor))
        
        lblSignup.attributedText = attributedString
    }
    func setupTextField()  {
        tfMobileNumber.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        tfMobileNumber.lineColor = UIColor(hexString: color.underlineActiveColor)
        tfMobileNumber.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        tfMobileNumber.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    func setUpGesture()  {
        
        leadingCnstOfPswdVW.constant = view.width
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        vwMobileNo.addGestureRecognizer(leftSwipe)
        vwPswd.addGestureRecognizer(rightSwipe)
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        lblSignup.addGestureRecognizer(tapgesture)
        lblSignup.isUserInteractionEnabled = true
    }
    
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer)
    {
       /* if sender.direction == .left {
            vwMobileNo.isHidden = true
            vwPswd.isHidden = false
        }*/
        if sender.direction == .right {
            leadingCnstOfPswdVW.constant = view.width
            leadingCnstOfMobVW.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnNextTap(_ sender: Any) {
        
        if let mobilenumber = tfMobileNumber.text, (mobilenumber.isEmpty || mobilenumber.count < 10 || mobilenumber.count > 12){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
        } else {
            leadingCnstOfPswdVW.constant = 0
            leadingCnstOfMobVW.constant = -view.width
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnSignInTap(_ sender: Any) {
        
        if let pswd = txtPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterPswd)
        }else{
            loggedInUser()
        }
    }
    
    @IBAction func btnForgotPswdTap(_ sender: Any) {
        
        if let resetPswd = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController") as? ResetPasswordViewController {
            resetPswd.mobileNo = tfMobileNumber.text ?? ""
            resetPswd.userTyep = "independent"
            self.navigationController?.pushViewController(resetPswd, animated: true)
        }
    }
    
    @IBAction func btnEyePswdTap(_ sender: UIButton) {
        
        txtPswd.isSecureTextEntry = !txtPswd.isSecureTextEntry
        sender.isSelected = !txtPswd.isSecureTextEntry
        
    }
    
    func openApplication() {
        self.view.makeToast("Login Successfull")
        Defaultss.setValue(Utility.generatRandomEncKey(), forKey: UDKey.kEncKey)
        Defaultss.synchronize()
        
        let userData = getUserDataFromStandard()
        if userData?.userType == USER_TYPE_INDEPENDENT {
            
            Defaultss.set(USER_TYPE_INDEPENDENT, forKey: SPECIFY_USERS)
            if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController{
                self.navigationController?.pushViewController(homeVC, animated: true)
                Utility.openSideMenu()
            }
            
        } else if userData?.userType == USER_TYPE_PPMV {
            
            Defaultss.set(USER_TYPE_PPMV, forKey: SPECIFY_USERS)
            if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "UserHomeVC") as? UserHomeVC{
                self.navigationController?.pushViewController(homeVC, animated: true)
                Utility.openSideMenu()
            }
        }
    }
    
    //MARK:- tappedOnLabel
        @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
            
            guard let text = self.lblSignup.text else { return }
            let signup = (text as NSString).range(of: "signup".localized)
            if gesture.didTapAttributedTextInLabel(label: self.lblSignup, inRange: signup) {
                
                if let selectionPopup = self.storyboard?.instantiateViewController(withIdentifier:"SelectionPopupViewController") as? SelectionPopupViewController{
                    selectionPopup.modalPresentationStyle = .overCurrentContext
                    selectionPopup.delegate = self
                    present(selectionPopup, animated: true, completion: nil)
                }
            }
        }
    
    //mARK:- API Method
    func loggedInUser()  {
        
        /*Defaultss.setValue(Utility.generatRandomEncKey(), forKey: UDKey.kEncKey)
        Defaultss.synchronize()*/
        
        if isConnectedToNetwork() {
            
            showHUD()
            
            let paraDict:[String:Any] = ["deviceToken" : Defaultss.value(forKey: UDKey.kDeviceToken) ?? "",
                                         "password": txtPswd.text ?? "",
                                         "userName": tfMobileNumber.text ?? "",
                                         "deviceId" : Defaultss.value(forKey: UDKey.kDeviceId) ?? "",
                                         "deviceTokenForNotification" : Defaultss.value(forKey: UDKey.kFCMNotificationToken) ?? ""]
            
            loginAPI(paradict: paraDict) { (success, strData, message, accessToken) in
                self.hideHUD()
                
               // let key = Defaultss.value(forKey: UDKey.kEncKey) as? String ?? encKeyForGetAnd
                
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse),
                           var userJson = json["userDetails"] as? [String:Any] {
                            userJson["accessToken"] = accessToken
                            Defaultss.setValue(accessToken, forKey: UDKey.kAccessToken)
                            Defaultss.synchronize()
                            print(userJson)
                            let user = User(dict: userJson)
                            Defaultss.set(true, forKey: UDKey.kIsLoggedIn)
                            Defaultss.set(self.tfMobileNumber.text, forKey: UDKey.kIsUser_Details_PhoneNo)
                            Defaultss.set(self.txtPswd.text, forKey: UDKey.kIsUser_Details_Password)
                            Defaultss.synchronize()
                            self.saveUserDataToStandard(user: user)
                            
                            self.openApplication()
                        }
                    }else{
                        self.view.makeToast(strResponse)
                    }
                }else if message != ""{
                    self.view.makeToast(message)
                }
                else{
                    self.view.makeToast(appMessages.somethingWrong)
                }
            }
            
        }else{

            if userType == USER_TYPE_INDEPENDENT {
                view.makeToast(appMessages.noConnection)
            } else {
                let defaultPhoneNumber = Defaultss.value(forKey: UDKey.kIsUser_Details_PhoneNo)
                let defaultPassword = Defaultss.value(forKey: UDKey.kIsUser_Details_Password)
                
                if  tfMobileNumber.text == defaultPhoneNumber as? String ?? "" && txtPswd.text == defaultPassword as? String ?? "" {
                    
                    Defaultss.set(true, forKey: UDKey.kIsLoggedIn)
                    Defaultss.set(self.tfMobileNumber.text, forKey: UDKey.kIsUser_Details_PhoneNo)
                    Defaultss.set(self.txtPswd.text, forKey: UDKey.kIsUser_Details_Password)
                    Defaultss.synchronize()
                    self.openApplication()
                }
            }
        }
    }
}

extension LoginViewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 12
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
}

extension LoginViewController : SelectionPopupViewControllerDelegate {
    
    func popupoptionSelected(strFrom: String) {
        
        if strFrom == IndependentSelected {
            
            if let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController{
                self.navigationController?.pushViewController(signupVC, animated: true)
            }
            
        } else if (strFrom == NetworkProviderSelected  || strFrom == NetworkOfficerSelected) {
             
            if let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "NetworkProviderVC") as? NetworkProviderVC {
                providerVC.selectedOption = strFrom
                self.navigationController?.pushViewController(providerVC, animated: true)
            }
        } 
    }
}
