//
//  AgreementViewController.swift
//  Tbstar
//
//  Created by Apple on 29/01/21.
//

import UIKit

class AgreementViewController: UIViewController {

    @IBOutlet weak var imgvCheckmark: UIImageView!
    var isAgree = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func btnAgreeTap(_ sender: Any) {
        isAgree = !isAgree
        if isAgree {
            imgvCheckmark.image = UIImage(named: "checked")
        }else {
            imgvCheckmark.image = UIImage(named: "uncheck")
        }
    }
    
    @IBAction func btnNextTap(_ sender: Any) {
        
        if isAgree {
            if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController{
                UserDefaults.standard.set(true, forKey: UDKey.kIsAgreeToTerms)
                UserDefaults.standard.synchronize()
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }else{
            view.makeToast("Please Agree To Terms And Condition")
        }
    }
}
