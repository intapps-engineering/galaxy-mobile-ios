//
//  SignupViewController.swift
//  Tbstar
//
//  Created by Apple on 28/01/21.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
import CryptoSwift

class SignupViewController: BaseViewController {

    
    @IBOutlet weak var tfFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfDesignation: SkyFloatingLabelTextField!
    @IBOutlet weak var tfSurname: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblFemale: UILabel!
    
    @IBOutlet weak var lblNewRegistration: UILabel!
    
    @IBOutlet weak var txtMobileNum: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlternateMobNo: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityType: SkyFloatingLabelTextField!
    @IBOutlet weak var txtState: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCnfmPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtFacilityName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtLGA:
        SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    
    
    @IBOutlet weak var heightCnstOfLGAVw: NSLayoutConstraint!
    @IBOutlet weak var heightCnstOfFcodeVw: NSLayoutConstraint!
    @IBOutlet weak var imgvDropdownFacility: UIImageView!
    
    let dropDown = DropDown()
    var designationDetail : DesignationDetail?
    var facilityList : [Facility]?
    var selectedFacility : Facility?
    
    var selectedGender = ""
    var isFacilityNameDropdown = false
    var otpVerification : OTP?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        setupTextField()
        getDetailForIndependentUser()
    }
    
    func setupLocalization() {
        
        lblNewRegistration.text = "newRegistration".localized
        
        tfFirstName.placeholder = "firstname".localized + "*"
        tfFirstName.selectedTitle = "firstname".localized + "*"
        tfFirstName.title = "firstname".localized + "*"
        
        tfMiddleName.placeholder = "middlename".localized
        tfMiddleName.selectedTitle = "middlename".localized
        tfMiddleName.title = "middlename".localized
        
        tfSurname.placeholder = "surname".localized + "*"
        tfSurname.selectedTitle = "surname".localized + "*"
        tfSurname.title = "surname".localized + "*"
        
        tfDesignation.placeholder = "designation".localized + "*"
        tfDesignation.selectedTitle = "designation".localized + "*"
        tfDesignation.title = "designation".localized + "*"
        
        lblGender.text = "sex".localized + " " + "optional".localized
        lblMale.text = "male".localized
        lblFemale.text = "female".localized
        
        txtMobileNum.placeholder = "primaryMobNo".localized + "*"
        txtMobileNum.selectedTitle = "primaryMobNo".localized + "*"
        txtMobileNum.title = "primaryMobNo".localized + "*"
        
        txtAlternateMobNo.placeholder = "alternateMobNo".localized
        txtAlternateMobNo.selectedTitle = "alternateMobNo".localized
        txtAlternateMobNo.title = "alternateMobNo".localized
        
        txtFacilityType.placeholder = "facilityType".localized
        txtFacilityType.selectedTitle = "facilityType".localized
        txtFacilityType.title = "facilityType".localized
        
        txtState.placeholder = "state".localized + "*"
        txtState.selectedTitle = "state".localized + "*"
        txtState.title = "state".localized + "*"
        
        txtLGA.placeholder = "lga".localized + "*"
        txtLGA.selectedTitle = "lga".localized + "*"
        txtLGA.title = "lga".localized + "*"
        
        txtFacilityName.placeholder = "facilityName".localized + "*"
        txtFacilityName.selectedTitle = "facilityName".localized + "*"
        txtFacilityName.title = "facilityName".localized + "*"
        
        txtAddress.placeholder = "address".localized + "*"
        txtAddress.selectedTitle = "address".localized + "*"
        txtAddress.title = "address".localized + "*"
        
        txtFacilityCode.placeholder = "facilityCode".localized
        txtFacilityCode.selectedTitle = "facilityCode".localized
        txtFacilityCode.title = "facilityCode".localized
        
        txtPswd.placeholder = "password".localized + "*"
        txtPswd.selectedTitle = "password".localized + "*"
        txtPswd.title = "password".localized + "*"
        
        txtCnfmPswd.placeholder = "cnfmPswd".localized + "*"
        txtCnfmPswd.selectedTitle = "cnfmPswd".localized + "*"
        txtCnfmPswd.title = "cnfmPswd".localized + "*"
        
        btnExit.setTitle("exit".localized, for: .normal)
        btnConfirm.setTitle("confirm".localized, for: .normal)
    }
    func setupTextField()  {
        
        tfFirstName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        tfFirstName.lineColor = UIColor(hexString: color.underlineActiveColor)
        tfFirstName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        tfFirstName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        tfMiddleName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        tfMiddleName.lineColor = UIColor(hexString: color.underlineActiveColor)
        tfMiddleName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        tfMiddleName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        tfSurname.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        tfSurname.lineColor = UIColor(hexString: color.underlineActiveColor)
        tfSurname.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        tfSurname.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtMobileNum.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNum.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNum.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtMobileNum.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAlternateMobNo.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAlternateMobNo.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtAddress.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtAddress.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtFacilityName.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityName.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtFacilityCode.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityCode.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityCode.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtFacilityCode.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtCnfmPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfmPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfmPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfmPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
        manageLGAView(shouldHide: true)
        
        /*tfFirstName.text = "test"
        tfSurname.text = "iOS"
        txtMobileNum.text = "1234567890"
        txtPswd.text = "Test@123"
        txtCnfmPswd.text = "Test@123"
        txtFacilityName.text = "test"
        txtAddress.text = "test"*/
    }
    func manageLGAView(shouldHide : Bool)  {
        heightCnstOfLGAVw.constant = shouldHide ? 0 : 120
        txtLGA.isHidden = shouldHide
        txtFacilityName.isHidden = shouldHide
    }
    func convertFacilityNameToDropdown(isTrue : Bool) {
        txtFacilityName.text = ""
        imgvDropdownFacility.isHidden = !isTrue
        isFacilityNameDropdown = isTrue
    }
    func manageFacilityCodeView(shouldHide : Bool)  {
        heightCnstOfFcodeVw.constant = shouldHide ? 0 : 60
        txtFacilityCode.isHidden = shouldHide
    }
    @IBAction func btnExitTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmTap(_ sender: Any) {
        
        if isValidDetail(){
            verifyAccount()
        }
    }
    
    @IBAction func btnFemaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womenselect"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "mannormal"), for: .normal)
        selectedGender = Gender.Female.rawValue
    }
    
    @IBAction func btnMaleTap(_ sender: Any) {
        btnFemale.setBackgroundImage(UIImage(named: "womennormal"), for: .normal)
        btnMale.setBackgroundImage(UIImage(named: "manselect"), for: .normal)
        selectedGender = Gender.Male.rawValue
    }
    
    @IBAction func btnPswdEyeTap(_ sender: UIButton) {
        txtPswd.isSecureTextEntry = !txtPswd.isSecureTextEntry
        sender.isSelected = !txtPswd.isSecureTextEntry
    }
    
    @IBAction func btnCnfrmEyeTap(_ sender: UIButton) {
        txtCnfmPswd.isSecureTextEntry = !txtCnfmPswd.isSecureTextEntry
        sender.isSelected = !txtPswd.isSecureTextEntry
    }
    func setupDesignationDropdown()  {
        
        dropDown.anchorView = tfDesignation
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.designationList.map{($0.name)}
        }else{
            dropDown.dataSource = ["Community Health Worker", "Community Health Extension Workers(CHEW)", "Community Pharmacist", "Doctor", "Laboratory Scientist","Nurse","Patent Medicine Vendor", "Others", "Network Officer"]
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            tfDesignation.text = item
            dropDown.hide()
        }
    }
    func setupFacilityTypeDropdown()  {
        
        dropDown.anchorView = txtFacilityType
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.publicFaciltyList.map{($0.name)}
            dropDown.dataSource += designationDetail!.privateFaciltyList.map{($0.name)}
            
        }else{
            dropDown.dataSource = ["General Hospital", "Primary Health Center", "Teaching Hospital", "Community Pharmacy", "Hospital","Laboratory", "Patent Medicine Store", "Nursing Home"]
        }
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtFacilityType.text = item
            dropDown.hide()
        }
    }
    func setupStateDropdown()  {
        
        dropDown.anchorView = txtState
        if designationDetail != nil{
            dropDown.dataSource = designationDetail!.addressList.map{($0.stateName)}
        }else{
            dropDown.dataSource = ["Abuja", "Abia", "Adamawa", "Akwa Ibom", "Anambra","Bauchi","Bayelsa", "Benue", "Borno","Cross River","Delta","Ebonyi","Edo","Ekiti","Enugu","Gombe","FCT","Imo","Jigawa","Kaduna","Kano","Katsina","Kebbi","Kogi","Kwara","Lagos","Nassarawa","Niger","Ogun","Ondo","Osun","Oyo","Plateau","Rivers","Sokoto","Taraba","Yobe","Zamfara"]
        }
        
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtState.text = item
            txtLGA.text = ""
            manageLGAView(shouldHide: false)
            dropDown.hide()
        }
    }
    func setupLGADropdown()  {
        
        dropDown.anchorView = txtLGA
        if designationDetail != nil, let state = txtState.text, !state.isEmpty{
            if let addressDetail = designationDetail!.addressList.filter({$0.stateName == state}).first{
                dropDown.dataSource = addressDetail.LGA ?? []
            }
        }else{
            dropDown.dataSource = ["Abuja", "Abia", "Adamawa", "Akwa Ibom", "Anambra","Bauchi","Bayelsa", "Benue", "Borno","Cross River","Delta","Ebonyi","Edo","Ekiti","Enugu","Gombe","FCT","Imo","Jigawa","Kaduna","Kano","Katsina","Kebbi","Kogi","Kwara","Lagos","Nassarawa","Niger","Ogun","Ondo","Osun","Oyo","Plateau","Rivers","Sokoto","Taraba","Yobe","Zamfara"]
        }
        
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            txtLGA.text = item
            dropDown.hide()
            getListOfFacilitiesInLGA()
        }
    }
    
    func isValidDetail() -> Bool {
        if let name = tfFirstName.text, name.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterFname)
            return false
        }
        if let surname = tfSurname.text, surname.isEmpty {
            showAlert(titleStr: appName, msg: appMessages.enterSurName)
            return false
        }
        
        if let designation = tfDesignation.text, designation.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectDesignation)
            return false
        }
        /*if selectedGender.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectGender)
            return false
        }*/
        if let mobNo = txtMobileNum.text, (mobNo.isEmpty || mobNo.count < 10 || mobNo.count > 12 || !mobNo.isNumeric){
            showAlert(titleStr: appName, msg: appMessages.validMobileNo)
            return false
        }
        if let alternateMobNO = txtAlternateMobNo.text, !alternateMobNO.isEmpty{
            if (alternateMobNO.isEmpty || alternateMobNO.count < 10 || alternateMobNO.count > 12 || !alternateMobNO.isNumeric){
                showAlert(titleStr: appName, msg: appMessages.validAlternateNo)
                return false
            }
        }
        if let facilityType = txtFacilityType.text, facilityType.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectFacility)
            return false
        }
        if let state = txtState.text, state.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectState)
            return false
        }
        if let lga = txtLGA.text, lga.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.selectLGA)
            return false
        }
        if let facilityName = txtFacilityName.text, facilityName.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterFacilityName)
            return false
        }
        if let address = txtAddress.text, address.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enetrAddress)
            return false
        }
        if let pswd = txtPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterPswd)
            return false
        }else{
            if !isValidPassword(password: txtPswd.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if let pswd = txtCnfmPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterCnfmPswd)
            return false
        }
        if txtPswd.text! != txtCnfmPswd.text{
            showAlert(titleStr: appName, msg: appMessages.pswdShouldBeSame)
            return false
        }
        return true
    }
    
    func getDetailForIndependentUser()  {
        if isConnectedToNetwork(){
            showHUD()
                getDetailsForIndependentUser(completionHandler: { ( success, strData) in
                    if success {
                            let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                            if let json = self.convertStringToDictionary(text: strResponse){
                                self.saveDesignationDetailToStandard(detail: json as NSDictionary)
                                self.designationDetail = DesignationDetail(dict: json)
                                self.hideHUD()
                            }else{
                                self.hideHUD()
                            }
                        
                    }else{
                        self.hideHUD()
                    }
                })
            
        }else{
            self.designationDetail = getDesignationDetailFromStandard()
            view.makeToast(appMessages.noConnection)
        }
        
    }
    func getListOfFacilitiesInLGA() {
        if isConnectedToNetwork(){
            showHUD()
            
            let paraDict = ["facilityState" : txtState.text!,
                            "lga": txtLGA.text!]
            
            getListOfFacilitiesFromLGAAPI(paradict: paraDict) { (success, strData) in
                if success{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        print(json)
                        if let list = json["facilityList"] as? [[String:Any]]{
                            self.facilityList = list.map(Facility.init)
                            self.facilityList = self.facilityList?.sorted{$0.facilityName < $1.facilityName}
                            self.convertFacilityNameToDropdown(isTrue: true)
                        }
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                }else{
                    self.hideHUD()
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    func verifyAccount(){
        
        if isConnectedToNetwork(){
            showHUD()
            
            //let token = Defaultss.value(forKey: UDKey.kNotificationToken) as? String ?? ""
            let paraDict = ["phoneNumber" : txtMobileNum.text!,
                                "userType": "independent",
                                "deviceToken":Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""]
            
            verifyAccountAPI(paradict: paraDict) { (success, strData,message) in
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    self.hideHUD()
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse),
                           let otpJson = json["otp"] as? [String:Any] {
                            print(otpJson)
                            self.otpVerification = OTP(dict: otpJson)
                            self.moveToVerificationCodeScreen()
                        }
                        else{
                            self.view.makeToast(appMessages.somethingWrong)
                        }
                    }
                    else{
                        self.hideHUD()
                        self.view.makeToast(strResponse)
                    }
                }else if message != ""{
                    self.view.makeToast(message)
                    self.hideHUD()
                }else{
                    self.hideHUD()
                }
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
    func openFacilityPopup()  {
        
        if let facilityPopupVC = self.storyboard?.instantiateViewController(withIdentifier: "FacilityListPopupViewController") as? FacilityListPopupViewController{
            facilityPopupVC.delegate = self
            facilityPopupVC.modalPresentationStyle = .overCurrentContext
            if facilityList != nil{
                facilityPopupVC.arrFacilityList = facilityList!
            }
            present(facilityPopupVC, animated: true, completion: nil)
        }
    }
    func moveToVerificationCodeScreen()  {
        
        if let verificationCodeVc = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeViewController") as? VerificationCodeViewController{
            verificationCodeVc.otpCode = otpVerification
            verificationCodeVc.signupDict = makeSignupDict()
            verificationCodeVc.ISFROMVERIFICATION = IndependentSelected
            self.navigationController?.pushViewController(verificationCodeVc, animated: true)
        }
    }
    func makeSignupDict() -> [String:Any] {
        
        let token = Defaultss.value(forKey: UDKey.kFCMNotificationToken) as? String ?? ""
        var signupDict = [String:Any]()
        if selectedFacility != nil{
            signupDict["facilityId"] = selectedFacility?.id ?? 0
        }
        if !selectedGender.isEmpty {
            signupDict["sex"] = selectedGender
        }
        signupDict["designation"] = tfDesignation.text ?? ""
        signupDict["userType"] = "independent"
        signupDict["firstName"] = tfFirstName.text ?? ""
        signupDict["surName"] = tfSurname.text ?? ""
        signupDict["middleName"] = tfMiddleName.text ?? ""
        signupDict["phoneNumber"] = txtMobileNum.text ?? ""
        signupDict["secondPhoneNumber"] = txtAlternateMobNo.text ?? ""
        signupDict["facilityType"] = txtFacilityType.text ?? ""
        signupDict["facilityName"] = txtFacilityName.text ?? ""
        signupDict["address"] = txtAddress.text ?? ""
        signupDict["facilityCode"] = txtFacilityCode.text ?? ""
        signupDict["state"] = txtState.text ?? ""
        signupDict["lga"] = txtLGA.text ?? ""
        signupDict["deviceToken"] = Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""
        signupDict["deviceId"] = Defaultss.value(forKey: UDKey.kDeviceId) ?? ""
        signupDict["deviceTokenForNotification"] = token
        signupDict["verificationOtp"] = otpVerification?.verificationOtp ?? ""
        signupDict["password"] = txtPswd.text ?? ""
        signupDict["confirmPassword"] = txtCnfmPswd.text ?? ""
        signupDict["termsConditionStatus"] = "1"
        return signupDict
    }
}


extension SignupViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfDesignation{
            setupDesignationDropdown()
            return false
        }else if textField == txtFacilityType{
            setupFacilityTypeDropdown()
            return false
        }else if textField == txtState{
            setupStateDropdown()
            return false
        }
        else if textField == txtLGA{
            setupLGADropdown()
            return false
        }else if textField == txtFacilityName && isFacilityNameDropdown{
            openFacilityPopup()
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobileNum || textField == txtAlternateMobNo{
            let maxLength = 12
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
}
extension SignupViewController: FacilityPopupDelegate{
    
    func selectedFacility(facility: Facility) {
        selectedFacility = facility
        txtFacilityName.text = facility.facilityName
        txtAddress.text = facility.address
        txtAddress.isUserInteractionEnabled = false
        manageFacilityCodeView(shouldHide: true)
    }
    func manuallyOptionSelected() {
        convertFacilityNameToDropdown(isTrue: false)
        txtFacilityName.text = ""
        txtAddress.text = ""
        txtAddress.isUserInteractionEnabled = true
        selectedFacility = nil
        manageFacilityCodeView(shouldHide: false)
    }
}
