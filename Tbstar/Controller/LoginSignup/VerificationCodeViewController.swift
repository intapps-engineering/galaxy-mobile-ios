//
//  VerificationCodeViewController.swift
//  Tbstar
//
//  Created by Viprak-Heena on 09/02/21.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import CryptoSwift

class VerificationCodeViewController: BaseViewController {

    var signupDict : [String:Any]?
    var otpCode : OTP?
    var token  = ""
    let dateformatter = DateFormatter()
    var timer : Timer?
    var totalSeconds = 0
   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInst: UILabel!
    @IBOutlet weak var txtVerificationCode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lblResend: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var ISFROMVERIFICATION : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpGesture()
        setupLocalization()
        token = Defaultss.value(forKey: UDKey.kNotificationToken) as? String ?? ""
        txtVerificationCode.text = otpCode?.verificationOtp ?? ""
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        countTimeBasedOnExpDate()
    }
    func setupLocalization() {
        
        lblTitle.text = "verificationCode".localized
        
        if let usertype = self.signupDict?["userType"] as? String{
            
            if usertype == USER_TYPE_INDEPENDENT {
                lblInst.text = "enterVcodeDetail".localized
            } else {
                lblInst.text = "Please tap on Submit to continue".localized
            }
        }
        
        
        txtVerificationCode.placeholder = "enterOtp".localized
        txtVerificationCode.selectedTitle = "enterOtp".localized
        txtVerificationCode.title = "enterOtp".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
        lblResend.text = "resendCode".localized + "resend".localized
    }
    func countTimeBasedOnExpDate()  {
        let expDate = dateformatter.date(from: otpCode?.expire ?? "")
        let todayDateStr = dateformatter.string(from: Date())
        let currentdate = dateformatter.date(from: todayDateStr)
        if expDate != nil && currentdate != nil{
            totalSeconds = Int(expDate!.timeIntervalSince(currentdate!))
            startTimer()
        }
    }
    func startTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateLabel), userInfo: nil, repeats: true)
    }
    @objc func updateLabel()  {
        totalSeconds -= 1
        if totalSeconds <= 0{
            timer?.invalidate()
            lblTimer.text = "OTP Expire"
        }else{
            let minutes = totalSeconds / 60 % 60
            let seconds = totalSeconds % 60
            lblTimer.text = String(format:"%02i:%02i", minutes, seconds)
        }
    }
    func isDetailValid() -> Bool {
        
        if let otp = txtVerificationCode.text, otp.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterOtp)
            return false
        }else if txtVerificationCode.text! != otpCode?.verificationOtp{
            showAlert(titleStr: appName, msg: appMessages.incorrectOtp)
            return false
        }
        return true
    }
    
    func setUpGesture()  {
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        lblResend.addGestureRecognizer(tapgesture)
        lblResend.isUserInteractionEnabled = true
    }
    
    //MARK:- tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        
        guard let text = lblResend.text else { return }
        let signup = (text as NSString).range(of: "resend".localized)
        if gesture.didTapAttributedTextInLabel(label: lblResend, inRange: signup) {
            timer?.invalidate()
            verifyAccount()
        }
    }
    @IBAction func btnSubmitTap(_ sender: Any) {
        
        signupDict?["verificationOtp"] = txtVerificationCode.text ?? ""
        signupDict?["deviceToken"] = otpCode?.deviceToken ?? ""
        
        if isDetailValid(){
            if isConnectedToNetwork(){
                showHUD()
                
                signUpAPI(paradict: signupDict ?? [:]) { (success, strJson, message) in
                    if success{
                        self.signupDict?["accessToken"] = strJson["accessToken"] ?? ""
                        self.signupDict?["userId"] = strJson["userId"] ?? ""
                        self.signupDict?["userType"] = strJson["userType"] ?? ""
                        self.signupDict?["faccode"] = strJson["faccode"] ?? ""
                        //faccode
                       // userId
                        let user = User(dict: self.signupDict ?? [:])
                        Defaultss.setValue(strJson["accessToken"] ?? "", forKey: UDKey.kAccessToken)
                        Defaultss.synchronize()
                        
                        self.saveUserDataToStandard(user: user)
                        self.hideHUD()
                        if let msg = strJson["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        self.openApplication()
                    }else if message != ""{
                        self.hideHUD()
                        self.view.makeToast(message)
                    }else{
                        self.hideHUD()
                        self.view.makeToast(appMessages.somethingWrong)
                    }
                }
            }else{
                view.makeToast(appMessages.noConnection)
            }
        }
        
        
    }
    
    func openApplication() {
        
        let userData = getUserDataFromStandard()
        
        if userData?.userType == USER_TYPE_INDEPENDENT {
            
            Defaultss.set(USER_TYPE_INDEPENDENT, forKey: SPECIFY_USERS)
            if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController{
                self.navigationController?.pushViewController(homeVC, animated: true)
                Utility.openSideMenu()
            }

        } else if userData?.userType == USER_TYPE_PPMV {
            
            Defaultss.set(USER_TYPE_PPMV, forKey: SPECIFY_USERS)
            if let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "UserHomeVC") as? UserHomeVC{
                self.navigationController?.pushViewController(homeVC, animated: true)
                Utility.openSideMenu()
            }
        }
    }
    
    func verifyAccount(){
        
        if isConnectedToNetwork(){
            showHUD()
            let token = Defaultss.value(forKey: UDKey.kDeviceToken) as? String ?? ""
            let paraDict = ["phoneNumber" : signupDict!["phoneNumber"] as? String ?? "",
                            "userType": signupDict!["userType"] as? String ?? "",
                            "deviceToken":token]
            
            verifyAccountAPI(paradict: paraDict) { (success, strData,message) in
                if strData != ""{
                    
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    self.hideHUD()
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse),
                           let otpJson = json["otp"] as? [String:Any] {
                            self.otpCode = OTP(dict: otpJson)
                            self.txtVerificationCode.text = self.otpCode?.verificationOtp ?? ""
                            self.countTimeBasedOnExpDate()
                        }
                        else{
                            self.view.makeToast(appMessages.somethingWrong)
                        }
                    }
                    else{
                        self.hideHUD()
                        self.view.makeToast(strResponse)
                    }
                    
                }else if message != ""{
                    self.view.makeToast(message)
                    self.hideHUD()
                }else{
                    self.hideHUD()
                }
            }
            
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
}
