//
//  ResetPasswordViewController.swift
//  Tbstar
//
//  Created by Apple on 29/01/21.
//

import UIKit
import SkyFloatingLabelTextField
import CryptoSwift
class ResetPasswordViewController: BaseViewController {

    @IBOutlet weak var txtVerificationCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCnfrmPswd: SkyFloatingLabelTextField!
    
    var mobileNo = ""
    var verificationToken = ""
    var userTyep = ""
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblInst: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        verifyAccount()
        setupTextField()
    }
    func setupLocalization() {
        
        lblTitle.text = "resetPswd".localized
        lblInst.text = "enterVcodeDetail".localized
        
        txtVerificationCode.placeholder = "enterOtp".localized
        txtVerificationCode.selectedTitle = "enterOtp".localized
        txtVerificationCode.title = "enterOtp".localized
        
        txtPswd.placeholder = "pswd".localized
        txtPswd.selectedTitle = "pswd".localized
        txtPswd.title = "pswd".localized
        
        txtCnfrmPswd.placeholder = "cnfmPswd".localized
        txtCnfrmPswd.selectedTitle = "cnfmPswd".localized
        txtCnfrmPswd.title = "cnfmPswd".localized
        
        btnSubmit.setTitle("submit".localized, for: .normal)
        
    }
    func setupTextField()  {
        txtVerificationCode.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtVerificationCode.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtVerificationCode.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtVerificationCode.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
        
        txtCnfrmPswd.selectedLineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmPswd.lineColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmPswd.selectedTitleColor = UIColor(hexString: color.underlineActiveColor)
        txtCnfrmPswd.titleColor = UIColor(hexString: color.underlineInactiveColor)
    }
    @IBAction func btnBackTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPswdEyeTap(_ sender: UIButton) {
        txtPswd.isSecureTextEntry = !txtPswd.isSecureTextEntry
        sender.isSelected = !txtPswd.isSecureTextEntry
    }
    
    @IBAction func btnCnfmpswdEyeTap(_ sender: UIButton) {
        txtCnfrmPswd.isSecureTextEntry = !txtCnfrmPswd.isSecureTextEntry
        sender.isSelected = !txtCnfrmPswd.isSecureTextEntry
    }
    
    @IBAction func btnSubmitTap(_ sender: UIButton) {
        if isValidDetail(){
            resetPassword()
        }
    }
    func isValidDetail() -> Bool {
        if let otp = txtVerificationCode.text, otp.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterOtp)
            return false
        }else if txtVerificationCode.text! != verificationToken{
            showAlert(titleStr: appName, msg: appMessages.incorrectOtp)
            return false
        }
        if let pswd = txtPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterPswd)
            return false
        }else{
            if !isValidPassword(password: txtPswd.text!){
                showAlert(titleStr: appName, msg: appMessages.validPswd)
                return false
            }
        }
        if let pswd = txtCnfrmPswd.text, pswd.isEmpty{
            showAlert(titleStr: appName, msg: appMessages.enterCnfmPswd)
            return false
        }
        if txtPswd.text! != txtCnfrmPswd.text{
            showAlert(titleStr: appName, msg: appMessages.pswdShouldBeSame)
            return false
        }
        return true
    }
    func verifyAccount(){
        
        if isConnectedToNetwork(){
            showHUD()
            
            let paraDict = ["phoneNumber" : mobileNo,
                            "userType": userTyep,
                            "deviceToken":Defaultss.value(forKey: UDKey.kDeviceToken) ?? ""]
            
            forgotPswdTokenAPI(paradict: paraDict) { (success, strData, message) in
                self.hideHUD()
                if strData != ""{
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if success{
                        if let json = self.convertStringToDictionary(text: strResponse){
                           if let resetToken = json["resettoken"] as? Int {
                            self.verificationToken = "\(resetToken)"
                            self.txtVerificationCode.text = "\(resetToken)"
                           }
                        }
                        else{
                            self.view.makeToast(appMessages.somethingWrong)
                        }
                    }
                    else{
                        self.view.makeToast(strResponse)
                    }
                }else if message != ""{
                    self.view.makeToast(message)
                }
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
    }
    func resetPassword() {
        
        if isConnectedToNetwork(){
            showHUD()
            
            let paraDict:[String:Any] = ["deviceToken" : Defaultss.value(forKey: UDKey.kDeviceToken) ?? "",
                                         "password": txtPswd.text ?? "",
                                         "confirmPassword" : txtCnfrmPswd.text ?? "",
                                         "userType":userTyep,
                                         "phoneNumber":mobileNo,
                                         "resettoken" : txtVerificationCode.text ?? ""]
            resetPasswordAPI(paradict: paraDict) { (success, strData, message) in
                self.hideHUD()
                
                if message != ""{
                    self.view.makeToast(message)
                }
                if success{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }else{
            view.makeToast(appMessages.noConnection)
        }
        
    }
}
