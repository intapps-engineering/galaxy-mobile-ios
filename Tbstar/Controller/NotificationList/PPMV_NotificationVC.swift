//
//  PPMV_NotificationVC.swift
//  Tbstar
//
//  Created by user1 on 15/06/21.
//

import UIKit

class PPMV_NotificationVC : BaseViewController {
    
    @IBOutlet weak var lblNotificationTitle: UILabel!
    
    @IBOutlet var tblNotifications : UITableView!
    
    var arrNotification = [PUSHNotification]()
    
    var selectedInedx = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocalization()
        getNotificationList()
        tblNotifications.reloadData()
    }
    
    func setupLocalization() {
        lblNotificationTitle.text = "notification".localized
    }
    
    func getNotificationList() {
        
        if isConnectedToNetwork(){
            showHUD()
            getAllNotificationListAPI { (success, strData) in
                if success {
                    let strResponse = self.decryptResponse(response: strData,key: encKeyForGetAnd)
                    if let json = self.convertStringToDictionary(text: strResponse){
                        if let list = json["allpushnotifications"] as? [[String :Any]] {
                            for i in 0..<list.count {
                                let dict = PUSHNotification(dict: list[i])
                                self.arrNotification.append(dict)
                            }
                            self.arrNotification = self.arrNotification.sorted{$0.createDate > $1.createDate}
                            
                            self.tblNotifications.reloadData()
                        }
                        self.hideHUD()
                    }else{
                        self.hideHUD()
                    }
                }else{
                    self.hideHUD()
                }
            }
        } else {
            hideHUD()
            view.makeToast(appMessages.noConnection)
        }
    }
    
    func AskForDeleteNotification() {
        
        print(selectedInedx)
        if let popup = self.storyboard?.instantiateViewController(withIdentifier: "AskForCovid19PopupViewController") as? AskForCovid19PopupViewController{
            popup.popupTitle = "Delete_Notification".localized
            popup.delegate = self
            popup.modalPresentationStyle = .overFullScreen
            present(popup, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension PPMV_NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblNotifications.dequeueReusableCell(withIdentifier: "cellNotification") as! cellNotification
        
        cell.setCell(notification: arrNotification[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrNotification[indexPath.row].pushType == "clinicScreeningPatient" {
            
            if userType == USER_TYPE_INDEPENDENT {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "FindCaseViewController") as? FindCaseViewController {
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            } else {
                if let VC = self.storyboard?.instantiateViewController(withIdentifier: "MyClientVC") as? MyClientVC {
                    self.navigationController?.pushViewController(VC, animated: true)
                }
            }
            
        } else if arrNotification[indexPath.row].pushType == "screening" {
            
        } else if arrNotification[indexPath.row].pushType == "treatment" {
            
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            selectedInedx = indexPath.row
            AskForDeleteNotification()
        }
    }
    
}

extension PPMV_NotificationVC : AskForCovid19PopupViewControllerDelegate{
    func selectedOption(isYes: Bool) {
        if isYes{
            var paradict = [String : Any]()
            paradict["alertId"] = arrNotification[selectedInedx]._id
            DeleteNotification(paradict: paradict, completionHandler: { (isSucces , strData , strMessage) in
                if strData != "" {
                    if isSucces {
                        self.arrNotification.remove(at: self.selectedInedx)
                        // tblNotifications.deleteRows(at: [selectedInedx], with: .fade)
                        self.getNotificationList()
                    }
                }
            })
        }
    }
}
